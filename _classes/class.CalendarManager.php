<?php
require_once('../_classes/class.DBConnection.php');

/**
 * class CalendarManager
 */
class CalendarManager{
    /**
	 * @var object $db database connection
	 */
	private static $db;
	/**
	 * calender serial number
	 * @var int $id
	 */
	private $id=0;

	private $usr_id;

	private $domain_id = 1;

	private $group_id = 0;

	private $year;  //date("Y");

	private $month; //date("m");

	private $day;   //date("d");

	private $week;  //date("W");

	private $start;   // UNIXTIME

	private $end;     // UNIXTIME

	private $completed = 'X';

	private $content = '';

	public static $detaultTimeH = array("00" => "AM 00", "01" => "AM 01", "02" => "AM 02", "03" => "AM 03", "04" => "AM 04", "05" => "AM 05", "06" => "AM 06", "07" => "AM 07", "08" => "AM 08", "09" => "AM 09", "10" => "AM 10", "11" => "AM 11", "12" => "PM 12", "13" => "PM 01", "14" => "PM 02", "15" => "PM 03", "16" => "PM 04", "17" => "PM 05", "18" => "PM 06", "19" => "PM 07", "20" => "PM 08", "21" => "PM 09", "22" => "PM 10", "23" => "PM 11");

	public static $detaultTimeM = array("00:00" => "00", "05:00" => "05", "10:00" => "10", "15:00" => "15", "20:00" => "20", "25:00" => "25", "30:00" => "30", "35:00" => "35", "40:00" => "40", "45:00" => "45", "50:00" => "50", "55:00" => "55");

	/**
	 * new CalendarManager($properties)
	 */
	public function __construct($properties) {
        $this->db = DBConnection::get("master")->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}


	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_NUM);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}

	public function getWeekList(){
		if(!$this->week){$this->week = date("W");}

		$query = "SELECT id, usr_id, content, completed, DATE_FORMAT(str_date,'%c') AS mth, DATE_FORMAT(str_date,'%w') AS wdy FROM `calendar` WHERE DATE_FORMAT(str_date,'%v') = '{$this->week}' ORDER BY wdy, id";
		$res = $this->db->query($query);

		$i=0;
		$d=0;

		while($row = $res->fetch(PDO::FETCH_ASSOC)){
			if($d == $row['wdy']){
				$i++;
			}else{
				$d = $row['wdy'];
				$i=0;
			}
			$week_data[$d][$i]['id']        = $row['id'];
			$week_data[$d][$i]['content']   = $row['content'];
			$week_data[$d][$i]['completed'] = $row['completed'];
		}
		return $week_data;
	}

	public function getMonthList(){
		if(!$this->year){$this->year = date("Y");}
		if(!$this->month){$this->month = date("m");}
		if($this->group_id > 0 ){
			$where[] = "`group_id` = '{$this->group_id}'";
		}
		$where[] = "DATE(str_date,'%m') = '{$this->month}'";
		$where[] = "DATE(str_date,'%Y') = '{$this->year}'";
		$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

		$query = "SELECT id, usr_id, content, completed, DATE(str_date,'%m') AS mth, DATE(str_date,'%d') AS dy FROM `calendar` ".$where_sql." ORDER BY id DESC";
		$res   = $this->db->query($query);

		$i=0;
		$d=0;

		while($row = $res->fetch(PDO::FETCH_ASSOC)){
			if($d == $row['dy']){
				$i++;
			}else{
				$d = $row['dy'];
				$i=0;
			}
			$month_data[$d][$i]['id']        = $row['id'];
			$month_data[$d][$i]['group_id']  = $row['group_id'];
			$month_data[$d][$i]['content']   = $row['content'];
			$month_data[$d][$i]['str_date']  = $row['str_date'];
			$month_data[$d][$i]['end_date']  = $row['end_date'];
		}
		return $month_data;
	}



	public function getDayList(){
		if(!$this->year){$this->year = date("Y");}
		if(!$this->month){$this->month = date("m");}
		if(!$this->day){$this->day = date("d");}
		if($this->group_id != 0 ){
			$where[] = "group_id = '{$this->group_id}'";
		}
		$where[] = "DATE_FORMAT(str_date,'%c') = '{$this->month}'";
		$where[] = "DATE_FORMAT(str_date,'%Y') = '{$this->year}'";
		$where[] = "DATE_FORMAT(str_date,'%e') = '{$this->day}'";
		$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

		$query = "SELECT id, usr_id, content, completed, DATE_FORMAT(str_date,'%c') AS mth, DATE_FORMAT(str_date,'%d') AS dy FROM `calendar` ".$where_sql." ORDER BY dy, id DESC";
		$res =& $this->db->query($query);

		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$day_data = array();
		foreach ($rows as $row) {
			$day_data[] = $row;
		}
		return $day_data;
	}


	public function getPeriodList(){
		if($this->group_id != 0 ){ $where[] = "group_id = '{$this->group_id}'"; }
		$where[] = "UNIX_TIMESTAMP(`str_date`) >=  '{$this->start}'";
		$where[] = "UNIX_TIMESTAMP(`str_date`) <  '{$this->end}'";
		$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
		$query = "SELECT *, UNIX_TIMESTAMP(`str_date`) AS `startDate`, UNIX_TIMESTAMP(`end_date`) AS `endDate`, DATE_FORMAT(`str_date`,'%Y') AS `year`, DATE_FORMAT(`str_date`,'%c') AS `month`, DATE_FORMAT(`str_date`,'%e') AS `day` FROM `calendar` ".$where_sql." ORDER BY `month` ASC, `day` ASC, `id` DESC";


		//$query = "SELECT *, UNIX_TIMESTAMP(`str_date`) AS `startDate`, UNIX_TIMESTAMP(`end_date`) AS `endDate`, DATE_FORMAT(`str_date`,'%Y') AS `year`, DATE_FORMAT(`str_date`,'%c') AS `month`, DATE_FORMAT(`str_date`,'%e') AS `day` FROM `calendar` ".$where_sql." ORDER BY `month` ASC, `day` ASC, `id` DESC";
		
		$res =& $this->db->query($query);
		$i=0;
		$d=0;
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}



	public static function getEvent($id){
        $db = DBConnection::get("master")->handle();
		$query = "SELECT * FROM `calendar` WHERE id='{$id}'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}


	public static function getDay($id){
        $db = DBConnection::get("master")->handle();
		$query = "SELECT c.id, c.usr_id, c.group_id, c.content, c.completed, DATE_FORMAT(c.str_date,'%Y') AS yr, DATE_FORMAT(c.str_date,'%m') AS mth, DATE_FORMAT(c.str_date,'%d') AS dy , (SELECT u.name FROM `usr` AS u WHERE u.id=c.usr_id) AS usr_name FROM `calendar` AS c  WHERE id='{$id}'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function insert($arry){
		$db = DBConnection::get()->handle();
		$time = time();
		$query = "INSERT INTO `calendar` (`domain_id`, `group_id`, `usr_id`, `str_date`, `end_date`, `allDay`, `title`, `content`, `reg_date`) VALUES ('{$arry[domain_id]}', '{$arry[group_id]}', '{$arry[usr_id]}', '{$arry[str_date]}', '{$arry[end_date]}', '{$arry[allDay]}', '{$arry[title]}', '{$arry[content]}', '$time')";
		$res = $db->query($query);
		$lastID = self::getOne("SELECT MAX(id) FROM `calendar`");
		return $lastID;
	}

	public static function modify($arry){
		$db = DBConnection::get()->handle();
		$query  = "UPDATE `calendar` SET
				`group_id` = '{$arry[group_id]}',
				`usr_id`   = '{$arry[usr_id]}',
				`str_date` = '{$arry[str_date]}',
				`end_date` = '{$arry[end_date]}',
				`allDay`   = '{$arry[allDay]}',
				`title`    = '{$arry[title]}',
				`content`  = '{$arry[content]}'
				WHERE id = '{$arry[id]}'";
		$result = $db->query($query);

		return $result;
	}

	public static function del($id){
		$db =& DBConnection::get()->handle();
		$event = self::getEvent($id);
		$query = "DELETE FROM `calendar` WHERE `id`=".$id;
		$result = $db->query($query);
		return $event;
	}

	public static function getUsrFromBirth($checkBirth,$groups){
        $db = DBConnection::get()->handle();

        if(trim($groups) > 0){
			 //$arryGroups = explode("|",$groups);
			 //$where_sql = (is_array($arryGroups)) ? " AND ug.group_id IN(".implode(",",$arryGroups).")" : "";
			 $where_sql = " AND ug.group_id = '{$groups}'";
			 $query = "SELECT u.`id`, u.`name`, ug.`group_id` FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON ( u.id = ug.usr_id ) WHERE u.`birth` LIKE '%{$checkBirth}%' ".$where_sql;
		}else{
			 $where_sql = "";
			 $query = "SELECT `id`, `name` FROM `usr` WHERE `birth` LIKE '%{$checkBirth}%' ".$where_sql;
		}


		//echo $query;
		$res  = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$day_data[] = $row;
		}
		return $day_data;
	}

}
?>
<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");

class AttendanceList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;

	private $browse = 0;

	private $type = "";

	private $root_id = 0;

	private $year = "2017";

	private $page = 1;

	private $perPage = 10;

	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {

		$this->db = DBConnection::get()->handle();

		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
	    		default: $order_by = " ORDER BY `adddate` DESC";
			}
		}



		if ($this->keyword != ''){
			$keywordForDB = Common::cleanupDB($this->keyword);
			$where[] = "(`title` like '%".$keywordForDB."%' || `summary` like '%".$keywordForDB."%' )";
		}

        switch ($this->type) {
			default:  //normal

				if($this->root_id > 0){ $where[] = " `root_id` = '{$this->root_id}'"; }
				if($this->year != ""){ $where[] = " `year` = '{$this->year}'"; }

				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT * ";

				if($isCount == "count"){
					$from_query = "FROM `attendance` ".$where_sql;
				}else{
					$from_query = "FROM `attendance` ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit=1) {
			//global $db;
			$from  = ($this->page-1)*$this->perPage;
            $query = $this->getCondition();
			//echo $query;

			if($islimit == 1){
				$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
				$res = $this->db->query($query);
			}else{
				$res = $this->db->query($query);
			}
			$rows = $res->fetchAll(PDO::FETCH_ASSOC);
			$list = array();
			foreach ($rows as $row) {
				$list[] = $row;
			}
			return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];

	}
}

?>

<?php

require_once('../_classes/class.DBConnection.php');

/**
 * class CommentManager
 * @author  yhlee , <yuho@naver.com>
 */
class CommentManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
    /**
	 * @var file sn $id
	 */
	private $id=0;
	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn 
	 */

	public function __construct($id=0) {
		if(isset($id)) $this->id = $id; 
		$this->db = DBConnection::get()->handle();
	}

	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}


	public static function getCommentList($item_id) {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT *, (SELECT u.name FROM usr AS u WHERE u.id=ic.usr_id) AS usr_name FROM `item_comment` AS ic WHERE item_id='{$item_id}' ORDER BY reg_date DESC";
		$res  = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public static function getComment($id) {
		$db =& DBConnection::get()->handle();
		$query   = "SELECT * FROM `item_comment` WHERE id='{$id}'";
		$res  = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function insertComment($arry) {
		$db =& DBConnection::get()->handle();

		$query  = "INSERT INTO `item_comment` (`item_id`, `usr_id`, `comment`, `reg_date`) VALUES  ({$arry[item_id]},{$arry[usr_id]},'$arry[comment]',UNIX_TIMESTAMP())";
		$result = $db->query($query);

		$lastID = self::getOne("SELECT MAX(id) FROM `item_comment` ");
		return $lastID;
	}


	public static function modifyComment($arry,$id) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `item_comment` SET comment = '{$arry[comment]}'  WHERE id='$id' LIMIT 1";
		$result = $db->query($query);
		return $result;
	}

	public static function deleteComment($id) {
		$db =& DBConnection::get()->handle();
		$res =& $db->query("DELETE FROM `item_comment` WHERE id='{$id}' LIMIT 1");
		return $res;
	}

	public static function getCountComment($item_id) {
		$db =& DBConnection::get()->handle();
		$count = self::getOne("SELECT count(*) FROM `item_comment` WHERE item_id = '$item_id'");
		return $count;
	}

}
?>
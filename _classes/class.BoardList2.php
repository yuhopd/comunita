<?php
require_once("../_classes/class.DBConnection.php");

class BoardList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
    /**
	 * @var int $browse
	 */
	private $browse = 0;
	/**
	 * @var  int  $usr_id  
	 */
	private $usr_id = 0 ;
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $type = 'normal';
	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 10;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
		//$this->db = DBConnection::get("slave")->handle();       
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY b.subject DESC";   break;
				case 2:  $order_by = " ORDER BY b.reg_date DESC"; break;
	    		default: $order_by = " ORDER BY b.id DESC";
			}
		}

		if ($this->keyword != ''){
			$keywordForDB = cleanupDB(cleanupHTML($this->keyword));
			$where[] = "(title like '%$keywordForDB%' || note like '%$keywordForDB%')";
		}

        switch ($this->type) {
			default:  //normal
			    $where[] = "usr_id = {$this->usr_id}";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT b.id, b.usr_id, b.subject, b.content, b.r_cnt, b.c_cnt, b.reg_date, (SELECT u.name FROM `usr` AS u WHERE u.id = b.usr_id) AS usr_name ";

				if($isCount == "count"){
					$from_query = "FROM `board` AS b ".$where_sql;
				}else{
					$from_query = "FROM `board` AS b ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List 
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList() {
			//global $db;
			$from  = ($this->page-1)*$this->perPage;
            $query = $this->getCondition();
			
			$res =& $this->db->limitQuery($query, $from, $this->perPage);
			if ($res->numRows() == 0){
				return false;
			}else{
				while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
					$list[] = $row;
				}
			}
			return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		//global $db;
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$result->fetchInto($row, DB_FETCHMODE_ASSOC);
		return $row['count'];

	}
}
?>
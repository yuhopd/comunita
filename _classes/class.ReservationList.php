<?php
require_once("../_classes/class.DBConnection.php");

class ReservationList{

    private $db;

	private $browse = 0;

	private $category = 0;
	
	private $page = 1;
	
	private $perPage = 10;

	private $keyword = '';

	private $blist = "";
	private $genre = "";

	public static $table = 'reservation';


	private $process_id = 0;

	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}


	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 'title': 
					$order_by = " ORDER BY i.title DESC";
				break;
	    		default: $order_by = " ORDER BY id DESC";
			}
		}
	  
        switch ($this->type) {
			default:
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT i.* ";
					
				if($isCount == "count"){
					$from_query = "FROM ".self::$table." AS i ".$where_sql;
				} else {
					$from_query = "FROM ".self::$table." AS i ".$where_sql.$order_by;
				}
			break;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get Groups List 
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList($limit='O') {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		//echo $query;
		if($limit == 'O'){
			$query = $query." LIMIT ".$from.", ".($this->perPage);
			$res = $this->db->query($query);
		}else{
			$res = $this->db->query($query);
		}
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];
	}


}


?>
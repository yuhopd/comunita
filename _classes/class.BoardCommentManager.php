<?php

require_once('../_classes/class.DBConnection.php');

/**
 * class CommentManager
 * @author  yhlee , <yuho@naver.com>
 */
class BoardCommentManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
    /**
	 * @var file sn $id
	 */
	private $id=0;
	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn 
	 */

	public function __construct($id=0) {
		if(isset($id)) $this->id = $id; 
		$this->db = DBConnection::get()->handle();
	}


	public static function getCommentList($board_id) {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT *, (SELECT u.name FROM usr AS u WHERE u.id=ic.usr_id) AS usr_name FROM `board_comment` AS ic WHERE board_id='{$board_id}' ORDER BY `adddate` DESC";
		$res  = $db->query($query);
		while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
			$list[] = $row;
		}
		$res->free();
		return $list;
	}


	public static function getComment($id) {
		$db =& DBConnection::get()->handle();
		$query   = "SELECT * FROM `board_comment` WHERE id='{$id}'";
		$res  = $db->query($query);
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$comment = $row;
		}
		$res->free();
		return $comment;
	}

	public static function insertComment($arry) {
		$db =& DBConnection::get()->handle();

		$query  = "INSERT INTO `board_comment` (`board_id`, `usr_id`, `comment`, `adddate`) VALUES  ({$arry[board_id]},{$arry[usr_id]},'$arry[comment]',UNIX_TIMESTAMP())";
		$result = $db->query($query);
        //echo $query;
		$lastID = $db->getOne("SELECT MAX(id) FROM `board_comment` ");
		return $lastID;
	}


	public static function modifyComment($arry,$id) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `board_comment` SET comment = '{$arry[comment]}'  WHERE id='$id' LIMIT 1";
		$result = $db->query($query);
		return $result;
	}

	public static function deleteComment($id) {
		$db =& DBConnection::get()->handle();
		$res =& $db->query("DELETE FROM `board_comment` WHERE id='{$id}' LIMIT 1");
		return $res;
	}

	public static function getCountComment($board_id) {
		$db =& DBConnection::get()->handle();
		$count = $db->getOne("SELECT count(*) FROM `board_comment` WHERE `board_id` = '$board_id'");
		return $count;
	}

}
?>
<?php
/*
    $Author: yhlee $
    $Date: 2007/11/26 06:01:34 $
    $Header: /home/cvs/docex/Docs2.0/_classes/class.GroupsList.php,v 1.12 2007/11/26 06:01:34 yhlee Exp $
    $Id: class.GroupsList.php,v 1.12 2007/11/26 06:01:34 yhlee Exp $
    $Source: /home/cvs/docex/Docs2.0/_classes/class.GroupsList.php,v $
*/

require_once('../_classes/class.DBConnection.php');

class GroupsList{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	/**
	 * @var  int  $browse
	 */
	private $browse = 0;
	/**
	 * @var  int  $language
	 */
	private $language = 0;
	/**
	 * @var  int  $period
	 */
	private $period = 0;
	/**
	 * @var  int  $sn
		   -> $dsn : docs       (type = 1)
		   -> $psn : portfolio  (type = 2)
		   -> $gsn : group      (type = 3)
		   -> $usn : member     (type = 4)
		   -> $msn : message    (type = 5)
	 */
	private $sn = 0 ;
	/**
	 * @var  int  $type  ( 1:docs ,2:portfolio, 3:group, 4:member, 5:message )
	 */
	private $type = 0;
	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 20;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('browse','language','period','sn','type','page','prePage','keyword');
	 */
	function __construct($properties) {
        $this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY cnt_v DESC";   break;
				case 2:  $order_by = " ORDER BY adddate DESC"; break;
				case 3:  $order_by = " ORDER BY cnt_msg DESC"; break;
				case 4:  $order_by = " ORDER BY cnt_m DESC";   break;
				case 5:  $order_by = " ORDER BY cnt_f DESC";   break;
				case 6:  $order_by = " ORDER BY BINARY g.title ASC";   break;
                case 7:  $order_by = " ORDER BY rateavg DESC";  break;
				default: $order_by = " ORDER BY gsn DESC";
			}
		}
		if($this->language != 0) {
			$where[] = "language = ".$this->language;
		}

		$time = time();
		if($this->period == 1) {
			$ago = ($time - (24*60*60));
			$where[] = "adddate > $ago";
		}else if($this->period == 2) {
			$ago = ($time - (7*24*60*60));
			$where[] = "adddate > $ago";
		}else if($this->period == 3) {
			$ago = ($time - (30*24*60*60));
			$where[] = "adddate > $ago";
		}

		if ($this->keyword != ''){
			//$keywordForDB = base64_decode($this->keyword);
			//$keywordForDB = cleanupDB(cleanupHTML($keywordForDB));

			$keywordForDB = cleanupDB(cleanupHTML($this->keyword));
			$where[] = "(g.title like '%$keywordForDB%' || g.summary like '%$keywordForDB%')";
            $where[] = "permission IN(2,3)";
		}else{
            $where[] = "permission IN(2,3)";
        }



		$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

        switch ($this->type) {
            case 4:  // members
				$select_fields = "SELECT g.gsn, g.gCode, g.owner, g.cnt_v, g.cnt_m, g.cnt_doc, g.cnt_pof, g.cnt_msg, g.adddate, g.modifydate, g.title, g.summary, g.is_image, gu.grade, (SELECT name FROM usr as u WHERE u.usn = g.owner) AS name ";
				$from_query = "FROM `groups_usrs` AS gu LEFT JOIN `groups` AS g ON gu.gsn = g.gsn WHERE gu.usn = {$this->sn} AND grade IN(1,2) ".$order_by;
                break;
			default:
				$select_fields = "SELECT groups.*, (SELECT name FROM usr WHERE usr.usn = groups.owner) AS name ";
				$from_query = "FROM groups ".$where_sql.$order_by;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;
		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get Groups List
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getGroupsList() {
		$from  = ($this->page-1)*$this->perPage;
		$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
		$res   = $this->db->query($query);
		$rows  = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	/**
	 * Get Groups Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->sdb->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];
	}
}



/* how to use */

// Groups List
/*
echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
$properties = array(
	"browse"   => 0,
	"language" => 0,
	"period"   => 0,
	"sn"       => 0,
	"type"     => 0,
	"page"     => 1,
	"perPage"  => 5,
	"keyword"  => ''
);
$groupslist = new GroupsList($properties);
$start_time1 = microtime();
//$count  = $groupslist->getTotalCount();
//echo "total count:::::::::$count<br />";
$list   = $groupslist->getGroupsList();
$tot = count($list);
for ($ii=0; $ii<$tot; $ii++){
	print_r ($list[$ii]);
	echo"<br><br>";
    //echo "<li>{$list[$ii][gsn]} :: {$list[$ii][title]}</li>";
}
$end_time1 = microtime();
$result1 = $end_time1 - $start_time1;


echo "<br><br><br>";
echo "array : ".$result1;
*/

?>
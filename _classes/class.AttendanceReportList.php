<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");


class AttendanceReportList{
    /**
	 * @var object $db database connection
	 */
    private static $db;

	private $browse = 0;

	private $usr_id = 0;

	private $type = "";

	private $page = 1;

	private $perPage = 10;

	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
	 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
	    		default: $order_by = " ORDER BY `date` DESC, `adddate` DESC";
			}
		}



		if ($this->keyword != ''){
			$keywordForDB = Common::cleanupDB($this->keyword);
			$where[] = "(`note` like '%".$keywordForDB."%')";
		}

        switch ($this->type) {
			default:  //normal

				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT ar.* ";
				if($isCount == "count"){
					$from_query = "FROM `attendance_report` AS ar ".$where_sql;
				}else{
					$from_query = "FROM `attendance_report` AS ar ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit="O") {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		if($islimit == 'O'){
			$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
			$res = $this->db->query($query);
		}else{
			$res = $this->db->query($query);
		}
		
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_NUM);
		return $row['count'];

	}
}

?>
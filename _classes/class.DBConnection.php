<?php


//require_once "DB.php";

/**
 * singleton pattern :: http://www-128.ibm.com/developerworks/opensource/library/os-php-designptrns/#N10110
 *
 * how to use
 * $mdb = DBConnection::get("master")->handle(); // return master database connection object
 * $sdb = DBConnection::get("slave")->handle(); // return slave database connection object
 *
 */
class DBConnection {

    /**
	 * @var string user value for database connection.
	 */
    private $user = "root";
    /**
	 * @var string password value for database connection.
	 */
    private $password = "wndrhrehd";
    /**
	 * @var string result host for database connection.
	 */
    private $host = "localhost";
    /**
	 * @var string datebase value for database connection.
	 */
    private $database = "comunita";
    /**
	 * @var object database connection object.
	 */
    
    private $db = null;

    /**
     * this Class can't "New DBConnection", cuz __construct is private.
     * @param string $db_type database connection host (masterr or slave)
     * @return void
     *
     */
    private function __construct($db_type = 'mysql')
    {
        //$this->_handle = new PDO("mysql:host=".$this->host.";dbname=".$this->database, $this->user, $this->password);
        //$this->_handle->query("SET NAMES 'utf8'");

        $this->_handle = new PDO("sqlite:"."../db/comunita.db","","");
        
    }


    /**
     * call database connection object construct
     * @param string $db_type database connection host (masterr or slave)
     * @return object $db
     *
     */
    public static function get($db_type='mysql') {
       static $db = null;
       if ( $db == null ){
           $db = new DBConnection($db_type);
       }
       return $db;
    }
    /**
    * return database connection object
    * @return object $_handle (database connection object)
    *
    */
    public function handle(){
        return $this->_handle;
    }
}

?>
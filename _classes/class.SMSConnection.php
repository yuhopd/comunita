<?php
require_once "DB.php";

class DBConnection {
    private $user     = "root";
    private $password = "wndrhrehd";
    private $host     = "localhost";
    private $database = "widefoot";
    private $_handle  = null;

	private function __construct(){    	
		$this->host = "localhost";
		$dsn = "mysql://".$this->user.":".$this->password."@".$this->host."/".$this->database;
		$this->_handle =& DB::Connect( $dsn, array() );				
		$this->_handle->query("SET NAMES 'utf8'");
    }

    public static function get() {
        static $db = null;
        if ( $db == null ){
            $db = new DBConnection();
		}
        return $db;
    }
    
    public function handle(){
        return $this->_handle;
    }
}
?>
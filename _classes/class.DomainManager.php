<?php
require_once('../_classes/class.DBConnection.php');
/**
 * class DomainManager
 * @author  yhlee , <yuho@naver.com>
 */
class DomainManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
    private $id=0;

    public function __construct($properties) {
        $this->db = DBConnection::get()->handle();
        if(isSet($properties)) {
            foreach ($properties as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    public static function insert_manager($arry) {
        $db       = DBConnection::get()->handle();
        $sequence = self::getMaxSequence($arry['position']) + 1;
        $time = time();
        $query    = "INSERT INTO `domain_manager` (`domain_id`, `uid`, `psw`,`name`, `phone`, `email`, `auth`, `adddate`)
		VALUES  ('{$arry[domain_id]}', '{$arry[uid]}', '{$arry[psw]}', '{$arry[name]}', '{$arry[phone]}', '{$arry[email]}', '{$arry[auth]}', '{$time}')";
        $result = $db->query($query);
        $lastID = $db->lastInsertId();
        return $lastID;
    }



}
?>
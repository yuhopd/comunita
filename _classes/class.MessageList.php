<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");
class MessageList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
	
	private $browse = 0;
	
	private $is_read = "X";
	
	private $send_id = 0;
	
	private $read_id = 0;

	private $page = 1;
	
	private $perPage = 20;
	
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
      
		$this->db = DBConnection::get()->handle();

		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY m.adddate DESC"; break;
	    		default: $order_by = " ORDER BY m.adddate DESC";
			}
		}



		if ($this->keyword != ''){
			$keywordForDB = Common::cleanupDB($this->keyword);
			$where[] = "(m.`content` like '%".$keywordForDB."%')";
		}



        switch ($this->type) {

            case 'search':  
				if($this->category != 0){
					$where[] = "`category` = '{$this->category}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT m.id, m.send_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.send_id) AS send_name, m.read_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.read_id) AS read_name, m.content, m.adddate, m.is_read ";

				if($isCount == "count"){
					$from_query = "FROM `massege` AS m ".$where_sql;
				}else{
					$from_query = "FROM `massege` AS m ".$where_sql.$order_by;
				}
				break;

			case 'noRead':  
				$where[] = "m.`read_id` = '{$this->read_id}'";
				$where[] = "m.`is_read` = 'X'";
			 	$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT m.id, m.send_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.send_id) AS send_name, m.read_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.read_id) AS read_name, m.content, m.adddate, m.is_read ";
				if($isCount == "count"){
					$from_query = "FROM `message` AS m ".$where_sql;
				}else{
					$from_query = "FROM `message` AS m ".$where_sql.$order_by;
				}
				break;

			default:  //normal
				$where[] = "m.`read_id` = '{$this->read_id}'";
			 	$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT m.id, m.send_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.send_id) AS send_name, m.read_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.read_id) AS read_name, m.content, m.adddate, m.is_read ";
				if($isCount == "count"){
					$from_query = "FROM `message` AS m ".$where_sql;
				}else{
					$from_query = "FROM `message` AS m ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List 
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit='O') {
			//global $db;
			$from  = ($this->page-1)*$this->perPage;
            $query = $this->getCondition();
			//echo $query;

			if($islimit=='O'){
				$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
				$res = $this->db->query($query);
			}else{
				$res = $this->db->query($query);
			}
			$rows = $res->fetchAll(PDO::FETCH_ASSOC);
			$list = array();
			foreach ($rows as $row) {
				$list[] = $row;
			}
			return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];
	}
}

?>
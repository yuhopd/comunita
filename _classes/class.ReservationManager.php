<?php
require_once('../_classes/class.DBConnection.php');
class ReservationManager{

	public static $db;

	private $id=0;
	private $type = "img";

	public static $table = 'reservation';

	private $file_name;
	private $file_rename;
	private $file_ext;
	private $size = 0;

    public  $src_path = "reservation";
	private $allowedExtensions = array("jpg","png","gif","pdf");

    public function __construct($properties) {        
        if(isSet($properties)) {
            foreach ($properties as $key => $value) {
                $this->{$key} = $value;
            }
        }
        self::$db = DBConnection::get()->handle();
    }

	public static function getItem($id) {
		$query = "SELECT * FROM ".self::$table." WHERE `id`='$id' ";
		$res = self::$db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function getColumn() {
        $query = "SELECT * FROM ".self::$table;
		$res   = self::$db->query($query);
		$total_column = $res->columnCount();
		for($i = 0; $i < $total_column; $i++){
			$meta = $res->getColumnMeta($i);
			$column[] = $meta['name'];
		}
		return $column;
	}

	public static function insert($arry) {
        $column = self::getColumn();
        $akey = array_keys($arry);
        $aVal = array_values($arry);

        for($i = 0; $i < count($akey); $i++){
            if (in_array($akey[$i], $column)) {
                $_sql[] = $akey[$i]." = '{$aVal[$i]}'";
            }
        }
        $sql = implode(",", $_sql);
        $query  = "INSERT INTO ".self::$table." SET ".$sql;
        
		$result = self::$db->query($query);
		$lastID = self::$db->lastInsertId();
        return $lastID;
    }

	public static function modify($arry) {
        $column = self::getColumn();
        $akey = array_keys($arry);
        $aVal = array_values($arry);

        for($i = 0; $i < count($akey); $i++){
            if (in_array($akey[$i], $column)) {
				$_sql[] = $akey[$i]." = '{$aVal[$i]}'";
			}
        }
        $sql = implode(",", $_sql);
        $query  = "UPDATE ".self::$table." SET ".$sql."  WHERE `id` = '{$arry[id]}'";
        
		$result = self::$db->query($query);
		return $result;
	}

	public static function delete($id) {
		$query  = "DELETE FROM ".self::$table." WHERE `id` = '{$id}' LIMIT 1";
		$res = self::$db->query($query);
		return $res;
    }



    public function saveFileToStorage(){
		if(is_null($this->path) || $this->path == ""){
			$this->path = date("ym");
		}
        if(!is_dir(PATH_DATA_DIR."/".$this->path)){
            mkdir (PATH_DATA_DIR."/".$this->path, 0777);
        }

		$this->file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

		// max file size in bytes
		$sizeLimit = 1024 * 1024 * 1024;
		$pathinfo = pathinfo($_FILES['file']['name']);

		$this->file_ext = $pathinfo['extension'];
		$uploadfile = PATH_DATA_DIR."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		
		if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
			$r["url"] = $webfile;
			$r["error"] = -1;
			$r["msg"] = "File is valid, and was successfully uploaded.";
		} else {
			$r["error"] = 500;
			$r["msg"] = "Possible file upload attack!";
		}


		$target_storage        = PATH_DATA_DIR."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		$target_photo_storage  = PATH_DATA_DIR."/".$this->path."/".$this->file_rename."_photo.".$this->file_ext;
		$target_thumb_storage  = PATH_DATA_DIR."/".$this->path."/".$this->file_rename."_thumb.".$this->file_ext;
		$upload_tmp_name = $target_storage;
		$percent_resizing = 2000;

		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}

		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_photo_storage, 200);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_photo_storage);
		}else{
			 imagegif($dst_img, $target_photo_storage);
		}
		imagedestroy($dst_img);









		$percent_resizing = 100;

		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}

		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_thumb_storage, 200);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_thumb_storage);
		}else{
			 imagegif($dst_img, $target_thumb_storage);
		}
		imagedestroy($dst_img);



		$r['path'] = $this->path;

		$r['file_name'] = $pathinfo['filename'];
		$r['file_rename'] = $this->file_rename;
		$r['file_ext'] = $this->file_ext;
		

        return $r;
	}
    
    public function deleteFile($id){
		$res = self::$db->query("SELECT * FROM `draw_img` WHERE `id` = '{$id}'");
		if($row = $res->fetch(PDO::FETCH_ASSOC)){
			self::$db->query("DELETE FROM `draw_img` WHERE `id` = '{$row[id]}' LIMIT 1");

			@unlink(PATH_DATA_DIR."/".$row['path']."/".$row['file_rename'].".".$row['file_ext']);
            if($row['file_ext'] == "jpg" || $row['file_ext'] == "png" || $row['file_ext'] == "gif"){
				@unlink(PATH_DATA_DIR."/".$row['path']."/".$row['file_rename']."_thumb.".$row['file_ext']);
			}
		}
	}

}
?>
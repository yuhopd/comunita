<?php
require_once('../_classes/class.DBConnection.php');

/**
 * class GroupManager
 * @author  yhlee , <yhlee@thinkfree.com>
 */
class GroupManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;

	private $id=0;

    public  $src_path = "/home/himh/public_html/box_data";
	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn
	 */
	public function __construct($id=0) {
		if(isset($id)) $this->id = $id;
		$this->db = DBConnection::get()->handle();
	}

	public static function getGroup($group_id) {
		$db = DBConnection::get()->handle();
		$query = "SELECT * FROM `group` WHERE id='$group_id'";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);	
        return $row;
	}

	public static function insert($arry) {
		$db = DBConnection::get()->handle();
		$sequence = self::getMaxSequence($arry['parent_id']);
		$sequence = $sequence + 1;
		$time = time();
		$query  = "INSERT INTO `group` (`parent_id`, `usr_id`, `title`, `summary`, `is_enable`, `use_attendance`, `use_calendar`, `sequence`, `reg_date`) VALUES  ('{$arry[parent_id]}', '{$arry[usr_id]}', '{$arry[title]}', '{$arry[summary]}', '{$arry[is_enable]}', '{$arry[use_attendance]}', '{$arry[use_calendar]}', '{$sequence}', '$time')";
		
		
		$res = $db->query($query);
		//$lastID = $db->getOne("SELECT MAX(id) FROM `group` ");
		$lastID = $db->lastInsertId();
		return $lastID;
	}

	public static function modify($arry) {
		$db = DBConnection::get()->handle();
		$query  = "UPDATE `group` SET
		                `usr_id`         = '{$arry[usr_id]}',
						`title`          = '{$arry[title]}',
						`is_enable`      = '{$arry[is_enable]}',
						`use_attendance` = '{$arry[use_attendance]}',
                        `use_calendar`   = '{$arry[use_calendar]}',
						`summary`        = '{$arry[summary]}'
				  WHERE `id` = '{$arry[group_id]}'";
		$res = $db->query($query);
		return $arry['group_id'];
	}

	public static function usr_edit($users, $group_id, $level) {
		$db = DBConnection::get()->handle();

        $post_array_usrs = explode("|", $users);
		$query = "SELECT u.id FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON ( u.id = ug.usr_id ) WHERE ug.group_id = '{$_POST[group_id]}' AND ug.level = $level";
		$res = $db->query($query);

		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$assoc = array();
		foreach ($rows as $row) {
			$arry_usrs[] = $row['id'];
		}

		for($i=0;$i<count($post_array_usrs);$i++){
			if(!in_array($post_array_usrs[$i],$arry_usrs)){
				$query = "INSERT INTO `usr_group` (`usr_id`, `group_id`, `level`) VALUES ('{$post_array_usrs[$i]}', $group_id, $level)";
				$result = $db->query($query);
			}
		}

		for($i=0;$i<count($arry_usrs);$i++){
			if(!in_array($arry_usrs[$i],$post_array_usrs)){
				$query = "DELETE FROM `usr_group` WHERE `usr_id` = '{$arry_usrs[$i]}' AND `group_id` = '{$group_id}' AND `level` = $level";
				$result = $db->query($query);
			}
		}
	}

	public static function del($id) {
		$db = DBConnection::get()->handle();
		$result = $db->query("DELETE FROM `group` WHERE id='$id'");
		$db->query("DELETE FROM `usr_group` WHERE group_id='$id'");

		return $result;
	}



	/**
	 * static  getRootIds($usr_id);
	 * @return boolean $result;
	 * @param  int     $id;
	 */
	public static function getRootIdFromUsr($usr_id) {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT `group_id` FROM `usr_group` WHERE `usr_id` = '{$usr_id}' LIMIT 1";
		$res = $db->query($query);
		if ($row   = $res->fetch(PDO::FETCH_ASSOC)) {
			$root = self::getRootId($row['group_id']);
		}
        return $root;
	}

	public static function getRootIdsFromUsr($usr_id) {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT `group_id` FROM `usr_group` WHERE `usr_id` = '{$usr_id}' LIMIT 1";
		$res = $db->query($query);
		if ($row   = $res->fetch(PDO::FETCH_ASSOC)) {
			$root = self::getRootId($row['group_id']);
		}
        return $root;
	}

	public static function getAttendanceAdminGroups() {
		$db =& DBConnection::get()->handle();
		//$query = "SELECT * FROM `group` WHERE `use_attendance`='O' ORDER BY `id` ASC";
		$query = "SELECT * FROM `group` WHERE `use_attendance`='O' ORDER BY `parent_id` ASC, `sequence` ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}
/*
	public static function getGroupUseAttendanceFromUsr($sys_id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT g.* FROM `group` AS g LEFT JOIN `usr_group` AS ug ON ( g.id = ug.group_id ) WHERE ug.`usr_id`='{$sys_id}' AND ug.level > 0 ORDER BY ug.`level` ASC, g.`parent_id` ASC, BINARY g.`title` ASC";
		$res = $db->query($query);
		while($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
			$list[] = $row;
		}
		$res->free();
		return $list;
	}

*/
	public static function getGroupUseAttendanceFromUsr($sys_id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT g.* FROM `group` AS g LEFT JOIN `usr_group` AS ug ON ( g.id = ug.group_id ) WHERE ug.`usr_id`='{$sys_id}' AND ug.`level` > 0 ORDER BY ug.level ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	/**
	 * static  getRootIds($usr_id);
	 * @return boolean $result;
	 * @param  int     $id;
	 */
	public static function getRootId($group_id) {
		$db =& DBConnection::get()->handle();
		$parent_id = self::getParentId($group_id);
		//$parent_id = $db->getOne("SELECT `parent_id` FROM `group` WHERE `id` = '{$group_id}'");
		if($parent_id == 0){
			$result = $group_id;
		}else{
			while($parent_id > 0) {
				$result = $parent_id;
				$parent_id = self::getParentId($parent_id);
				//$parent_id = $db->getOne("SELECT `parent_id` FROM `group` WHERE `id` = '{$parent_id}'");
			}
		}
		return $result;
	}


	public static function getAttendanceAdminGroupId($group_id) {
		$db =& DBConnection::get()->handle();
		$parent_id = 0;
		$res = $db->query("SELECT `parent_id`, `use_attendance` FROM `group` WHERE `id` = '{$group_id}'");
		if ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
			$parent_id = $row['parent_id'];
			$use_attendance = $row['use_attendance'];
		}

		if($use_attendance == "O"){
			$result = $group_id;
		}else if($parent_id == 0){
			$result = $group_id;
		}else{
			while($parent_id > 0 && $use_attendance == "X") {
				$result = $parent_id;
				$res = $db->query("SELECT `parent_id`, `use_attendance` FROM `group` WHERE `id` = '{$parent_id}'");
				$row   = $res->fetch(PDO::FETCH_ASSOC);
				$parent_id = $row['parent_id'];
				$use_attendance = $row['use_attendance'];
				
			}
		}
		return $result;
	}

	public static function getRootGroup($group_id) {
		$db =& DBConnection::get()->handle();
		$root_id = self::getRootId($group_id);
	    $group = self::getGroup($root_id);
		return $group;
	}

	public static function getParentId($group_id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT `parent_id` FROM `group` WHERE `id` = '{$group_id}'";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row['parent_id'];
		} else {
			return false;
		}
	}

	public static function getParentGroup($group_id) {
		$db =& DBConnection::get()->handle();
		$parent_id = self::getParentId($group_id);
		$query = "SELECT * FROM `group` WHERE `id` = '{$parent_id}'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
        return $row;
	}

	public static function getAllRootIds() {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT * FROM `group` WHERE `parent_id` = 0 ORDER BY `id` ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function getUseCalendarGroup() {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT * FROM `group` WHERE `use_calendar` = 'O' ORDER BY `id` ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function getUseCalendarGroupColor() {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT id, color FROM `group` WHERE `use_calendar` = 'O' ORDER BY `id` ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[$row['id']] = $row['color'];
		}
		return $list;
	}

	public static function setGroupOrders($group_id,$arryUsr) {
		$db =& DBConnection::get()->handle();
        $sequence = 1;
        for($i=0;$i<=count($arryUsr);$i++){
			$res = $db->query("UPDATE `usr_group` SET `sequence` = '{$sequence}' WHERE `group_id` = '{$group_id}' AND `usr_id` = '{$arryUsr[$i]}'");
			$sequence++;
		}
		return $group_id;
	}

	public static function upSequence($id) {
		$db =& DBConnection::get()->handle();

		$query0  = "SELECT * FROM `group` WHERE id='{$id}'";
		$res0    = $db->query($query0);
		$row0    = $res0->fetch(PDO::FETCH_ASSOC);
		
		$query1  = "SELECT * FROM `group` WHERE `sequence` < {$row0[sequence]} AND `parent_id`='{$row0[parent_id]}' ORDER BY `sequence` DESC LIMIT 1";
		$res1    = $db->query($query1);
		$row1    = $res1->fetch(PDO::FETCH_ASSOC);

		$query2  = "UPDATE `group` SET `sequence`='{$row1[sequence]}' WHERE id='{$id}'";
		$res2    = $db->query($query2);
		$query3  = "UPDATE `group` SET `sequence`='{$row0[sequence]}' WHERE id='{$row1[id]}'";
		$res3    = $db->query($query3);
	}


	public static function downSequence($id) {
		$db =& DBConnection::get()->handle();

		$query0  = "SELECT * FROM `group` WHERE id='{$id}'";
		$res0    = $db->query($query0);
		$row0    = $res0->fetch(PDO::FETCH_ASSOC);

		$query1  = "SELECT * FROM `group` WHERE `sequence` > {$row0[sequence]} AND `parent_id`='{$row0[parent_id]}' ORDER BY `sequence` ASC LIMIT 1";
		$res1    = $db->query($query1);
		$row1    = $res1->fetch(PDO::FETCH_ASSOC);

		$query2  = "UPDATE `group` SET `sequence`='{$row1[sequence]}' WHERE id='{$id}'";
		$res2    = $db->query($query2);
		$query3  = "UPDATE `group` SET `sequence`='{$row0[sequence]}' WHERE id='{$row1[id]}'";
		$res3    = $db->query($query3);
	}

	public static function getMaxSequence($parent_id) {
		$db =& DBConnection::get()->handle();
		$res = $db->query("SELECT MAX(sequence) FROM `group` WHERE `parent_id` = '{$parent_id}'");
		$row = $res->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}
}

?>
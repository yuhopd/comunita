<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");
class BoardCommentList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
    /**
	 * @var int $browse
	 */
	private $browse = 0;
	/**
	 * @var  int  $usr_id  
	 */
	private $category = 1 ;
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $type = "";

	private $board_id = 0;

	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 20;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
      
		$this->db = DBConnection::get()->handle();

		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY c.`id` DESC";   break;
	    		default: $order_by = " ORDER BY c.`id` ASC";
			}
		}


        switch ($this->type) {
			default:  //normal
				if($this->board_id != 0){
					$where[] = "`board_id` = '{$this->board_id}'";
				}
		    
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT c.`id`, c.`board_id`, c.`usr_id`, c.`comment`, c.`adddate`, (SELECT u.name FROM usr AS u WHERE u.id=c.usr_id) AS usr_name  ";
				if($isCount == "count"){
					$from_query = "FROM `board_comment` AS c ".$where_sql;
				}else{
					$from_query = "FROM `board_comment` AS c ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;
		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List 
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit="O") {
			$from  = ($this->page-1)*$this->perPage;
            $query = $this->getCondition();
			//echo $query;
			if($islimit=="O"){
				$res =& $this->db->limitQuery($query, $from, $this->perPage);
			}else{
				$res =& $this->db->query($query);
			}

			if ($res->numRows() == 0){
				return false;
			}else{
				while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
					$list[] = $row;
				}
			}
			return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$result->fetchInto($row, DB_FETCHMODE_ASSOC);
		return $row['count'];

	}
}

?>
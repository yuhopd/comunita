<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");
class BoardList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
	
	private $browse = 0;
	
	private $category = 0;
	
	private $group_id = 0;
	
	private $type = "";

	private $page = 1;
	
	private $perPage = 20;
	
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
      
		$this->db = DBConnection::get()->handle();

		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY b.title DESC";   break;
				case 2:  $order_by = " ORDER BY b.reg_date DESC"; break;
				case 3:  $order_by = " ORDER BY b.adddate DESC"; break;
	    		default: $order_by = " ORDER BY b.id DESC";
			}
		}



		if ($this->keyword != ''){
			$keywordForDB = Common::cleanupDB($this->keyword);
			$where[] = "(b.`reg_date` like '%".$keywordForDB."%' || b.`title` like '%".$keywordForDB."%' || b.`summary` like '%".$keywordForDB."%' || b.`content` like '%".$keywordForDB."%')";
		}



        switch ($this->type) {

            case 'search':  
				if($this->category != 0){
					$where[] = "`category` = '{$this->category}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT b.id, replace(b.title, '{$this->keyword}', '<span class=\"red\">{$this->keyword}</span>') as `title`, b.name, replace(b.summary, '{$this->keyword}', '<span class=\"red\">{$this->keyword}</span>') as `summary`, b.content, b.reg_date, b.adddate, b.lastUpdate, b.hit, (SELECT t.rename FROM `board_thumb` AS t WHERE t.board_id=b.id) AS thumb_rename, (SELECT t.ext FROM `board_thumb` AS t WHERE t.board_id=b.id) AS thumb_ext ";

				if($isCount == "count"){
					$from_query = "FROM `board` AS b".$where_sql;
				}else{
					$from_query = "FROM `board` AS b".$where_sql.$order_by;
				}
				break;

            case 'all': 
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT b.id, (SELECT c.title FROM `board_category` AS c WHERE c.id=b.category) AS category_title, b.`title`, (SELECT u.name FROM `usr` AS u WHERE u.id=b.usr_id) AS usr_name, b.name, b.summary, b.content, b.reg_date, b.adddate, b.lastUpdate, b.hit, (SELECT t.rename FROM `board_files` AS t WHERE t.board_id=b.id  ORDER BY t.id ASC LIMIT 1) AS thumb_rename, (SELECT t.ext FROM `board_files` AS t WHERE t.board_id=b.id ORDER BY t.id ASC LIMIT 1) AS thumb_ext ";

				if($isCount == "count"){
					$from_query = "FROM `board` AS b".$where_sql;
				}else{
					$from_query = "FROM `board` AS b".$where_sql.$order_by;
				}
				break;

            case 'group': 
				$where[] = "b.`group_id` = '{$this->group_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT b.id, (SELECT c.title FROM `board_category` AS c WHERE c.id=b.category) AS category_title, b.`title`, (SELECT u.name FROM `usr` AS u WHERE u.id=b.usr_id) AS usr_name, b.name, b.summary, b.content, b.reg_date, b.adddate, b.lastUpdate, b.hit ";

				if($isCount == "count"){
					$from_query = "FROM `board` AS b".$where_sql;
				}else{
					$from_query = "FROM `board` AS b".$where_sql.$order_by;
				}

				break;

			default:  //normal
			    //$where[] = "b.`category` = '{$this->category}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT b.id, b.`title`, (SELECT u.name FROM `usr` AS u WHERE u.id=b.usr_id) AS usr_name, b.name, b.summary, b.content, b.reg_date, b.adddate, b.lastUpdate, b.hit, (SELECT t.rename FROM `board_files` AS t WHERE t.board_id=b.id  ORDER BY t.id ASC LIMIT 1) AS thumb_rename, (SELECT t.ext FROM `board_files` AS t WHERE t.board_id=b.id ORDER BY t.id ASC LIMIT 1) AS thumb_ext ";

				if($isCount == "count"){
					$from_query = "FROM `board` AS b".$where_sql;
				}else{
					$from_query = "FROM `board` AS b".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List 
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit=1) {
			//global $db;
			$from  = ($this->page-1)*$this->perPage;
            $query = $this->getCondition();
			//echo $query;

			if($islimit==1){
				$res =& $this->db->limitQuery($query, $from, $this->perPage);
			}else{
				$res =& $this->db->query($query);
			}

			if ($res->numRows() == 0){
				return false;
			}else{
				while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
					$list[] = $row;
				}
			}
			return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$result->fetchInto($row, DB_FETCHMODE_ASSOC);
		return $row['count'];

	}
}

?>
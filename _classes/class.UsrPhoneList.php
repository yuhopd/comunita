<?php
require_once("../_classes/class.DBConnection.php");

class UsrPhoneList{
    /**
	 * @var object $db database connection
	 */
    private static $db;
    /**
	 * @var int $type
	 */
	private $type = "";
    /**
	 * @var int $browse
	 */
	private $browse = 0;
	/**
	 * @var  int  $usr_id
	 */
	private $usr_id = 0 ;
	/**
	 * @var  int  $sameage_id
	 */
	private $sameage_id = 0;
	private $root_id = 0;
	private $group_id = 0;
	private $group_level = 0;
	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 20;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
		if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 2:  $order_by = " ORDER BY sameage_id DESC"; break;
				case 1:  $order_by = " ORDER BY ameage_id ASC "; break;
				default: $order_by = " ORDER BY name ASC";
			}
		}

        switch ($this->type) {

            case 'usr_group_list':
				$where[] = "usr_group.usr_id = '{$this->usr_id}'";
				//$where[] = "usr_group.level < 2";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT usr_group.id, usr_group.group_id, usr_group.level, usr_group.duty, `group`.title, `group`.parent_id ";

				if($isCount == "count"){
					$from_query = "FROM `group` LEFT JOIN usr_group ON `group`.id = usr_group.group_id ".$where_sql;
				}else{
					$from_query = "FROM `group` LEFT JOIN usr_group ON `group`.id = usr_group.group_id ".$where_sql." ORDER BY usr_group.id DESC";
				}
			break;

            case 'search':
				$where[] = "u.name LIKE '%{$this->keyword}%' OR u.phone LIKE '%{$this->keyword}%' OR u.email LIKE '%{$this->keyword}%'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.name, u.phone, u.email, u.birth, u.gender, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
					//$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
					//$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;

            case 'family_search':
				//$where[] = "uf.usr_id != '{$this->usr_id}'";
				$where[] = "name LIKE '%{$this->keyword}%' OR phone LIKE '%{$this->keyword}%' OR email LIKE '%{$this->keyword}%'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT * ";

				if($isCount == "count"){
					$from_query = "FROM `usr` ".$where_sql;
				}else{
					$from_query = "FROM `usr` ".$where_sql.$order_by;
				}
			break;

			case 'allWithOutGroup':
				//$where[] = "ug.group_id != '{$this->group_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.gender ";
				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;

			case 'allWithRootGroup':
			    $where[] = "ug.group_id = '{$this->root_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.gender ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql.$order_by;
				}
			break;


            case 'group_list_level':
				$where[] = "ug.level = {$this->group_level}";
				$where[] = "ug.group_id = '{$this->group_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.phone, u.email, u.birth, u.gender, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql.$order_by;
				}
			break;


            case 'group_list':
				$where[] = "ug.level < 2";
				$where[] = "ug.group_id = '{$this->group_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.phone, u.email, u.birth, u.gender, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql.$order_by;
				}
			break;

            case 'family_list':

				$where[] = "uf.usr_id = '{$this->usr_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.phone, u.email, u.birth, u.gender, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_family` AS uf ON u.id = uf.family_id ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_family` AS uf ON u.id = uf.family_id ".$where_sql.$order_by;
				}
			break;


			default:  //normal
			    if($this->sameage_id != 0){
					$where[] = "u.sameage_id = '{$this->sameage_id}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.sameage_id, u.name, u.phone, u.email, u.birth, u.gender, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage ";

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u  ".$where_sql;
					//$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
					//$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;


		return $query;
	}

	/**
	 * Get Groups List
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList($limit = 'O') {
			$from  = ($this->page-1)*$this->perPage;
			$query = $this->getCondition();
			//echo $query;
            if($limit == 'O'){
				$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
				$res = $this->db->query($query);
			}else{
				$res = $this->db->query($query);
			}
          
			$rows = $res->fetchAll(PDO::FETCH_ASSOC);
			$list = array();
			foreach ($rows as $row) {
				$list[] = $row;
			}
			return $list;
	}

	/**
	 * Get Groups Count
	 * @return int $count
	 */
	public function getTotalCount() {
		//global $db;
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];

	}
}

?>
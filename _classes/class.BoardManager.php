<?php

require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.Common.php');
require_once("../_lib/_docs_log.inc");
/**
 * class BoardManager
 * @author  yhlee , <yuhopd@gmail.com>
 */
class BoardManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	private $id = 0;
	private $reg_date = "";
	private $category = 0;
	private $file_name;
	private $file_rename;
	private $file_ext;
	private $size = 0;
    public  $src_path = "/home/himh/public_html/data/cafe";

	/**
	 * new BoardManager($properties)
	 * @return void
	 * @param array $properties 
	 */
	public function __construct($properties) {
        $this->db =& DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	public function getLastIndex(){
		$lastID = $this->db->getOne("SELECT MAX(id) FROM `board` WHERE `category` = '{$this->category}'");
		return $lastID;
	}

	public static function isItem($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT count(*) FROM `board` WHERE `id` = '$id'";
		$isCount = $db->getOne($query);
		if($isCount > 0){
			return "O";
		}else{
			return "X";
		}
	}

	public static function getItem($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board` WHERE `id` = '$id'";
		$res  =& $db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			# 선택한 데이타 가지고 오기
			$row['title']	= stripslashes($row['title']);
			$row['summary'] = stripslashes($row['summary']);
			$row['content'] = stripslashes($row['content']);
			$row['reg_date'] = substr($row['reg_date'],0,10);

			// $row['content'] = nl2br($row['content']);

		}
		$res->free();
		return $row;
	}


	public function getLastItem() {
		$query =  "SELECT * FROM `board` WHERE `category` = '{$this->category}' ORDER BY `adddate` DESC LIMIT 1";
		$res   =& $this->db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			# 선택한 데이타 가지고 오기
			$row['title']	= stripslashes($row['title']);
			$row['summary'] = stripslashes($row['summary']);
			$row['content'] = stripslashes($row['content']);
			$row['reg_date'] = substr($row['reg_date'],0,10);
		}
		$res->free();
		return $row;
	}

	public function getPrevItem($id) {
		$query =  "SELECT * FROM `board` WHERE `id` < '{$id}' AND `category` = '{$this->category}' ORDER BY `id` DESC LIMIT 1";
		$res   =& $this->db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){ }
		$res->free();
		return $row;
	}

	public function getNextItem($id) {
		$query =  "SELECT * FROM `board` WHERE `id` > '{$id}' AND `category` = '{$this->category}' ORDER BY `id` ASC LIMIT 1";  
		$res   =& $this->db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){ }
		$res->free();
		return $row;
	}

	public function updateHit($id) {
		$this->db->query ("UPDATE `board` SET `hit`=`hit`+1 WHERE `id`='$id'");
	}

	public static function getFiles($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board_files` WHERE `board_id`= '$id' ORDER BY `id` ASC";
		$res   =& $db->query($query);
		if ($res->numRows() == 0){
			return false;
		}else{
			while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
				$list[] = $row;
			}
		}
		return $list;
	}

	public static function getAllThumbFile($limit) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board_files` WHERE (`ext`='jpg' || `ext`='png' || `ext`='gif') ORDER BY id DESC LIMIT $limit";
		$res   =& $db->query($query);
		if ($res->numRows() == 0){
			return false;
		}else{
			while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
				$list[] = $row;
			}
		}
		return $list;
	}


	public static function getThumbFile($board_id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board_files` WHERE `board_id`='{$board_id}' AND (`ext`='jpg' || `ext`='png' || `ext`='gif') ORDER BY id ASC LIMIT 1";
		$res   =& $db->query($query);
		if ($res->numRows() == 0){
			return false;
		}else{
			$res->fetchInto($row,DB_FETCHMODE_ASSOC);
			return $row;
		}
		
	}


	public static function insert($arry) {
		$db =& DBConnection::get()->handle();
        $ip = Common::getRealIpAddr();
		$query  = "INSERT INTO `board` (`group_id`, `category`, `usr_id`, `title`, `name`, `summary`, `content`, `reg_date`, `adddate`, `lastUpdate`, `reg_ip`, `hit`) VALUES  ('{$arry[group_id]}', '{$arry[category]}' ,'{$arry[usr_id]}', '{$arry[title]}', '{$arry[name]}', '{$arry[summary]}', '{$arry[content]}', '{$arry[reg_date]}', UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), '$ip', 0 )";
		$result = $db->query($query);
		$lastID = $db->getOne("SELECT MAX(id) FROM `board` ");

		$db->query ("UPDATE `board_files` SET `board_id`='{$lastID}' WHERE `board_id`=0 ");
		return $lastID;
	}

	public static function updateFiles($pages_id) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `board_files` SET 
		                `board_id` = '{$pages_id}'
				  WHERE `board_id` = 0";
		$result = $db->query($query);
		return $result;
	}


	public static function modify($arry) {
		$db =& DBConnection::get()->handle();
        $ip = Common::getRealIpAddr();
		$query  = "UPDATE `board` SET 
		                `group_id`    = '{$arry[group_id]}',
		                `category`   = '{$arry[category]}',
						`title`      = '{$arry[title]}', 
						`summary`    = '{$arry[summary]}', 
						`content`    = '{$arry[content]}',
						`lastUpdate` = UNIX_TIMESTAMP(),
						`reg_ip`     = '$ip'
				  WHERE `id` = '{$arry[board_id]}'";
		$result = $db->query($query);
		return $result;
	}


    /* save file to storage  */
    public function save_file_to_storage($upload_tmp_name){
		$this->path = self::get_file_path_name($this->id);
		if(is_null($this->path) || $this->path == ''){
			$this->path = date("ym");
		}
        if(!is_dir($this->src_path."/".$this->path)){
            mkdir ($this->src_path."/".$this->path, 0777);
        }
		$target_storage = $this->src_path."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		$target_thumb_storage = $this->src_path."/".$this->path."/".$this->file_rename."_thumb.".$this->file_ext;
		$target_src_storage = $this->src_path."/".$this->path."/".$this->file_rename."_src.".$this->file_ext;
		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
		//specify what percentage you are resizing to
		$percent_resizing = 100;

        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}

		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_thumb_storage);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_thumb_storage);
		}else{
			 imagegif($dst_img, $target_thumb_storage);
		}
		imagedestroy($dst_img);




		$percent_resizing = 600;
        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}

		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_storage);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_storage);
		}else{
			 imagegif($dst_img, $target_storage);
		}
		imagedestroy($dst_img);




		$result_of_upload = move_uploaded_file($upload_tmp_name,$target_src_storage);

        return $result_of_upload;
    }

    
    public static function makeThumb($path,$rename,$ext){

		$upload_tmp_name = "../data/cafe/".$path."/".$rename.".".$ext;
		$target_thumb_storage = "../data/cafe/".$path."/".$rename."_tiny.".$ext;
       // if(!file_exists($target_thumb_storage)){
			@unlink($target_thumb_storage);
			list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
			$percent_resizing = 40;
			if($width < $height){
				$reWidth = $percent_resizing;
				$reHeight = round((($height/$width)*$percent_resizing));
			}else{
				$reHeight = $percent_resizing;
				$reWidth = round((($width/$height)*$percent_resizing));
			}	

			$dst_img = imagecreatetruecolor($reWidth,$reHeight);
			if($ext == "jpg"){
				 $src_img = imagecreatefromjpeg($upload_tmp_name);
			}else if($ext == "png"){
				 $src_img = imagecreatefrompng($upload_tmp_name);
			}else{
				 $src_img = imagecreatefromgif($upload_tmp_name);
			}

			imagecolorallocate($dst_img, 255, 255, 255);
			imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
			imageinterlace($dst_img);
			if($ext=="jpg"){
				 imagejpeg($dst_img, $target_thumb_storage);
			}else if($ext=="png"){
				 imagepng($dst_img, $target_thumb_storage);
			}else{
				 imagegif($dst_img, $target_thumb_storage);
			}
			imagedestroy($dst_img);
		//}
    }

    public static function get_file_path_name($id){
		$db =& DBConnection::get()->handle();
		$path = $db->getOne("SELECT `path` FROM `board_files` WHERE `id`='$id'");
        return $path;
    }

    public function  addFile(){
		$query  = "INSERT INTO `board_files` (`board_id`, `category`, `path`, `name`, `rename`, `ext`, `size`, `adddate`) VALUES ('{$this->id}', '{$this->category}', '{$this->path}', '{$this->file_name}', '{$this->file_rename}', '{$this->file_ext}', {$this->size}, UNIX_TIMESTAMP())"; 
		$result = $this->db->query($query);
		return $result;
	}

    public function deleteFile($id){
		$res =& $this->db->query("SELECT * FROM `board_files` WHERE `id` = '{$id}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$this->db->query("DELETE FROM `board_files` WHERE `id` = '{$row[id]}' LIMIT 1");
			@unlink($this->src_path."/".$row['path']."/".$row['rename'].".".$row['ext']);
            if($row['ext'] == "jpg" || $row['ext'] == "png" || $row['ext'] == "gif"){
				@unlink($this->src_path."/".$row['path']."/".$row['rename']."_thumb.".$row['ext']);
			}
		}
	}


	public function del($id) {
		$res =& $this->db->query("SELECT * FROM `board` WHERE `id` = '{$id}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$res1 =& $this->db->query("SELECT * FROM `board_files` WHERE `board_id` = '{$id}'");
			while ($res1->fetchInto($row1,DB_FETCHMODE_ASSOC)){				
				$this->deleteFile($row1['id']);
			}
			$this->db->query("DELETE FROM `board` WHERE `id` = '{$row[id]}' LIMIT 1");
		}
		return $row;
	}

    /* category */
    
	public static function getCategoryItem($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board_category` WHERE `id` = '$id'";
		$res  =& $db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			# 선택한 데이타 가지고 오기
			$row['title']	= stripslashes($row['title']);
			$row['summary'] = stripslashes($row['summary']);
			$row['head']    = stripslashes($row['head']);
		}
		$res->free();
		return $row;
	}

	public static function getCategoryFiles($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `board_files` WHERE `category`= '$id' ORDER BY `id` ASC";
		$res   =& $db->query($query);
		if ($res->numRows() == 0){
			return false;
		}else{
			while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
				$list[] = $row;
			}
		}
		return $list;
	}


	public static function insertCategory($arry) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO `board_category` (`group_id`, `category`, `title`, `summary`, `head`, `type`) VALUES  ('{$arry[group_id]}','{$arry[category]}', '{$arry[title]}', '{$arry[summary]}', '{$arry[head]}', '{$arry[type]}')";
		$result = $db->query($query);
		$lastID = $db->getOne("SELECT MAX(id) FROM `board_category` ");

		//$db->query ("UPDATE `board_files` SET `category`='{$lastID}' , `board_id`='0' WHERE `board_id`=0 ");
		return $lastID;
	}


	public static function modifyCategory($arry) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `board_category` SET `category` = '{$arry[category]}' , `title` = '{$arry[title]}', `summary` = '{$arry[summary]}', `head` = '{$arry[head]}', `type` = '{$arry[type]}' WHERE `id` = '{$arry[id]}' ";
		$result = $db->query($query);
		return $result;
	}

	public function deleteCategory($id) {
		$res =& $this->db->query("SELECT * FROM `board_category` WHERE `id` = '{$id}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$res1 =& $this->db->query("SELECT * FROM `board_files` WHERE `category` = '{$id}'");
			while ($res1->fetchInto($row1,DB_FETCHMODE_ASSOC)){				
				$this->deleteFile($row1['id']);
			}
			$this->db->query("DELETE FROM `board_category` WHERE `id` = '{$row[id]}' LIMIT 1");
		}
		return $res;
	}

}
?>
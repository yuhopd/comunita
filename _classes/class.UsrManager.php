<?php
require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.Upload.php');
//require_once("../_lib/_docs_log.inc");
/**
 * class UsrManager
 * @author  yhlee , <yuho@naver.com>
 */
class UsrManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	private $id=0;


	public static $usrPosition = array(
		0  => "",
		1  => "성도",

	    10 => "서리집사(남)",
		11 => "서리집사(여)",
		12 => "타집사",
		13 => "명예집사",
		14 => "안수집사",
		15 => "협동집사",
		16 => "공로집사",
		17 => "은퇴집사",
		18 => "협동은퇴집사",
		19 => "전입안수집사",


		20 => "권사",
		21 => "은퇴권사",
		22 => "명예권사",
		23 => "전입권사",
		24 => "시무권사",
		25 => "협동권사",
		26 => "전입명예권사",
		27 => "전입은퇴권사",

		30 => "장로",
		32 => "전입장로",
		33 => "협동장로",
		35 => "시무장로",
		37 => "은퇴장로",
        38 => "전입은퇴장로",
		39 => "원로장로",

		40 => "사모",
		50 => "교역자",

		100 => "담임목사",
		101 => "부목사",
		102 => "목사",
		103 => "영어예배목사",
		104 => "협동목사",
		105 => "교육목사",
		106 => "찬양목사",

		115 => "전임전도사",
		116 => "준전임전도사",
		117 => "찬양전도사",
		118 => "교육전도사",
		119 => "전도사",

		120 => "선교사",
		121 => "직원"
	);

	public static $usrChristen = array(
		0 => "없음",
		1 => "등록",
		2 => "학습",
		3 => "유아세례",
		4 => "세례"
	);

	public static $usrStatus = array(
		0 => "없음",
		1 => "본교회",
		2 => "타교회",
		//3 => "교인",
		4 => "비교인",
		//5 => "타종교",
		6 => "전출",
		//7 => "미확인",
		8 => "새가족",
		//9 => "불신",
	    10 => "별세",
		11 => "미등록자"
	);

	private $file_name;
	private $file_rename;
	private $file_ext;
	private $size = 0;
	private $path = "";
    public  $src_path = "/home/jgdchurch/public_html/dev/data/usr_img";


    public function __construct($properties) {
        $this->db =& DBConnection::get()->handle();
        if(isSet($properties)) {
            foreach ($properties as $key => $value) {
                $this->{$key} = $value;
            }
        }
	}


	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$res  = $db->query($sql);
		$row  = $res->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}

	/**
	 * static getUsr($usr_id);
	 * @return array $array
	 * @param int $usr_id
	 */
	public static function isAccount($phone) {
		$db = DBConnection::get()->handle();
		$query  = "SELECT * FROM `usr` WHERE `phone`='{$phone}' LIMIT 1";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		if($row){
			$usr = $row;
		}else{
			$usr = false;
		}
		return $usr;

	}



	/**
	 * static getUsr($usr_id);
	 * @return array $array
	 * @param int $usr_id
	 */
	public static function getUsr($usr_id) {
		$db = DBConnection::get()->handle();
		$query  = "SELECT u.*, (SELECT s.title FROM `sameage` AS s WHERE s.id = u.sameage_id) AS sameage FROM `usr` AS u WHERE u.id='$usr_id'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}


	/**
	 * static getUsr($usr_id);
	 * @return array $array
	 * @param int $usr_id
	 */
	public static function getUsrThumb($usr_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `usr_img` WHERE `usr_id`='{$usr_id}'";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);
		if($row){
			$thumb = $row;
		}else{
			$thumb = false;
		}
		return $thumb;
	}
	/**
	 * static insertUsr($arry);
	 * @return $lastID
	 * @param arry $arry = array{'email', 'psw', 'name'};
	 */
	public static function insertUsr($arry) {
		$db     = DBConnection::get()->handle();
		$time = time();
		$query  = "INSERT INTO `usr` (`email`, `psw`, `name`, `level`, `auth`, `key`, `reg_date`) VALUES  ('{$arry[email]}','$psw','{$arry[name]}',1,'X','$md5_email','$time')";
		$result = $db->query($query);
		$lastID = $db->getOne("SELECT MAX(id) FROM `usr`");
		return $lastID;
	}

	public static function insert($arry) {
		$db       = DBConnection::get()->handle();
		$sequence = self::getMaxSequence($arry['position']) + 1;
		$time = time();
		$query    = "INSERT INTO `usr` (`domain_id`, `sameage_id`, `email`, `name`,`tel`, `phone`, `address_1`, `address_2`, `summary`, `car_number`,`car_type`, `birth`, `lunar`, `householder`, `status`, `christen`, `position`, `level`, `gender`, `register`, `sequence`, `reg_date`)
		VALUES  ('{$arry[domain_id]}', '{$arry[sameage_id]}', '{$arry[email]}', '{$arry[name]}', '{$arry[tel]}', '{$arry[phone]}', '{$arry[address_1]}', '{$arry[address_2]}', '{$arry[summary]}', '{$arry[car_number]}', '{$arry[car_type]}', '{$arry[birth]}', '{$arry[lunar]}', '{$arry[householder]}', '{$arry[status]}', '{$arry[christen]}', '{$arry[position]}', 1, '{$arry[gender]}', '{$arry[register]}', '{$sequence}', '$time')";
		$result = $db->query($query);
		$lastID = $db->lastInsertId();
		return $lastID;
	}

	public static function modify($arry) {
		$db  = DBConnection::get()->handle();
		$usr = self::getUsr($arry['usr_id']);
		if($usr['position'] != $arry['position']){
			$query = "UPDATE `usr` SET `sequence`=`sequence`-1 WHERE `position` = '{$usr[position]}' AND `sequence` > '{$usr[sequence]}'";
			$result = $db->query($query);
			$sequence = self::getMaxSequence($arry['position']) + 1;
		}else{
            $sequence = $usr['sequence'];
		}
		$time = time();

		$query = "UPDATE `usr` SET
					`domain_id` = '{$arry[domain_id]}',
					`sameage_id`= '{$arry[sameage_id]}',
					`email`     = '{$arry[email]}',
					`name`      = '{$arry[name]}',
					`tel`       = '{$arry[tel]}',
					`phone`     = '{$arry[phone]}',
					`address_1` = '{$arry[address_1]}',
					`address_2` = '{$arry[address_2]}',
					`summary`   = '{$arry[summary]}',
					`car_number`= '{$arry[car_number]}',
					`car_type`  = '{$arry[car_type]}',
					`birth`     = '{$arry[birth]}',
					`lunar`     = '{$arry[lunar]}',
					`householder` = '{$arry[householder]}',
					`status`    = '{$arry[status]}',
					`christen`  = '{$arry[christen]}',
					`position`  = '{$arry[position]}',
					`sequence`  = '{$sequence}',
					`gender`    = '{$arry[gender]}',
					`register`  = '{$arry[register]}',					
					`reg_date`  = '{$time}'
				WHERE id='{$arry[usr_id]}' ";
		$result = $db->query($query);
		return $arry['usr_id'];
	}

	public static function modifyPositionUsr($id,$sequence) {
		$db     = DBConnection::get()->handle();
		$psw    = md5($arry['psw']);
		$query  = "UPDATE `usr` SET `sequence` = '$sequence' WHERE `id` = '{$id}'";
		$result = $db->query($query);
		return $result;
	}

	public static function updateUsr($arry) {
		$db     = DBConnection::get()->handle();
		$psw    = md5($arry['psw']);
		$query  = "UPDATE `usr` SET `psw` = '$psw' , `name` = '{$arry[name]}' , `auth` = 'O' WHERE `email` = '{$arry[email]}' AND `key` = '{$arry[key]}'";
		$result = $db->query($query);
		return $result;
	}

	public static function addGroupUsr($group_id, $usr_id) {
		$db     = DBConnection::get()->handle();
		$query  = "INSERT INTO `usr_group` (`usr_id`,`group_id`,`level`) VALUES ($usr_id, $group_id, 0)";
		$result = $db->query($query);
	}

	public static function updateUsrPoint($usr_id, $point) {
		$db     = DBConnection::get()->handle();
		$psw    = md5($arry['psw']);
		$query  = "UPDATE `usr` SET `point` = '$point' WHERE `id` = '{$usr_id}'";
		$result = $db->query($query);
		return $result;
	}

	public static function modifyUsr($arry) {
		$db     =  DBConnection::get()->handle();
		$psw    = md5($arry['psw']);
		$query  = "UPDATE `usr` SET `psw`='$psw', `name`='{$arry[name]}', `auth`='O', `phone`='{$arry[phone]}', `email`='{$arry[email]}' WHERE id='{$arry[usr_id]}'";
		$result = $db->query($query);
		return $arry['usr_id'];
	}

	public static function modifyUsrSummary($arry) {
		$db     = DBConnection::get()->handle();
		$query  = "UPDATE `usr` SET `summary`='{$arry[summary]}' WHERE `id`='{$arry[usr_id]}'";
		$result = $db->query($query);
		return $arry['usr_id'];
	}

	public static function authUsr($arry) {
		$db     = DBConnection::get()->handle();
		$query  = "UPDATE `usr` SET `auth` = 'O' WHERE `email` = '{$arry[email]}' AND `key` = '{$arry[key]}'";
		$result = $db->query($query);
		return $result;
	}

	public static function getSameage($sameage_id){
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `sameage` WHERE `id`='{$sameage_id}'";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function getSameageList(){
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `sameage` ORDER BY `gisu` ASC";
		$res   = $db->query($query);
        $rows   = $res->fetchAll(PDO::FETCH_ASSOC);

		return $rows;
	}

	public static function getGroupList($usr_id){
		$db    = DBConnection::get()->handle();
		$query = "SELECT g.`id`, g.`title` FROM `group` AS g LEFT JOIN `usr_group` AS ug ON g.id = ug.group_id WHERE ug.usr_id='{$usr_id}' AND ug.level < 2";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function requestFriend($usr_id,$family_id) {
		$db    = DBConnection::get()->handle();
		$time = time();
		$isRelations = self::getOne("SELECT COUNT(id) FROM `usr_family` WHERE `usr_id` = '$usr_id' AND `family_id` = '$family_id' ");
	    if($isRelations > 0){
			$query = "UPDATE `usr_family` SET auth='O' WHERE `usr_id` = '$usr_id' AND `family_id` = '$family_id' ";
			$res   = $db->query($query);
		}else{
			$query = "INSERT INTO `usr_family` (`usr_id`, `friend_id`, `auth`, `reg_date`) VALUES ('$usr_id', '$friend_id', 'O', '$time' )";
			$res   = $db->query($query);
		}
		if($res){
			return "ok";
		}else{
			return "error";
		}
	}

	public static function addFamily($usr_id,$family_id,$relations) {
		$db     = DBConnection::get()->handle();
		$time   = time();
        $query1 = "INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$family_id}', '{$relations}', '$time' )";
		$res1   = $db->query($query1);
		$query2 = "INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$family_id}', '{$usr_id}', '{$relations}', '$time' )";
		$res2   = $db->query($query2);
	}
/*
	public static function insertFamily($usr_id,$family_id,$relations) {
		$db  =& DBConnection::get()->handle();

		$res = $db->query("SELECT * FROM `usr_family` WHERE `usr_id` = '{$family_id}'");
		while($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
			if($relations == 1){
				if($row['relations'] == 1){

				}else if($row['relations'] == 2){

				}else if($row['relations'] == 3){

				}else if($row['relations'] == 4){


				}
			}else if($relations == 3){

			}else if($relations == 3){

			}else if($relations == 4){
					$res =& $db->query("INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$row[family_id]}', '{$relations}', '$time' )");
			}

			if($row['relations'] == 1){ //부부 -- 자녀
				if($relations == 2){ // 부모자녀
					$res =& $db->query("INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$row[family_id]}', '{$relations}', '$time' )");
				}else if($relations == 3){ //형제/자매

				}else if($relations == 4){ //조부모/손녀손자
					$res =& $db->query("INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$row[family_id]}', '{$relations}', '$time' )");
				}
			}else if($row['relations'] == 2){
				if($relations == 1){
					$res =& $db->query("INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$row[family_id]}', '{$relations}', '$time' )");
				}else if($relations == 3){

				}else if($relations == 4){
					$res =& $db->query("INSERT INTO `usr_family` (`usr_id`, `family_id`, `relations`, `adddate`) VALUES ('{$usr_id}', '{$row[family_id]}', '{$relations}', '$time' )");
				}
			}else if($row['relations'] == 3){

			}else if($row['relations'] == 4){

			}else if($row['relations'] == 9){
			}
		}
		$res->free();
        self:addFamily($usr_id,$family_id,$relations);
	}
*/

	public static function modifyFamily($usr_id,$family_id,$relations) {
		$db =& DBConnection::get()->handle();

		$query1 = "UPDATE `usr_family` SET `relations`='$relations' WHERE `usr_id` = '{$usr_id}' AND `family_id` = '{$family_id}'";
		$res1   = $db->query($query1);
		$query2 = "UPDATE `usr_family` SET `relations`='$relations' WHERE `usr_id` = '{$family_id}' AND `family_id` = '{$usr_id}'";
		$res2   = $db->query($query2);
	}

	public static function delFamily($usr_id,$family_id) {
		$db = DBConnection::get()->handle();
		$db->query("DELETE FROM `usr_family` WHERE `usr_id` = '{$usr_id}' AND `family_id` = '{$family_id}'");
		$db->query("DELETE FROM `usr_family` WHERE `usr_id` = '{$family_id}' AND `family_id` = '{$usr_id}'");
	}

	/**
	 * static getUsr($usr_id);
	 * @return array $array
	 * @param int $usr_id
	 */
	public static function getRelations($usr_id,$family_id) {
		$db =& DBConnection::get()->handle();
		$sql  = "SELECT * FROM usr_family WHERE usr_id = '{$usr_id}' AND family_id = '{$family_id}'";
		$res  = $db->query($sql);
		$row  = $res->fetch(PDO::FETCH_ASSOC);
		return $row['relations'];
	}

	public static function getFamilyRelations($usr_id,$family_id,$relations) {
		$db =& DBConnection::get()->handle();
		$query1  = "SELECT * FROM `usr` WHERE id='{$usr_id}'";
		$res1 = $db->query($query1);
		$row1 = $res1->fetch(PDO::FETCH_ASSOC);

		$query2  = "SELECT * FROM `usr` WHERE id='{$family_id}'";
		$res2 = $db->query($query2);
		$family = $res2->fetch(PDO::FETCH_ASSOC);

		$uBirth = explode("-",$usr['birth']);
		$usrBirth = mktime(0,0,0,$uBirth[1],$uBirth[2],$uBirth[0]);

		$fBirth = explode("-",$family['birth']);
		$familyBirth = mktime(0,0,0,$fBirth[1],$fBirth[2],$fBirth[0]);

/*
		$query3 = "SELECT gisu FROM `sameage` WHERE id = sameage_id";
		$res3   = $db->query($query3);
		$family3 = $res3->fetch(PDO::FETCH_ASSOC);
*/

		$rerult = "기타";
		if($relations == 1){
			$rerult = "부부";
		}else if($relations == 2){
			if($family['gender']=="M"){
                if($usrBirth > $familyBirth){ $rerult = "부"; }else{ $rerult = "자"; }
			}else{
                if($usrBirth > $familyBirth){ $rerult = "모"; }else{ $rerult = "녀"; }
			}
		}else if($relations == 3){
			if($family['gender']=="M"){
                if($usrBirth > $familyBirth){
					if($usr['gender']=="M"){ $rerult="형"; }else{ $rerult="오빠"; }
				}else if($usrBirth == $familyBirth){
					$rerult = "쌍둥이";
				}else{
					$rerult = "동생";
				}
			}else{
                if($usrBirth > $familyBirth){
					if($usr['gender']=="M"){ $rerult="누나"; }else{ $rerult="언니"; }
				}else if($usrBirth == $familyBirth){
					$rerult = "쌍둥이";
				}else{
					$rerult = "동생";
				}
			}
		}else if($relations == 4){
			if($family['gender']=="M"){
                if($usrBirth > $familyBirth){ $rerult = "조부"; }else{ $rerult = "손자"; }
			}else{
                if($usrBirth > $familyBirth){ $rerult = "조모"; }else{ $rerult = "손녀"; }
			}

		}else if($relations == 5){
			if($family['gender']=="M"){
                if($usrBirth > $familyBirth){ 
					if($usr['gender']=="M"){ $rerult="장인"; }else{ $rerult="장모"; }
				}else{
					$rerult="사위";
				}
			}else{
                if($usrBirth > $familyBirth){
					if($usr['gender']=="M"){ $rerult="시아버지"; }else{ $rerult="시어머니"; }
				}else{
					$rerult="며느리";
				}
			}
		}else if($relations == 9){
			$rerult = "친척";
		}

		return $rerult;
	}

    public static function get_file_path_name($usr_id){
		$db = DBConnection::get()->handle();
		$path = self::getOne("SELECT `path` FROM `usr_img` WHERE `usr_id`='$usr_id'");
        return $path;
    }


    public static function usrImageUpdateFromBase64($base64,$usr_id){
		$thumb = self::getUsrThumb($usr_id);
		$target_thumb_storage = "../../data/usr_img/".$thumb['path']."/".$thumb['rename']."_small.".$thumb['ext'];
		
		$ifp = fopen( $target_thumb_storage , "wb" );
		$data = explode(',',$base64);
		fwrite($ifp, base64_decode($data[1]));
		fclose( $ifp );
	}

    /* save file to storage  */
    public static function saveFileToStorage($arry,$_file){
		//$path = $arry[];

		$src_path   = "../../data/usr_img";
		$pathinfo = pathinfo($_file['name']);

		if(is_null($path) || $path == ''){
			$path = date("ym");
		}
		if(!is_dir($src_path."/".$path)){
			mkdir ($src_path."/".$path, 0777);
		}
		
		
		// max file size in bytes
		$file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);
		
		//specify what percentage you are resizing to
		$percent_resizing = 200;
		$file_ext = "jpg";
		
		$target_storage = $src_path."/".$path."/".$file_rename.".".$file_ext;
		$target_thumb_storage = $src_path."/".$path."/".$file_rename."_small.".$file_ext;
		$target_src_storage = $src_path."/".$path."/".$file_rename."_src.".$file_ext;
		
		$upload_tmp_name = $target_storage;
		
		
		if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_tmp_name)) {
		
		
			$exif = exif_read_data($upload_tmp_name, 0, true);
			if($exif['IFD0']['Orientation'] == 6 || $exif['IFD0']['Orientation'] == 3 || $exif['IFD0']['Orientation'] == 8){
				if($file_ext=="jpg"){
					 $source = imagecreatefromjpeg($upload_tmp_name);
				}else if($file_ext=="png"){
					 $source = imagecreatefrompng($upload_tmp_name);
				}else{
					 $source = imagecreatefromgif($upload_tmp_name);
				}
		
				if($exif['IFD0']['Orientation'] == 6){
					$rotate = imagerotate($source, 270, 0);
				}else if($exif['IFD0']['Orientation'] == 3){
					$rotate = imagerotate($source, 180, 0);
				}else if($exif['IFD0']['Orientation'] == 8){
					$rotate = imagerotate($source, 90, 0);
				}
				if($file_ext=="jpg"){
					 imagejpeg($rotate, $upload_tmp_name, 100);
				}else if($file_ext=="png"){
					 imagepng($rotate, $upload_tmp_name);
				}else{
					 imagegif($rotate, $upload_tmp_name);
				}
				imagedestroy($rotate);
			}
		
		
		
			list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
			if($width < $height){
				$reWidth = $percent_resizing;
				$reHeight = round((($height/$width)*$percent_resizing));
			}else{
				$reHeight = $percent_resizing;
				$reWidth = round((($width/$height)*$percent_resizing));
			}
			$dst_img = imagecreatetruecolor($reWidth,$reHeight);
			if($file_ext == "jpg"){
				 $src_img = imagecreatefromjpeg($upload_tmp_name);
			}else if($file_ext == "png"){
				 $src_img = imagecreatefrompng($upload_tmp_name);
			}else{
				 $src_img = imagecreatefromgif($upload_tmp_name);
			}
		
			imagecolorallocate($dst_img, 255, 255, 255);
			imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
			imageinterlace($dst_img);
			if($file_ext=="jpg"){
				 imagejpeg($dst_img, $target_thumb_storage, 100);
			}else if($file_ext=="png"){
				 imagepng($dst_img, $target_thumb_storage);
			}else{
				 imagegif($dst_img, $target_thumb_storage);
			}
			imagedestroy($dst_img);
			$arryUpload = array(
				"usr_id" => $_POST['usr_id'],
				"rename" => $file_rename,
				"path" => $path,
				"ext" => $file_ext
			);
			UsrManager::updateUsrImgUpload($arryUpload);
			
			$r['url'] = $target_thumb_storage;
			$r["error"] = -1;
			$r["msg"] = "File is valid, and was successfully uploaded.";
		} else {
			$r["error"] = 500;
			$r["msg"] = "Possible file upload attack!";
		}
		
		
        return $result;
    }

    /* save file to storage  */
    public function save_file_to_storage(){
		$this->path = self::get_file_path_name($this->id);
		if(is_null($this->path) || $this->path == ''){
			$this->path = date("ym");
		}
        if(!is_dir($this->src_path."/".$this->path)){
            mkdir ($this->src_path."/".$this->path, 0777);
        }

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array("jpg","png","gif");
		// max file size in bytes
		$this->file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

		$sizeLimit = 10 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit, $this->file_rename, $this->path);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload($this->src_path);

		//specify what percentage you are resizing to
		$percent_resizing = 80;
		$this->file_ext = $result['ext'];

		$target_storage = $this->src_path."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		$target_thumb_storage = $this->src_path."/".$this->path."/".$this->file_rename."_small.".$this->file_ext;
		$target_src_storage = $this->src_path."/".$this->path."/".$this->file_rename."_src.".$this->file_ext;

		$upload_tmp_name = $target_storage;

		$exif = exif_read_data($upload_tmp_name, 0, true);
		if($exif['IFD0']['Orientation'] == 6 || $exif['IFD0']['Orientation'] == 3 || $exif['IFD0']['Orientation'] == 8){
			if($this->file_ext=="jpg"){
				 $source = imagecreatefromjpeg($upload_tmp_name);
			}else if($this->file_ext=="png"){
				 $source = imagecreatefrompng($upload_tmp_name);
			}else{
				 $source = imagecreatefromgif($upload_tmp_name);
			}

            if($exif['IFD0']['Orientation'] == 6){
				$rotate = imagerotate($source, 270, 0);
			}else if($exif['IFD0']['Orientation'] == 3){
				$rotate = imagerotate($source, 180, 0);
			}else if($exif['IFD0']['Orientation'] == 8){
				$rotate = imagerotate($source, 90, 0);
			}
			if($this->file_ext=="jpg"){
				 imagejpeg($rotate, $upload_tmp_name, 100);
			}else if($this->file_ext=="png"){
				 imagepng($rotate, $upload_tmp_name);
			}else{
				 imagegif($rotate, $upload_tmp_name);
			}
			imagedestroy($rotate);
		}

		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}
		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_thumb_storage, 100);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_thumb_storage);
		}else{
			 imagegif($dst_img, $target_thumb_storage);
		}
		imagedestroy($dst_img);
        return $result;
    }

    public function updateUsrImg(){
		$query = "SELECT `path`, `rename`, `ext` FROM `usr_img` WHERE `usr_id` = '{$this->id}'";
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		if($row){
			@unlink($img_path."/".$row['path']."/".$row['rename'].'.'.$row['ext']);
			@unlink($img_path."/".$row['path']."/".$row['rename'].'_small.'.$row['ext']);
			//@unlink($img_path."/".$row['path']."/".$row['rename'].'_src.'.$row['ext']);
			$this->db->query("UPDATE `usr_img` SET `path`='{$this->path}', `rename`='{$this->file_rename}', `ext`='{$this->file_ext}'  WHERE `usr_id`='{$this->id}'");
    		//docs_log("User ===>".$this->id."===".$this->file_rename."===".$this->file_ext);
		}else{
			$this->db->query("INSERT INTO `usr_img` (`usr_id`, `path`, `rename`, `ext`) VALUES ('{$this->id}', '{$this->path}', '{$this->file_rename}', '{$this->file_ext}')");
		}
		return $res;
	}



    public static function updateUsrImgUpload($fileInfo){
		$db    = DBConnection::get()->handle();
		$query = "SELECT `path`, `rename`, `ext` FROM `usr_img` WHERE `usr_id` = '{$fileInfo[usr_id]}'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		if($row){
			@unlink($img_path."/".$row['path']."/".$row['rename'].'.'.$row['ext']);
			@unlink($img_path."/".$row['path']."/".$row['rename'].'_small.'.$row['ext']);
			//@unlink($img_path."/".$row['path']."/".$row['rename'].'_src.'.$row['ext']);
			$db->query("UPDATE `usr_img` SET `path`='{$fileInfo[path]}', `rename`='{$fileInfo[rename]}', `ext`='{$fileInfo[ext]}'  WHERE `usr_id`='{$fileInfo[usr_id]}'");
    		//docs_log("User ===>".$this->id."===".$this->file_rename."===".$this->file_ext);
		}else{
			$db->query("INSERT INTO `usr_img` (`usr_id`, `path`, `rename`, `ext`) VALUES ('{$fileInfo[usr_id]}', '{$fileInfo[path]}', '{$fileInfo[rename]}', '{$fileInfo[ext]}')");
		}
		return $res;
	}

	public static function getUseMessageUsrList() {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `usr` WHERE `level` > 1 ORDER BY `sameage_id` DESC, BINARY `name` ASC";
		$res   = $db->query($query);

		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function upSequence($id) {
		$db     = DBConnection::get()->handle();
		$query0 = "SELECT * FROM `usr` WHERE id='{$id}'";
		$res0 = $db->query($query0);
		$row0 = $res0->fetch(PDO::FETCH_ASSOC);

		$query1 = "SELECT * FROM `usr` WHERE `sequence` < {$row0[sequence]} AND `position`='{$row0[position]}' ORDER BY `sequence` DESC LIMIT 1";
		$res1 = $db->query($query1);
		$row1 = $res1->fetch(PDO::FETCH_ASSOC);

		$query2  = "UPDATE `usr` SET `sequence`='{$row1[sequence]}' WHERE id='{$id}'";
		$res2 =& $db->query($query2);
		$query3  = "UPDATE `usr` SET `sequence`='{$row0[sequence]}' WHERE id='{$row1[id]}'";
		$res3 =& $db->query($query3);
	}

	public static function downSequence($id) {
		$db = DBConnection::get()->handle();

		$query0  = "SELECT * FROM `usr` WHERE id='{$id}'";
		$res0 = $db->query($query0);
		$row0 = $res0->fetch(PDO::FETCH_ASSOC);
		
		$query1  = "SELECT * FROM `usr` WHERE `sequence` > {$row0[sequence]} AND `position`='{$row0[position]}' ORDER BY `sequence` ASC LIMIT 1";
		$res1 = $db->query($query1);
		$row1 = $res1->fetch(PDO::FETCH_ASSOC);

		$query2  = "UPDATE `usr` SET `sequence`='{$row1[sequence]}' WHERE id='{$id}'";
		$res2 = $db->query($query2);
		$query3  = "UPDATE `usr` SET `sequence`='{$row0[sequence]}' WHERE id='{$row1[id]}'";
		$res3 = $db->query($query3);
	}

	public static function getMaxSequence($position) {
		$db = DBConnection::get()->handle();
		$sequence = self::getOne("SELECT MAX(sequence) FROM `usr` WHERE `position` = '{$position}'");
		return $sequence;
	}

	public static function getUsrGroup($id) {
		$db = DBConnection::get()->handle();
		$query = "SELECT * FROM `usr_group` WHERE `id`='{$id}'";
		$res = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function modifyUsrGroup($arry) {
		$db =& DBConnection::get()->handle();
		$res = $db->query("UPDATE `usr_group` SET `level` = '{$arry[level]}', `duty` = '{$arry[duty]}' WHERE `id`='{$arry[id]}'");
		return $res;
	}


	public static function updateGroups($usr_id) {
		$db =& DBConnection::get()->handle();
		$groups = self::getGroupList($usr_id);
		if($groups != false){
			for($f=0;$f<count($groups);$f++){
				$arrayGroup[]	= $groups[$f]['id']."@".$groups[$f]['title'];
			}
		}
		$strGroup = (is_array($arrayGroup)) ? implode("|", $arrayGroup) : "";
		$query = "UPDATE `usr` SET `groups` = '{$strGroup}' WHERE `id`='{$usr_id}' ";
		$result = $db->query($query);
		return $arry['usr_id'];
	}

	public static function getGroups($strGgroups) {
		$arryG = null;
        if(trim($strGgroups) == ""){

			$arryG = false;
		}else{

			$groups = explode("|",$strGgroups);
			$f = 0;
			for($i=0;$i<count($groups);$i++){
				if(trim($groups[$i]) != ""){
					$group = explode("@",$groups[$i]);
					$arryG[$f]["id"] = trim($group[0]);
					$arryG[$f]["title"] = trim($group[1]);
					$f++;
				}
			}
		}
		return $arryG;
	}


    public static function getAttendanceCount($usr_id){
        $year = "2018";
	    $count = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `usr_id`='{$usr_id}' AND `worship` IN('O','L') AND `date` LIKE '$year%'");
	    return $count;
    }

    public static function getAttendanceGbsCount($usr_id){
        $year = "2018";
	    $count = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `usr_id`='{$usr_id}' AND `gbs` IN('O','L') AND `date` LIKE '$year%'");
	    return $count;
    }



	public static function modifyUsrPhoneAuth($arry) {
		$db = DBConnection::get()->handle();
		$query  = "UPDATE `usr` SET
			`key`   = '{$arry[key]}'
			 WHERE  `phone`='{$arry[phone]}'";
		$result = $db->query($query);
		return $result;
	}


	public static function modifyUsrAuth($arry) {
		$db = DBConnection::get()->handle();
		$query  = "UPDATE `usr` SET
			`auth` = '{$arry[auth]}'
			 WHERE  `id`='{$arry[id]}'";
		$result = $db->query($query);
		return $arry['id'];
	}

	public static function sendPhoneAuth($phone,$key) {
        $to_phone = $phone;
		$from_phone = "02-444-9561";
		$content = "OTP 인증번호는 ".$key." 입니다";
		$url = "http://www.widefoot.net/api/index.php";
		$fields = array(
			'mode'       => "send",
			'domain_id'  => 4,
			'to_phone'   => urlencode($to_phone),
			'from_phone' => urlencode($from_phone),
			'content'    => urlencode($content)
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
	}


	public static function sendUsrInfo($usr_id) {
		$usr = self::getUsr($usr_id);

        $to_phone = $usr['phone'];
		$from_phone = "02-444-9561";
		$content = "비밀번호는 ".$usr['psw']." 입니다";
		$url = "http://www.widefoot.net/api/index.php";
		$fields = array(
			'mode'       => "send",
			'domain_id'  => 4,
			'to_phone'   => urlencode($to_phone),
			'from_phone' => urlencode($from_phone),
			'content'    => urlencode($content)
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
	}



}
?>
<?php
require_once("../_classes/class.DBConnection.php");

class ContactList{
    /**
	 * @var object $db database connection
	 */
    private static $db;
    /**
	 * @var int $type
	 */
	private $type = "";
    /**
	 * @var int $browse
	 */
	private $browse = 0;


	private $usr_id = 0 ;
	private $sameage_id = 0;
	private $root_id = 0;
	private $group_id = 0;
	private $group_level = 0;
	private $position = 0;
	private $column = "";
    private $isAllSelect = "X";
	private $page = 1;
	private $perPage = 20;
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
		if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 2:  $order_by = " ORDER BY `sameage_id` DESC ,`name` ASC"; break;
				case 1:  $order_by = " ORDER BY `sameage_id` ASC , `name` ASC"; break;
				default: $order_by = " ORDER BY `name` ASC";
			}
		}

        switch ($this->type) {

            case 'usr_group_list':
				$where[] = "ug.usr_id = '{$this->usr_id}'";
				//$where[] = "ug.level < 2";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT ug.id, ug.group_id, ug.level, ug.duty, g.title, g.parent_id ";

				if($isCount == "count"){
					$from_query = "FROM `group` AS g LEFT JOIN `usr_group` AS ug ON g.id = ug.group_id ".$where_sql;
				}else{
					$from_query = "FROM `group` AS g LEFT JOIN `usr_group` AS ug ON g.id = ug.group_id ".$where_sql." ORDER BY ug.id DESC";
				}
			break;


            case 'advancedSearch':
				if($this->column == "birth"){
					$arrayKey = explode("," ,$this->keyword);
					for($f=0; $f<count($arrayKey); $f++){
						$where[] = "u.`birth` LIKE '%{$arrayKey[$f]}%'";
					}
				}else{
					$where[] = "u.{$this->column} LIKE '%{$this->keyword}%'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" OR ", $where) : "";

				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT u.id, u.name, u.groups, u.tel, u.phone, u.email, u.birth, u.lunar, u.position, u.gender ";
				}

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;


            case 'search':
				$where[] = "u.name LIKE '%{$this->keyword}%' OR u.phone LIKE '%{$this->keyword}%' OR u.email LIKE '%{$this->keyword}%' OR u.car_number LIKE '%{$this->keyword}%'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT u.id, u.name, u.groups, u.tel, u.phone, u.email, u.birth, u.lunar, u.position, u.gender ";
				}

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;

            case 'family_list':
				$where[] = "usr_family.usr_id = '{$this->usr_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT usr.* ";
				}
				if($isCount == "count"){
					$from_query = "FROM usr LEFT JOIN usr_family ON usr.id = usr_family.family_id ".$where_sql;
				}else{
					$from_query = "FROM usr LEFT JOIN usr_family ON usr.id = usr_family.family_id ".$where_sql.$order_by;
				}
			break;

			case 'family_search':

				$where[] = "u.name LIKE '%{$this->keyword}%' OR u.phone LIKE '%{$this->keyword}%' OR u.email LIKE '%{$this->keyword}%'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT u.id, u.name, u.groups, u.tel, u.phone, u.email, u.birth, u.lunar, u.position, u.gender ";
				}

				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;

            case 'group':
				//$where[] = "ug.level < 2";
				$where[] = "ug.group_id = '{$this->group_id}'";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

	         	$order_by_position = " ORDER BY ug.`sequence` ASC";

				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT u.id, u.sameage_id, u.name, u.groups, u.tel, u.phone, u.email, u.birth, u.lunar, u.position, u.gender ";
				}
				if($isCount == "count"){
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON u.id = ug.usr_id ".$where_sql.$order_by_position;
				}
			break;

			case 'position':
	         	
				if($this->position == "10" || $this->position == "11"){

					$order_by_position = " ORDER BY `name` ASC";
				}else { 
					$order_by_position = " ORDER BY `position` DESC, `sequence` ASC";
				}
			    


                if($this->position == "30"){
					$where[] = "position = '30' OR position = '32' OR position = '33' OR position = '35' OR position = '37' OR position = '39' ";
				}else if($this->position == "20"){
					$where[] = "position = '20' OR position = '21' ";
				}else {
					$where[] = "position = '{$this->position}'";
				}

				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT * ";
				}

				if($isCount == "count"){
					$from_query = "FROM `usr` ".$where_sql;
				}else{
					$from_query = "FROM `usr` ".$where_sql.$order_by_position;
				}
			break;

			default:  //normal
			    if($this->sameage_id > 0){
					$where[] = "sameage_id = '{$this->sameage_id}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				if($this->isAllSelect == "O"){
					$select_fields = "SELECT * ";
				}else{
					$select_fields = "SELECT * ";
				}

				if($isCount == "count"){
					$from_query = "FROM `usr` ".$where_sql;
				}else{
					$from_query = "FROM `usr` ".$where_sql.$order_by;
				}
			break;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;
		$query = $select_query.$from_query;
		return $query;
	}

	public function getList($limit = 'O') {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		if($limit == 'O'){
			$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
			$res = $this->db->query($query);
		}else{
			$res = $this->db->query($query);
		}
		//echo $query;
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];
	}
}

?>
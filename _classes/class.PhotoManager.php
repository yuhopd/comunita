<?php
require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.Common.php');
require_once('../_classes/class.Upload.php');
//require_once("../_lib/_docs_log.inc");
/**
 * class visitManager
 * @author  yhlee , <yuhopd@gmail.com>
 */
class PhotoManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	private $id = 0;
	private $domain_id = 0;
	private $usr_id = 0;
	private $item_id = 0;

	private $mode = "visit";
	private $path = "";
	private $title = "";
	private $summary = "";
	private $file_rename;
	private $file_ext;

    public  $src_path = "/home/himh/public_html/data/photo";


	/**
	 * new MassageManager($properties)
	 * @return void
	 * @param array $properties
	 */
	public function __construct($properties) {
        $this->db =& DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}



	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}


	public static function getItem($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `photo` WHERE `id` = '$id'";
		$res  =& $db->query($query);
    	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
		}
		$res->free();
		return $row;
	}

	public function insert($arry) {
		$query  = "INSERT INTO `photo` (`domain_id`,`mode`, `item_id`, `usr_id`, `title`, `summary`, `rename`, `ext`, `path`, `adddate`) VALUES ('{$this->domain_id}','{$this->mode}', '{$this->item_id}', '{$this->usr_id}', '{$this->title}', '{$this->summary}', '{$this->file_rename}', '{$this->file_ext}', '{$this->path}', UNIX_TIMESTAMP())";
		$result = $this->db->query($query);
		$lastID = $this->db->lastInsertId();
		return $lastID;
	}


	public static function update($item_id) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `photo` SET `item_id` = {$item_id} WHERE `item_id` = 0";
		$result = $db->query($query);
		$lastID = self::getOne("SELECT MAX(id) FROM `photo` ");
		return $lastID;
	}


	public static function del($id) {
		$db =& DBConnection::get()->handle();
		$res =& $db->query("SELECT * FROM `photo` WHERE `id` = '{$id}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			@unlink($img_path."/".$row['path']."/".$row['rename'].'.'.$row['ext']);
			@unlink($img_path."/".$row['path']."/".$row['rename'].'_small.'.$row['ext']);
			$db->query("DELETE FROM `photo` WHERE `id` = '{$row[id]}' LIMIT 1");
		}
		return $row;
	}

    public static function get_file_path_name($id){
		$db =& DBConnection::get()->handle();
		$path = self::getOne("SELECT `path` FROM `photo` WHERE `id`='$id'");
        return $path;
    }

    /* save file to storage  */
    public function save_file_to_storage(){
		$this->path = self::get_file_path_name($this->photo_id);
		if(is_null($this->path) || $this->path == ''){
			$this->path = date("ym");
		}
        if(!is_dir($this->src_path."/".$this->path)){
            mkdir ($this->src_path."/".$this->path, 0777);
        }

		$allowedExtensions = array("jpg","png","gif");
		$this->file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

		$sizeLimit = 10 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit, $this->file_rename, $this->path);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload($this->src_path);



		//specify what percentage you are resizing to
		$percent_resizing = 100;
		$this->file_ext = $result['ext'];

		$target_storage = $this->src_path."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		$target_thumb_storage = $this->src_path."/".$this->path."/".$this->file_rename."_small.".$this->file_ext;
		$target_src_storage = $this->src_path."/".$this->path."/".$this->file_rename."_src.".$this->file_ext;

		$upload_tmp_name = $target_storage;


		$exif = exif_read_data($upload_tmp_name, 0, true); 
		if($exif['IFD0']['Orientation'] == 6 || $exif['IFD0']['Orientation'] == 3 || $exif['IFD0']['Orientation'] == 8){
			if($this->file_ext=="jpg"){
				 $source = imagecreatefromjpeg($upload_tmp_name);
			}else if($this->file_ext=="png"){
				 $source = imagecreatefrompng($upload_tmp_name);
			}else{
				 $source = imagecreatefromgif($upload_tmp_name);
			}

            if($exif['IFD0']['Orientation'] == 6){
				$rotate = imagerotate($source, 270, 0);
			}else if($exif['IFD0']['Orientation'] == 3){
				$rotate = imagerotate($source, 180, 0);
			}else if($exif['IFD0']['Orientation'] == 8){
				$rotate = imagerotate($source, 90, 0);
			}
			if($this->file_ext=="jpg"){
				 imagejpeg($rotate, $upload_tmp_name, 100);
			}else if($this->file_ext=="png"){
				 imagepng($rotate, $upload_tmp_name);
			}else{
				 imagegif($rotate, $upload_tmp_name);
			}
			imagedestroy($rotate);
		}



		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);


        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}
		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_thumb_storage, 100);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_thumb_storage);
		}else{
			 imagegif($dst_img, $target_thumb_storage);
		}
		imagedestroy($dst_img);
        return $result;
    }
}
?>
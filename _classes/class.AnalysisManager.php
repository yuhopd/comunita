<?php
require_once("../_lib/common_function.php");
require_once('../_classes/class.DBConnection.php');

/**
 * class GroupManager
 * @author  yhlee , <yhlee@thinkfree.com>
 */
class AnalysisManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;

	private $year;

	private $group_id = 0;

	private $halfYear = 1; // 1:전반기 2:후반기
	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn
	 */
	function __construct($properties) {
		//$this->db = DBConnection::get("slave")->handle();
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}


	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}


	public function getWeekDayStr() {
		$arryDate = getSundaysFromYear($this->year,0);
		$startTimestamp = mktime(0, 0, 0, 1, 1, $this->year);
		$startDayFromWeek = date("w",$startTimestamp);
		if($startDayFromWeek > 0){
			$gap = (7-$startDayFromWeek) + $week;
			$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
		}

        $start = 26*($this->halfYear - 1);
		$end   = 26*($this->halfYear);
		$mon = 0;
		for($i = 0 ; $i<52 ; $i++){
			$weekday[$i] =  date("j",strtotime("+{$i} week",$startTimestamp));
			if($mon != date("n",strtotime("+{$i} week",$startTimestamp))){
				$mon =  date("n",strtotime("+{$i} week",$startTimestamp));
				$arryMonth[$i] = $mon." 월";
			}else{
				$arryMonth[$i] = "";
			}
		}

		$result["week"] = implode(",",$weekday);
		$result["month"] = implode(",",$arryMonth);
		return $result;
	}


	public function getWorshipCountFromGroup() {

		$arryDate = getSundaysFromYear($this->year,0);
		$start = 26*($this->halfYear - 1);
		$end   = 26*($this->halfYear);

		for($w=0 ; $w < 52 ; $w++){
			$group_que_O = self::getOne("SELECT COUNT(a.id) FROM `attendance_usr` AS a LEFT JOIN `usr_group` AS ug ON (a.usr_id = ug.usr_id) WHERE ug.level < 2 AND a.`date`='{$arryDate[$w]}' AND a.worship='O' AND ug.group_id='{$this->group_id}'");
			$arryData_O[$w] = $group_que_O;
			$group_que_L = self::getOne("SELECT COUNT(a.id) FROM `attendance_usr` AS a LEFT JOIN `usr_group` AS ug ON (a.usr_id = ug.usr_id) WHERE ug.level < 2 AND a.`date`='{$arryDate[$w]}' AND a.worship='L' AND ug.group_id='{$this->group_id}'");
			$arryData_L[$w] = $group_que_L;
		}

		$result["O"] = implode(",",$arryData_O);
		$result["L"] = implode(",",$arryData_L);
		return $result;
	}

	public function getWorshipCountAll() {

		$arryDate = getSundaysFromYear($this->year,0);
		$start = 26*($this->halfYear - 1);
		$end   = 26*($this->halfYear);

		for($w=0 ; $w < 52 ; $w++){
			$group_que_O = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `date`='{$arryDate[$w]}' AND worship='O'");
			$arryData_O[$w] = $group_que_O;
			$group_que_L = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `date`='{$arryDate[$w]}' AND worship='L'");
			$arryData_L[$w] = $group_que_L;
		}

		$result["O"] = implode(",",$arryData_O);
		$result["L"] = implode(",",$arryData_L);
		return $result;
	}


	public function getArrayWorshipCountFromGroup() {
		$arryDate = getSundaysFromYear($this->year,0);
		$start = 26*($this->halfYear - 1);
		$end   = 26*($this->halfYear);
		for($w=0 ; $w < 52 ; $w++){
			$arryData_O[$w] = self::getOne("SELECT COUNT(a.id) FROM `attendance_usr` AS a LEFT JOIN `usr_group` AS ug ON (a.usr_id = ug.usr_id) WHERE ug.level < 2 AND a.`date`='{$arryDate[$w]}' AND a.worship IN('O','L') AND ug.group_id='{$this->group_id}'");
			$arryData_L[$w] = self::getOne("SELECT COUNT(a.id) FROM `attendance_usr` AS a LEFT JOIN `usr_group` AS ug ON (a.usr_id = ug.usr_id) WHERE ug.level < 2 AND a.`date`='{$arryDate[$w]}' AND a.gbs IN('O','L') AND ug.group_id='{$this->group_id}'");
		}

		$result["worship"] = $arryData_O;
		$result["gbs"] = $arryData_L;
		return $result;
	}

	public function getArrayWorshipCountAll() {
		$arryDate = getSundaysFromYear($this->year,0);
		$start = 26*($this->halfYear - 1);
		$end   = 26*($this->halfYear);

		for($w=0 ; $w < 52 ; $w++){
			$group_que_O = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `date`='{$arryDate[$w]}' AND `worship`='O'");
			$arryData_O[$w] = $group_que_O;
			$group_que_L = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `date`='{$arryDate[$w]}' AND `worship`='L'");
			$arryData_L[$w] = $group_que_L;
		}

		$result["O"] = $arryData_O;
		$result["L"] = $arryData_L;
		return $result;
	}


	public static function getAnalysisWeekDayFromYear($year,$week){
		$startTimestamp = mktime(0, 0, 0, 1, 1, $year);
		$startDayFromWeek = date("w",$startTimestamp);
		if($startDayFromWeek > 0){
			$gap = (7-$startDayFromWeek) + $week;
			$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
		}
		for($i=0;$i<52;$i++){
			$arrayDate[$i] =  date("n/j",strtotime("+{$i} week",$startTimestamp));
		}

		return $arrayDate;
	}


	public static function getAnalysisWeekDay($year,$week){
		$startTimestamp = mktime(0, 0, 0, 1, 1, $year);
		$startDayFromWeek = date("w",$startTimestamp);
		if($startDayFromWeek > 0){
			$gap = (7-$startDayFromWeek) + $week;
			$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
		}
		$month = 0;
		for($i=0;$i<52;$i++){
			$month_n =  date("n",strtotime("+{$i} week",$startTimestamp));
            if($month == $month_n){
				$arrayDate[$i] = "";
			}else{
				$arrayDate[$i] = date("n/j",strtotime("+{$i} week",$startTimestamp));
				$month = $month_n;
			}

		}
		return $arrayDate;
	}

}

?>
<?php

// 막지막 수정일 : 2005.01.31
// 제작자 : yuhopd
// public : new Paging()

class Paging {

var $total;             // 총 글수
var $num_line;          // 한 페이지당 글수
var $num_page;          // 한 화면당 페이지 수
var $start;             // 시작 페이지
var $page;              // 총 페이지
var $url;               // url

var $params = NULL;

    // public   : Paging($t, $s, $l, $p, $url) 
	// function : 페이징 생성자
	// $t       : 총 글수
	// $s       : 시작하는 글의 페이지수
	// $l       : 한페이지 안에 보여질 글수
	// $p       : 한화면에 보여질 페이지 수
    function Paging($t, $s, $l, $p, $url){
         $this->total    = $t;
		 $this->num_line = $l;
		 $this->num_page = $p;
		 $this->start    = $s;
		 $this->page     = floor($s/($l*$p));
		 $this->url      = $url;
	}

    function addParam($param_name, $param_value) {
         $cur_index = sizeof($this->params);
         $this->params[$cur_index]["name"] = $param_name;
         $this->params[$cur_index]["value"] = $param_value;
    }


  ## 페이징 정보를 갖는 배열은 만들어봅시다.
    function generate() {
         $param_str = "";
         for($i = 0; $i < sizeof($this->params); $i++){
              $param_str .= $this->params[$i]["name"]."=".$this->params[$i]["value"]."&";
         }
         // 처음 페이지
         $pages["first"]["name"] = "◀◀";
         $pages["first"]["url"] = $this->url."?start=1"."&".$param_str;
    
	     // 마지막 페이지
         $pages["last"]["name"] = "▶▶";
         $pages["last"]["url"] = $this->url."?".$this->page_var_name."=".$total_pages."&".$param_str;
    
	     if($this->getPrePage() == null){
		      $pages["prev"]["name"] = "";
		      $pages["prev"]["url"] = "#";
              // 이전 목록
			  
         }else{
              $pages["prev"]["name"] = "◀";
              $pages["prev"]["url"] = $this->url."?"."start=".$this->getPrePage()."&".$param_str;
		 }
		 
         if($this->getNextPage()==null){
         // 다음 목록
              $pages["next"]["name"] = "";
              $pages["next"]["url"] = "#";
		 }else{
		      $pages["next"]["name"] = "▶";
              $pages["next"]["url"] = $this->url."?"."start=".$this->getNextPage()."&".$param_str;
		 }


	     for($i=0 ; $i < $this->num_page ; $i++){
			  
              $ln =  $this->getStartPage($i);
			  $vk =  $this->page*$this->num_page+ $i +1;
		      if($ln < $this->total){	
				   if($ln!=$this->start){
                        $pages[$i]["name"] = $vk;
                        $pages[$i]["url"]  = $this->url."?"."start=".$ln."&".$param_str;
				   }else{
                        $pages[$i]["name"] = $vk;
                        //$pages[$i]["url"]  = "";			   
				   }
		      }
         }

		 return $pages;
    }
   
    // public   : getPrePage()
	// function : 이전페이지를 구하는 함수
    function getPrePage(){
		 $v = ($this->page) * ($this->num_line*$this->num_page) - $this->num_line;

         if(($this->start + 1) > ($this->num_line*$this->num_page)){
			 $pre_page = $v;
		 }else{
             $pre_page = null;
		 }
		 return $pre_page;
	}

    // public   : getNextPage()
	// function : 다음페이지를 구하는 함수
	function getNextPage(){
		 $v = ($this->page+1)*($this->num_line*$this->num_page);
		 if($this->total > $v){
			 $next_page = $v;
		 }else{
			 $next_page = null;
		 }
		 return $next_page;
	}

    // public   : getStartPage($o)
    function getStartPage($o){
		$ln = ($this->page * $this->num_page + $o)*$this->num_line;
        return $ln;
	}  
	//-------------------------------------------------------------------------
}

?>
<?php
require_once('../_classes/class.DBConnection.php');
require_once("../_lib/_docs_log.inc");
/**
 * class GroupManager
 * @author  yhlee , <yhlee@thinkfree.com>
 */
class BoardFileManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;

	private $id=0;
	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn 
	 */
	public function __construct($id=0) {
		if(isset($id)) $this->id = $id; 
		$this->db = DBConnection::get()->handle();
	}

	public static function getFileCount($board_id) {
		$db =& DBConnection::get()->handle();	
		$countFile = $db->getOne("SELECT count(*) FROM `board_file` WHERE board_id = '$board_id'");
		return $countFile;
	}

	public static function getFileList($board_id) {
		$db =& DBConnection::get()->handle();
		$query  = "SELECT * FROM `board_file` WHERE board_id = '$board_id'";
		$res =& $db->query($query);

		while($res->fetchInto($row, DB_FETCHMODE_ASSOC)){
			 $list = $row;
		}
		return $list;
	}

	public static function delete($board_id) {
		$db =& DBConnection::get()->handle();	
		$query  = "DELETE FROM `board` WHERE id='$board_id' LIMIT 1";
		$res =& $db->query($query);
		return $res;
	}
}
?>
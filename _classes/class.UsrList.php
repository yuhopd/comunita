<?php
require_once("../_classes/class.DBConnection.php");

class UsrList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
    /**
	 * @var int $browse
	 */
	private $email = '';
	/**
	 * @var  int  $usr_id  
	 */
	private $usr_id = 0 ;
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $nickname = '';
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $name = '';
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $phone = '';	
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $birth = '';
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $level = '';
	/**
	 * @var  int  $auth   
	 */
	private $auth = 'O';
	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 20;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {      
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY u.level DESC"; break;
				case 2:  $order_by = " ORDER BY u.sameage_id DESC"; break;
	    		default: $order_by = " ORDER BY u.id DESC";
			}
		}



        switch ($this->type) {

            case 'search': 
				$keywordForDB = cleanupDB(cleanupHTML($this->keyword));
				$where[] = "(u.email like '%$keywordForDB%' || u.name like '%$keywordForDB%')";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.email, u.nickname, u.name, u.phone, u.birth, u.level ";
					
				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;
	
			default:  //normal
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";

				$select_fields = "SELECT u.id, u.email, u.nickname, u.name, u.phone, u.birth, u.level ";
					
				if($isCount == "count"){
					$from_query = "FROM `usr` AS u ".$where_sql;
				}else{
					$from_query = "FROM `usr` AS u ".$where_sql.$order_by;
				}
			break;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get Groups List 
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList() {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();

		$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
		$res = $this->db->query($query);

		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	/**
	 * Get Groups Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];

	}
}



/* how to use */

// Groups List
/*
echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
$properties = array(
	"browse"   => 0,
	"language" => 0,
	"period"   => 0,
	"sn"       => 0,
	"type"     => 0,
	"page"     => 1,
	"perPage"  => 5,
	"keyword"  => ''
);
$groupslist = new GroupsList($properties);
$start_time1 = microtime();
//$count  = $groupslist->getTotalCount();
//echo "total count:::::::::$count<br />";
$list   = $groupslist->getGroupsList();
$tot = count($list);
for ($ii=0; $ii<$tot; $ii++){
	print_r ($list[$ii]);
	echo"<br><br>";
    //echo "<li>{$list[$ii][gsn]} :: {$list[$ii][title]}</li>";
}
$end_time1 = microtime();
$result1 = $end_time1 - $start_time1;


echo "<br><br><br>";
echo "array : ".$result1;
*/

?>
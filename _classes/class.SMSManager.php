<?php
require_once('../_classes/class.DBConnection.php');
require_once ("../_lib/_docs_log.inc");
/**
 * class SMSManager
 * @author  yuho , <yuhopd@gmail.com>
 */
class SMSManager{
    private $db;
	private $id       = 0;
	private $category = 0;
	private $size     = 0;

	public function __construct($properties) {
        $this->db =& DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}
/*
	public static function send($domain_id, $to_phone, $from_phone, $content, $type=4) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO widefoot.`em_tran` (`tran_phone`, `tran_callback`, `tran_status`, `tran_date`, `tran_msg`, `tran_type`, `tran_etc1`) VALUES ('{$to_phone}', '{$from_phone}' ,1, NOW(), '{$content}', '{$type}', '{$domain_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function reserveSend($domain_id, $to_phone, $from_phone, $content, $tran_date,  $type=4) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO widefoot.`em_tran` (`tran_phone`, `tran_callback`, `tran_status`, `tran_date`, `tran_msg`, `tran_type`, `tran_etc1`) VALUES ('{$to_phone}', '{$from_phone}' ,1, '{$tran_date}', '{$content}', '{$type}', '{$domain_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function del($tran_pr) {
		$db =& DBConnection::get()->handle();
		$res =& $db->query("SELECT * FROM widefoot.`em_tran` WHERE `tran_pr` = '{$tran_pr}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$db->query("DELETE FROM widefoot.`em_tran` WHERE `tran_pr` = '{$row[tran_pr]}' LIMIT 1");
		}
		return $row;
	}
*/


	public static function sendLMS($domain_id, $to_phone, $from_phone, $subject, $content, $type=1) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO infobank.`em_mmt_tran` (`date_client_req`, `subject`, `content`, `attach_file_group_key`, `callback`, `service_type`, `broadcast_yn`, `msg_status`, `recipient_num`) VALUES
		(NOW(),'{$subject}', '{$content}', '0', '{$from_phone}', '2', 'N', '{$type}', '{$to_phone}')";
		$result = $db->query($query);
		return $result;
	}

	public static function reserveSendLMS($domain_id, $to_phone, $from_phone, $subject, $content, $tran_date,  $type=1) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO infobank.`em_smt_tran` (`date_client_req`, `subject`, `content`, `attach_file_group_key`, `callback`, `service_type`, `broadcast_yn`, `msg_status`, `recipient_num`) VALUES
		('{$tran_date}','{$subject}', '{$content}', '0', '{$from_phone}', '2', 'N', '{$type}', '{$to_phone}')";
		$result = $db->query($query);
		return $result;
	}



	public static function send($domain_id, $to_phone, $from_phone, $content, $type=1) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO infobank.`em_smt_tran` (`date_client_req`, `content`, `callback`, `service_type`, `broadcast_yn`, `msg_status`, `recipient_num`, `TRAN_ETC1`) VALUES
		(NOW(), '{$content}', '{$from_phone}', '0', 'N', '{$type}', '{$to_phone}', '{$domain_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function reserveSend($domain_id, $to_phone, $from_phone, $content, $tran_date,  $type=1) {
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO infobank.`em_smt_tran` (`date_client_req`, `content`, `callback`, `service_type`, `broadcast_yn`, `msg_status`, `recipient_num`, `TRAN_ETC1`) VALUES
		('{$tran_date}', '{$content}', '{$from_phone}', '0', 'N', '{$type}', '{$to_phone}', '{$domain_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function del($tran_pr) {
		$db =& DBConnection::get()->handle();
		$res =& $db->query("SELECT * FROM infobank.`em_smt_tran` WHERE `tran_pr` = '{$tran_pr}'");
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
			$db->query("DELETE FROM infobank.`em_smt_tran` WHERE `tran_pr` = '{$row[tran_pr]}' LIMIT 1");
		}
		return $row;
	}



/*
	public static function send($domain_id, $to_phone, $from_phone, $content, $type=4, $lms_id=NULL) {
		$db =& DBConnection::get()->handle();
        $to_phone = str_replace("-","",$to_phone);
        $from_phone = str_replace("-","",$from_phone);
		$query  = "INSERT INTO smstnt.`MSG_DATA` (`USERDATA`, `CUR_STATE`, `CALL_TO`, `CALL_FROM`, `SMS_TXT`, `MSG_TYPE`, `CONT_SEQ`) VALUES ('{$domain_id}', 0, '{$to_phone}', '{$from_phone}', '{$content}', '{$type}', '{$lms_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function reserveSend($domain_id, $to_phone, $from_phone, $content, $req_date, $type=4, $lms_id=NULL) {
		$db =& DBConnection::get()->handle();
        $to_phone = str_replace("-","",$to_phone);
        $from_phone = str_replace("-","",$from_phone);
		$query  = "INSERT INTO smstnt.`MSG_DATA` (`USERDATA`, `CUR_STATE`, `REQ_DATE`, `CALL_TO`, `CALL_FROM`, `SMS_TXT`, `MSG_TYPE`, `CONT_SEQ`) VALUES ('{$domain_id}', 0, '{$req_date}', '{$to_phone}', '{$from_phone}', '{$content}', '{$type}', '{$lms_id}')";
		$result = $db->query($query);
		return $result;
	}

	public static function addLMSContents($subject, $content) {
		$db =& DBConnection::get()->handle();
        $to_phone = str_replace("-","",$to_phone);
        $from_phone = str_replace("-","",$from_phone);
		$src_path = "/home/himh/public_html/data/smstnt";
		$path = date("ym");
		if(!is_dir($src_path."/".$path)){
            mkdir ($src_path."/".$path, 0777);
        }

		$file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);
		$pathStyles = $src_path."/".$path."/".$file_rename.".txt";


		$query  = "INSERT INTO smstnt.`MMS_CONTENTS_INFO` (`FILE_CNT`, `MMS_BODY`, `MMS_SUBJECT`, `FILE_TYPE1`, `FILE_NAME1`) VALUES (1, '{$content}', '{$subject}', 'TXT', '{$pathStyles}')";
		$result = $db->query($query);
        if($result){
			$lastID = $db->getOne("SELECT MAX(CONT_SEQ) FROM smstnt.`MMS_CONTENTS_INFO` ");
		}else{
			$lastID = false;
		}


        $content =  iconv("UTF-8", "EUC-KR", $content);
		$fp = fopen($pathStyles, "w");
		fwrite($fp, $content);
		fclose($fp);

		

		return $lastID;
	}


*/

}
?>
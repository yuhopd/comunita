<?php
require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.Common.php');
//require_once("../_lib/_docs_log.inc");
/**
 * class MessageManager
 * @author  yhlee , <yuhopd@gmail.com>
 */
class MessageManager{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;
	private $id = 0;
	private $reg_date = "";
	private $category = 0;
	private $size = 0;
    public  $src_path = "/home/himh/public_html/data/message";

	/**
	 * new MassageManager($properties)
	 * @return void
	 * @param array $properties 
	 */
	public function __construct($properties) {
        self::$db =& DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	public function getLastIndex(){
		$lastID = self::getOne("SELECT MAX(id) FROM `message`");
		return $lastID;
	}


	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_NUM);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}

	public static function getNoReadCount($read_id){
		$count = self::getOne("SELECT count(*) FROM `message` WHERE `is_read` = 'X' AND `read_id` = '{$read_id}' ");
		return $count;
	}

	public static function isItem($id) {
		
		$query = "SELECT count(*) FROM `message` WHERE `id` = '$id'";
		$isCount = self::getOne($query);
		if($isCount > 0){
			return "O";
		}else{
			return "X";
		}
	}

	public static function getItem($id) {
		$db     = DBConnection::get()->handle();
		$query  = "SELECT m.id, m.send_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.send_id) AS send_name, m.read_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.read_id) AS read_name, m.content, m.adddate, m.is_read FROM `message` AS m WHERE `id` = '$id'";
		$res    = $db->query($query);
		$row    = $res->fetch(PDO::FETCH_ASSOC);
    	if($row){
			# 선택한 데이타 가지고 오기
			$row['content'] = nl2br($row['content']);
		}
		return $row;
	}


	public static function getNoReadItem($read_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT  m.id, m.send_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.send_id) AS send_name, m.read_id, (SELECT u.name FROM `usr` AS u WHERE u.id=m.read_id) AS read_name, m.content, m.adddate, m.is_read  FROM `message` AS m HERE `is_read` = 'X' AND `read_id` = '{$read_id}' ORDER BY `adddate` DESC LIMIT 1";
		$res   = $db->query($query);
		$row   = $res->fetch(PDO::FETCH_ASSOC);
    	if ($row){
			$row['content'] = stripslashes($row['content']);
		}
		return $row;
	}

	public static function updateRead($id) {
		$db     = DBConnection::get()->handle();
		$db->query ("UPDATE `message` SET `is_read`='O' WHERE `id`='$id'");
	}


	public static function insert($domain_id, $send_id, $read_id, $content) {
		$db     = DBConnection::get()->handle();
	    $ip     = Common::getRealIpAddr();
		$query  = "INSERT INTO `message` (`domain_id`, `send_id`, `read_id`, `content`, `adddate`, `is_read`) VALUES ('{$domain_id}', '{$send_id}' ,'{$read_id}', '{$content}', UNIX_TIMESTAMP(), 'X' )";
		$result = $db->query($query);
		$lastID = $db->lastInsertId();
		return $lastID;
	}

	public static function del($id) {
		$db  = DBConnection::get()->handle();
		$res = $db->query("SELECT * FROM `message` WHERE `id` = '{$id}'");
		$row = $res->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			$db->query("DELETE FROM `message` WHERE `id` = '{$row[id]}' LIMIT 1");
		}
		return $row;
	} 
}
?>
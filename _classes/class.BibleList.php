<?php
require_once("../_classes/class.DBConnection.php");

class BibleList{
    /**
	 * @var object $db database connection
	 */
    private static $db;
    /**
	 * @var int $browse
	 */
	private $browse = 0;
	/**
	 * @var int $book
	 */
	private $book = 1;
	/**
	 * @var int $chapter
	 */
	private $chapter = 1;
	/**
	 * @var int $verse
	 */
	private $verse = 1;	
	/**
	 * @var  int  $type  ( normal ) 
	 */
	private $type = 'normal';
	/**
	 * @var  int  $page
	 */
	private $page = 1;
	/**
	 * @var  int  $perPage
	 */
	private $perPage = 10;

	/**
	 * @var  string  $keyword
	 */
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('usr_id','type','page','prePage','keyword');
	 */
	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				//case 1:  $order_by = " ORDER BY `id` DESC";   break;
	    		default: $order_by = " ORDER BY `verse` ASC";
			}
		}

		if ($this->keyword != ''){
			$keywordForDB = cleanupDB(cleanupHTML($this->keyword));
			$where[] = "( `message` like '%$keywordForDB%' )";
		}

        switch ($this->type) {
			default:  //normal
			    $where[] = "`book` = {$this->book}";
			    $where[] = "`chapter` = {$this->chapter}";
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT `id`, `book`, `chapter`, `verse`, `message` ";

				if($isCount == "count"){
					$from_query = "FROM `bible` ".$where_sql;
				}else{
					$from_query = "FROM `bible` ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List 
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList($islimit=1) {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		if($islimit==1){
			$res =& $this->db->limitQuery($query, $from, $this->perPage);
		}else{
			$res =& $this->db->query($query);
		}

		if ($res->numRows() == 0){
			return false;
		}else{
			while ($res->fetchInto($row, DB_FETCHMODE_ASSOC)) {
				$list[] = $row;
			}
		}
		return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$result = $this->db->query($query);
		$result->fetchInto($row, DB_FETCHMODE_ASSOC);
		return $row['count'];

	}
}
?>
<?php
require_once("../_lib/common_function.php");
require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.GroupManager.php');
//require_once("../_lib/_docs_log.inc");
/**
 * class AttendanceManager

 */
class AttendanceManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	private $year;
	private $group_id = 0;
	private $halfYear = 1; // 1:전반기 2:후반기

	/**
	 * new BoxManager($id)
	 * @return void
	 * @param int $gsn
	 */
	function __construct($properties) {
		//$this->db = DBConnection::get("slave")->handle();
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}


	public static function getReport($attendance_id,$date) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_report` WHERE `attendance_id`='{$attendance_id}' AND `date`='{$date}'";
		$res   = $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

    public static function saveReportUsr($arry){
		$db    = DBConnection::get()->handle();
		$query = "INSERT INTO `attendance_usr` (`attendance_id`, `group_id`, `usr_id`, `date`, `note`, `worship`, `gbs`, `qt`, `bible`, `prayer`, `adddate`) VALUES ('{$arry[attendance_id]}', '{$arry[group_id]}', '{$arry[usr_id]}', '{$arry[date]}', '{$arry[note]}', '{$arry[worship]}', '{$arry[gbs]}', '{$arry[qt]}', '{$arry[bible]}', '{$arry[prayer]}', UNIX_TIMESTAMP()) ON DUPLICATE KEY UPDATE  `note`='{$arry[note]}', `worship`='{$arry[worship]}', `gbs`='{$arry[gbs]}', `qt`='{$arry[qt]}', `bible`='{$arry[bible]}', `prayer`='{$arry[prayer]}',`adddate`=UNIX_TIMESTAMP()";
	    $res   = $db->query($query);
	}

    public static function saveReport($arry){
		$db    = DBConnection::get()->handle();
		$query = "INSERT INTO `attendance_report` (`attendance_id`, `group_id`, `date`, `suggest`, `report`, `unusual`, `bigo`, `adddate`) VALUES ('{$arry[attendance_id]}', '{$arry[group_id]}', '{$arry[date]}', '{$arry[suggest]}', '{$arry[report]}', '{$arry[unusual]}', '{$arry[bigo]}', UNIX_TIMESTAMP()) ON DUPLICATE KEY UPDATE  `suggest`='{$arry[suggest]}', `report`='{$arry[report]}', `unusual`='{$arry[unusual]}', `bigo`='{$arry[bigo]}', `adddate`=UNIX_TIMESTAMP()";
	    $res   = $db->query($query);
	}

	public static function getTalentFromUsr($usr_id,$group_id,$date) {
		$db     = DBConnection::get()->handle();
		$query  = "SELECT * FROM `attendance_usr_talent` WHERE `usr_id`='$usr_id' AND `group_id`='$group_id' AND `date` = '$date'";
		$res    = $db->query($query);
		$report = $res->fetch(PDO::FETCH_ASSOC);
		return $report;
	}

	public static function getNoteFromUsr($usr_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_usr` WHERE `usr_id`='$usr_id' ORDER BY `date` DESC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}
/*------------------------------------------------------------------------------*/
	public static function getUsrFromGroup($group_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT u.id, u.name, u.phone FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON (u.id = ug.usr_id) WHERE ug.group_id='{$group_id}' AND ug.level < 2 ORDER BY BINARY u.name ASC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function getAttendanceUsrCountFromGroup($group_id,$date) {
		$db    = DBConnection::get()->handle();
		$count = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `group_id`='{$group_id}' AND `date`='{$date}'");
		return $count;
	}
	public static function getAttendanceUsrFromGroup($group_id,$date) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT u.* FROM `usr` AS u LEFT JOIN `attendance_usr` AS au ON (u.id = au.usr_id) WHERE au.group_id='{$group_id}' AND au.date ='{$date}' ORDER BY BINARY u.name ASC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}
/*------------------------------------------------------------------------------*/

	public static function getAttendanceUsrCount($attendance_id,$date) {
		$db    = DBConnection::get()->handle();
		$count = self::getOne("SELECT COUNT(*) FROM `attendance_usr` WHERE `attendance_id`='{$attendance_id}' AND `date`='{$date}'");
		return $count;
	}

	public static function getAttendanceUsrList($attendance_id,$date) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT u.* FROM `usr` AS u LEFT JOIN `attendance_usr` AS au ON (u.id = au.usr_id) WHERE au.attendance_id='{$attendance_id}' AND au.date ='{$date}' ORDER BY BINARY u.name ASC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public static function getUsrListFromGroup($group_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT u.* FROM `usr` AS u LEFT JOIN `usr_group` AS ug ON (u.`id` = ug.`usr_id`) WHERE ug.`group_id`='{$group_id}' AND ug.`level` < 2 ORDER BY ug.`sequence` ASC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public static function getUsrListFromAttendance($attendance_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT u.`id`, u.`name`, u.`phone` FROM `usr` AS u LEFT JOIN `usr_attendance` AS ua ON (u.`id` = ua.`usr_id`) WHERE ua.`attendance_id`='{$attendance_id}' AND ua.`level` < 2 ORDER BY ua.`sequence` ASC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function getAttendanceFromUsr($usr_id,$attendance_id,$date) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_usr` WHERE `usr_id`='$usr_id' AND `attendance_id`='$attendance_id' AND `date`='$date'";
		$res   = $db->query($query);
		$report = $res->fetch(PDO::FETCH_ASSOC);
		return $report;
	}

	public static function getAttendanceUsr($attendance_usr_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_usr` WHERE `id`='$attendance_usr_id'";
		$res   = $db->query($query);
		$attendanceUsr = $res->fetch(PDO::FETCH_ASSOC);
		return $attendanceUsr;
	}


	public static function getAttendanceFromRootGroup($root_id) {
		$db =& DBConnection::get()->handle();
		$query2  = "SELECT `id`,`title` ,`use_attendance` FROM `group` WHERE `parent_id`='$root_id' AND `is_enable`='O' ";
		$res2    = $db->query($query2);
		while($row2 = $res2->fetch(PDO::FETCH_ASSOC)){
			$list[] = $row2;
			$query3  = "SELECT `id`, `title` ,`use_attendance` FROM `group` WHERE `parent_id`='{$row2[id]}' AND `is_enable`='O' ";
			$res3    =& $db->query($query3);
			while($row3 = $res3->fetch(PDO::FETCH_ASSOC)){
				$list[] = $row3;
				$query4  = "SELECT `id`, `title` ,`use_attendance` FROM `group` WHERE `parent_id`='{$row3[id]}' AND `is_enable`='O' ";
				$res4    =& $db->query($query4);
				while($row4 = $res4->fetch(PDO::FETCH_ASSOC)){
					$list[] = $row4;
					$query5  = "SELECT `id`, `title` ,`use_attendance` FROM `group` WHERE `parent_id`='{$row4[id]}' AND `is_enable`='O' ";
					$res5    =& $db->query($query5);
					while($row5 = $res5->fetch(PDO::FETCH_ASSOC)){
						$list[] = $row5;
					}
				}
			}

		}
		return $list;
	}

    /* Attendance Settings*/
	public static function getAttendanceSetting($attendance_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance` WHERE `id`='{$attendance_id}'";
		$res   = $db->query($query);
		if($row = $res->fetch(PDO::FETCH_ASSOC)){
			 $attendance = $row;
		}else{
			$attendance = false;
		}
		return $attendance;
	}

	public static function getAttendanceSettingList($root_id, $year) {
		$db = DBConnection::get()->handle();
		if(!$year){ $year = date("Y"); }
		$query  = "SELECT * FROM `attendance` WHERE `root_id`='{$root_id}' AND `year` = '{$year}' ORDER BY `adddate` ASC";
		$res = $db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public static function getAttendanceSettingListFromGroup($group_id) {
		$db =& DBConnection::get()->handle();

		$year = date("Y");

		$query  = "SELECT * FROM `attendance` WHERE `group_id`='{$group_id}' AND `year` = '{$year}' ";
		echo $query;
		$res =& $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}


	public static function insertAttendanceSetting($arry){
		$db =& DBConnection::get()->handle();
		$root_id = GroupManager::getAttendanceAdminGroupId($arry['group_id']);

		$query  = "INSERT INTO `attendance` (`title`, `summary`, `group_id`, `root_id`, `year`, `start_date`, `end_date`, `every_date_type`, `every_date_week`, `template`,`adddate`) VALUES ('{$arry[title]}', '{$arry[summary]}', '{$arry[group_id]}', '{$root_id}', '{$arry[year]}', '{$arry[start_date]}', '{$arry[end_date]}', '{$arry[every_date_type]}', '{$arry[every_date_week]}', '{$arry[template]}', UNIX_TIMESTAMP())";
		$result = $db->query($query);
		$lastID = self::getOne("SELECT MAX(id) FROM `attendance` ");

		$query2  = "SELECT * FROM `usr_group` WHERE `group_id`='{$arry[group_id]}' ";
		$res2    =& $db->query($query2);
		while($row2 = $res2->fetch(PDO::FETCH_ASSOC)){
			$db->query("INSERT INTO `usr_attendance` (`usr_id`, `attendance_id`, `level`, `sequence`) VALUES ( '{$row2[usr_id]}', '$lastID', '{$row2[level]}', '{$row2[sequence]}' )");
		}
		return $lastID;
	}

	public static function modifyAttendanceSetting($arry){
		$db =& DBConnection::get()->handle();
		$root_id = GroupManager::getAttendanceAdminGroupId($arry['group_id']);
		$query  = "UPDATE `attendance` SET
		                `title`           = '{$arry[title]}',
		                `summary`         = '{$arry[summary]}',
						`group_id`        = '{$arry[group_id]}',
						`root_id`         = '{$root_id}',
						`start_date`      = '{$arry[start_date]}',
						`end_date`        = '{$arry[end_date]}',
						`every_date_type` = '{$arry[every_date_type]}',
						`every_date_week` = '{$arry[every_date_week]}',
						`template`        = '{$arry[template]}',
						`adddate`         = UNIX_TIMESTAMP()
				  WHERE `id` = '{$arry[id]}'";

		$result = $db->query($query);

		$query2  = "SELECT * FROM `usr_group` WHERE `group_id`='{$arry[group_id]}' ";
		$res2    =& $db->query($query2);
		while($row2 = $res2->fetch(PDO::FETCH_ASSOC)){
			$db->query("INSERT INTO `usr_attendance` (`usr_id`, `attendance_id`, `level`, `sequence`) VALUES ( '{$row2[usr_id]}', '$lastID', '{$row2[level]}', '{$row2[sequence]}' ) ON DUPLICATE KEY UPDATE  `level`='{$row2[level]}', `sequence`='{$row2[sequence]}'");
		}
		return $lastID;

	}

	public static function modifyOneAttendanceSetting($key, $val, $id){
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `attendance` SET
		                `{$key}`  = '{$val}',
						`adddate` = UNIX_TIMESTAMP()
				  WHERE `id` = '{$id}'";
		$result = $db->query($query);
	}

	public static function deleteAttendance($id) {
		$db =& DBConnection::get()->handle();
		$result = $db->query("DELETE FROM `attendance` WHERE id='$id' LIMIT 1");
		$db->query("DELETE FROM `attendance_report` WHERE `attendance_id`='$id'");
		$db->query("DELETE FROM `attendance_usr` WHERE `attendance_id`='$id'");
		return $result;
	}

	public static function getComment($id) {
		$db    =& DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_comment` WHERE `id`='{$id}'";
		$res   =& $db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function getCommentList($attendance_usr_id) {
		$db    = DBConnection::get()->handle();
		$query = "SELECT * FROM `attendance_comment` WHERE `attendance_usr_id`='{$attendance_usr_id}' ORDER BY `adddate` DESC";
		$res   = $db->query($query);
		$rows   = $res->fetchAll(PDO::FETCH_ASSOC);
		$list  = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function getCommentCount($attendance_usr_id) {
		$db =& DBConnection::get()->handle();
		$count = self::getOne("SELECT COUNT(*) FROM `attendance_comment` WHERE `attendance_usr_id`='{$attendance_usr_id}' ");
		return $count;
	}

	public static function insertComment($arry){
		$db =& DBConnection::get()->handle();
		$query  = "INSERT INTO `attendance_comment` (`attendance_usr_id`, `usr_id`, `comment`, `adddate`) VALUES ('{$arry[attendance_usr_id]}', '{$arry[usr_id]}', '{$arry[comment]}', UNIX_TIMESTAMP())";
		$result = $db->query($query);
		$lastID = self::getOne("SELECT MAX(id) FROM `attendance_comment` ");
	}

	public static function deleteComment($id){
		$db =& DBConnection::get()->handle();
		$result = $db->query("DELETE FROM `attendance_comment` WHERE id='$id' LIMIT 1");
		//$db->query("DELETE FROM `usr_group` WHERE group_id='$id'");
		return $result;
	}

}
?>
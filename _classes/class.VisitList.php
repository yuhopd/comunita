<?php
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.Common.php");
class VisitList{
    /**
	 * @var object $sdb database connection
	 */
    private static $db;


    private $usr_id = 0;

	private $visit_usr_id = 0;

	private $browse = 0;

	private $page = 1;

	private $perPage = 20;

	private $keyword = '';


	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
				case 1:  $order_by = " ORDER BY `reg_date` DESC"; break;
	    		default: $order_by = " ORDER BY `adddate` DESC";
			}
		}

		if ($this->keyword != ''){
			$keywordForDB = Common::cleanupDB($this->keyword);
			//$where[] = "(v.`content` like '%".$keywordForDB."%')";
			$where[] = "( v.`place` LIKE '%$keywordForDB%' || v.`title` LIKE '%$keywordForDB%' || v.`content` LIKE '%$keywordForDB%' )";
		}



        switch ($this->type) {

            case 'search':
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT * ";

				if($isCount == "count"){
					$from_query = "FROM `visit` ".$where_sql;
				}else{
					$from_query = "FROM `visit` ".$where_sql.$order_by;
				}
				break;

			default:  //normal
			    if($this->usr_id > 0){
					$where[] = "`usr_id` = '{$this->usr_id}' ";
				}
			 	$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT * ";
				if($isCount == "count"){
					$from_query = "FROM `visit` ".$where_sql;
				}else{
					$from_query = "FROM `visit` ".$where_sql.$order_by;
				}
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get List
	 * @return array $List
	 * @param  int   $page
	 */
	public function getList($islimit='O') {
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		
		if($islimit=='O'){
			$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
			$res = $this->db->query($query);
		}else{
			$res = $this->db->query($query);
		}
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	/**
	 * Get Total Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];

	}
}

?>
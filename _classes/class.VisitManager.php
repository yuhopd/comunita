<?php
require_once('../_classes/class.DBConnection.php');
require_once('../_classes/class.Common.php');
require_once('../_classes/class.Upload.php');

/**
 * class visitManager
 * @author  yhlee , <yuhopd@gmail.com>
 */
class VisitManager{
    /**
	 * @var object $sdb database connection
	 */
    private $db;
	private $id = 0;
	private $domain_id = 0;
	private $photo_id = 0;
	private $usr_id = 0;

	private $mode = "visit";
	private $path = "";

	private $file_name;
	private $file_rename;
	private $file_ext;
	private $size = 0;

    public  $src_path = "/home/himh/public_html/data/visit";


	public static $arryVisitType = array(
		1 => "축복심방",
		2 => "구역심방",
		3 => "병원심방",
		4 => "개업심방",
		5 => "이사심방",
		6 => "장례",
		7 => "위로심방",
		8 => "감사심방",
	    99 => "기타"
	);

	/**
	 * new MassageManager($properties)
	 * @return void
	 * @param array $properties
	 */
	public function __construct($properties) {
        $this->db =& DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}


	public static function getOne($sql){
		$db   = DBConnection::get()->handle();
		$stmt = $db->query($sql);
		$row  = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}


	public function getLastIndex(){
		$lastID = self::getOne("SELECT MAX(id) FROM `visit`");
		return $lastID;
	}

	public static function getItem($id) {
		$db =& DBConnection::get()->handle();
		$query = "SELECT * FROM `visit` WHERE `id` = '$id'";
		$res  =& $db->query($query);
    	$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public static function insert($arry) {
		$db =& DBConnection::get()->handle();
        $ip = Common::getRealIpAddr();
		$query  = "INSERT INTO `visit` (`usr_id`, `visit_usr_id`, `write_usr_id`, `reg_date`, `type`, `place`, `title`, `content`, `adddate`) VALUES ('{$arry[usr_id]}', '{$arry[visit_usr_id]}' ,'{$arry[write_usr_id]}' ,'{$arry[reg_date]}', '{$arry[type]}', '{$arry[place]}', '{$arry[title]}', '{$arry[content]}', UNIX_TIMESTAMP())";
		$result = $db->query($query);
		$lastID = self::getOne("SELECT MAX(id) FROM `visit` ");
		return $lastID;
	}

	public static function modify($arry) {
		$db =& DBConnection::get()->handle();
		$query  = "UPDATE `visit` SET
		                `usr_id`         = '{$arry[usr_id]}',
						`visit_usr_id`   = '{$arry[visit_usr_id]}',
						`write_usr_id`   = '{$arry[write_usr_id]}',
						`reg_date`       = '{$arry[reg_date]}',
						`type`           = '{$arry[type]}',
						`place`          = '{$arry[place]}',
						`title`          = '{$arry[title]}',
                        `content`        = '{$arry[content]}',
						`adddate`        = UNIX_TIMESTAMP()
				  WHERE `id` = '{$arry[visit_id]}'";

		$result = $db->query($query);
		return $arry['group_id'];
	}

	public static function del($id) {
		$db  =& DBConnection::get()->handle();
		$res =& $db->query("SELECT * FROM `visit` WHERE `id` = '{$id}'");
		if($row = $res->fetch(PDO::FETCH_ASSOC)){
			$db->query("DELETE FROM `visit` WHERE `id` = '{$row[id]}' LIMIT 1");
		}
		return $row;
	}

    public static function get_file_path_name($id){
		$db =& DBConnection::get()->handle();
		$path = self::getOne("SELECT `path` FROM `photo` WHERE `id`='$id'");
        return $path;
    }

    /* save file to storage  */
    public function save_file_to_storage(){
		$this->path = self::get_file_path_name($this->photo_id);
		if(is_null($this->path) || $this->path == ''){
			$this->path = date("ym");
		}
        if(!is_dir($this->src_path."/".$this->path)){
            mkdir ($this->src_path."/".$this->path, 0777);
        }

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array("jpg","png","gif");
		// max file size in bytes
		$this->file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

		$sizeLimit = 10 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit, $this->file_rename, $this->path);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload($this->src_path);



		//specify what percentage you are resizing to
		$percent_resizing = 100;
		$this->file_ext = $result['ext'];

		$target_storage = $this->src_path."/".$this->path."/".$this->file_rename.".".$this->file_ext;
		$target_thumb_storage = $this->src_path."/".$this->path."/".$this->file_rename."_small.".$this->file_ext;
		$target_src_storage = $this->src_path."/".$this->path."/".$this->file_rename."_src.".$this->file_ext;

		$upload_tmp_name = $target_storage;
		list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);


        if($width < $height){
			$reWidth = $percent_resizing;
            $reHeight = round((($height/$width)*$percent_resizing));
		}else{
			$reHeight = $percent_resizing;
            $reWidth = round((($width/$height)*$percent_resizing));
		}
		$dst_img = imagecreatetruecolor($reWidth,$reHeight);
		if($this->file_ext == "jpg"){
			 $src_img = imagecreatefromjpeg($upload_tmp_name);
		}else if($this->file_ext == "png"){
			 $src_img = imagecreatefrompng($upload_tmp_name);
		}else{
			 $src_img = imagecreatefromgif($upload_tmp_name);
		}

		imagecolorallocate($dst_img, 255, 255, 255);
		imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
		imageinterlace($dst_img);
		if($this->file_ext=="jpg"){
			 imagejpeg($dst_img, $target_thumb_storage, 100);
		}else if($this->file_ext=="png"){
			 imagepng($dst_img, $target_thumb_storage);
		}else{
			 imagegif($dst_img, $target_thumb_storage);
		}
		imagedestroy($dst_img);



        return $result;
    }

    public function insertImg(){
		$res = $this->db->query("INSERT INTO `photo` (`domain_id`,`mode`, `item_id`, `usr_id`, `title`, `summary`, `rename`, `ext`, `path`, `adddate`) VALUES ('{$this->domain_id}','{$this->mode}', '{$this->id}', '{$this->usr_id}', '{$this->title}', '{$this->summary}', '{$this->file_rename}', '{$this->file_ext}', '{$this->path}', UNIX_TIMESTAMP())");
		return $res;
	}
}
?>
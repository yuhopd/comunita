<?php
require_once("../_classes/class.DBConnection.php");

class PhotoList{
    private static $db;

	private $domain_id = 1;
	private $mode = "";
	private $item_id = 0;
	private $usr_id = 0;

	private $page = 1;
	private $perPage = 20;
	private $keyword = '';

	/**
	 * @return void
	 * @param  array  $properties
		 $properties = array ('domain_id','mode','item_id','usr_id','page','perPage','keyword');
	 */
	function __construct($properties) {
		$this->db = DBConnection::get()->handle();
		foreach ($properties as $key => $value){
			$this->{$key} = $value;
		}
	}

	/**
	 * Get Filtering Condition
     * @param   string  $isCount "count"  value can be used when just get total-count.
	 * @return  string  $sql
	 */
	private function getCondition($isCount = null) {
        if($isCount == 'count'){
			$order_by = "";
		}else{
			switch ($this->browse) {
	    		default: $order_by = " ORDER BY `adddate` DESC";
			}
		}

        switch ($this->type) {


            case 'usrImg':
				if($this->usr_id > 0){
					$where[] = " `usr_id` = '{$this->usr_id}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT *";
				if($isCount == "count"){
					$from_query = "FROM `photo` ".$where_sql;
				}else{
					$from_query = "FROM `photo` ".$where_sql.$order_by;
				}
			break;

			default:

				//$where[] = " `domain_id` = '{$this->domain_id}'";
				$where[] = " `mode` = '{$this->mode}'";
				$where[] = " `item_id` = '{$this->item_id}'";
				if($this->usr_id > 0){
					$where[] = " `usr_id` = '{$this->usr_id}'";
				}
				$where_sql = (is_array($where)) ? " WHERE ".implode(" AND ", $where) : "";
				$select_fields = "SELECT *";
				if($isCount == "count"){
					$from_query = "FROM `photo` ".$where_sql;
				}else{
					$from_query = "FROM `photo` ".$where_sql.$order_by;
				}
			break;
		}

		$select_query = ($isCount == "count") ? "SELECT count(*) AS count " : $select_fields;

		$query = $select_query.$from_query;
		return $query;
	}

	/**
	 * Get Groups List
	 * @return array $gList
	 * @param  int   $page
	 */
	public function getList() {
			//global $db;
		$from  = ($this->page-1)*$this->perPage;
		$query = $this->getCondition();
		$query = $query." LIMIT ".$from.", ".($from+$this->perPage);
		$res = $this->db->query($query);
		$rows = $res->fetchAll(PDO::FETCH_ASSOC);
		$list = array();
		foreach ($rows as $row) {
			$list[] = $row;
		}
		return $list;
	}

	/**
	 * Get Groups Count
	 * @return int $count
	 */
	public function getTotalCount() {
		$query  = $this->getCondition("count");
		$res = $this->db->query($query);
		$row = $res->fetch(PDO::FETCH_ASSOC);
		return $row['count'];
	}
}
?>
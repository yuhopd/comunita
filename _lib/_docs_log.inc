<?php
require_once 'Log.php';


/**
 * log the specific infomation into file.
 * @param $p_sLog	log sentence
 * @param $p_sLogID	the default value is "docs_default".
 * 					this value is used for the log file name. If you want
 * 					to log at specific file, set this value.
 */
function docs_log($p_sLog, $p_sLogID = 'docs_default'){
	$LOG_DIR_PATH = "/Users/jason/PhpstormProjects/himh/log/";
	$file_location = $LOG_DIR_PATH.$p_sLogID.".txt";
	@docs_readyFile($file_location);
	$logger = &Log::singleton('file', $file_location, $p_sLogID);
	if(isset($p_sLog)){
		@$logger->log($p_sLog);
	} 
}

function docs_readyFile($p_sPhysicalFileLocation){
	mkdir($LOG_DIR_PATH);
	
	if((filesize($p_sPhysicalFileLocation) / 1048576) > 10){
		unlink($p_sPhysicalFileLocation);
	}
	touch($p_sPhysicalFileLocation);
}
?>
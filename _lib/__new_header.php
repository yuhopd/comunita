<?php
require_once("../_lib/common_function.php");
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.MessageManager.php");
session_start();
$db =  DBConnection::get()->handle();
if(!$_GET["theme"]){ $_GET["theme"] = "default";  }
$sel_path = explode('/',$_SERVER['REQUEST_URI']);
$top_menu = array('menu','menu','menu','menu','menu','menu','menu','menu');

if($sel_path[1] == 'calendar'){
	$top_menu[0] = 'active';
}else if($sel_path[1] == 'contact'){
	$top_menu[1] = 'active';
}else if($sel_path[1] == 'attendance'){
	$top_menu[2] = 'active';
}else if($sel_path[1] == 'report'){
	$top_menu[3] = 'active';
}else if($sel_path[1] == 'analysis'){
	$top_menu[4] = 'active';
}else if($sel_path[1] == 'mypage'){
	$top_menu[5] = 'active';
}
header("Content-type: text/html; charset=utf-8");
if($_SESSION['sys_id']){
	$strNoMessageRead = "";
	$noMessageRead = MessageManager::getNoReadCount($_SESSION['sys_id']);
    if($noMessageRead > 0){
		$strNoMessageRead = "(".$noMessageRead.")";
	}
}
?>


<?php
    $html_metas["title"] = (isset($custom_title)) ? $custom_title : "himh.net";
    $html_metas["description"] = (isset($custom_description)) ? $custom_description : "himh.net, iles anytime, anywhere.";
    $html_metas["keywords"] = (isset($custom_keywords)) ? $custom_keywords : "CRM, Online web office";
    $html_metas["author"] = (isset($custom_author)) ? $custom_author : "himh";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HIMH | Contacts</title>

	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="YES" />
	<meta name="description" content="<?=$html_metas['description']?>" />
    <meta name="keywords" content="<?=$html_metas['keywords']?>" />
    <meta name="author" content="<?=$html_metas['author']?>" />

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <link href="../css/plugins/jsTree/style.min.css" rel="stylesheet">


	<link rel="stylesheet" type="text/css" href="../css/common.css" />
	<link rel="stylesheet" type="text/css" href="../css/attendance.css" />
	<link rel="stylesheet" type="text/css" href="../css/redmond/jquery-ui-1.7.2.custom.css" />


	<?=$html_css?>



	<script type="text/javascript" src="../js/build/require-min.js" data-main="../js/himh/Main"></script>
	<?=$html_js?>



    <script type="text/javascript">
        /*
		jQuery(document).ready(function() {
			$("#SearchContact").submit(function() {
				$.contact.search();
				return false;
			});

			$("#usr_dialog").dialog({
				autoOpen: false,
				width: 400,
				height: 'auto',
				modal:true,
				open: function() { },
				close: function() { $("#usr_dialog").empty(); },
				title: "회원정보",
				position: {my:"top", at:"top"}
			});

			$("#sub_dialog").dialog({
				autoOpen: false,
				width: 400,
				height: 'auto',
				position: {my:"top", at:"top" },
				modal:true,
				open: function() { },
				close: function() { $("#sub_dialog").empty(); },
				title: ""
			});

			$("#search_dialog").dialog({
				autoOpen: false,
				width: 300,
				height: 'auto',
				position: {my:"top", at:"top" },
				modal:true,
				open: function() { },
				close: function() { $("#search_dialog").empty(); },
				title: ""
			});

			$("#dialog").dialog({
				autoOpen: false,
				resizable: true,
				position: {my:"top", at:"top" },
				width: 450,
				height: 'auto',
				open: function() { },
				close: function() { $("#dialog").empty();  }
			});
			//$("select, input:checkbox, input:radio").uniform();
        });
        */
	</script>

</head>


<body>
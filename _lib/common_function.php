<?php
//-----------------------------------------------------------------//

#   public   : error(string text)
#   function : 에러 함수
#   text     : 오류 메세지 
function error($text){
echo"
<script language=javascript>
    window.alert('$text');
    history.go(-1);
</script>
";
exit; 
}

function error_go($text){
echo"
<script language=javascript>
    window.alert('$text');
    history.go(-1);
</script>
";
exit; 
}
function error_close($text){
echo"
<script language=JavaScript>
    window.alert('$text');
    window.close();
</script>
";
exit; 
}
    
function alert($text){
echo"
<script language=JavaScript>
    window.alert('$text');
</script>
";
}


function _encode($str){
    return iconv("euc-kr", "utf-8",$str);
	//return $str;
}

function _decode($str){
    return iconv("utf-8", "euc-kr",$str);
}

function ray_encode($str){
    return iconv("euc-kr", "utf-8",$str);
}

//-----------------------------------------------------------------//

#   public   : Copyright()
#   function : 카피 라이트
function Copyright(){}

//-----------------------------------------------------------------//

#   public   : getmicrotime()
#   function : 마이크로 타이밍을 구하는 함수
function getmicrotime() {
    $microtimestmp = split(" ",microtime());
    return $microtimestmp[0]+$microtimestmp[1];
}

//-----------------------------------------------------------------//
function _FileType($ext) {
        $ext = strtolower($ext);
        switch( $ext ) {
            case "gif":
            case "jpg":
            case "bmp":
            case "png":
            case "rle":
            case "tif":
            case "pcx":
                return "image";
                break;
            case "avi":
            case "mpg":
            case "mpeg":
            case "asf";
            case "asx";
            case "rm";
            case "ram";
            case "ra";
                return "avi";
                break;
            case "html":
            case "htm":
                return "html";
                break;
            case "txt":
            case "c":
            case "cc":
            case "cpp":
            case "js":
            case "php":
            case "php3":
            case "php4":
                return "txt";
                break;
			case "xls";
			case "xlsx";
			    return "xls";
				break;
			case "doc";
			case "docx";
			    return "doc";
				break;
			case "ppt";
			case "pptx";
			    return "ppt";
				break;
			case "hwp";
			    return "hwp";
				break;
			case "pdf";
			    return "pdf";
				break;
			case "psd";
			    return "psd";
				break;
			case "fla";
			    return "fla";
				break;
			case "swf";
			    return "swf";
				break;
            case "arj":
            case "zip":
            case "tar":
            case "tgz":
            case "arc":
            case "lzh":
            case "rar":
            case "cab":
            case "ace":
            case "gzip":
            case "gz";
                return "zip";
                break;
            default:
                return "unknown";
                break;
        }
    }


function cleanupHtml($value) {
    $cleanValue = htmlentities($value);
    return $cleanValue;
}

function cleanupDB($post) {
	if (get_magic_quotes_gpc()){
        $post = stripslashes($post);
	}
    $cleanPost = mysql_real_escape_string($post);
    return $cleanPost;
}

function setLang() {
    if($_POST["selLang"]){
		@setcookie("selLang","$_POST[selLang]", time()+60*60*24*30,"/","himh.net");  // 30 day
		$_SESSION["sys_lang"] = $_POST["selLang"];
	}else if($_COOKIE["selLang"]){
        $_SESSION["sys_lang"] = $_COOKIE["selLang"];
	}else{
        $_SESSION["sys_lang"] = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	}
}

/**
 * return multi-language date html code
 * @global array $_POST
 * @param integer $adddate unix timestamp value
 * @param string $type flag for return language
 * @return string $adddate
 */
function getMultiLangDate($adddate, $type) {
    global $_POST;
    if ($type == "MjY")
    {
        $adddate = date("Y년 n월 j일", $adddate);
        //$adddate = date("Y年 n月 j日", $adddate);
        //$adddate = date("M j, Y", $adddate);
    }
    else if ($type == "FjY")
    {
        $adddate = date("Y년 n월 j일", $adddate);
        //$adddate = date("Y年 n月 j日", $adddate);
        //$adddate = date("F j, Y", $adddate);
    }
    else if ($type == "mdYagi")
    {
        $adddate = date("Y년 n월 j일 a g:i", $adddate);
        //$adddate = date("Y年 n月 j日 a g:i", $adddate);
        //$adddate = date("j-n-Y a g:i", $adddate);
    }
    else if ($type == "FjY")
    {
        $adddate = date("Y년 n월 j일", $adddate);
        //$adddate = date("Y年 n月 j日", $adddate);
        //$adddate = date("F j, Y", $adddate);
    }
    else if ($type == "FjYagi")
    {
        $adddate = date("Y년 n월 j일 a g:i", $adddate);
        //$adddate = date("Y年 n月 j日 a g:i", $adddate);
        //$adddate = date("F j, Y a g:i", $adddate);
    }
    return $adddate;
}


/**
 * update document's description to mater DB
 * @param integer  $total    Number of items to page (used only if itemData is not provided).
 * @param integer  $perpage  Number of items to display on each page.
 * @param string   $url      name of the page, with a "%d" if append == TRUE.
 * @return string  $pager    Returns back/next/first/last and page links
 */
function getPaging($total,$perpage=10, $url, $delta=4, $urlVar="page")
{
	global $tr;
    require_once ("../_lib/Pager/Pager.php");
	$pager_options = array(
		'mode'			    => 'Sliding',
		'append'            => false,  //don't append the GET parameters to the url
		'path'              => '',
		'fileName'          => $url,  //Pager replaces "%d" with the page number...
		'perPage'		    => $perpage,
		'delta'			    => $delta,
		'urlVar'		    => $urlVar,
		'totalItems'	    => $total,
		'prevImg'		    => "<span class='prev'>&lt</span>" ,
		'nextImg'		    => "<span class='next'>&gt</span>",
		'altFirst'		    => "page %d",
		'altPrev'		    => "이전 페이지",

		'curPageSpanPre'	=> "<span class='selected'>",
		'curPageSpanPost'	=> "</span>",
		//'curPageLinkClassName' => "selected",
		'firstPagePre'		=> "<span class='first'>",
		'firstPageText'	    => "&lt&lt",
		'firstPagePost'	    => "</span>",

		'lastPagePre'		=> "<span class='last'>",
		'lastPageText'	    => "&gt&gt",
		'lastPagePost'	    => "</span>",

		'altNext'			=> "다음 페이지",
		'altLast'			=> "page %d",
		'separator'		    => "<span class='bar'></span>",
		'spacesBeforeSeparator'	=> 1,
		'spacesAfterSeparator'	=> 1,
		'clearIfVoid '		=> true
	);

	$pager = Pager::factory($pager_options);
    return $pager->links;
}

/**
 * encrypt text by custom keyword
 * @param string $txt text to encrypt
 * @return string $tmp
 */
function dx_encrypt($txt) {
	$key = "qhdks$cjfwj!";
	$key_len = strlen($key);
	$rot_ptr_set = 9;
	$rot_ptr = $rot_ptr_set;

	$tmp = "";
	$txt = strrev($txt);
	$txt_len = strlen($txt);
	for ($i=0; $i<$txt_len; $i++) {
		if($rot_ptr >= $key_len) $rot_ptr = 0;
		$tmp .= $txt[$i] ^ $key[$rot_ptr];
		$v = ord($tmp[$i]);
		$tmp[$i] = chr(((($v << 3) & 0xf8) | (($v >> 5) & 0x07)));
		$rot_ptr ++;
	}
	$tmp = base64_encode($tmp);
	$tmp = strrev($tmp);
	return $tmp;
}

/**
 * decrypt text by custom keyword
 * @param string $txt text to encrypt
 * @return string $tmp
 */
function dx_decrypt($txt) { $key = "qhdks$cjfwj!"; $key_len = strlen($key);
	$rot_ptr_set = 9;
	$rot_ptr = $rot_ptr_set;

	$tmp = "";
	$txt = strrev($txt);
	$txt = base64_decode($txt);
	$txt_len = strlen($txt);
	for ($i=0; $i<$txt_len; $i++) {
		if($rot_ptr >= $key_len) $rot_ptr = 0;
		$v = ord($txt[$i]);
		$txt[$i] = chr(((($v >> 3) & 0x1f) | (($v << 5) & 0xe0)));
		$tmp .= $txt[$i] ^ $key[$rot_ptr];
		$rot_ptr ++;
	}
	$tmp = strrev($tmp);
	return $tmp;
}


function getSundaysFromYear($year,$week){
	$startTimestamp = mktime(0, 0, 0, 1, 1, $year);
	$startDayFromWeek = date("w",$startTimestamp);
	if($startDayFromWeek > 0){
		$gap = (7-$startDayFromWeek) + $week;
		$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
	}
	for($i=0;$i<52;$i++){
		$arrayDate[$i] =  date("Y-m-d",strtotime("+{$i} week",$startTimestamp));
	}

	return $arrayDate;
}
?>
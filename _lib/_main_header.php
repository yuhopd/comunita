<?php
require_once("../_lib/common_function.php");
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.MessageManager.php");
session_start();
$db =& DBConnection::get()->handle();
if(!$_GET["theme"]){ $_GET["theme"] = "default";  }
$sel_path = explode('/',$_SERVER['REQUEST_URI']);
$top_menu = array('menu','menu','menu','menu','menu','menu','menu','menu');

if($sel_path[1] == 'calendar'){
	$top_menu[0] = 'menu_sel';
}else if($sel_path[1] == 'attendance'){
	$top_menu[1] = 'menu_sel';
}else if($sel_path[1] == 'sms'){
	$top_menu[2] = 'menu_sel';
}else if($sel_path[1] == 'analysis'){
	$top_menu[3] = 'menu_sel';
}else if($sel_path[1] == 'message'){
	$top_menu[4] = 'menu_sel';
}else if($sel_path[1] == 'admin'){
	$top_menu[5] = 'menu_sel';
}
header("Content-type: text/html; charset=utf-8");
if($_SESSION['sys_id']){ 
	$strNoMessageRead = "";
	$noMessageRead = MessageManager::getNoReadCount($_SESSION['sys_id']);
    if($noMessageRead > 0){
		$strNoMessageRead = "(".$noMessageRead.")";
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<?php
    $html_metas["title"] = (isset($custom_title)) ? $custom_title : "himh.net";
    $html_metas["description"] = (isset($custom_description)) ? $custom_description : "himh.net, iles anytime, anywhere.";
    $html_metas["keywords"] = (isset($custom_keywords)) ? $custom_keywords : "CRM, Online web office";
    $html_metas["author"] = (isset($custom_author)) ? $custom_author : "himh";
?>
    <title>himh.net</title>   
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="YES" />




	<meta name="description" content="<?=$html_metas['description']?>" />
    <meta name="keywords" content="<?=$html_metas['keywords']?>" />
    <meta name="author" content="<?=$html_metas['author']?>" />

	<link href="/css/redmond/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/_themes/<?=$_GET["theme"]?>/theme.css" rel="stylesheet"  type="text/css" />
	<?=$html_css?>

	<script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
	<script src="/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
	<script src="/js/common.js" type="text/javascript"></script>
	<script src="/js/jquery.common.js" type="text/javascript"></script>
	<script src="../js/visit/jquery.visit.js" type="text/javascript"></script>
	<?=$html_js?>
	
	<script type="text/javascript">
		$(function() {		
			$("#usr_dialog").dialog({
				bgiframe: true,
				autoOpen: false,
				width: 400,
				height: 'auto',
				open: function() { },
				close: function() { $("#usr_dialog").empty(); },
				title: "회원정보",
				position: ["center",100],
				modal: true
			});


			$("#sub_dialog").dialog({
				bgiframe: true,
				autoOpen: false,
				width: 400,
				height: 'auto',
				open: function() { },
				close: function() { ("#sub_dialog").empty(); },
				title: "회원정보"
			});
		});
	</script>
</head>
<body>

<div class="header">
    <div class="topNav">
		<ul class="top_nav">
			<li class="logo"><img src="../images/logo.png" style="height:35px;" /></li>

			<li class="<?=$top_menu[0]?>"><a href="#">서비스안내</a></li>
			<li class="<?=$top_menu[1]?>"><a href="#">서비스신청</a></li>
			<li class="<?=$top_menu[2]?>"><a href="#">himh교적</a></li>
			<li class="<?=$top_menu[3]?>"><a href="#">himhSMS</a></li>
			<li class="<?=$top_menu[3]?>"><a href="#">고객센터</a></li>

			<!-- li class="<?=$top_menu[4]?>"><a href="../message/index.php">쪽지 <span id="topNoMessageRead"><?=$strNoMessageRead?></span></a></li -->
			<!-- li class="<?=$top_menu[5]?> ui-corner-tl ui-corner-tr"><a href="../mybox/index.php">내 폴더</a></li -->
			<!-- li class="<?=$top_menu[6]?> ui-corner-tl ui-corner-tr"><a href="../mybox/index.php?mode=share">공유된 폴더</a></li -->
			<!-- li class="<?=$top_menu[7]?>"><a href="../share/index.php">친구</a></li -->

			
			<?php if($_SESSION['sys_id']){ 
			$thumb = UsrManager::getUsrThumb($_SESSION['sys_id']);
			if($thumb){
			?>
			<li id="top_usr_img" class="usr_thumb">
				<div class="usr_img" onClick="$.common.usr_info(<?=$_SESSION['sys_id']?>);" style="background-image:url(../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>_small.<?=$thumb['ext']?>);">&nbsp;</div>
			</li>
			<?php }else{ ?>
			<li id="top_usr_img" class="usr_thumb">	
				<div class="usr_img" onClick="$.common.usr_info(<?=$_SESSION['sys_id']?>);"></div>
			</li>
			<?php } ?>
			<li class="info">
				<div class="email" id="top_usr_name">
					<a href="#" onClick="javascript:usr_info('<?=$_SESSION['sys_id']?>');return false;"><?=$_SESSION['sys_name']?></a> (<?=$_SESSION['sys_email']?>)
				</div>
				<div class="signout">
					<a href="../signin/signout.php">로그아웃</a>&nbsp;
					<a href="../user/index.php">회원정보</a>
				</div>
			</li>
			<?php
			}else{
			?>
			<li class="info">
				<a href="../signin/?mode=signin">로그인</a>&nbsp;
				<a href="../signin/?mode=signup">회원가입</a>
			</li>
			<?php 
			}
			?>
		</ul>
	</div>
	<div class="topLine"></div>
</div>
<div class="container">
	<div class="contents" id="main_box">
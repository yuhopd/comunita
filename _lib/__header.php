<?php
require_once("../_lib/common_function.php");
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.MessageManager.php");
session_start();
$db =  DBConnection::get()->handle();
if(!$_GET["theme"]){ $_GET["theme"] = "default";  }
$sel_path = explode('/',$_SERVER['REQUEST_URI']);
$top_menu = array('menu','menu','menu','menu','menu','menu','menu','menu');

if($sel_path[1] == 'calendar'){
	$top_menu[0] = 'active';
}else if($sel_path[1] == 'contact'){
	$top_menu[1] = 'active';
}else if($sel_path[1] == 'attendance'){
	$top_menu[2] = 'active';
}else if($sel_path[1] == 'report'){
	$top_menu[3] = 'active';
}else if($sel_path[1] == 'analysis'){
	$top_menu[4] = 'active';
}else if($sel_path[1] == 'mypage'){
	$top_menu[5] = 'active';
}
header("Content-type: text/html; charset=utf-8");
if($_SESSION['sys_id']){
	$strNoMessageRead = "";
	$noMessageRead = MessageManager::getNoReadCount($_SESSION['sys_id']);
    if($noMessageRead > 0){
		$strNoMessageRead = "(".$noMessageRead.")";
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
    $html_metas["title"] = (isset($custom_title)) ? $custom_title : "himh.net";
    $html_metas["description"] = (isset($custom_description)) ? $custom_description : "himh.net, iles anytime, anywhere.";
    $html_metas["keywords"] = (isset($custom_keywords)) ? $custom_keywords : "CRM, Online web office";
    $html_metas["author"] = (isset($custom_author)) ? $custom_author : "himh";
?>
    <title>himh.net</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="YES" />
	<meta name="description" content="<?=$html_metas['description']?>" />
    <meta name="keywords" content="<?=$html_metas['keywords']?>" />
    <meta name="author" content="<?=$html_metas['author']?>" />


	<link rel="stylesheet" type="text/css" href="../css/reset.css" />
	<link rel="stylesheet" type="text/css" href="../css/main.css" />
	<link rel="stylesheet" type="text/css" href="../css/attendance.css" />

	<link rel="stylesheet" type="text/css" href="../css/redmond/jquery-ui-1.7.2.custom.css" />
	<link rel="stylesheet" type="text/css" href="../js/lightbox/themes/default/jquery.lightbox.css" />
	<!--[if IE 6]>
	<link rel="stylesheet" type="text/css" href="../js/lightbox/themes/default/jquery.lightbox.ie6.css" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/typography.css" />
	<!--[if lt IE 9]>  <script src="../js/html5.js"></script>  <![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/common.css" />
	<link rel="stylesheet" type="text/css" href="../js/fineuploader/fineuploader.css" />
	<?=$html_css?>


	<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="../js/jquery-ui-1.9.1.custom.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?scale=2&sensor=false"></script>

	<script type="text/javascript" src="../js/visit/jquery.visit.js"></script>
	<script type="text/javascript" src="../js/jquery.common.js"></script>
	<script type="text/javascript" src="../js/jquery.sms.js"></script>

	<script type="text/javascript" src="../js/contact/jquery.contact.js"></script>
	<script type="text/javascript" src="../js/form_elements.js"></script>
	<script type="text/javascript" src="../js/fineuploader/jquery.fineuploader-3.0.min.js"></script>
	<script type="text/javascript" src="../js/lightbox/jquery.lightbox.js"></script>
	<script type="text/javascript" src="../js/jquery.photo.js"></script>
	<script type="text/javascript" src="../js/jquery.family.js"></script>
	<script type="text/javascript" src="../js/jquery.message.js"></script>
	<script type="text/javascript" src="../js/loStorage/lostorage.js" charset="utf-8"></script>



	<script type="text/javascript">
    $(function() {
		$("a.lightbox").lightbox();
	});
	</script>
	<?=$html_js?>



	<script type="text/javascript">
		jQuery(document).ready(function() {
			$("#SearchContact").submit(function() {
				$.contact.search();
				return false;
			});

			$("#usr_dialog").dialog({
				autoOpen: false,
				width: 400,
				height: 'auto',
				modal:true,
				open: function() { },
				close: function() { $("#usr_dialog").empty(); },
				title: "회원정보",
				position: {my:"top", at:"top"}
			});

			$("#sub_dialog").dialog({
				autoOpen: false,
				width: 400,
				height: 'auto',
				position: {my:"top", at:"top" },
				modal:true,
				open: function() { },
				close: function() { $("#sub_dialog").empty(); },
				title: ""
			});

			$("#search_dialog").dialog({
				autoOpen: false,
				width: 300,
				height: 'auto',
				position: {my:"top", at:"top" },
				modal:true,
				open: function() { },
				close: function() { $("#search_dialog").empty(); },
				title: ""
			});

			$("#dialog").dialog({
				autoOpen: false,
				resizable: true,
				position: {my:"top", at:"top" },
				width: 450,
				height: 'auto',
				open: function() { },
				close: function() { $("#dialog").empty();  }
			});
			//$("select, input:checkbox, input:radio").uniform();
		});
	</script>
</head>
<body <?=$html_body_tag?> >
<!--Header-->
<header>
    <!--Logo-->
    <div id="logo"><a href="/"><img src="../images/logo.png" alt="" /></a></div>
    <!--Search-->
    <div class="header_search">
        <form id="SearchContact">
            <input type="search" name="keyword" placeholder="연락처 검색" />
            <a class="submit" href="javascript:$.contact.search();"></a>
        </form>
    </div>
</header>
<!--Dreamworks Container-->
<div id="container">
    <!--Primary Navigation-->
    <nav id="primary_nav">
        <ul>
			<li class="<?=$top_menu[0]?> nav_calendar <?=$top_menu[0]?>"><a href="../calendar/index.php">달력</a></li>
            <li class="<?=$top_menu[1]?> nav_contact"><a href="../contact/index.php">주소록</a></li>
            <li class="<?=$top_menu[2]?> nav_attendance"><a href="../attendance/index.php">출석부</a></li>
			<?php if($_SESSION['sys_level'] > 3){?>
			<li class="<?=$top_menu[3]?> nav_report"><a href="../report/index.php">보고서</a></li>
			<?php } ?>
			<li class="<?=$top_menu[5]?> nav_dashboard"><a href="../mypage/index.php?mode=message">My Page</a></li>

			<!-- li class="<?=$top_menu[5]?> nav_stolage"><a href="../storage/index.php">자료실</a></li -->
        </ul>
    </nav>
	<!--Main Content-->
	<section id="main_content">

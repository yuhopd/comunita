var config = global.config;
var Promise = require('promise');
var db = require("odbc")(),
cn = "DRIVER="+config.DB.DRIVER+";SERVER="+config.DB.HOST+";PORT="+config.DB.PORT+";DB="+config.DB.DATABASE+";UID="+config.DB.USER+";PWD="+config.DB.PASSWORD;
db.openSync(cn);
var odbcConnection = function () {

    this.actionQuery = function(query){
        return new Promise(function (resolve, reject) {
            db.query(query, [], function(err) {
                if(err){
                    reject(err);
                }else{
                    resolve();
                }
            });
        });
    };

    this.oneQuery = function(query){
        return new Promise(function(resolve, reject){
            db.query(query, function (err, rows, fields) {
                if(err){
                    reject(err);
                }else{
                    if(rows.length > 0){
                        resolve(rows[0]);
                    }else{
                        reject(false);
                    }
                }
            });
        });
    };


    this.getQuery = function(query){
        return new Promise(function(resolve, reject){
            db.query(query, function (err, rows, fields) {
                if(err){
                    reject(err);
                }else{
                    if(rows.length > 0){
                        resolve(rows[0]);
                    }else{
                        reject(false);
                    }
                }
            });
        });
    };

    this.listQuery = function(query){
        return new Promise(function(resolve, reject){
            db.query(query, function (err, rows, fields) {
                if(err){
                    reject(err);
                }else{
                    resolve(rows);
                }
            });
        });
    };

};
module.exports = new odbcConnection();
 





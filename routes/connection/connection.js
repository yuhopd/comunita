var fs = require('fs');
var path = require('path');
var initSqlJs = require('../sqljs/sql-wasm.js');
var Promise = require('promise');
var config = global.config;

var dbfilepath = path.resolve("db/kukucms.db");
console.log(dbfilepath);
console.log(config);

var sqlitConnection = function () {
    this.actionQuery = function(query){

            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                db.exec(query);
                const data = db.export();
                const buffer = new Buffer.from(data);
                fs.writeFileSync(dbfilepath, buffer);
                db.close();
                resolve();
            }, function(dbError){
                reject(dbError);
            });
        });
    };


    this.oneQuery = function(query){
        var _this = this;
        return new Promise(function(resolve, reject){
            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                var contents = db.exec(query);

                if(contents.length > 0){
                    var row =  _this.paserData(contents);
                    resolve(row[0][0]);
                }else{
                    reject(false);
                }
                db.close();
            }, function(dbError){
                reject(dbError);
            });
        });
    };


    this.getQuery = function(query){
        var _this = this;
        return new Promise(function(resolve, reject){
            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                var contents = db.exec(query);

                if(contents.length > 0){
                    var row =  _this.paserData(contents);
                    resolve(row[0]);
                }else{
                    reject(false);
                }
                db.close();
            }, function(dbError){
                reject(dbError);
            });
        });
    };

    this.listQuery = function(query){
        var _this = this;
        return new Promise(function (resolve, reject) {
            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                var contents = db.exec(query);
                if(contents.length > 0){
                    var list =  _this.paserData(contents);
                    resolve(list);
                }else{
                    reject(false);
                }
                db.close();
            }, function(dbError){
                reject(dbError);
            });
        });
    };


    this.listPageQuery = function(query, countQuery){
        var _this = this;
        return new Promise(function (resolve, reject) {
            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                var contents = db.exec(query);
                var _count = db.exec(countQuery);

                if(contents.length > 0){
                    var list  = _this.paserData(contents);
                    var row   = _this.paserData(_count);

                    list.total = row[0]['count'];
                    resolve(list);
                }else{
                    reject(false);
                }
                db.close();
            }, function(dbError){
                reject(dbError);
            });
        });
    };




    this.getGroupListQuery = function(){
        var _this = this;
        return new Promise(function (resolve, reject) {
            initSqlJs().then(function(SQL){
                const filebuffer = fs.readFileSync(dbfilepath);
                const db = new SQL.Database(filebuffer);
                var items = [];
                var query1 = "SELECT * FROM `group` WHERE `parent_id`='0' AND `is_enable`='O' ORDER BY `sequence` ASC";
                var contents1 = db.exec(query1);
                if(contents1.length > 0){
                    var rows1 = _this.paserData(contents1);
                    rows1.forEach(function(row1){
                        var query2 = "SELECT * FROM `group` WHERE `parent_id`='"+row1.id+"' AND `is_enable`='O' ORDER BY `sequence` ASC";
                        var contents2 = db.exec(query2);
                        if(contents2.length > 0){
                            row1.child = [];
                            var rows2 = _this.paserData(contents2);
                            rows2.forEach(function(row2){
                                var query3 = "SELECT * FROM `group` WHERE `parent_id`='"+row2.id+"' AND `is_enable`='O' ORDER BY `sequence` ASC";
                                var contents3 = db.exec(query3);
                                if(contents3.length > 0){
                                    row2.child = [];
                                    var rows3 = _this.paserData(contents3);
                                    rows3.forEach(function(row3){

                                        var query4 = "SELECT * FROM `group` WHERE `parent_id`='"+row3.id+"' AND `is_enable`='O' ORDER BY `sequence` ASC";
                                        var contents4 = db.exec(query4);
                                        if(contents4.length > 0){
                                            row3.child = [];
                                            var rows4 = _this.paserData(contents4);
                                            rows4.forEach(function(row4){
                                                row3.child.push(row4);
                                            });
                                        }
                                        row2.child.push(row3);
                                    });
                                }
                                row1.child.push(row2);
                                
                            });
                        }
                        items.push(row1);
                    });
                }

                

                resolve(items);
                db.close();
            }, function(dbError){
                reject(dbError);
            });
        });
    };


    this.paserData = function(data){
        var col = data[0].columns;
        var val  = data[0].values;
        var rows = [];
        var ff = 0;
        val.forEach(function(k){
            var idx = 0;
            var row = {};
            col.forEach(function(key){
                row[key] = val[ff][idx];
                idx++;
            });
            rows.push(row);
            ff++;
        });
        return rows;
    };
};
module.exports = new sqlitConnection();
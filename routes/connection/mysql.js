var config = global.config;
var Promise = require('promise');
var mysql   = require('mysql');

var connection = mysql.createConnection({
  host     : config.DB.HOST,
  user     : config.DB.USER,
  password : config.DB.PASSWORD,
  database : config.DB.DATABASE
});



var mysqlConnection = function () {
    this.actionQuery = function(query){
        return new Promise(function (resolve, reject) {
            connection.query(query, [], function(dbError) {
                if(dbError){
                    reject(dbError);
                }else{
                    resolve();
                }
            });
        });
    };

    this.getQuery = function(query){
        return new Promise(function(resolve, reject){
            connection.query(query, function (dbError, rows, fields) {
                if(dbError){
                    reject(dbError);
                }else{
                    if(rows.length > 0){
                        resolve(rows[0]);
                    }else{
                        reject(false);
                    }
                }
            });
        });
    };

    this.listQuery = function(query){
        return new Promise(function(resolve, reject){
            connection.query(query, function (dbError, rows, fields) {
                if(dbError){
                    reject(dbError);
                }else{
                    resolve(rows);
                }
            });
        });
    };

};
module.exports = new mysqlConnection();
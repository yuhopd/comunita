const express = require('express');
const router = express.Router();
const fso = require('fs');
const fs = require('fs-extra');
const path = require('path');
const multer = require('multer');
const uploadRootPath = require('app-root-path');
      uploadRootPath.setPath(path.resolve('public/data'))
const middlePostParser = multer({dest: uploadRootPath.resolve('/temp')});
const Util = require('./lib/Util');
const CryptoJS = require('crypto-js');
const moment = require('moment');
const request = require('request');

const DBConnection = require('./dao/DBConnection');
const TableManager = require('./dao/TableManager_mysql');

const { NONAME } = require('dns');

var getRandomNumberFromDate = function(){
    const d = new Date();
    const date = d.toISOString().split('T')[0];
    const time = d.toTimeString().split(' ')[0].replace(/:/g, '-');
    const microtime = d.getMilliseconds();
    const random = Math.floor(Math.random() * 1000) + 100;
    return `${date}${time}${microtime}${random}`;
};


new DBConnection("mysql",{
    host: "192.168.0.115", 
    port: "3306",
    user: "root", 
    password: "kukudocs", 
    database: "yuhoshop",
    // dbfilepath : "storage_db/kukudocs.db"
}, async function(db){
    
//- admin login --------------------------------------------------------------------------------------
    router.get('/admin/', async function(req, res, next) {
        if(req.session.userid){
            res.redirect('/admin/index');
            res.end();
        }else{
            res.redirect('/admin/login');
            res.end();
        }
    });

    router.get('/admin/login', async function(req, res, next) {
        if(req.session.userid){
            res.redirect('/admin/index');
            res.end();
        }else{
            res.render('admin/signin.html', {
                "title":'yuhomall Admin'
            });
        }
    });

    router.post('/admin/loginAction', async function(req, res, next) {
        var uid = req.body['uid'];
        var psw = req.body['psw'];
            const _usr = new TableManager(db,'usr',{search:["name","email"]});
            const row = await _usr.getItemFrom([{key:"email",val:uid}]);
            psw = CryptoJS.MD5(psw).toString();
            
            console.log(row);
   
            if(row['psw'] == psw){
                
                req.session.userid ={
                    "uid" : uid,
                    "usr_id" : row.id,
                    "name" : row.name,
                    "email" : row.email,
                    "level" : row.level,
                    "createCurTime" : new Date()
                };

                console.log(req.session.userid);


                res.send({
                    "error" : -1,
                    "usr" : row
                });
                res.end();
            }else{
                req.session.destroy();
                res.send({
                    "error" : 100,
                    "usr" : {}
                });
                res.end();
            }
      
    });

    router.get('/admin/logout', async function(req, res, next) {
        req.session.destroy();
        res.redirect('/admin/login');
        res.end();
    });

    router.get('/admin/index', async function(req, res, next) {
        if(req.session.userid){
            res.render('admin/index.html', {
                "title":'yuhomall Admin',
                "session" : req.session.userid
            });
        }else{
            res.redirect('/admin/login');
            res.end();
        }
    });

    router.get('/admin/home', async function(req, res, next) {
        if(req.session.userid){
            const _category = new TableManager(db,'board_category',{search:[]});
            const _slideCategory = new TableManager(db,'slide_category',{search:[]});
            const slist = await _slideCategory.getList({});
            const blist = await _category.getList({});    
            res.send({
                "boardList" : blist,
                "slideList" : slist,
                "session" : req.session.userid
            });
        }else{
            res.redirect('/admin/login');
            res.end();
        }
    });

//- Shop ---------------------------------------------------------------------------------------------
    router.get('/admin/shopOrderList', async function(req, res, next) {
        
        var status = req.query.status;
        var page = req.query.page;
        var q = req.query.q;
        var pay_method = req.query.pay_method;
        var start_date = req.query.start_date;
        var end_date = req.query.end_date;

        if(!q){ q = ""; }
        if(!page){ page = 1;}
        if(!pay_method){ pay_method = "all";}
        if(!start_date){start_date = "";}
        if(!end_date){end_date = "";}


            const _bill = new TableManager(db,'bill',{search:['title','summary','content']});
            const _bill_product = new TableManager(db,'bill_product',{search:[]});
            const _product = new TableManager(db,'product',{search:[]});
            const _product_file = new TableManager(db,'product_file',{search:[]});
            var rows = await _bill.getList({
                "status":status,
                "islimit" : "O",
                "page" : page,
                "perPage" : 20
            });
            var items = [];
            if(rows){
                for(row of rows){
                    row.reg_date = moment.unix(row.adddate).format('YYYY-MM-DD HH:mm');
                    const _b_p = await _bill_product.getList({
                        "islimit" : "X",
                        kv:[{"key":"bill_id", "val":row.id}]
                    });

                    var plist = [];
                    if(_b_p){
                        for (row2 of _b_p){
                            var product =  await _product.getItem(row2.product_id);
                            var _thumb = "";
                            var thumb =  await _product_file.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'product_id','val':row2.product_id}]);

                            if(thumb == false){
                                _thumb = "../admin/assets/img/new-gallery-img.png";
                            }else{
                                _thumb = "../data/product/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
                            }
                            row2.thumb = _thumb;
                            row2.product = product;
                            plist.push(row2);
                        }
                    }
                    row.plist = plist;
                    items.push(row);
                }
            }
            var total = await _bill.getTotalCount({"status":status});
            res.send({
                "pay_method" : pay_method,
                "status" : status,
                "q" : q,
                "orderList" : items,
                "total": total,
                "start_date" : start_date,
                "end_date" : end_date
            }); 
    });

//- Order Detail -------------------------------------------------------------------------------------
    router.get('/admin/orderDetail', async function(req, res, next) {
        var q = req.query.q;
        var o_code = req.query.o_code;

        const _bill = new TableManager(db,'bill',{search:['title','summary','content']});
        const _bill_product = new TableManager(db,'bill_product',{search:[]});
        const _product = new TableManager(db,'product',{search:[]});
        const _option = new TableManager(db,'product_option',{search:[]});
        const _product_file = new TableManager(db,'product_file',{search:[]});

        var bills = await _bill.getItemFrom([{'key':'code', 'val':o_code}]);

        var b_p_list = await _bill_product.getList({kv:[{'key':'bill_id','val':bills.id}]});

        var productInfo = [];
        var optionInfo = [];
        var thumb_img = [];
        for (b_p of b_p_list){
            var product = await _product.getItemFrom([{'key':'id', 'val':b_p.product_id}]);

            var option = await _option.getList({kv:[{'key':'product_id','val':b_p.product_id}]});
            
            var _thumb = "";
            var thumb = await _product_file.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'product_id','val':b_p.product_id}]);

            if(thumb == false){
                _thumb = "../admin/assets/img/new-gallery-img.png";
            }else{
                _thumb = "../data/product/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
            }

            productInfo.push(product);
            optionInfo.push(option);
            thumb_img.push(_thumb)
        }


        res.send({
            "q" : q,
            "bills" : bills,
            "b_p_list" : b_p_list,
            "productInfo" : productInfo,
            "optionInfo" : optionInfo,
            "thumb_img" : thumb_img
        });

    });

    router.post('/admin/changeOrderStatus', async function(req, res, next) {
        var body = req.body;
        const _bill = new TableManager(db,'bill',{search:[]});

        var param1 = {
            'id' : body['b_id'],
            'status' : body['newStatus'],
            'trans_num' : body['n_trans_num']
        }
        var param2 = {
            'id' : body['b_id'],
            'status' : body['newStatus']
        }
        
        if(body.n_trans_num){
            const data1 = await _bill.modify(param1);
            res.send({
                "result" : data1
            });
        res.end();
        } else {
            const data2 = _bill.modify(param2);
            res.send({
                "result" : data2
            });
            res.end();
        }
    });

//- Product ------------------------------------------------------------------------------------------
    router.get('/admin/shopProductList', async function(req, res, next) {
        var page = req.query.page;
        var q = req.query.q;
        var mode = req.query.mode;
        var group_id = req.query.group_id;

        const _product = new TableManager(db,'product',{search:['id','code','title']});
        const _product_file = new TableManager(db,'product_file',{search:[]});
        const _product_group = new TableManager(db, 'product_group',{search:[]});
        const _group = new TableManager(db,'group',{search:[]});

        if(!page){ page = 1;}
        if(!q){ q = ""; }
        // default s
        if(mode == "default"){
            var rows = await _product.getList({
                "islimit" : "O",
                "page" : page,
                "perPage" : 20,
            });
            var groups = await _group.getList({})
        
            var items = [];
            if(rows){
                for(row of rows){
                    var _thumb = "";
                    var thumb = await _product_file.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'product_id','val':row.id}]);
        
                    if(thumb == false){
                        _thumb = "../admin/assets/img/new-gallery-img.png";
                    }else{
                        if(thumb.path && thumb.rename && thumb.ext){
                            _thumb = "../data/product/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
                        }else{
                            _thumb = "../admin/assets/img/new-gallery-img.png";
                        }

                    }
                    row.thumb = _thumb;
        
                    var _p_groups = await _product_group.getList({
                        kv:[{
                            'key':'product_id',
                            'val':row.id
                        }]
                    });
        
                    var groups2 = []
                    for(_p_group of _p_groups){
                        var _groups = await _group.getItem(_p_group.group_id);
                        groups2.push(_groups)
                    };
                    row.groups = groups2;
        
                    items.push(row);
                };
            }
        
            var total = await _product.getTotalCount({});

            res.send({
                "q" : q,
                "productList" : items,
                "groupList" : groups,
                "total": total,
                "sequence" : "none",
            });
            res.end();
                    }
        // default e
        // // category s
        else if(mode == "category"){
            var groups = await _group.getList({})
            var filterGroup = await _product_group.getList({
                "islimit" : "O",        
                "page" : page,
                "perPage" : 20,
                "browse" : "sequence",
                "asc" : "ASC",
                kv : [
                    {
                        "key" : "group_id",
                        "val" : group_id
                    }
                ]
            });
        
            var items = [];
            for(groupproduct of filterGroup){
                var _f_id = groupproduct.product_id;
                var row = await _product.getItem(_f_id);
                if(row){
                    var _thumb = "";
                    var thumb = await _product_file.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'product_id','val':row.id}]);

                    if(thumb == false){
                        _thumb = "../admin/assets/img/new-gallery-img.png";
                    }else{
                        _thumb = "../data/product/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
                    }
                    row.thumb = _thumb;

                    var _p_groups = await _product_group.getList({
                        kv:[{
                            'key':'product_id',
                            'val':row.id
                        }]
                    });

                    var groups2 = []
                    for(_p_group of _p_groups){
                        var _groups = await _group.getItem(_p_group.group_id);
                        groups2.push(_groups)
                    };
                    row.groups = groups2;
        
                    items.push(row);
                }
            }

            var total = await _product_group.getTotalCount({
                kv : [
                    {
                        "key" : "group_id",
                        "val" : group_id
                    }
                ]
            });
            
            res.send({
                "q" : group_id,
                "productList" : items,
                "groupList" : groups,
                "total": total,
                "sequence" : "inline"
            });
            res.end();
        
        }
        // category e
        // search s
        else if(mode == "search"){
            const rows = await _product.getList({
                "islimit" : "O",
                "page" : page,
                "perPage" : 20,
                "q" : q
            })
            var groups = await _group.getList({})
        
            var items = [];
            if(rows){
                for(row of rows){
                    var _thumb = "";
                    var thumb = await _product_file.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'product_id','val':row.id}]);
        
                    if(thumb == false){
                        _thumb = "../admin/assets/img/new-gallery-img.png";
                    }else{
                        if(thumb.path && thumb.rename && thumb.ext){
                            _thumb = "../data/product/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
                        }else{
                            _thumb = "../admin/assets/img/new-gallery-img.png";
                        }

                    }
                    row.thumb = _thumb;
        
                    var _p_groups = await _product_group.getList({
                        kv:[{
                            'key':'product_id',
                            'val':row.id
                        }]
                    });
        
                    var groups2 = []
                    for(_p_group of _p_groups){
                        var _groups = await _group.getItem(_p_group.group_id);
                        groups2.push(_groups)
                    };
                    row.groups = groups2;
        
                    items.push(row);
                };
            }
        
            var total = await _product.getTotalCount({
                "q" : q
            });

            res.send({
                "q" : q,
                "productList" : items,
                "groupList" : groups,
                "total": total,
                "sequence" : "none"
            });
            res.end();
            
        }            
        // search e
    });

    router.post('/admin/productUpSequence', async function(req, res, next) {
        var gid = req.body._gid;
        var pid = req.body._pid;

        const _product_group = new TableManager(db, 'product_group',{search:[]});

        var clickedOne = await _product_group.getItemFrom([{'key':'group_id','val':gid},{'key':'product_id','val':pid}]);

        if(clickedOne !== false){
            var kv = [{"key" : "group_id", "val" : clickedOne.group_id}];
            var UpperOne = await _product_group.getUpSequence(kv, clickedOne.sequence);

            if(UpperOne !== false) {
                var param_row0 = {
                    'id'         : clickedOne.id,
                    'sequence'   : UpperOne.sequence,
                }
                var param_row1 = {
                    'id'         : UpperOne.id,
                    'sequence'   : clickedOne.sequence,
                }
                var row0 = await _product_group.modify(param_row0);
                var row1 = await _product_group.modify(param_row1);

                res.send({
                    "result" : row0,
                    "result" : row1,
                });
            }else{
                res.send("It's the top.");
                console.log("================================================top")
                console.log("It's the top.")
            }
        }   
    });

    router.post('/admin/productDownSequence', async function(req, res, next) {
        var gid = req.body._gid;
        var pid = req.body._pid;

        const _product_group = new TableManager(db, 'product_group',{search:[]});

        var clickedOne = await _product_group.getItemFrom([{'key':'group_id','val':gid},{'key':'product_id','val':pid}]);

        if(clickedOne !== false){
            var kv = [{"key" : "group_id", "val" : clickedOne.group_id}];
            var LowerOne = await _product_group.getDownSequence(kv, clickedOne.sequence);

            if(LowerOne !== false) {
                var param_row0 = {
                    'id'         : clickedOne.id,
                    'sequence'   : LowerOne.sequence,
                }
                var param_row1 = {
                    'id'         : LowerOne.id,
                    'sequence'   : clickedOne.sequence,
                }
                var row0 = await _product_group.modify(param_row0);
                var row1 = await _product_group.modify(param_row1);

                 res.send({
                    "result" : row0,
                    "result" : row1,
                });
            }else{
                res.send("It's the lowest.");
                console.log("================================================bottom")
                console.log("It's the lowest.")
            }
        }
    });

    router.get('/admin/productInsert', async function(req, res, next) {
        var _this = this;
        var temp_product_id = req.query.temp_product_id;
        const _group = new TableManager(db,'group',{search:[]});
        const _product = new TableManager(db,'product',{search:[]});


        var product_list = await _product.getList({});

        var items = [];
        var mode = req.query.mode;
        var productIcons = ["신상품", "추천", "기획", "인기", "이벤트", "예약", "베스트", "세일"];

        // 1depth
        var rows1 = await _group.getList({
            "browse" : "sequence",
            "asc" : "ASC",
            kv : [
                {
                    "key" : "is_enable",
                    "val" : "O"
                },
                {
                    "key" : "parent_id",
                    "val" : "0"
                }
            ]
        });

        // 2depth
        // if(rows1.length > 0) {
        //     for(i = 0; i < rows1.length; i++) {
        //         var rows2 = await _group.getList({
        //             "browse" : "sequence",
        //             "asc" : "ASC",
        //             kv : [
        //                 {
        //                     "key" : "is_enable",
        //                     "val" : "O"
        //                 },
        //                 {
        //                     "key" : "parent_id",
        //                     "val" : rows1[i].id
        //                 }
        //             ]
        //         });

        //         // 3depth
        //         if(rows2.length > 0) {
        //             var rows3 = new Array();
        //             rows1[i].child = [];
        //             for(a = 0; a < rows2.length; a++){
        //                 rows3 = await _group.getList({
        //                     "browse" : "sequence",
        //                     "asc" : "ASC",
        //                     kv : [
        //                         {
        //                             "key" : "is_enable",
        //                             "val" : "O"
        //                         },
        //                         {
        //                             "key" : "parent_id",
        //                             "val" : rows2[a].id
        //                         }
        //                     ]
        //                 });
        //                 // 4depth
        //                 if(rows3.length > 0) {
        //                     var rows4 = new Array();
        //                     rows2[a].child = [];
        //                     for(b = 0; b < rows3.length; b++){
        //                         rows4 = await _group.getList({
        //                             "browse" : "sequence",
        //                             "asc" : "ASC",
        //                             kv : [
        //                                 {
        //                                     "key" : "is_enable",
        //                                     "val" : "O"
        //                                 },
        //                                 {
        //                                     "key" : "parent_id",
        //                                     "val" : rows3[b].id
        //                                 }
        //                             ]
        //                         });
        //                         if(rows4.length > 0) {
        //                             rows3[b].child = [];
        //                             for(c=0; c < rows3.length; c++) {
        //                                 rows3[b].child.push(rows4[c]);
        //                             }
        //                         }
        //                         rows2[a].child.push(rows3[b]);
        //                     }
        //                 }
        //                 rows1[i].child.push(rows2[a]);
        //             }
        //         }
        //     }
        //     items.push(rows1);
        // }

        // 2depth
        if(rows1.length > 0) {
            var rows2 = new Array();
            for(row1 of rows1) {
                rows2 = await _group.getList({
                    "browse" : "sequence",
                    "asc" : "ASC",
                    kv : [
                        {
                            "key" : "is_enable",
                            "val" : "O"
                        },
                        {
                            "key" : "parent_id",
                            "val" : row1.id
                        }
                    ]
                });

                // 3depth
                if(rows2.length > 0) {
                    var rows3 = new Array();
                    row1.child = [];
                    for(row2 of rows2) {
                        rows3 = await _group.getList({
                            "browse" : "sequence",
                            "asc" : "ASC",
                            kv : [
                                {
                                    "key" : "is_enable",
                                    "val" : "O"
                                },
                                {
                                    "key" : "parent_id",
                                    "val" : row2.id
                                }
                            ]
                        });

                        // 4depth
                        if(rows3.length > 0) {
                            var rows4 = new Array();
                            row2.child = [];
                            for(row3 of rows3) {
                                rows4 = await _group.getList({
                                    "browse" : "sequence",
                                    "asc" : "ASC",
                                    kv : [
                                        {
                                            "key" : "is_enable",
                                            "val" : "O"
                                        },
                                        {
                                            "key" : "parent_id",
                                            "val" : row3.id
                                        }
                                    ]
                                });

                                if(rows4.length > 0) {
                                    row3.child = [];
                                    for(row4 of rows4) {
                                        row3.child.push(row4);
                                    };
                                }
                                row2.child.push(row3);
                            };
                        }
                        row1.child.push(row2);
                    };
                }
                items.push(row1);
            };
        }
        res.send({
            "mode" : mode,
            "temp_product_id" : temp_product_id,
            "row" : {},
            "productIcons": productIcons,
            "selectBoxItems" : items,
            "product_list" : product_list,
        });

    });

    router.post('/admin/productInsertAction', async function(req, res, next) {
        var body = req.body;
        var temp_product_id = body['product_id'];

        var param = {
            'title'      : body['title'],
            'icon'      : body['icons'],
            'is_display'      : body['is_display'],
            'code'    : body['code'],
            'keyword'    : body['keyword'],
            'stock'    : body['stock'],
            'cost'    : body['cost'],
            'stock'    : body['stock'],
            'price'    : body['price'],
            'sale_price'    : body['sale_price'],
            'point'    : body['point'],
            'summary'    : body['summary'],
            'content'    : body['content'],
            'hit'    : body['hit'] || "0",
            'vote'    : body['vote'] || "0",
            'delivery'    : body['delivery'] || "0",
            'sale_stock'    : body['sale_stock'] || "0",
            'lastUpdate'    : body['lastUpdate'] || "0",
        };

        const _product = new TableManager(db,'product',{search:[]});
        const _product_file = new TableManager(db,'product_file',{search:[]});
        const _product_group = new TableManager(db,'product_group',{search:[]});
        const _product_option = new TableManager(db,'product_option',{search:[]});

        //product 테이블에 데이터 삽입
        const data = await _product.insert(param);
        var product_id = await _product.getLastIndex();

        //product_file의 임시아이디를 실제 product_id로 변경
        const data1 = await _product_file.modifyFrom(
            {
                "product_id" : product_id
            },
            [{
                "key":"product_id",
                "val": temp_product_id
                }]
        );

        //product_group의 임시아이디를 실제 product_id로 변경
        const data2 = await _product_group.modifyFrom(
            {
                "product_id" : product_id
            },
            [{
                "key":"product_id",
                "val": temp_product_id
            }]
        );

        //product_option의 임시아이디를 실제 product_id로 변경
        const data3 = await _product_option.modifyFrom(
            {
                "product_id" : product_id
            },
            [{
                "key":"product_id",
                "val": temp_product_id
            }]
        );

        res.send({
            "message" : "success"
        });

    });

    router.get('/admin/productModify', async function(req, res, next) {
        var _this = this;
        var product_id = req.query.product_id;
        var mode = req.query.mode;

        const _group = new TableManager(db,'group',{search:[]});
        const _product = new TableManager(db,'product',{search:[]});
        const _product_group = new TableManager(db,'product_group',{search:[]});

        var selectedProduct = await _product.getItem(product_id);
        var product_group_list = await _product_group.getList({
            kv : [
                {
                    "key" : "product_id",
                    "val" : product_id
                }
            ]});

        var items = [];
        var productIcons = ["신상품", "추천", "기획", "인기", "이벤트", "예약", "베스트", "세일"];

        // 1depth
        var rows1 = await _group.getList({
            "browse" : "sequence",
            "asc" : "ASC",
            kv : [
                {
                    "key" : "is_enable",
                    "val" : "O"
                },
                {
                    "key" : "parent_id",
                    "val" : "0"
                }
            ]
        });

        // 2depth
        if(rows1.length > 0) {
            var rows2 = new Array();
            for(row1 of rows1) {
                rows2 = await _group.getList({
                    "browse" : "sequence",
                    "asc" : "ASC",
                    kv : [
                        {
                            "key" : "is_enable",
                            "val" : "O"
                        },
                        {
                            "key" : "parent_id",
                            "val" : row1.id
                        }
                    ]
                });

                // 3depth
                if(rows2.length > 0) {
                    var rows3 = new Array();
                    row1.child = [];
                    for(row2 of rows2) {
                        rows3 = await _group.getList({
                            "browse" : "sequence",
                            "asc" : "ASC",
                            kv : [
                                {
                                    "key" : "is_enable",
                                    "val" : "O"
                                },
                                {
                                    "key" : "parent_id",
                                    "val" : row2.id
                                }
                            ]
                        });

                        // 4depth
                        if(rows3.length > 0) {
                            var rows4 = new Array();
                            row2.child = [];
                            for(row3 of rows3) {
                                rows4 = await _group.getList({
                                    "browse" : "sequence",
                                    "asc" : "ASC",
                                    kv : [
                                        {
                                            "key" : "is_enable",
                                            "val" : "O"
                                        },
                                        {
                                            "key" : "parent_id",
                                            "val" : row3.id
                                        }
                                    ]
                                });

                                if(rows4.length > 0) {
                                    row3.child = [];
                                    for(row4 of rows4) {
                                        row3.child.push(row4);
                                    };
                                }
                                row2.child.push(row3);
                            };
                        }
                        row1.child.push(row2);
                    };
                }
                items.push(row1);
            };
        }
        res.send({
            "mode" : mode,
            "product_id" : product_id,
            "row" : selectedProduct,
            "productIcons": productIcons,
            "selectBoxItems" : items,
            "numOfAddedGroup" : product_group_list.length
        });

    });

    router.post('/admin/productModifyAction', async function(req, res, next) {
        var body = req.body;
        var product_id = body['product_id'];

        var param = {
            'id' : product_id,
            'title'      : body['title'],
            'icon'      : body['icons'],
            'is_display'      : body['is_display'],
            'code'    : body['code'],
            'keyword'    : body['keyword'],
            'stock'    : body['stock'],
            'cost'    : body['cost'],
            'stock'    : body['stock'],
            'price'    : body['price'],
            'sale_price'    : body['sale_price'],
            'point'    : body['point'],
            'summary'    : body['summary'],
            'content'    : body['content'],
            'hit'    : body['hit'] || "0",
            'vote'    : body['vote'] || "0",
            'delivery'    : body['delivery'] || "0",
            'sale_stock'    : body['sale_stock'] || "0",
            'lastUpdate'    : body['lastUpdate'] || "0",
        };

        const _product = new TableManager(db,'product',{search:[]});
        const _product_file = new TableManager(db,'product_file',{search:[]});
        const _product_group = new TableManager(db,'product_group',{search:[]});
        const _product_option = new TableManager(db,'product_option',{search:[]});

        //product 테이블에 데이터 삽입
        const data = await _product.modify(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);

        res.send({
            "message" : "success"
        });

    });

    router.get('/admin/productDeleteAction', async function(req, res, next) {
        var _this = this;

        var product_id = req.query.product_id;
        //product_option 테이블의 옵션 삭제
            const _product_option = new TableManager(db,'product_option',{search:[]});
            const rows1 = await _product_option.getList({
                kv:[
                    {
                        "key" : "product_id",
                        "val" : product_id
                    }
                ]
            });
            if(rows1) {
                for(row1 of rows1) {
                    const data1 = await _product_option.del(row1.id);
                }
            }

        //product_file 테이블의 파일 삭제
            const _product_file = new TableManager(db,'product_file',{search:[]});
            const rows2 = await _product_file.getList({
                kv:[
                    {
                        "key" : "product_id",
                        "val" : product_id
                    }
                ]
            });

            if(rows2) {
                for(row2 of rows2) {
                    //실제 파일 삭제
                    const fileName = row2.rename + "." + row2.ext;
                    const filePath = uploadRootPath + "/product/" + fileName;
                    fso.unlinkSync(filePath);

                    //파일 DB 삭제
                    const data2 = await _product_file.del(row2.id);
                }
            }

        //product_group 테이블의 그룹 삭제
            const _product_group = new TableManager(db,'product_group',{search:[]});
            const rows3 = await _product_group.getList({
                kv:[
                    {
                        "key" : "product_id",
                        "val" : product_id
                    }
                ]
            });
            if(rows3) {
                for(row3 of rows3) {
                    const data3 = await _product_group.del(row3.id);
                }
            }

        //product 테이블의 상품 삭제
            const _product = new TableManager(db,'product',{search:[]});
            const data4 = await _product.del(product_id);

        res.send({"message" : "Product Delete Success"});
    });

    router.post('/admin/productAddGroup', async function(req, res, next) {
        var body = req.body;
        const _product_group = new TableManager(db,'product_group',{search:[]});
        var kv = [{"key" : "product_id", "val" : body.product_id}];
        var maxSequence = await _product_group.getMaxSequence(kv);
        if(maxSequence){
            maxSequence += 1;
        }else{
            maxSequence = 1;
        }


        var param = {
            'group_id'   : body['group_id'],
            'product_id' : body['product_id'],
            'sequence'   : maxSequence,
        };

        var data = _product_group.insert(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
    });

    router.get('/admin/productDeleteGroup', async function(req, res, next) {
        var _this = this;

        var group_id = req.query.group_id;
        var product_id = req.query.product_id;

        const _product_group = new TableManager(db,'product_group',{search:[]});

        const row = await _product_group.getItemFrom([
            {
                "key":"product_id",
                "val": product_id
            },
            {
                "key":"group_id",
                "val": group_id
            }
        ]);
        const sequence = row.sequence;

        //그룹 삭제
        const data = await _product_group.del(row.id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);

        //삭제된 그룹의 시퀀스 관련하여 시퀀스 수정
        const list = await _product_group.getList({
            kv:[
                {"key" : "product_id", "val" : product_id},
                {"key" : "sequence", "opt" : ">", "val" : sequence}
            ]
        });

        if(list){
            for(i=0; i < list.length; i++) {
                var param = {
                    'id': list[i]['id'],
                    'sequence' : list[i]['sequence'] - 1
                }
                const data2 = await _product_group.modify(param);
                res.send({"result":data2}) // async & await 확인 필요

                // const buffer2 = new Buffer.from(data2);
                // fso.writeFileSync(dbfilepath, buffer2);
            }
        }
    });

    router.get('/admin/productGroupList', async function(req, res, next) {
        var product_id = req.query.product_id;
        var groupTitleList = [];
        const _product_group = new TableManager(db,'product_group',{search:[]});
        const _group = new TableManager(db,'group',{search:[]});

        var listGroup = await _product_group.getList({
            kv:[{
                "key" : "product_id",
                "val" : product_id
            }]
        });

        if(listGroup){
            for(i=0; i < listGroup.length; i++) {
                var row = await _group.getItem(listGroup[i]['group_id']);
                groupTitleList.push(row);
            }
        }

        res.send({
            "listGroup" : listGroup,
            "groupTitleList" : groupTitleList,
            "product_id" : product_id
        });

    });

    router.get('/admin/productInsertOption', async function(req, res, next) {
        var mode = req.query.mode;
        var temp_product_id = req.query.temp_product_id;
        res.send({
            "mode" : mode,
            "optionRow" : {},
            "temp_product_id" : temp_product_id
        });
    });

    router.post('/admin/productInsertOptionAction', async function(req, res, next) {
        var body = req.body;
        const _product_option = new TableManager(db,'product_option',{search:[]});
        var kv = [{"key" : "product_id", "val" : body.product_id}];
        var maxSequence = await _product_option.getMaxSequence(kv);
        maxSequence += 1;

        var param = {
            'product_id': body['product_id'],
            'mode'    : body['mode'],
            'title'     : body['title'],
            'stock': body['stock'],
            'price' : body['price'],
            'point' : body['point'],
            "sequence" : maxSequence
        };

        var data = await _product_option.insert(param);
        res.send({
            "result":data,
            "product_id" : body['product_id']
        }) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);

        // res.send({"product_id" : body['product_id']});
    });

    router.get('/admin/productModifyOption', async function(req, res, next) {
        var mode = req.query.mode;
        var option_id = req.query.option_id;
        var product_id = req.query.product_id;

        const _product_option = new TableManager(db,'product_option',{search:[]});
        var optionRow = await _product_option.getItem(option_id);

        res.send({
            "mode" : mode,
            "optionRow" : optionRow
        });
    });

    router.post('/admin/productModifyOptionAction', async function(req, res, next) {
        var body = req.body;
        const _product_option = new TableManager(db,'product_option',{search:[]});
        var param = {
            'id': body['option_id'],
            'product_id': body['product_id'],
            'title'     : body['title'],
            'stock': body['stock'],
            'price' : body['price'],
            'point' : body['point'],
        };

        var data = await _product_option.modify(param);
        res.send({
            "result":data,
            "product_id" : body['product_id']
        }) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);

        // res.send({"product_id" : body['product_id']});
    });

    router.get('/admin/productDeleteOptionAction', async function(req, res, next) {
        var _this = this;

        var option_id = req.query.option_id;
        var product_id = req.query.product_id;

        const _product_option = new TableManager(db,'product_option',{search:[]});
            
        const data = await _product_option.del(option_id);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
    });

    router.get('/admin/productOptionList', async function(req, res, next) {
        var product_id = req.query.product_id;

            const _product_option = new TableManager(db,'product_option',{search:[]});
            var list = await _product_option.getList({
                kv:[{
                    "key" : "product_id",
                    "val" : product_id
                }]
            });

            res.send({
                "optionList" : list,
                "product_id" : product_id
            });
    });

    router.get('/admin/productFileUpload', async function(req, res, next) {
        var product_id = req.query.product_id;
        res.send({"product_id": product_id});
    });

    router.post('/admin/productFileUploadAction', middlePostParser.any(), async function (req, res, next) {
        var body = req.body;
        var param = {
            product_id : 0,
            name : '',
            rename : '',
            ext : '',
            size : 0,
            adddate : 0,
            path : '',
            mode : ''
        };
        param.product_id = body['product_id'];
        param.size = body['file_size'];
        param.mode = body['mode'];
        if(param.mode == 'productUploadThumb'){
            param.mode = 'thumb';
        }else{
            param.mode = 'img';
        }
    
        
        var originalName = '';
        if (req.files && req.files[0] && req.files[0]['path']){
            _file = req.files[0];
            originalName = _file.originalname;
            var dotPos = originalName.lastIndexOf('.');
            param.name = originalName.substring(0, dotPos);
            if (dotPos > -1 && dotPos < originalName.length){
                param.ext = originalName.substring(dotPos+1);
            }
        }
    
        param.rename= getRandomNumberFromDate();

        var _product_file = new TableManager(db,'product_file',{search:[]});

        var writeFileName = param.rename + '.' + param.ext;
        var file = uploadRootPath + "/product/" + writeFileName;
        param.path = writeFileName;
        fs.move(_file.path, file, async function(err){
            if (err) {
                res.status(500).send(err);
            }else{
                console.log(param);
                var data = await _product_file.insert(param);
                res.send({
                    "result":data,
                    "param" : param
                })

                // var buffer = new Buffer.from(data);
                // fso.writeFileSync(dbfilepath, buffer);

                // res.send(param);
            }
        });
    });

    router.get('/admin/productFileList', async function(req, res, next) {
        var product_id = req.query.product_id;
        var mode = req.query.mode;
        console.log(mode);
        const _product_file = new TableManager(db,'product_file',{search:[]});
        let list = [];

        if(mode == "productUploadThumb" || mode == "thumb"){
            list = await _product_file.getList({
                kv:[
                    {'key':'product_id','val':product_id},
                    {'key':'mode','val': 'thumb'}
                ]
            });
        }else{
            list = await _product_file.getList({
                kv:[
                    {'key':'product_id','val':product_id},
                    {'key':'mode','val': 'img'}
                ]
            });
        }
        res.send({
            "product_id" : product_id,
            "list" : list
        }); 
    });

    router.get('/admin/productFileDelete', async function(req, res, next) {
        var _this = this;
        var file_id = req.query.file_id;

        const _product_file = new TableManager(db,'product_file',{search:[]});

        const row = await _product_file.getItem(file_id);

        //실제 파일 삭제
        const fileName = row.rename + "." + row.ext;
        const filePath = uploadRootPath + "/product/" + fileName;
        // const filePath = path.resolve("uploads/"+fileName);
        fso.unlinkSync(filePath);  // async & await 확인 필요 (officejs에서도 이 부분은 그대로 뒀음)

        //DB에서 삭제
        const data = _product_file.del(row.id);
        res.send({"result":data})  // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);

        res.send("ProductFile Delete Success");
    });

//- User ---------------------------------------------------------------------------------------------
    router.get('/admin/usrList', async function(req, res, next) {
        if(req.session.userid){
            var page = req.query.page;
            var q = req.query.q;
            var browse = req.query.browse;
            var asc = req.query.asc;

            if(!q){ q = ""; }
            if(!page){ page = 1;}
            if(!browse){ browse = '';}
            if(!asc){ asc = 'DESC';}


            const _usr = new TableManager(db,'usr',{search:["name","email","phone"]});
            
            let list = await _usr.getList({
                "islimit" : "O",
                "page" : page,
                "perPage" : 20,
                "browse" : browse,
                "asc" : asc,
                "q" : q
            });


            var items = [];
            if(list.length > 0){
                var _no = 1 + ((page - 1) * 20) ;
                for (row of list){
                    var _thumb  = "";
                    row.no = _no;
                    items.push(row);
                    _no++;
                }
            }

            let total = await _usr.getTotalCount({
                "browse" : browse,
                "asc" : asc,
                "q" : q
            });

            res.send({
                "title":'yuhomall Admin',
                "list" : items,
                "total" : total,
                "browse" : browse,
                "asc" : asc,
                "q" : q,
                "session" : req.session.userid
            });
            res.end();
        }
    });

    // router.get('/admin/usrInsert', function(req, res, next) {

    //         const _branch = new TableManager(db,'branch',{search:[]});
    //         const branchList = _branch.getList({});
    //         res.send({
    //             "mode" : "insert",
    //             "row" : {},
    //             "branchList" : branchList,
    //             "session" : req.session.userid
    //         });

    // }); 

    // router.post('/admin/usrInsertAction', function (req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'branch_id' : body['branch_id'],
    //         'level'     : body['level'],
    //         'name'      : body['name'],
    //         'phone'     : body['phone'],
    //         'birth'     : body['birth'],
    //         'email'     : body['email'],
    //         'gender'    : body['gender'],
    //         'height'    : body['height'],
    //         'weight'    : body['weight'],
    //         'summary'   : body['summary']
    //     };



    //         const _table = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const data = _table.insert(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    router.get('/admin/usrModify', async function(req, res, next) {
        var id = req.query.id;
        
            
        
            const _usr = new TableManager(db,'usr',{search:["name","email","phone"]});
            const _files = new TableManager(db,'files',{search:[]});
            const row = await _usr.getItem(id);

            var _thumb = "";
            var thumb = await _files.getItemFrom([{"key":"mode","val":"usr_img"},{"key":"item_id","val":id}]);
            if(thumb !== false){
                _thumb = "../data/usr_img/" + thumb.path + "/" + thumb.rename + "_thumb." + thumb.ext;
            }else{
                _thumb = "images/member.png";   
            }
            row.thumb = _thumb;
            res.send({
                "mode" : "modify",
                "row" :row,
                "session" : req.session.userid
            });
    }); 

    // router.post('/admin/usrModifyInfoAction', function (req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'id'        : body['usr_id'],
    //         'branch_id' : body['branch_id'],
    //         'name'      : body['name'],
    //         'birth'     : body['birth'],
    //         'lunar'     : body['lunar'],
    //         'phone'     : body['phone'],
    //         'email'     : body['email'],
    //         'gender'    : body['gender'],
    //         'register'  : body['register'],
    //         'height'    : body['height'],
    //         'weight'    : body['weight'],
    //         'level'     : body['level'],
    //         'address'   : body['address']
    //     };


    //         const _table = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const data = _table.modify(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.post('/admin/usrModifyAction', function (req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'id'        : body['usr_id'],
    //         'name'      : body['name'],
    //         'phone'     : body['phone'],
    //         'email'     : body['email'],
    //         'birth'     : body['birth'],
    //         'gender'    : body['gender']
    //     };

    //         const _table = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const data = _table.modify(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.get('/admin/usrPswModify', function(req, res, next) {
    //     var id = req.query.id;

    //         const _usr = new TableManager(db,'usr',{search:[]});
    //         const row = _usr.getItem(id);
    //         res.send({
    //             "usr_id" : id,
    //             "row" : row
    //         });
    //         
    //         res.end();

    // }); 

    // router.post('/admin/usrPswModifyAction', function (req, res, next) {
    //     var body = req.body;

    //     var _psw = body['psw'];
    //     var _psw_confirm = body['psw_confirm'];
    //     if(_psw == _psw_confirm){
    //         var param = {
    //             'id'        : body['id'],
    //             'psw'       : CryptoJS.MD5(_psw).toString(),
    //             'level'     : body['level']
    //         };
    

    //             const _table = new TableManager(db,'usr',{search:["name","email","phone"]});
    //             const data = _table.modify(param);
    //             const buffer = new Buffer.from(data);
    //             fso.writeFileSync(dbfilepath, buffer);

    //             res.send({
    //                 "usr_id" : body['id'],
    //                 "error" : -1,
    //                 "message" : "비밀번호가 업데이트 되었습니다. "
    //             });
    //             
    //             res.end();

    //     }else if(_psw == ""){
    //         res.send({
    //             "error" : 500,
    //             "message" : "비밀번호를 입력하세요!"
    //         });
    //     }else{
    //         res.send({
    //             "error" : 500,
    //             "message" : "비밀번호가 일치하지 않습니다."
    //         });
    //         res.end();
    //     }
       
    // });

    // router.get('/admin/usrImgModify', function(req, res, next) {
    //     var id = req.query.id;
    //         const _usr = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const row = _usr.getItem(id);
    //         const _file = new FilesManager(db);
    //         const thumb = _file.getList(id,'usr_img');
    //         res.send({
    //             "title":'yuhomall Admin',
    //             "mode" : "modify",
    //             "row" : row,
    //             "thumb" : thumb,
    //             "id" : id,
    //             "session" : req.session.userid
    //         });
    //         
    //         res.end();
    // }); 

    // router.post('/admin/usrImgUploadAction', middlePostParser.any(), function(req, res, next) {
    //     var body = req.body || {};
    //     var param = {
    //         'mode'     : body['mode'],
    //         'item_id'  : body['id']
    //     }

    //     var originalName = '';
    //     if (req.files && req.files[0] && req.files[0]['path']){
    //         param['file'] = req.files[0];
    //         originalName = param.file.originalname;

    //         param['file_name'] = originalName;
    //         var dotPos = originalName.lastIndexOf('.');
    //         if (dotPos > -1 && dotPos < originalName.length){
    //             param['file_ext'] = originalName.substring(dotPos+1);
    //             // param['file_ext'] = param.ext.toLowerCase();
    //         }
    //     }
    //     var fileExt = param['file_ext'];
    //     var randomNumber = Util.getRandomNumberFromDate();
    //     var writeDirPath = uploadRootPath.resolve(param.mode+"/"+randomNumber.yearMonth);
    //     var fileRename = randomNumber.randomNumber;
    //     var writeFileName = fileRename + '.' + fileExt;
    //     var file = uploadRootPath.resolve(param.mode+"/"+randomNumber.yearMonth+"/"+writeFileName);
    //     var file_thumb = uploadRootPath.resolve(param.mode+"/"+randomNumber.yearMonth+"/"+ fileRename + '_thumb.' + fileExt);



    //         const _usr_img = new TableManager(db,'usr_img',{search:[]});

    //         fs.ensureDir(writeDirPath, function(err){
    //             fs.move(param['file'].path, file, function(err){
    //                 if (err) {
    //                     res.status(500).send(err);
    //                     res.end();
    //                 } else {

    //                     sizeOf(file,function(err, dimensions) {
    //                     // console.log(dimensions.width, dimensions.height);

    //                         var _w = dimensions.width;
    //                         var _h = dimensions.height;

    //                         param['width'] = _w;
    //                         param['height'] = _h;
    //                         var percent_resizing = 100;
    //                         if(_w > _h){
    //                             sharp(file).resize({width:percent_resizing}).toFile(file_thumb);
    //                         }else{
    //                             sharp(file).resize({height:percent_resizing}).toFile(file_thumb);
    //                         }

    //                         let data;
    //                         var row = _usr_img.getItemFrom([{"key":"usr_id","val":body['id']}]);
    //                         if(row == false){
    //                             var _array = {
    //                                 'rename' : fileRename,
    //                                 'ext' : fileExt,
    //                                 'usr_id' : body['id'],
    //                                 'path' : randomNumber.yearMonth
    //                             };  
    //                             data = _usr_img.insert(_array);
    //                         }else{

    //                             var _array = {
    //                                 'id' : row.id,
    //                                 'rename' : fileRename,
    //                                 'ext' : fileExt,
    //                                 'usr_id' : row.usr_id,
    //                                 'path' : randomNumber.yearMonth
    //                             };

    //                             data = _usr_img.modify(_array);
    //                         }


    //                         const buffer = new Buffer.from(data);
    //                         fso.writeFileSync(dbfilepath, buffer);
    //                         
    //                         res.end();
    //                     });
                    
    //                 }
    //             });
    //         });

    // });

    // router.get('/admin/usrDeleteAction', function (req, res, next) {
    //     var id = req.query.id;

    //         const _table = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const data = _table.del(id);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.get('/admin/usrAttendanceList', function(req, res, next) {
    //     var usr_id = req.query.usr_id;

    //         const _usr = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         const usr = _usr.getItem(usr_id);
    //         const _attendance = new TableManager(db,'attendance',{search:[]});
    //         const _branch = new TableManager(db,'branch',{search:[]});
    //         const _period = new TableManager(db,'period',{search:[]});
    //         const list = _attendance.getList({"kv":[{"key":'usr_id',"val":usr_id}],"browse":"id","asc":"DESC"});
    //         var items = [];
    //         if(list.length > 0){
    //             list.forEach(function(row){
    //                 var period = _period.getItem(row.period_id);
    //                 var branch = _branch.getItem(row.branch_id);
    //                 row.branch = branch;
    //                 row.period = period;
    //                 items.push(row);
    //             });
    //         }
    //         res.send({
    //             "branch_id" : usr.branch_id,
    //             "usr_id" : usr_id,
    //             "list" : items
    //         });

    // }); 

    // router.get('/admin/usrPaymentList', function(req, res, next) {
    //     var usr_id = req.query.usr_id;


    //         // 이용권 확인 ------------------------------------------------------------------------------------------------------------------
    //         let useVoucher = chackVoucher(db, usr_id, "X");
    //         // 이용권 확인 ------------------------------------------------------------------------------------------------------------------


    //         const _payment = new TableManager(db,'voucher',{search:[]});
    //         const list = _payment.getList({"kv":[{"key":'usr_id',"val":usr_id}],"asc":"DESC"});
    //         res.send({
    //             "usr_id" : usr_id,
    //             "list" : list,
    //             "voucher" : useVoucher
    //         });

    // }); 

    // router.get('/admin/usrNoteList', function(req, res, next) {

    //     if(req.session.userid){
    //         var usr_id = req.query.usr_id;
    //         var writer_id = req.session.userid.usr_id;

    //             const _notice = new TableManager(db,'notice',{search:[]});
    //             const list = _notice.getList({"kv":[{"key":'usr_id',"val":usr_id}],"asc":"DESC"});
    //             res.send({
    //                 "usr_id" : usr_id,
    //                 "writer_id" : writer_id,
    //                 "list" : list
    //             });
    //             
    //             res.end();

    //     }
    // }); 

    // router.get('/admin/usrNoticeInsert', function(req, res, next) {
    //     var usr_id = req.query.usr_id;
    //     var writer_id = req.query.writer_id;
    //     res.send({
    //         "mode" : "insert",
    //         "usr_id" : usr_id,
    //         "writer_id" : writer_id,
    //         "row" : {}
    //     });

    //     res.end();
    // });

    // router.post('/admin/usrNoticeInsertAction', function(req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'usr_id'    : body['usr_id'],
    //         'writer_id' : body['writer_id'],
    //         'title'     : body['title'],
    //         'content'   : body['content']
    //     };


    //         const _notice = new TableManager(db,'notice',{search:[]});
    //         const data = _notice.insert(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.get('/admin/usrNoticeModify', function(req, res, next) {

    //     var id = req.query.id;

    //         const _notice = new TableManager(db,'notice',{search:[]});
    //         const row = _notice.getItem(id);

    //         res.send({
    //             "mode" : "modify",
    //             "row" : row
    //         });
    //         

    // });
    
    // router.post('/admin/usrNoticeModifyAction', function(req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'id'        : body['id'],
    //         'usr_id'    : body['usr_id'],
    //         'writer_id' : body['writer_id'],
    //         'title'     : body['title'],
    //         'content'   : body['content']
    //     };

    //         const _notice = new TableManager(db,'notice',{search:[]});
    //         const data = _notice.modify(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.get('/admin/usrNoticeDeleteAction', function(req, res, next) {
    //     var id = req.query.id;

    //         const _notice = new TableManager(db,'notice',{search:[]});
    //         const data = _notice.del(id);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         
    //         res.end();

    // });

    // router.get('/admin/usrVoucherModify', function(req, res, next) {
    //     var id = req.query.id;

    //         const _user = new TableManager(db,'usr',{search:[]});
    //         const row = _user.getItem(id);
    //         res.send({
    //             "mode" : "modify",
    //             "usr" : row
    //         });
    //         
    //         res.end();

    // });

    // router.post('/admin/usrVoucherModifyAction', function(req, res, next) {
    //     var body = req.body;
    //     var param = {
    //         'id'          : body['id'],
    //         'pay_type'    : body['pay_type'],
    //         'pay_count'   : body['pay_count'],
    //         'pay_expired' : body['pay_expired']
    //     };
    //         const _usr = new TableManager(db,'usr',{search:[]});
    //         const data = _usr.modify(param);
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         res.send({
    //             "error" : -1,
    //             "msg" : "이용권 정보가 수정되었습니다. "
    //         });
    //         
    //         res.end();


    // });

    // router.get('/admin/usrCardModify', function(req, res, next) {
    //     var usr_id = req.query.usr_id;

    //         const _usr = new TableManager(db,'usr',{search:[]});
    //         const _card = new TableManager(db,'usr_card',{search:[]});
    //         const usr = _usr.getItem(usr_id);
    //         const row = _card.getItemFrom([{"key":"usr_id","val":usr_id}]);
    //         if(row == false){
    //             res.send({
    //                 "mode" : "insert",
    //                 "usr" : usr,
    //                 "row" : {}
    //             });
    //         }else{
    //             res.send({
    //                 "mode" : "modify",
    //                 "usr" : usr,
    //                 "row" : row
    //             });
    //         }
    //         
    //         res.end();
    // });

    // router.post('/admin/usrCardModifyAction', function(req, res, next) {
    //     var body = req.body;

    //     var param = {
    //         'usr_id'     : body['usr_id'],
    //         'cardNumb'   : body['cardNumb'],
    //         'cardCompany': body['cardCompany'],
    //         'expiryDate' : body['expiryDate'],
    //         'usrBirth'   : body['usrBirth'],
    //         'cardPassword' : body['cardPassword']
    //     };

    //     var _mm = param.expiryDate.substring(0, 2);
    //     var _yy = param.expiryDate.substring(2, 4);

    //     var expiry = _yy+_mm;

    //         const _card = new TableManager(db,'usr_card',{search:[]});
    //         let data;
    //         if(body['mode'] == "insert"){
    //             var parm = {
    //                 'usr_id'     : body['usr_id'],
    //                 'cardNumb'   : body['cardNumb'],
    //                 'cardCompany': body['cardCompany'],
    //                 'expiryDate' : body['expiryDate'],
    //                 'usrBirth'   : body['usrBirth']
    //             };
    //             data = _card.insert(parm);
    //         }else{
    //             var parm = {
    //                 'id'         : body['id'],
    //                 'usr_id'     : body['usr_id'],
    //                 'cardNumb'   : body['cardNumb'],
    //                 'cardCompany': body['cardCompany'],
    //                 'expiryDate' : body['expiryDate'],
    //                 'usrBirth'   : body['usrBirth']
    //             };
    //             data = _card.modify(parm);
    //         }
    //         const buffer = new Buffer.from(data);
    //         fso.writeFileSync(dbfilepath, buffer);
    //         res.send({
    //             code:"A0200",
    //             data:{
    //                 respMessage : "카드정보가 저장되었습니다."
    //             }
    //         });
    //         res.end();
                


       
    // });

    // router.post('/admin/usrCardModifyAction2', function(req, res, next) {
    //     var body = req.body;

    //     var param = {
    //         'usr_id'     : body['usr_id'],
    //         'cardNumb'   : body['cardNumb'],
    //         'expiryDate' : body['expiryDate'],
    //         'usrBirth'   : body['usrBirth'],
    //         'cardPassword' : body['cardPassword']
    //     };

    //     var _mm = param.expiryDate.substring(0, 2);
    //     var _yy = param.expiryDate.substring(2, 4);

    //     var expiry = _yy+_mm;



    //         var randomNumber = Util.getRandomNumberFromDate();
    //         var rename = randomNumber.randomNumber;
    
    //         var api_url = paymentDomain + "/api/v1/card/cert";
    //         var auth  = "pgapi MjY3MDIwMDAyMDpNQTAxOjkwQUM1NTI5N0IwMDY1QTFFRjU3QkRFQjE3NTVGMzgw";
    //         const options = {
    //             uri: api_url, 
    //             method: 'POST',
    //             headers : {
    //                 "Authorization" : auth
    //             },
    //             body: {
    //                 "mid": paymentMid,
    //                 "orderNumb": rename,
    //                 "userName": "T1646029486",
    //                 "userEmail": "",
    //                 "productType": "REAL",
    //                 "productName": "bodywithus",
    //                 "totalAmount": 100,
    //                 "taxFreeAmount": 0,
    //                 "payload": param.usr_id,
    //                 "interestType": "PG",
    //                 "cardNumb": param.cardNumb,
    //                 "expiryDate": expiry,
    //                 "installMonth": 0,
    //                 "currencyType": "KRW",
    //                 "password2" : param.cardPassword,
    //                 "userInfo" : param.usrBirth
    //             },
    //             json:true
    //         };
    
    //         console.log(options);
    //         request.post(options, function(err,httpResponse,body2){ 
    //             console.log(body2);

    //             if(body2.code == "A0200"){
    //                 const _card = new TableManager(db,'usr_card',{search:[]});
    //                 let data;
    //                 if(body['mode'] == "insert"){
    //                     var parm = {
    //                         'usr_id'     : body['usr_id'],
    //                         'cardNumb'   : body['cardNumb'],
    //                         'expiryDate' : body['expiryDate'],
    //                         'usrBirth'   : body['usrBirth']
    //                     };
    //                     data = _card.insert(parm);
    //                 }else{
    //                     var parm = {
    //                         'id'         : body['id'],
    //                         'usr_id'     : body['usr_id'],
    //                         'cardNumb'   : body['cardNumb'],
    //                         'expiryDate' : body['expiryDate'],
    //                         'usrBirth'   : body['usrBirth']
    //                     };
    //                     data = _card.modify(parm);
    //                 }
    //                 const buffer = new Buffer.from(data);
    //                 fso.writeFileSync(dbfilepath, buffer);

    //                 res.send(body2);
    //                 res.end();
    //             }else{
    //                 res.send(body2);
    //                 res.end();
    //             }
                
    //         });


       
    // });

    // router.get('/admin/usrCardDelete', function(req, res, next) {
    //     var id = req.query.id;

    //         const _card = new TableManager(db,'usr_card',{search:[]});
    //         const row = _card.getItem(id);
    //         if(row == false){
    //             res.send({
    //                 "mode" : "insert",
    //                 "usr" : {}
    //             });
    //         }else{
    //             res.send({
    //                 "mode" : "modify",
    //                 "usr" : row
    //             });
    //         }
    //         
    //         res.end();

    // });

    // router.get('/admin/getEventList', function(req, res, next) {
    //     const branch_id = req.query.branch_id;
    //     const month = req.query.month;
    //     const year = req.query.year;

    //         const _attendance = new TableManager(db,'attendance',{search:[]});
    //         let event = [];
    //         for(var i=1; i<=31 ; i++){
    //             var day = i+"";
    //             if(i<10){
    //                 day = "0"+i;
    //             }
    //             let count = _attendance.getTotalCount({kv:[{"key":"branch_id","val":branch_id},{"key":"c_year","val":year}, {"key":"c_month","val":month}, {"key":"c_day","val":day}]});
    //             if(count > 0){
    //                 let startday = year+"-"+month+"-"+day+" 00:00:00";
    //                 let endday = year+"-"+month+"-"+day+" 24:00:00";
    //                 let row = {
    //                     'dayType':'event',
    //                     "branch_id" : branch_id,
    //                     "title" : "출석 ("+count+")",
    //                     "start" : startday,
    //                     "end" : endday,
    //                     "color" : "#278ccf",
    //                     "textColor" : "#ffffff"
    //                 };
    //                 event.push(row);
    //             }
    //         }
    //         res.send({
    //             "events" : event
    //         });
    //         
    //         res.end();

    // });

    // router.get('/admin/getAttendanceList', function(req, res, next) {
    //     const branch_id = req.query.branch_id;
    //     const month = req.query.month;
    //     const year = req.query.year;

    //         const _attendance = new TableManager(db,'attendance',{search:[]});
    //         let event = [];
    //         let list;
    //         if(branch_id == "all"){
    //             list = _attendance.getList({kv:[{"key":"c_year","val":year}, {"key":"c_month","val":month}],"browse":"record_round","asc":"DESC"});
    //         }else{
    //             list = _attendance.getList({kv:[{"key":"branch_id","val":branch_id},{"key":"c_year","val":year}, {"key":"c_month","val":month}],"browse":"record_round","asc":"DESC"});
    //         }
            
    //         const _usr = new TableManager(db,'usr',{search:["name","email","phone"]});
    //         if(list.length > 0){
    //             list.forEach(function(row){
    //                 let bgcolor = "#343e4b";
    //                 if(row.status == "1"){
    //                     bgcolor = "#b85e80";
    //                 }else if(row.status == "2"){
    //                     bgcolor = "#7e91aa";
    //                 }else if(row.status == "3"){
    //                     bgcolor = "#4387bf";
    //                 }else if(row.status == "4"){
    //                     bgcolor = "#3b9ff3";
    //                 }
    //                 let usr = _usr.getItem(row.usr_id);
    //                 let startday = year+"-"+month+"-"+row.c_day+" 00:00:00";
    //                 let endday = year+"-"+month+"-"+row.c_day+" 24:00:00";


    //                 if(row.record_round == null){
    //                     row.record_round = 0;
    //                 }

    //                 if(row.evaluation == null){
    //                     row.evaluation = "";
    //                 }

    //                 let _row = {
    //                     'dayType':'event',
    //                     "branch_id" : branch_id,
    //                     "usr_id" : usr.id,
    //                     "title" : usr.name + " " + row.record_round + "R "+ row.evaluation,
    //                     "start" : startday,
    //                     "end" : endday,
    //                     "color" : bgcolor,
    //                     "attendance" : row,
    //                     "textColor" : "#ffffff"
    //                 };


    //                 if(row.status == "1"){
    //                     _row.title = usr.name + " 예약";
    //                 }
    //                 event.push(_row);
    //             });
    //         }
    //         res.send({
    //             "events" : event
    //         });
    //         
    //         res.end();

    // });



//- group --------------------------------------------------------------------------------------------
    router.get('/admin/groupList', async function(req, res, next) {
        var _this = this;
        const _group = new TableManager(db,'group',{search:[]});
        var items = [];
        // 1depth
        var rows1 = await _group.getList({
            "browse" : "sequence",
            "asc" : "ASC",
            kv : [
                {
                    "key" : "is_enable",
                    "val" : "O"
                },
                {
                    "key" : "parent_id",
                    "val" : "0"
                }
            ]
        });
        // console.log(rows1);
        // console.log(rows1.length);

        // 2depth
        if(rows1.length > 0) {
            var rows2 = new Array();
            for(row1 of rows1){
                // console.log(row1.id);
                rows2 = await _group.getList({
                    "browse" : "sequence",
                    "asc" : "ASC",
                    kv : [
                        {
                            "key" : "is_enable",
                            "val" : "O"
                        },
                        {
                            "key" : "parent_id",
                            "val" : row1.id
                        }
                    ]
                });

                // 3depth
                if(rows2.length > 0) {
                    var rows3 = new Array();
                    row1.child = [];
                    for(row2 of rows2){
                        rows3 = await _group.getList({
                            "browse" : "sequence",
                            "asc" : "ASC",
                            kv : [
                                {
                                    "key" : "is_enable",
                                    "val" : "O"
                                },
                                {
                                    "key" : "parent_id",
                                    "val" : row2.id
                                }
                            ]
                        });

                        // 4depth
                        if(rows3.length > 0) {
                            var rows4 = new Array();
                            row2.child = [];
                            for(row3 of rows3){
                                rows4 = await _group.getList({
                                    "browse" : "sequence",
                                    "asc" : "ASC",
                                    kv : [
                                        {
                                            "key" : "is_enable",
                                            "val" : "O"
                                        },
                                        {
                                            "key" : "parent_id",
                                            "val" : row3.id
                                        }
                                    ]
                                });
                                // console.log("===4depth sssss===")
                                // console.log(row3.id);
                                // console.log(rows4);
                                // console.log("===4depth eeeee===")

                                if(rows4.length > 0) {
                                    row3.child = [];
                                    for(row4 of rows4){
                                        row3.child.push(row4);
                                    };
                                }
                                row2.child.push(row3);
                            };
                        }
                        row1.child.push(row2);
                    };
                }
                items.push(row1);
            };
        }
        res.send(items);
    });

    router.get('/admin/groupModify/:id', async function(req, res, next) {
        var id = req.params.id;
        var mode = req.query.mode;

        const _group = new TableManager(db,'group',{search:[]});


        let row = await _group.getItem(id);


        row.head = Util.stripSlashes(row.head);

        res.send({
            "mode" : mode,
            "row" : row
        })
    });

    router.post('/admin/groupModifyAction', async function(req, res, next) {
        var body = req.body;
        console.log(body);
        var param = {
            'title'    : body['title'],
            'summary'  : body['summary'],
            'head'     : body['head'],
            'is_enable': body['is_enable'],
            'is_blank' : body['is_blank'],
            'id'       : body['id']
        };

        const _group = new TableManager(db,'group',{search:[]});
        // var group_id = await _group.getLastIndex();
        // console.log(group_id);

        //product_file의 임시아이디를 실제 product_id로 변경
        if(body['temp_file_groupId']){
            const _group_file = new TableManager(db,'group_files',{search:[]});
            const data1 = await _group_file.modifyFrom(
                {
                    "group_id" : body['id']
                },
                [{
                    "key":"group_id",
                    "val": body['temp_file_groupId']
                }]
            );
        }

        const data = await _group.modify(param);
        res.send({"result":data}) // async & await 확인 필
    });

    router.get('/admin/groupInsert/:parent_id', async function(req, res, next) {
        var parent_id = req.params.parent_id;
        var mode = req.query.mode;
        const _group = new TableManager(db,'group',{search:[]});

        if(parent_id != '0') {
            var row = await _group.getItem(parent_id);

            res.send({
                "mode" : mode,
                "parent_id" : parent_id,
                "row" : row
            });
        }else {
            var row = {
                title : '',
                summary : '',
                head : '',
                foot : '',
                is_enable : '',
                is_blank : '',
                id : '0',
            }
            res.send({
                "mode" : mode,
                // "parent_id" : parent_id,
                "row" : row
            });
        }

    });

    router.post('/admin/groupInsertAction/:parent_id', async function(req, res, next) {
        // var parent_id = req.params.parent_id;
        var body = req.body;
        console.log("===========");
        console.log(body);
        console.log(body['parent_id']);
        console.log("===========");
        const _group = new TableManager(db,'group',{search:[]});
        var kv = [{"key" : "parent_id", "val" : body.parent_id}];
        var maxSequence = await _group.getMaxSequence(kv);

        maxSequence += 1;
        console.log(maxSequence);
        var temp_group_id = body['group_id'];
        console.log(temp_group_id);

        var param = {
            'parent_id': body['parent_id'],
            'title'    : body['title'],
            'summary'  : '',
            'head'     : body['head'],
            'is_enable': body['is_enable'],
            'is_blank' : body['is_blank'],
            'foot' : '',
            "sequence" : maxSequence
        };
        var data = await _group.insert(param);

        if(body['parent_id'] == 0) {
            const _group_file = new TableManager(db,'group_files',{search:[]});
            var group_id = await _group.getLastIndex();
            console.log(group_id);

            //product_file의 임시아이디를 실제 product_id로 변경
            const data1 = await _group_file.modifyFrom(
                {
                    "group_id" : group_id
                },
                [{
                    "key":"group_id",
                    "val": temp_group_id
                }]
            );
        }


        res.send("Insert Success");

    });

    router.post('/admin/groupUpSequence', async function(req, res, next) {
        var _this = this;
        var body = req.body;
        const _group = new TableManager(db,'group',{search:[]});
        var clickedOne = await _group.getItem(body.id);
        console.log(clickedOne);

        if(clickedOne !== false){
            var kv = [{"key" : "parent_id", "val" : clickedOne.parent_id}];
            var UpperOne = await _group.getUpSequence(kv, clickedOne.sequence);
            console.log("=======UpperOne=ssssssss======");
            console.log(UpperOne.sequence);
            console.log("=======UpperOne=eeeeeeee======");

            if(UpperOne !== false) {
                var param_row0 = {
                    'id'         : clickedOne.id,
                    'sequence'   : UpperOne.sequence,
                }
                var param_row1 = {
                    'id'         : UpperOne.id,
                    'sequence'   : clickedOne.sequence,
                }
                var row0 = await _group.modify(param_row0);
                var row1 = await _group.modify(param_row1);

                res.send({
                    "result" : row0,
                    "result" : row1,
                });
            }else{
                res.send("It's the top.");
            }
        }

    });

    router.post('/admin/groupDownSequence', async function(req, res, next) {
        var _this = this;
        var body = req.body;
        const _group = new TableManager(db,'group',{search:[]});
        var clickedOne = await _group.getItem(body.id);

        if(clickedOne !== false){
            var kv = [{"key" : "parent_id", "val" : clickedOne.parent_id}];
            var LowerOne = await _group.getDownSequence(kv, clickedOne.sequence);
            console.log("=======LowerOne=ssssssss======");
            console.log(LowerOne.sequence);
            console.log("=======LowerOne=eeeeeeee======");

            if(LowerOne !== false) {
                var param_row0 = {
                    'id'         : clickedOne.id,
                    'sequence'   : LowerOne.sequence,
                }
                var param_row1 = {
                    'id'         : LowerOne.id,
                    'sequence'   : clickedOne.sequence,
                }
                var row0 = await _group.modify(param_row0);
                var row1 = await _group.modify(param_row1);

                res.send({
                    "result" : row0,
                    "result" : row1,
                });
            }else{
                res.send("It's the lowest.");
            }
        }
    });

    router.get('/admin/groupDelete', async function(req, res, next) {
        var _this = this;
        var query = req.query;

        const _group = new TableManager(db,'group',{search:[]});
        const data = await _group.del(query.id);

        res.send("Delete Success");
    });
    
    // router.get('/admin/groupFileList', async function(req, res, next) {
    //     var group_id = req.query.id;
    //     const _group_files = new TableManager(db,'group_files',{search:[]});
    //     const list = await _group_files.getList({kv:[{'key':'group_id','val':group_id}]});            
    //     res.send({
    //         "group_id" : group_id,
    //         "list" : list
    //     });    
    // });

    // router.get('/admin/groupFileUpload', async function(req, res, next) {
    //     var group_id = req.query.group_id;
    //     var mode = req.query.mode;
    //     res.send({
    //         "group_id": group_id,
    //         "mode" : mode
    //     });
    // });

    // router.post('/admin/groupFileUploadAction', middlePostParser.any(), async function (req, res, next) {
    //     var body = req.body;
    //     console.log(body);
    //     var param = {
    //         group_id : 0,
    //         type : '',
    //         path : '',
    //         name : '',
    //         rename : '',
    //         ext : '',
    //         size : 0
    //     };
    //     param.group_id = body['group_id'];
    //     // param.group_id = getRandomNumberFromDate();

    //     param.size = body['file_size'];
    //     param.type = 'img';
    
    //     var originalName = '';
    //     if (req.files && req.files[0] && req.files[0]['path']){
    //         _file = req.files[0];
    //         originalName = _file.originalname;
    //         var dotPos = originalName.lastIndexOf('.');
    //         param.name = originalName.substring(0, dotPos);
    //         if (dotPos > -1 && dotPos < originalName.length){
    //             param.ext = originalName.substring(dotPos+1);
    //         }
    //     }
    
    //     param.rename= getRandomNumberFromDate();

    //     var _group_file = new TableManager(db,'group_files',{search:[]});

    //     var writeFileName = param.rename + '.' + param.ext;
    //     var file = uploadRootPath + "/group/" + writeFileName;
    //     param.path = writeFileName;
    //     fs.move(_file.path, file, async function(err){
    //         if (err) {
    //             res.status(500).send(err);
    //         }else{
    //             console.log(param);
    //             var data = await _group_file.insert(param);
    //             res.send({
    //                 "result":data,
    //                 "param" : param
    //             })
    //         }
    //     });
    // });

//- board --------------------------------------------------------------------------------------------
    router.get('/admin/boardList', async function(req, res, next) {
        var category = req.query.category;
        var page = req.query.page;
        if(!page){ page = 1;}

        const _board = new TableManager(db,'board',{search:['title','summary','content']});
        const _category = new TableManager(db,'board_category',{search:[]});
        const _files = new TableManager(db,'files',{search:[]});
        var rows = await _board.getList({
            "category" : category,
            "page" : page,
            "perPage" : 10
        });
        var items = [];
        if(rows){
            rows.forEach(async function(row){ //foreach로 되어야 이미지가 뜸
                var _thumb = "";
                var thumb = await _files.getItemFrom([{'key':'mode', 'val':'thumb'},{'key':'item_id','val':row.id}]);
                if(thumb == false){
                    _thumb = "../admin/assets/img/new-gallery-img.png";
                }else{
                    _thumb = "../data/cafe/" + thumb.path + "/" + thumb.file_rename + "_thumb." + thumb.file_ext;
                }
                row.thumb = _thumb;
                items.push(row);
            });
        }
        var total = await _board.getTotalCount({"category":category});
        var categ = await _category.getList({}); 
        res.send({
            "category" : category,
            "categoryList" : categ,
            "boardList" : items,
            "total": total
        });
        res.end();

    });

    router.get('/admin/boardInsert', async function(req, res, next) {
        var category = req.query.category;
        const _category = new TableManager(db,'board_category',{search:[]});
        let categ = _category.getList({}); 
        res.send({
            "category" : category,
            "categorylist" : categ,
            "mode" : "insert",
            "row" : {}
        });
        res.end();

    });

    router.post('/admin/boardInsertAction', async function(req, res, next) {
        var body = req.body;
        var ip = "";
        var param = {
            'ip'         : ip,
            'title'      : body['title'],
            'summary'    : body['summary'],
            'content'    : body['content'],
            'category'   : body['category'],
            'reg_date'   : body['reg_date']
        };
        const _board = new TableManager(db,'board',{search:['title','summary','content']});
        const data = await _board.insert(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/boardModify', async function(req, res, next) {
        var id = req.query.id;
        const _board = new TableManager(db,'board',{search:['title','summary','content']});
        const _category = new TableManager(db,'board_category',{search:[]});
        let row = await _board.getItem(id);
        let categ = await _category.getList({});
        res.send({
            "title":'yuhomall Admin',
            "mode" : "modify",
            "categorylist" : categ,
            "row" :row
        });
        res.end();

    });

    router.post('/admin/boardModifyAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'id'         : body['id'],
            'title'      : body['title'],
            'summary'    : body['summary'],
            'content'    : body['content'],
            'category'   : body['category'],
            'reg_date'   : body['reg_date']
        };
        const _board = new TableManager(db,'board',{search:['title','summary','content']});
        const data = await _board.modify(param);
        res.send({"result" : data});
        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/boardDelete', async function(req, res, next) {
        var id = req.query.id;
        const _board = new TableManager(db,'board',{search:['title','summary','content']});
        const data = await _board.del(id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- board category -----------------------------------------------------------------------------------
    router.get('/admin/categoryList', async function(req, res, next) {
        var page = req.query.page;
        if(!page){ page = 1;}
        const _category = new TableManager(db,'board_category',{search:[]});
        const categ = await _category.getList({});
        res.send({
            "title":'yuhomall Admin',
            "categoryList" : categ
        });
    });

    router.get('/admin/categoryModify', async function(req, res, next) {
        var id = req.query.id;
        const _category = new TableManager(db,'board_category',{search:[]});
        const row = await _category.getItem(id);
        res.send({
            "title":'yuhomall Admin',
            "mode" : "modifyCategory",
            "row" :row
        });
    });

    router.post('/admin/categoryModifyAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'id'       : body['id'],
            'category' : body['category'],
            'title'    : body['title'],
            'summary'  : body['summary'],
            'head'     : body['head'],
            'type'     : body['type']
        };
        const _category = new TableManager(db,'board_category',{search:[]});
        const data = await _category.modify(param);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/categoryInsert', function(req, res, next) {
        res.send({
            "title":'yuhomall Admin',
            "mode" : "categoryInsert",
            "row" : {}
        });
    });

    router.post('/admin/categoryInsertAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'category' : body['category'],
            'title'    : body['title'],
            'summary'  : body['summary'],
            'head'     : body['head'],
            'type'     : body['type']
        };

        const _category = new TableManager(db,'board_category',{search:[]});
        const data = await _category.insert(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/categoryDelete', async function(req, res, next) {
        var id = req.query.id;
        const _category = new TableManager(db,'board_category',{search:[]});
        const data = await _category.del(id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- files --------------------------------------------------------------------------------------------
    router.get('/admin/fileList', async function(req, res, next) {
        var mode = req.query.mode;
        var item_id = req.query.item_id;
        const _files = new TableManager(db,'files',{search:[]});
        const list = await _files.getItemFrom([{'key':'mode', 'val':mode},{'key':'item_id','val':item_id}]);            
        res.send({
            "mode" : mode,
            "item_id" : item_id,
            "filelist" : list
        });    
        res.end();
    });

    router.post('/admin/fileUpload', middlePostParser.any(), async function(req, res, next) {
        var body = req.body || {};
        var param = {
            'mode'     : body['mode'],
            'item_id'  : body['id']
        }
        var upload = await Util.upload(req.files[0]);
        var writeFileName = upload.file_rename + '.' + upload.file_ext;
        var writeDirPath = uploadRootPath.resolve(param.mode+"/"+upload.path);
        var file = uploadRootPath.resolve(param.mode+"/"+upload.path+"/"+writeFileName);
        const _files = new TableManager(db,'files',{search:[]});

        fs.ensureDir(writeDirPath, async function(err){
            fs.move(req.files[0].path, file, async function(err){
                if (err) {
                    res.status(500).send(err);
                    res.end();
                } else {

                    param['file_rename'] = upload.file_rename;
                    param['path'] = upload.path;
                    param['file_type'] = "img";
                    param['file_size'] = req.files[0]['size'];

                    const data = await _files.insert(param);
                    res.send({"result":data}) // async & await 확인 필요

                    // const buffer = new Buffer.from(data);
                    // fso.writeFileSync(dbfilepath, buffer);
                    res.end();
                }
            });
        });

    });

    router.get('/admin/fileDelete/:file_id', async function(req, res, next) {
        var file_id = req.params.file_id;
        const _files = new TableManager(db,'files',{search:[]});
        const data = await _files.del(file_id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- pages --------------------------------------------------------------------------------------------
    router.get('/admin/pagesList', async function(req, res, next) {
        var page = req.query.page;
        if(!page){ page = 1;}
        const _pages = new TableManager(db,'pages',{search:[]});
        const list = await _pages.getList({
            "page" : page,
            "perPage" : 10
        });
        res.send({
            "list" : list
        });
        res.end();
    });

    router.get('/admin/pagesModify', async function(req, res, next) {
        var id = req.query.id;
        const _pages = new TableManager(db,'pages',{search:[]});
        const row = await _pages.getItem(id);
        res.send({
            "mode" : "modify",
            "row" :row
        });  
        res.end();
    });

    router.post('/admin/pagesModifyAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'id'       : body['id'],
            'code'     : body['code'],
            'title'    : body['title'],
            'summary'  : body['summary'],
            'content'  : body['content']
        };
        const _pages = new TableManager(db,'pages',{search:[]});
        const data = await _pages.modify(param);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/pagesInsert', function(req, res, next) {
        res.send({
            "title":'yuhomall Admin',
            "mode" : "insert"
        });
        res.end();
    });

    router.post('/admin/pagesInsertAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'code'     : body['code'],
            'title'    : body['title'],
            'summary'  : body['summary'],
            'content'  : body['content']
        };
        const _pages = new TableManager(db,'pages',{search:[]});
        const data = await _pages.insert(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/pagesDelete', async function(req, res, next) {
        var pages_id = req.query.pages_id;
        const _pages = new TableManager(db,'pages',{search:[]});
        const data = await _pages.del(pages_id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- slide --------------------------------------------------------------------------------------------
    router.get('/admin/slideList', async function(req, res, next) {
        const code = req.query.code;
        const _slide = new TableManager(db,'slide',{search:[]});
        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const category = await _slideCategory.getItemFrom([{'key':'code','val':code}]);
        const list = await _slide.getList({kv:[{"key":"code","val":code}]});
        res.send({
            "title":'yuhomall Admin',
            "category" : category,
            "slideList" : list
        });
    });

    router.post('/admin/slideUpload', middlePostParser.any(), async function(req, res, next) {
        const body = req.body || {};

        
        var upload = await Util.upload(req.files[0]);
        let param = {
            'code'        : body['code'],
            'category_id' : body['category_id'],
            'img_rename'  : upload.file_rename,
            'img_ext'     : upload.file_ext
        }
        const _slide = new TableManager(db,'slide',{search:[]});
        var writeDirPath = uploadRootPath.resolve("slide");
        var writeFileName = upload.file_rename + '.' + upload.file_ext;
        var _file = uploadRootPath.resolve("slide/"+writeFileName);
        fs.ensureDir(writeDirPath, async function(err){
            fs.move(upload.file.path, _file, async function(err){
                if (err) {
                    res.status(500).send(err);
                    res.end();
                } else {
                    const data = await _slide.insert(param);
                    res.send({"result":data}) // async & await 확인 필요

                    // const buffer = new Buffer.from(data);
                    // fso.writeFileSync(dbfilepath, buffer);
                    res.end();            
                }
            });
        });

    });

    router.get('/admin/slideModify', async function(req, res, next) {
        var id = req.query.id;
        const _slide = new TableManager(db,'slide',{search:[]});
        const row = await _slide.getItem(id);
        res.send({
            "title":'yuhomall Admin',
            "mode" : "modify",
            "row" :row
        });
    });

    router.post('/admin/slideModifyAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'id'        : body['slide_id'],
            'title'     : body['title'],
            'summary'   : body['summary'],
            'endDate'   : body['endDate'],
            'link_url'  : body['link_url'],
            'is_blank'  : body['is_blank'],
            'status'    : body['status'],
            'startDate' : body['startDate'],

        };
        const _slide = new TableManager(db,'slide',{search:[]});
        const data = await _slide.modify(param);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

    router.get('/admin/slideDelete', async function(req, res, next) {
        var slide_id = req.query.slide_id;
        const _slide = new TableManager(db,'slide',{search:[]});
        const data = await _slide.del(slide_id);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- slide category -----------------------------------------------------------------------------------
    router.get('/admin/slideCategoryList', async function(req, res, next) {
        var page = req.params.page;
        if(!page){ page = 1;}

        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const list = await _slideCategory.getList({
            "page" : page,
            "perPage" : 100
        });
        res.send({
            "title":'yuhomall Admin',
            "list" : list
        });
        res.end();

    });

    router.get('/admin/slideCategoryModify/:id', async function(req, res, next) {
        var id = req.params.id;

        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const row = await _slideCategory.getItem(id);
        res.send({
            "title":'yuhomall Admin',
            "mode" : "modify",
            "row" :row
        });  
        res.end();

    });

    router.post('/admin/slideCategoryModifyAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'id'       : body['id'],
            'code'     : body['code'],
            'title'    : body['title'],
            'summary'  : body['summary'],
            'size_w'   : body['size_w'],
            'size_h'   : body['size_h']
        };


        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const data = await _slideCategory.modify(param);
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
       
    });

    router.get('/admin/slideCategoryInsert', function(req, res, next) {
        res.send({
            "title":'yuhomall Admin',
            "mode" : "insert",
            "row" : {}
        });
        res.end();
    });

    router.post('/admin/slideCategoryInsertAction', async function(req, res, next) {
        var body = req.body;
        var param = {
            'code'    : body['code'],
            'title'   : body['title'],
            'summary' : body['summary'],
            'size_w'  : body['size_w'],
            'size_h'  : body['size_h']
        };

        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const data = await _slideCategory.insert(param);
        res.send({"result":data}) // async & await 확인 필요

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
   
    });

    router.get('/admin/slideCategoryDelete/:id', async function(req, res, next) {
        var id = req.params.id;
        const _slideCategory = new TableManager(db,'slide_category',{search:[]});
        const data = await _slideCategory.del(id)
        res.send({"result" : data});

        // const buffer = new Buffer.from(data);
        // fso.writeFileSync(dbfilepath, buffer);
        res.end();
    });

//- mobile App -----------------------------------------------------------------------------------

    router.get('/app/index', async function(req, res, next) {
        res.render('app/app.html', {
        });
        res.end();
    
    });

//----------------------------------------------------------------------------------------------------
});
module.exports = router;
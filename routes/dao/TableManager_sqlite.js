var fso = require('fs');
var fs = require('fs-extra');
var TableManager = function (db, table, options) {
    this.db = db;
    this.table = table;
    this.filds = [];    
    this.search = [];
    this.init(options);
};

TableManager.prototype = {

    init : function (options) {
        _this = this;
        _this.filds = this.getColumn();
        if(typeof(options.search) !== "undefined"){
            _this.search = options.search;
        }
    },

    getColumn : function(){
        var _this = this;
        var query0 = `PRAGMA table_info('${_this.table}');`;
        var contents0 = _this.db.exec(query0);
        var list0 =  _this.paserData(contents0);
        var  columns = [];
        list0.forEach(function(v){
            if(v.name !== 'id'){
                columns.push(v.name);
            }
        });
        return columns;
    },

    getLastIndex : function(){
        var _this = this;
		var query = `select seq from sqlite_sequence where name="${_this.table}"`;
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            console.log(row);
            return row[0]['seq'];
        }else{
            return false;
        }
	},

    getItem : function(id){
        var _this = this;
        var query = `SELECT * FROM ${_this.table} WHERE id = '${id}'`;
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0];
        }else{
            return false;
        }
    },

    getItemFrom : function(kv){
        var _this = this;
        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                if(v.opt){
                    _where.push(""+key+" "+v.opt+" '"+val+"'");
                }else{
                    _where.push(""+key+" = '"+val+"'");
                }
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }
		var query  = `SELECT * FROM ${_this.table} ${whereQuery}`;
       console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0];
        }else{
            return false;
        }
    },

    insert : function(arr){
        var _this = this;
        var timestamp = new Date().getTime();
        arr.adddate = timestamp;
        var _fild = [];
        var _val = [];
        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _fild.push(v);
                _val.push(`'${arr[v]}'`);
            }
        });
        var query_fild = _fild.join(",");
        var query_val = _val.join(",");

        var query  = `INSERT INTO ${_this.table} (${query_fild}) VALUES (${query_val})`;
        console.log(query);
        _this.db.exec(query);
        var data = _this.db.export();
        const buffer = new Buffer.from(data);
        fso.writeFileSync(_this.db.dbfilepath, buffer);

        return data;
    },

    modify : function(arr){
        var _this = this;
        var _set = [];
        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _set.push(`${v} = '${arr[v]}'`);
            }
        });
        var query_set = _set.join(",");
        var query  = `UPDATE ${_this.table} SET ${query_set} WHERE id = '${arr.id}'`;
        console.log(query);
        _this.db.exec(query);
        var data = _this.db.export();
        const buffer = new Buffer.from(data);
        fso.writeFileSync(_this.db.dbfilepath, buffer);
        return data;
    },

    modifyFrom : function(arr, kv){
        var _this = this;
        var _set = [];

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                if(v.opt){
                    _where.push(""+key+" "+v.opt+" '"+val+"'");
                }else{
                    _where.push(""+key+" = '"+val+"'");
                }
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _set.push(`${v} = '${arr[v]}'`);
            }
        });
        
        var query_set = _set.join(",");
        var query  = `UPDATE ${_this.table} SET ${query_set} ${whereQuery}`;
        
        console.log(query);
        _this.db.exec(query);
        var data = _this.db.export();
        const buffer = new Buffer.from(data);
        fso.writeFileSync(_this.db.dbfilepath, buffer);
        return data;
    },

    addSequence: function(kv,seq){
        var _this = this;

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence < ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM ${_this.table} ${whereQuery} ORDER BY sequence ASC LIMIT 1`;
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0];
        }else{
            return false;
        }
    },

    getUpSequence: function(kv,seq){
        var _this = this;

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence < ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM ${_this.table} ${whereQuery} ORDER BY sequence ASC LIMIT 1`;
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0];
        }else{
            return false;
        }
    },

    getDownSequence: function(kv,seq){
        var _this = this;

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence > ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM ${_this.table} ${whereQuery} ORDER BY sequence ASC LIMIT 1`;
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0];
        }else{
            return false;
        }
    },

    getMaxSequence : function(kv) {


        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }


        var query = `SELECT MAX(sequence) FROM ${_this.table} ${whereQuery}`;
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0]['MAX(sequence)'];
        }else{
            return false;
        }
    },

    del : function(id){
        var query = `DELETE FROM ${_this.table} WHERE id = '${id}'`;
        // console.log(query);
        _this.db.exec(query);
        var data = _this.db.export();
        return data;
    },

    getCondition : function(param){
        var _this = this;

        var browse = param["browse"];
        var asc    = param["asc"];

        var page      = param["page"];
        var perPage   = param["perPage"];
        var islimit   = param["islimit"];
        var isCount   = param["isCount"];

        

        var q = param["q"];
        var query = ``;
        var selectQuery = ``;
        var _where = [];
        var _whereSearch = [];


        if(typeof(param['kv']) !== "undefined"){
            param['kv'].forEach(function(v){
                var key = v.key;
                var val = v.val;

                if(v.opt){
                    _where.push(""+key+" "+v.opt+" '"+val+"'");
                }else{
                    _where.push(""+key+" = '"+val+"'");
                }
            });
        }

        if(typeof(q) !== "undefined" && q !== ""){
            _this.search.forEach(function(v){
                _whereSearch.push("("+v+" LIKE '%"+q+"%')");
            });
            var __search  =  _whereSearch.join(" OR ");
            _where.push("("+__search+")");
        }

        var order_by = ``;
        if(typeof(asc) !== "undefined" && asc !== ""){
            //asc = asc;
        }else{
            asc = "DESC";
        }

        if(typeof(browse) !== "undefined" && browse !== ""){
            order_by = `ORDER BY ${browse} ${asc}`;
        }else{
            order_by = `ORDER BY id ${asc}`;
        }

        if(isCount == "O"){
            selectQuery = `SELECT count(*) as count FROM ${_this.table} `;
        }else{
            selectQuery = `SELECT * FROM ${_this.table} `;
        }

        if(_where.length > 0){
            var query_where = _where.join(" AND ");
            whereQuery = `WHERE ${query_where}`;
        }else{
            whereQuery = ``;
        }

        if(islimit == "O"){
            var from  = (page-1)*perPage;
            query = `${selectQuery} ${whereQuery} ${order_by} LIMIT ${from}, ${perPage}`;
        }else{
            query = `${selectQuery} ${whereQuery} ${order_by}`;
        }
        return query;
    },

    getList : function(param){
        var _this = this;
        var query = _this.getCondition(param);
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var list =  _this.paserData(contents);
            return list;
        }else{
            return false;
        }
    },

    getTotalCount : function(param){
        var _this = this;
        param.isCount = "O";
        var query = _this.getCondition(param);
        console.log(query);
        var contents = _this.db.exec(query);
        if(contents.length > 0){
            var row =  _this.paserData(contents);
            return row[0]['count'];
        }else{
            return false;
        }
    },

    paserData : function(data){
        var col = data[0].columns;
        var val  = data[0].values;
        var rows = [];
        var ff = 0;
        val.forEach(function(k){
            var idx = 0;
            var row = {};
            col.forEach(function(key){

                if(val[ff][idx] == null){
                    val[ff][idx] = "";
                }
                row[key] = val[ff][idx];
                idx++;
            });
            rows.push(row);
            ff++;
        });
        return rows;
    }

};
module.exports = TableManager;
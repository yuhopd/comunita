var fso = require('fs');
var fs = require('fs-extra');
var path = require('path');

var DBConnection = function (dbType,config, callback) {
    this.dbType = dbType;
    this.config = config;
    this.init(callback);
};

DBConnection.prototype = {
    init : function (callback) {
        var _this = this;
        if(_this.dbType == "mysql"){
            _this.mysqlConn(callback);
        }else if(_this.dbType == "sqlite"){
            _this.sqliteConn(callback);

        }
    },


    mysqlConn :async function(callback){
        var _this = this;
        const mysql = require('mysql2/promise');
        const db = await mysql.createPool({
            host: _this.config.host, 
            port: _this.config.port, 
            user: _this.config.user, 
            password: _this.config.password, 
            database: _this.config.database
        });
        callback(db);
    },

    sqliteConn :async function(callback){
        var _this = this;
        const initSqlJs = require('../sqljs/sql-wasm.js');
        const dbfilepath = path.resolve(_this.config.dbfilepath);
        const filebuffer = fs.readFileSync(dbfilepath);    
        initSqlJs().then(function(SQL){
            const db = new SQL.Database(filebuffer);
            db.dbfilepath = dbfilepath;
            
            callback(db);
        });
        
    }
};

module.exports = DBConnection;
const TableManager = function (db, table, options) {
    this.db = db;
    this.table = table;
    this.filds = [];    
    this.search = [];
    this.init(options);
};

TableManager.prototype = {

    init : async function (options) {
        _this = this;
        if(typeof(options.search) !== "undefined"){
            _this.search = options.search;
        }
    },

    getColumn : async function(){
        var _this = this;
        var query = `DESCRIBE \`${_this.table}\``;
        const [row] = await _this.db.query(query);
        var  columns = [];
        row.forEach(function(v){
            if(v.Field !== 'id'){
                columns.push(v.Field);
            }
        });
        return columns;
    },

    getLastIndex : async function(){
        var _this = this;
		var query = `SELECT * FROM \`${_this.table}\` ORDER BY id DESC LIMIT 1`;
        const [row] = await _this.db.query(query);
        return row[0]['id'];
	},

    getItem : async function(id){
        var _this = this;
        var query = `SELECT * FROM \`${_this.table}\` WHERE id = '${id}'`;
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row[0];
        } else {
           return false;
        }    
    },

    getItemFrom : async function(kv){
        var _this = this;
        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                if(v.opt){
                    _where.push(`\`${key}\` ${v.opt} '${val}'`);
                }else{
                    _where.push(`\`${key}\` = '${val}'`);
                }
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }
		var query  = `SELECT * FROM \`${_this.table}\` ${whereQuery}`;
        // console.log(query);
        const [row] = await _this.db.query(query);
        // console.log(row);

        if(row.length > 0){
            return row[0];
        } else {
           return false;
        }    
    },

    insert : async function(arr){
        var _this = this;
        _this.filds = await _this.getColumn();
        var timestamp = new Date().getTime();
        arr.adddate = timestamp;
        var _fild = [];
        var _val = [];
        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _fild.push(`\`${v}\``);
                _val.push(`'${arr[v]}'`);
            }
        });
        var query_fild = _fild.join(",");
        var query_val = _val.join(",");
        var query  = `INSERT INTO \`${_this.table}\` (${query_fild}) VALUES (${query_val})`;
        console.log(query);
        const [result] = await _this.db.query(query);
        return result;
    },

    modify :async function(arr){
        var _this = this;
        var _set = [];
        _this.filds = await _this.getColumn();
        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _set.push(`\`${v}\` = '${arr[v]}'`);
            }
        });
        var query_set = _set.join(",");
        var query  = `UPDATE \`${_this.table}\` SET ${query_set} WHERE id = '${arr.id}'`;
        console.log(query);
        const [result] = await _this.db.query(query);
        return result;
    },

    modifyFrom : async function(arr, kv){
        var _this = this;
        var _set = [];
        _this.filds = await _this.getColumn();
        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                if(v.opt){
                    _where.push(""+key+" "+v.opt+" '"+val+"'");
                }else{
                    _where.push(""+key+" = '"+val+"'");
                }
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        _this.filds.forEach(function(v){
            if(typeof(arr[v]) !== "undefined"){
                _set.push(`\`${v}\` = '${arr[v]}'`);
            }
        });
        
        var query_set = _set.join(",");
        var query  = `UPDATE \`${_this.table}\` SET ${query_set} ${whereQuery}`;
        
        console.log(query);
        const [result] = await _this.db.query(query);
        return result;
    },

    addSequence: async function(kv,seq){
        var _this = this;

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence < ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM \`${_this.table}\` ${whereQuery} ORDER BY sequence ASC LIMIT 1`;
        console.log(query);
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row[0];
        }else{
            return false;
        }
    },

    getUpSequence: async function(kv,seq){
        var _this = this;

        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence < ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM \`${_this.table}\` ${whereQuery} ORDER BY sequence DESC LIMIT 1`;
        console.log(query);
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row[0];
        }else{
            return false;
        }
    },

    getDownSequence: async function(kv,seq){
        var _this = this;
        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            _where.push(`sequence > ${seq}`);

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query  = `SELECT * FROM \`${_this.table}\` ${whereQuery} ORDER BY sequence ASC LIMIT 1`;
        console.log(query);
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row[0];
        }else{
            return false;
        }
    },

    getMaxSequence : async function(kv) {
        var _this = this;
        if(typeof(kv) !== "undefined" && kv.length > 0){
            var whereQuery = ``;
            var _where = [];
            kv.forEach(function(v){
                var key = v['key'];
                var val = v['val'];
                _where.push(`${key} = '${val}'`);
            });

            if(_where.length > 0){
                var query_where = _where.join(" AND ");
                whereQuery = `WHERE ${query_where}`;
            }else{
                whereQuery = ``;
            }
        }

        var query = `SELECT MAX(sequence) AS max FROM \`${_this.table}\` ${whereQuery}`;
        console.log(query);
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row[0]['max'];
        }else{
            return false;
        }
    },

    del : async function(id){
        var _this = this;
        var query = `DELETE FROM \`${_this.table}\` WHERE id = '${id}'`;

        const [result] = await _this.db.query(query);
        return result;
    },

    getCondition : function(param){
        var _this = this;

        var browse = param["browse"];
        var asc    = param["asc"];

        var page      = param["page"];
        var perPage   = param["perPage"];
        var islimit   = param["islimit"];
        var isCount   = param["isCount"];

        

        var q = param["q"];
        var query = ``;
        var selectQuery = ``;
        var _where = [];
        var _whereSearch = [];


        if(typeof(param['kv']) !== "undefined"){
            param['kv'].forEach(function(v){
                var key = v.key;
                var val = v.val;

                if(v.opt){
                    _where.push(`\`${key}\` ${v.opt} '${val}'`);
                }else{
                    _where.push(`\`${key}\` = '${val}'`);
                }
            });
        }

        if(typeof(q) !== "undefined" && q !== ""){
            _this.search.forEach(function(v){
                _whereSearch.push("("+v+" LIKE '%"+q+"%')");
            });
            var __search  =  _whereSearch.join(" OR ");
            _where.push("("+__search+")");
        }

        var order_by = ``;
        if(typeof(asc) !== "undefined" && asc !== ""){
            //asc = asc;
        }else{
            asc = "DESC";
        }

        if(typeof(browse) !== "undefined" && browse !== ""){
            order_by = `ORDER BY ${browse} ${asc}`;
        }else{
            order_by = `ORDER BY id ${asc}`;
        }

        if(isCount == "O"){
            selectQuery = `SELECT count(*) as count FROM \`${_this.table}\` `;
        }else{
            selectQuery = `SELECT * FROM \`${_this.table}\` `;
        }

        if(_where.length > 0){
            var query_where = _where.join(" AND ");
            whereQuery = `WHERE ${query_where}`;
        }else{
            whereQuery = ``;
        }

        if(islimit == "O"){
            var from  = (page-1)*perPage;
            query = `${selectQuery} ${whereQuery} ${order_by} LIMIT ${from}, ${perPage}`;
        }else{
            query = `${selectQuery} ${whereQuery} ${order_by}`;
        }
        return query;
    },

    getList : async function(param){
        var _this = this;       
        var query = _this.getCondition(param);
        const [row] = await _this.db.query(query);
        if(row.length > 0){
            return row;
        } else {
           return false;
        }
    },

    getTotalCount : async function(param){
        var _this = this;
        param.isCount = "O";
        var query = _this.getCondition(param);
        console.log(query);
        const [row] = await _this.db.query(query);
        return row[0]['count'];
    }
};
module.exports = TableManager;
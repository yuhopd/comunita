var Hashids = require('hashids');
var AdmZip = require('adm-zip');

module.exports = {
    date: function (format, timestamp) {
        var jsdate, f
        // Keep this here (works, but for code commented-out below for file size reasons)
        // var tal= [];
        var txtWords = [
            'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ]
        // trailing backslash -> (dropped)
        // a backslash followed by any character (including backslash) -> the character
        // empty string -> empty string
        var formatChr = /\\?(.?)/gi
        var formatChrCb = function (t, s) {
            return f[t] ? f[t]() : s
        }
        var _pad = function (n, c) {
            n = String(n)
            while (n.length < c) {
                n = '0' + n
            }
            return n
        }
        f = {
            // Day
            d: function () {
                // Day of month w/leading 0; 01..31
                return _pad(f.j(), 2)
            },
            D: function () {
                // Shorthand day name; Mon...Sun
                return f.l()
                    .slice(0, 3)
            },
            j: function () {
                // Day of month; 1..31
                return jsdate.getDate()
            },
            l: function () {
                // Full day name; Monday...Sunday
                return txtWords[f.w()] + 'day'
            },
            N: function () {
                // ISO-8601 day of week; 1[Mon]..7[Sun]
                return f.w() || 7
            },
            S: function () {
                // Ordinal suffix for day of month; st, nd, rd, th
                var j = f.j()
                var i = j % 10
                if (i <= 3 && parseInt((j % 100) / 10, 10) === 1) {
                    i = 0
                }
                return ['st', 'nd', 'rd'][i - 1] || 'th'
            },
            w: function () {
                // Day of week; 0[Sun]..6[Sat]
                return jsdate.getDay()
            },
            z: function () {
                // Day of year; 0..365
                var a = new Date(f.Y(), f.n() - 1, f.j())
                var b = new Date(f.Y(), 0, 1)
                return Math.round((a - b) / 864e5)
            },

            // Week
            W: function () {
                // ISO-8601 week number
                var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3)
                var b = new Date(a.getFullYear(), 0, 4)
                return _pad(1 + Math.round((a - b) / 864e5 / 7), 2)
            },

            // Month
            F: function () {
                // Full month name; January...December
                return txtWords[6 + f.n()]
            },
            m: function () {
                // Month w/leading 0; 01...12
                return _pad(f.n(), 2)
            },
            M: function () {
                // Shorthand month name; Jan...Dec
                return f.F()
                    .slice(0, 3)
            },
            n: function () {
                // Month; 1...12
                return jsdate.getMonth() + 1
            },
            t: function () {
                // Days in month; 28...31
                return (new Date(f.Y(), f.n(), 0))
                    .getDate()
            },

            // Year
            L: function () {
                // Is leap year?; 0 or 1
                var j = f.Y()
                return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0
            },
            o: function () {
                // ISO-8601 year
                var n = f.n()
                var W = f.W()
                var Y = f.Y()
                return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0)
            },
            Y: function () {
                // Full year; e.g. 1980...2010
                return jsdate.getFullYear()
            },
            y: function () {
                // Last two digits of year; 00...99
                return f.Y()
                    .toString()
                    .slice(-2)
            },

            // Time
            a: function () {
                // am or pm
                return jsdate.getHours() > 11 ? 'pm' : 'am'
            },
            A: function () {
                // AM or PM
                return f.a()
                    .toUpperCase()
            },
            B: function () {
                // Swatch Internet time; 000..999
                var H = jsdate.getUTCHours() * 36e2
                // Hours
                var i = jsdate.getUTCMinutes() * 60
                // Minutes
                // Seconds
                var s = jsdate.getUTCSeconds()
                return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3)
            },
            g: function () {
                // 12-Hours; 1..12
                return f.G() % 12 || 12
            },
            G: function () {
                // 24-Hours; 0..23
                return jsdate.getHours()
            },
            h: function () {
                // 12-Hours w/leading 0; 01..12
                return _pad(f.g(), 2)
            },
            H: function () {
                // 24-Hours w/leading 0; 00..23
                return _pad(f.G(), 2)
            },
            i: function () {
                // Minutes w/leading 0; 00..59
                return _pad(jsdate.getMinutes(), 2)
            },
            s: function () {
                // Seconds w/leading 0; 00..59
                return _pad(jsdate.getSeconds(), 2)
            },
            u: function () {
                // Microseconds; 000000-999000
                return _pad(jsdate.getMilliseconds() * 1000, 6)
            },

            // Timezone
            e: function () {
                // Timezone identifier; e.g. Atlantic/Azores, ...
                // The following works, but requires inclusion of the very large
                // timezone_abbreviations_list() function.
                /*              return that.date_default_timezone_get();
                 */
                var msg = 'Not supported (see source code of date() for timezone on how to add support)'
                throw new Error(msg)
            },
            I: function () {
                // DST observed?; 0 or 1
                // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
                // If they are not equal, then DST is observed.
                var a = new Date(f.Y(), 0)
                // Jan 1
                var c = Date.UTC(f.Y(), 0)
                // Jan 1 UTC
                var b = new Date(f.Y(), 6)
                // Jul 1
                // Jul 1 UTC
                var d = Date.UTC(f.Y(), 6)
                return ((a - c) !== (b - d)) ? 1 : 0
            },
            O: function () {
                // Difference to GMT in hour format; e.g. +0200
                var tzo = jsdate.getTimezoneOffset()
                var a = Math.abs(tzo)
                return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4)
            },
            P: function () {
                // Difference to GMT w/colon; e.g. +02:00
                var O = f.O()
                return (O.substr(0, 3) + ':' + O.substr(3, 2))
            },
            T: function () {
                return 'UTC'
            },
            Z: function () {
                // Timezone offset in seconds (-43200...50400)
                return -jsdate.getTimezoneOffset() * 60
            },

            // Full Date/Time
            c: function () {
                // ISO-8601 date.
                return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb)
            },
            r: function () {
                // RFC 2822
                return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb)
            },
            U: function () {
                // Seconds since UNIX epoch
                return jsdate / 1000 | 0
            }
        }

        var _date = function (format, timestamp) {
            jsdate = (timestamp === undefined ? new Date() // Not provided
                    : (timestamp instanceof Date) ? new Date(timestamp) // JS Date()
                    : new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
            )
            return format.replace(formatChr, formatChrCb)
        }

        return _date(format, timestamp)
    },

    microtime: function (getAsFloat) {

        var s
        var now

            now = (Date.now ? Date.now() : new Date().getTime()) / 1e3
            if (getAsFloat) {
                return now
            }

            // Math.round(now)
            s = now | 0

            return (Math.round((now - s) * 1e3) / 1e3) + ' ' + s

    },

    mktime: function () {
        var d = new Date()
        var r = arguments
        var i = 0
        var e = ['Hours', 'Minutes', 'Seconds', 'Month', 'Date', 'FullYear']

        for (i = 0; i < e.length; i++) {
            if (typeof r[i] === 'undefined') {
                r[i] = d['get' + e[i]]()
                // +1 to fix JS months.
                r[i] += (i === 3)
            } else {
                r[i] = parseInt(r[i], 10)
                if (isNaN(r[i])) {
                    return false
                }
            }
        }

        // Map years 0-69 to 2000-2069 and years 70-100 to 1970-2000.
        r[5] += (r[5] >= 0 ? (r[5] <= 69 ? 2e3 : (r[5] <= 100 ? 1900 : 0)) : 0)

        // Set year, month (-1 to fix JS months), and date.
        // !This must come before the call to setHours!
        d.setFullYear(r[5], r[3] - 1, r[4])

        // Set hours, minutes, and seconds.
        d.setHours(r[0], r[1], r[2])

        var time = d.getTime()

        // Divide milliseconds by 1000 to return seconds and drop decimal.
        // Add 1 second if negative or it'll be off from PHP by 1 second.
        return (time / 1e3 >> 0) - (time < 0)
    },

    rand : function (min, max) {
        var argc = arguments.length;
        if (argc === 0) {
            min = 0
            max = 2147483647
        } else if (argc === 1) {
            throw new Error('Warning: rand() expects exactly 2 parameters, 1 given')
        }
        return Math.floor(Math.random() * (max - min + 1)) + min
    },

    getRandomNumberFromDate : function(){
        var mktime = this.mktime(this.date('s'));
        var microtime = this.microtime(mktime) + '';
        microtime = microtime.substring(11, 14);

        microtime = microtime.length == 2 ? microtime + this.rand(0, 9) : microtime;
        microtime = microtime.length == 1 ? microtime + this.rand(0, 9) + this.rand(0, 9) : microtime;

        return {
            yearMonth : this.date('ym'),
            daily : this.date('ymd'),
            randomNumber : this.date('ymdHms') + microtime + this.rand(0, 9) + this.rand(0, 9) + this.rand(0, 9)
        };
    },

    getCKey : function(fileName){
        var hashIds = new Hashids(fileName);
        return hashIds.encode(1,2,3,4,5);
    },

    getZipEntrySize : function(fileName){
        var entrySize = 0;

        try{
            var zip = new AdmZip(fileName);
            var zipEntries = zip.getEntries(); // an array of ZipEntry records
            zipEntries.forEach(function(entry){
                if (! entry.isDirectory){
                    entrySize += entry.header.size;
                }
            });
        }catch(e){
            throw new Error(e);
        }

        return entrySize;
    },

    upload : function(file){
        var _this = this;
        let file_name = "";
        let file_ext = "";
        let file_rename = "";
        let path = "";
        let file_size = file.size;
        var originalName = file.originalname;
        file_name = originalName;
        var dotPos = originalName.lastIndexOf('.');
        if (dotPos > -1 && dotPos < originalName.length){
            file_ext = originalName.substring(dotPos+1).toLowerCase();
        }
        var randomNumber = _this.getRandomNumberFromDate();
        file_rename = randomNumber.randomNumber;
        path = randomNumber.yearMonth;

        var res = {
            "path" : path,
            "file_name" : file_name,
            "file_ext" : file_ext,
            "file_size" : file_size,
            "file_rename" : file_rename,
            "file" : file
        }
        return res;
    },

    addSlashes : function(str){
        return str.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    },

    stripSlashes : function(str){
        return str.replace(/\\(.?)/g, function (s, n1){switch (n1){case '\\':return '\\';case '0':return '\u0000';case '':return '';default:return n1;}});
    }

};
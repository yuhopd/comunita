define([
	'jquery'
	, 'jquery-ui'
  ], function ($, Common) {
	  var Search = function () {
		//-----------------------------------------------------------------------------------------
		this.searchContact = function(usrType){
            var _this = this;
            _this.visitUsrType = usrType;
            $.ajax({
                type: "GET",
                url: "_visitContactSearch.php",
                data:{ },
                dataType: "html",
                success: function(data){
                    $("#search_dialog").html(data);
                },
                complete : function(){
                    $("#search_dialog a.btn").on("click", function(){
                        var action = $(this).attr("action");
                        if(action == "visitSearch"){
                            _this.search();
                        }
                        return false;
                    });

                    $("#visit_qq").on("keypress", function(e){
                        var keycode = (e.keyCode ? e.keyCode : e.which);
                        if (keycode == '13') {
                            _this.search();
                        }
                    });
                }
            });
            $("#search_dialog").dialog('option', 'title', '연락처 추가');
            $("#search_dialog").dialog('option', 'width', 350);
            $("#search_dialog").dialog('option', 'height', "auto");
            $("#search_dialog").dialog("open");
        };

        this.search = function(page) {
            var _this = this;
            var q = $("#visit_qq").val();
            if(page == undefined){ page = 1; }
            if(q != ""){
                var keyword = encodeURIComponent(q);
                _this.searchContactList({"q":keyword,"page":page});
            }
        };

        this.searchContactList = function(json) {
            var _this = this;
            $.ajax({
                type : "GET",
                url : "_visitContactList.php",
                data : json,
                dataType : "html",
                success: function(data){
                    $("#visitSearchList").html(data);
                },
                complete : function(){

                    $("#visitSearchList a[action='visitAddUser']").on("click",function(e){
                    
                        var meta = $(this).attr("meta");
                        
                        _this.addContact(meta);
                        return false;
                    });	

                }
            });
        };

        this.addContact = function(visitVal) {
            var _this = this;
            var arrayVisit = visitVal.split("@");
            if(_this.visitUsrType == "usr_id"){
                $("#visitForm #usr_name").append('<div class="usrGroup" usrid="'+arrayVisit[0]+'"><a href="#" action="userInfo" usrId="'+arrayVisit[0]+'">'+arrayVisit[1]+'</a> <a class="remove" action="removeUsr" mode="usr_name" usrId="'+arrayVisit[0]+'"><i class="fa fa-times"></i></a></div>');
            }else if(_this.visitUsrType == "visit_usr_id"){
                $("#visitForm #visit_usr_name").append('<div class="usrGroup" usrid="'+arrayVisit[0]+'"><a href="#" action="userInfo" usrId="'+arrayVisit[0]+'">'+arrayVisit[1]+'</a> <a class="remove" action="removeUsr" mode="usr_name" usrId="'+arrayVisit[0]+'"><i class="fa fa-times"></i></a></div>');
            }


            $("#visitForm a.remove").on("click",function(e){
                var action = $(this).attr("action");
                var usrId  = $(this).attr("usrId");
                var mode  = $(this).attr("mode");
                if(action == "removeUsr"){
                    _this.removeUsr(mode,usrId);
                } 
                return false;
            });

            $("#search_dialog").dialog("close");
        };




	    //-----------------------------------------------------------------------------------------	
	};
	return Search;
});

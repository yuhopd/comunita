define([
	'jquery'
  ], function ($) {
	  var Family = function () {
		//-----------------------------------------------------------------------------------------

		this.page = 1;
		this.pagination = 30;
		
		this.usr_id = 0;
		this.listBox = "";

		this.callback = null;

		this.list = function (json){
			var _this = this;
			_this.listBox = json.listBox;
			_this.usr_id  = json.usr_id;
			if ($.isFunction(json.callback)){
				_this.callback = json.callback;
			}
			_this.listWarp(_this.callback);
		};

	
		this.listWarp = function(callback) {
			var _this = this;
			var keyword = encodeURIComponent($.trim($('#family_q').val()));
			$.ajax({
				type : "GET",
				url : "_familyList.php",
				data : {
					"q" : keyword,
					"usr_id":_this.usr_id
				},
				dataType : "html",
				success: function(data){
					$("#"+_this.listBox).html(data);
				},
				complete : function(){
					if ($.isFunction(callback)){
						callback(_this.usr_id);
					}
				}
			});
		};

		this.search = function(usr_id) {
			var _this = this;
			_this.list({"usr_id":usr_id , "listBox":"familySearchList"});
	    };

		this.deleteAction = function(usr_id,family_id) {
			var _this = this;
			if(!confirm("정말로 회원 가족관계를 삭제하시겠습니까?")){
			}else{
				$.ajax({
					type : "GET",
					url : "_familyAction.php",
					data : {
						"mode": "usr_family_delete",
						"usr_id": usr_id,
						"family_id": family_id							
					},
					dataType : "json",
					success: function(data){
						_this.list({"usr_id":usr_id, "listBox": "usrFamilyList"});
					}
				});
			}
		};

		this.insert = function(usr_id) {
			var _this = this;
			$("#sub_dialog").dialog('option', 'title', '가족관계 추가' );
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', "auto");

			$.ajax({
				type : "GET",
				url : "_familySearch.php",
				data : {
					"usr_id":usr_id
				},
				dataType : "html",
				success: function(data){
					$("#sub_dialog").html(data);
				},
				complete : function(){
					$("#sub_dialog a.btn").on("click", function(){
						var usr_id = $(this).attr("usrid");
						var action = $(this).attr("action");
						if(action == "familySearch"){
							_this.search(usr_id);
						}else if(action == "familyInsertAction"){
							_this.insertAction(usr_id);
						}
                        return false;
					});


					$("#family_q").on("keypress", function(e){
						var keycode = (e.keyCode ? e.keyCode : e.which);
						if (keycode == '13') {
							_this.search(usr_id);
						}
					});

				}
			});

			$("#sub_dialog").dialog('option', 'buttons', {});
			$('#sub_dialog').dialog('open');
		};

		this.insertAction = function(usr_id) {		
			var _this = this;
            var family_id = $("#familySearchList input.radio:checked").val();
		    var relations = $('#relations').val();
			if(relations == 0){
				alert("관계를 선택하세요!");
			} else if(family_id == 0 || !family_id){
				alert("가족을 선택하세요!");
			}else{
				$.ajax({
					type : "GET",
					url : "_familyAction.php",
					data : {
						"mode"      : "usr_family_insert",
						"usr_id"    : usr_id,
						"family_id" : family_id,
						"relations" : relations
					},
					dataType : "json",
					success: function(data){
						if(data.error > 0){
							alert(data.message);
						}else{
							$('#family_q').val("");
							_this.list({"usr_id":usr_id, "listBox": "usrFamilyList"});
							$('#sub_dialog').dialog('close');
						}
					}
				});
			}
		};

	    //-----------------------------------------------------------------------------------------	
	};
	return Family;
});

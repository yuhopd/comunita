define([
	'jquery'
	, 'jquery-ui'
	, 'fineuploader'
  ], function ($) {
	  var Reservation = function () {
		//-----------------------------------------------------------------------------------------

            this.id = 0;
			this.mode = "";
			this.usr_id  = 0;
			this.item_id = 0;
			this.page = 1;

			this.common = null;
			this.search = null;

			this.visitUsrType = "";
			this.listBox = "contantBox";
			
			this.list = function (json){
				var _this = this;
				if(json.listBox  != undefined){ _this.listBox = json.listBox; }
				if(json.mode  != undefined){ _this.mode = json.mode; }
				_this.listWarp();
			};

			this.listWarp = function(){
				var _this = this;

				var src = "_reservationList.php";
				$.ajax({
					type: "GET",
					url: src,
					data: {"page":_this.page,"mode":_this.mode},
					dataType: 'html',
					success: function(data){
						$("#"+_this.listBox).html(data);
					},
					complete : function(){
						if(_this.listBox == "usrVisitList"){
							$("#usrVisitList a.btn").on("click", function(){
								var usr_id = $(this).attr("usrid");
								var action = $(this).attr("action");
								if(action == "visitInsert"){
									_this.insert(usr_id);
								} else if(action == "visitView"){
									_this.view(usr_id);
								}
								return false;
							});
						}else if(_this.listBox == "mainBox"){
							$("#mainBox a.btn").on("click", function(){
								var usr_id = $(this).attr("usrid");
								var visit_id = $(this).attr("visitId");
								var action = $(this).attr("action");
								if(action == "visitInsert"){
									_this.insert();
								} else if(action == "visitView"){
									_this.view(visit_id);
								} else if(action == "visitEdit"){
									_this.modify(visit_id,"mainBox");
								}
								return false;
							});
						}

						

						$(".pagging > a").on('click', function(e){
							var page = $(this).attr("href");
							_this.list({"page":page});
							return false;
						});	
					}
				});
			};
			
			this.view = function(id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "_reservationView.php?id="+id,
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방 보고서' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {});
						$('#sub_dialog').dialog('open');
					}
				});
			};

            this.insert = function (usr_id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "_reservationForm.php",
					data: {"mode":"insert", "usr_id":usr_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$('#sub_dialog').find("a.btn").on("click",function(){
							var action = $(this).attr("action");
							var mode = $(this).attr("mode");
							if(action == "searchContact"){
								_this.search = new Search();
								_this.search.searchContact(mode);
							}
						});



						$("#sub_dialog").dialog('option', 'title', '심방보고서 작성' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"보고서 작성" : function() { 
								_this.insertAction();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
						
					}
				});
			};

			this.insertAction = function(){
				var _this = this,
					write_usr_id = $.trim($("#visitForm input[name='write_usr_id']").val()),
				    type    = $.trim($("#visitForm select[name='type']").val()),
				    reg_date = $.trim($("#visitForm input[name='reg_date']").val()),
				    title   = $.trim($("#visitForm input[name='title']").val()),
				    place   = $.trim($("#visitForm input[name='place']").val()),
				    content = $.trim($("#visitForm textarea[name='content']").val());

                var arryUsrId = new Array();
                $.each($("#usr_name > .usrGroup"), function() {
					arryUsrId.push($(this).attr("usrId"));
				});
				var usr_id = arryUsrId.join("|");

				var arryVisitUsrId = new Array();
                $.each($("#visit_usr_name > .usrGroup"), function() {
					arryVisitUsrId.push($(this).attr("usrId"));
				});
				var visit_usr_id = arryVisitUsrId.join("|");



				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == 0) {
					alert("심방종류를 선택하세요!");
					return false;
				}

				if(title == '') {
					alert("성경본문을 입력하세요!");
					return false;
				}

				if(content == '') {
					alert("내용을 입력하세요^^");
					return false;
				}	
				$.ajax({
					type: "POST",
					url: "../report/_reservationAction.php",
					data: {
						mode: "visitInsert",
			            usr_id : usr_id,
						visit_usr_id : visit_usr_id,
						write_usr_id : write_usr_id,
						reg_date : reg_date,
						type : type,
						place : place,
						title : title,
						content : content					
					},
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
			};

			this.modify = function (visit_id,listBox){
				var _this = this;

				if(listBox  != undefined){ _this.listBox = listBox; }
				$("#sub_dialog").dialog('option', 'title', '심방보고서 수정' );
				$("#sub_dialog").dialog('option', 'height', 'auto');
				$("#sub_dialog").dialog('option', 'width', 500);
				$("#sub_dialog").dialog('option', 'buttons', {
					"저장" : function() { 
						_this.modifyAction();
					} , 
						
					"삭제" : function() { 
						_this.deleteAction(visit_id);
					} , 

					"닫기" : function() { 
						$(this).dialog("close"); 
					}
				});

				$.ajax({
					type: "GET",
					url: "_reservationForm.php",
					data: {"mode":"modify", "id":visit_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$('#sub_dialog').dialog('open');

						$("#datepicker").datepicker( {dateFormat:'yy-mm-dd'} );
							
						$("#sub_dialog a.btn").on('click', function(e){
							var action = $(this).attr("action");
							var mode = $(this).attr("mode");
	
							if(action == "searchContact"){
								_this.searchContact(mode);
							} 
	


							
							/*
							$('#photoBoxUploader').fineUploader({
								request: {
									endpoint: "../user/_reservationImgAction.php",
									params: {
										"mode": "visit",
										"usr_id"  : <?=$_GET['usr_id']?>,
										"item_id" : <?=$_GET['id']?>,
									}
								},
								multiple: false,
								validation: {
									allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
									forceMultipart:true,
									sizeLimit: 5120000 // 50 kB = 50 * 1024 bytes
								},
								text: {
									uploadButton: '이미지 첨부'
								},
								debug: true
							}).on('complete', function(event, id, fileName, responseJSON) {
					
								$.photo.listWarp();
								$('#photoBoxUploader .qq-upload-list').hide();
							});
							*/
					
							//$.photo.list({"mode":"visit", "listBox":"photoBox", "item_id":<?=$_GET['id']?>});
					





							return false;
						});

						$("#sub_dialog a.remove").on("click",function(e){
							var action = $(this).attr("action");
							var usrId  = $(this).attr("usrId");
							var mode  = $(this).attr("mode");
							if(action == "removeUsr"){
								_this.removeUsr(mode,usrId);
							} 
							return false;
						});
						
					}
				});
			};

			this.modifyAction = function(){
				var _this = this,
				    id      = $.trim($("#visitForm input[name='id']").val()),
					write_usr_id = $.trim($("#visitForm input[name='write_usr_id']").val()),
				    type    = $.trim($("#visitForm select[name='type']").val()),
				    reg_date   = $.trim($("#visitForm input[name='reg_date']").val()),
				    title   = $.trim($("#visitForm input[name='title']").val()),
				    place   = $.trim($("#visitForm input[name='place']").val()),
				    content = $.trim($("#visitForm textarea[name='content']").val());

				//var visit_usr_id = $("#visit_usr_name > usrGroup").attr("usrId").join("|");
                var arryUsrId = new Array();
                $.each($("#usr_name > .usrGroup"), function() {
					arryUsrId.push($(this).attr("usrId"));
				});
				var usr_id = arryUsrId.join("|");

				var arryVisitUsrId = new Array();
                $.each($("#visit_usr_name > .usrGroup"), function() {
					arryVisitUsrId.push($(this).attr("usrId"));
				});
				var visit_usr_id = arryVisitUsrId.join("|");


                
				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == 0) {
					alert("심방종류를 선택하세요!");
					return false;
				}
				if(title == '') {
					alert("제목을 이력하세요^^");
					return false;
				}
				if(content == '') {
					alert("내용을 이력하세요^^");
					return false;
				}
				$.ajax({
					type: "POST",
					url: "../report/_reservationAction.php",
					data: {
						mode: "visitModify",
						id : id,
			            usr_id : usr_id,
						visit_usr_id : visit_usr_id,
						write_usr_id : write_usr_id,
						reg_date : reg_date,
						type : type,
						place : place,
						title : title,
						content : content					
					},
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
				
			};

			this.deleteAction = function(visit_id) {
				var _this = this;
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					
				}else{
					$.ajax({
						type: "GET",
						url: "../report/_reservationAction.php",
						data: {"mode":"visitDelete", "id":visit_id },
						dataType: 'json',
						success: function(data){
							_this.listWarp();
							$('#sub_dialog').dialog('close');
						}
					});
				}
			};
			
			this.removeUsr = function(mode,id) {
				if(mode == "usr_name"){
					$("#"+mode+" div[usrid="+id+"]").remove();
				}else if(mode == "visit_usr_name"){
					$("#"+mode+" div[usrid="+id+"]").remove();
				}
			};
	
	    //-----------------------------------------------------------------------------------------	
	};
	return Reservation;
});

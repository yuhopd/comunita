define([
	'jquery'
	, 'Common'
	, 'nestable'
  ], function ($, Common) {
	  var Attendance = function () {
		//-----------------------------------------------------------------------------------------
        this.group_id = 0;
		this.id = 0;
		this.page = 1;

		this.recentlyPage = function(page)  {
			var _this = this;
			_this.page = page;
			_this.recently();
		};

		this.recently = function()  {
            var _this = this;
			$.ajax({
				type: "GET",
				url: "_attendanceRecently.php",
				data: {"page":_this.page},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);
				}
			});
		};

		this.list = function(json)  {
            var _this = this;
			if(json.group_id != undefined){ _this.group_id = json.group_id; }
			if(json.id != undefined){ _this.id = json.id; }
			$.ajax({
				type: "GET",
				url: "_attendanceList.php",
				data: {
					"attendance_id":_this.id
				},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);
				},
				complete : function(){    
					$("#mainBox a.btn").on('click', function(e){
						var action = $(this).attr("action");
						if(action == "panoramaView"){
							_this.panoramaView();
						}

						//_this.common.usr_info(userinfo);
					});

					$(".project-actions > a").on('click', function(e){
						var group = $(this).attr("group");
						var attendance = $(this).attr("attendance");
						var date = $(this).attr("date");
						_this.report(group,attendance,date);
						return false;
					});
			
				}
			});
		};
		
		this.reportFromID = function(report_id,date)  {
            var _this = this;
			_this.id = report_id;
			$.ajax({
				type: "GET",
				url: "../attendance/_attendanceReport.php",
				data: {
					"id" : report_id,
					"date" :date
				},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);
				}
			});
		};

		this.panoramaView = function()  {
            var _this = this;
			$.ajax({
				type: "GET",
				url: "_attendancePanoramaView.php",
				data: {
					"attendance_id" : _this.id
				},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);
				},
				complete : function(){    
					$("#mainBox a.btn").on('click', function(e){
						var action = $(this).attr("action");
						
					});

					$("a.check").on('click', function(e){
						var group = $(this).attr("group");
						var attendance = $(this).attr("attendance");
						var date = $(this).attr("date");
						_this.report(group,attendance,date);
						return false;
					});
			
				}
			});
		};

		this.report = function(group_id,attendance_id,date)  {
			var _this = this;

			_this.common = new Common();
			$.ajax({
				type: "GET",
				url: "_attendanceReport.php",
				data: {
					"group_id" : group_id,
					"id" : attendance_id,
					"date" :date
				},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);

				},
				complete : function(){    
					//_this.total();
					

					$("#mainBox a").on('click', function(e){

						var action = $(this).attr("action");
						var mode = $(this).attr("mode");
						var usrId = $(this).attr("usrId");

						if(action == "userInfo"){
							_this.common.usr_info(usrId);
						}else if(action == "update"){
							_this.upstate($(this),mode,usrId);
						}
												
						return false;
					});






				}
			});
		};

		this.upstate = function(_e,mode,usr_id){
			var _this = this;
			var wrap = $("#attendanceReportForm tr[usrId='"+usr_id+"']");
			var input = wrap.find("input[name='"+mode+"']");
			var val = input.val();

            var attendance = {
				"O" : "fa-check-circle-o text-success",
				"L" : "fa-clock-o text-info",
				"X" : "fa-times-circle-o text-danger",
				"N" : "fa-edit"
			};

			if(mode == "worship"){
				if(val == "X"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.X).addClass(attendance.O);
					input.val("O");
				}else if(val == "L"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.L).addClass(attendance.X);
					input.val("X");
				}else if(val == "O"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.O).addClass(attendance.L);
					input.val("L");
				} else {
					_e.find("i").removeClass(attendance.N).addClass(attendance.O);
					input.val("O");
				}
			}else if(mode == "gbs"){
				if(val == "X"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.X).addClass(attendance.O);
					input.val("O");
				}else if(val == "L"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.L).addClass(attendance.X);
					input.val("X");
				}else if(val == "O"){
					_e.find("i").removeClass(attendance.N).removeClass(attendance.O).addClass(attendance.L);
					input.val("L");
				} else {
					_e.find("i").removeClass(attendance.N).addClass(attendance.O);
					input.val("O");
				}
			}else if(mode == "qt"){
				if(val >= 5){
                   val = 0;
				}else if (val >= 0){
                   val++;
				}else{
                   val = 0;
				}
				wrap.find("a[mode='qt']").html(val);
				input.val(val);
			
			}else if(mode == "bible"){
				if(val >= 5){
                   val = 0;
				}else if (val >= 0){
                   val++;
				}else{
                   val = 0;
				}
				wrap.find("a[mode='bible']").html(val);
				input.val(val);
			}else if(mode == "prayer"){
				if(val >= 5){
                   val = 0;
				}else if (val >= 0){
                   val++;
				}else{
                   val = 0;
				}
				wrap.find("a[mode='prayer']").html(val);
				input.val(val);
			}
			_this.upstateAction(usr_id);
		};

		this.upstateAction = function(usr_id)  {

			var _this = this;
            var attendance_id = $("#attendance_id").val();
            var group_id = $("#group_id").val();
            var reg_date = $("#reg_date").val();



			var worship = $("#roll_list li[usr_id="+usr_id+"] input[name=worship]").val();
			var gbs = $("#roll_list li[usr_id="+usr_id+"] input[name=gbs]").val();
			var qt = $("#roll_list li[usr_id="+usr_id+"] input[name=qt]").val();
			var bible = $("#roll_list li[usr_id="+usr_id+"] input[name=bible]").val();
			var prayer = $("#roll_list li[usr_id="+usr_id+"] input[name=prayer]").val();
			var note = $("#roll_list li[usr_id="+usr_id+"] input[name=note]").val();


			$.ajax({
				type: "POST",
				url: "_attendanceAction.php",
				data: {
					"mode" : "usrUpdate",
					"attendance_id" : attendance_id,
					"group_id" : group_id,
					"date" : reg_date,
					"usr_id" : usr_id,
					"worship" : worship,
					"gbs" : gbs,
					"qt" : qt,
					"bible" : bible,
					"prayer" : prayer,
					"note": note				
				},
				dataType: 'json',
				success: function(data){
					if(data.error < 0){						
						//_this.oneUpdate(data);
					}else{
						alert(data.message);
					}					
				}
			});
		};

		this.check = function(date,usr_id)  {
			var _this = this;		    
			$("#dialog").dialog('option', 'title', '출석체크' );
			$("#dialog").dialog('option', 'close', function() { $("#roll_list li[usr_id="+usr_id+"]").removeClass("focus"); });
			$.ajax({
				type: "GET",
				url:  "../attendance/_attendanceCheckForm.php",
				data: {"mode":"modify", "attendance_id":_this.id,"usr_id": usr_id, "date":date},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);
				},
				complete : function(){
					$("#roll_list li[usr_id="+usr_id+"]").addClass("focus");
				}
			});

			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.checkAction();
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		};
		
		this.checkAction = function()  {
			var _this = this;
			var form_var = $('#attendanceCheckForm').serialize();
			$.ajax({
				type: "POST",
				url: "../attendance/_attendanceAction.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){						
						_this.oneUpdate(data);
						$('#dialog').dialog("close"); 
					}else{
						alert(data.message);
						$('#dialog').dialog("close"); 
					}					
				}
			});
		};

		this.oneUpdate = function(json)  {
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=worship]").val(json.worship);
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=gbs]").val(json.gbs);
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=qt]").val(json.qt);
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=bible]").val(json.bible);
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=prayer]").val(json.prayer);
			$("#roll_list li[usr_id="+json.usr_id+"] input[name=note]").val(json.note);
			$("#roll_list li[usr_id="+json.usr_id+"] .state span[ref=worship]").removeClass().addClass("worship_"+json.worship);
			$("#roll_list li[usr_id="+json.usr_id+"] .state span[ref=gbs]").removeClass().addClass("gbs_"+json.gbs);
			$("#roll_list li[usr_id="+json.usr_id+"] .state span.qt").html(json.qt);
			$("#roll_list li[usr_id="+json.usr_id+"] .state span.bible").html(json.bible);
			$("#roll_list li[usr_id="+json.usr_id+"] .state span.prayer").html(json.prayer);
			$("#roll_list li[usr_id="+json.usr_id+"] .note").html(json.note);
		};

		this.reportSave = function(){
			var group_id = $("#group_id").val();
			var attendance_id = $("#attendance_id").val();
			var reg_date = $("#reg_date").val();
			var usr_id   = makeVal($("#roll_list input[name='usr_id']").serializeArray());
			var worship  = makeVal($("#roll_list input[name='worship']").serializeArray());
			var gbs      = makeVal($("#roll_list input[name='gbs']").serializeArray());
			var qt       = makeVal($("#roll_list input[name='qt']").serializeArray());
			var bible    = makeVal($("#roll_list input[name='bible']").serializeArray());
			var prayer   = makeVal($("#roll_list input[name='prayer']").serializeArray());
			var note     = makeVal($("#roll_list input[name='note']").serializeArray());
			var suggest  = $("#suggest").html();
			var report   = $("#report").html();
			var unusual  = $("#unusual").html();
			var bigo     = $("#bigo").html();

			$.ajax({
				type: "POST",
				url: "../attendance/_attendanceAction.php",
				data: {
					"mode":"report_save",
                    "group_id":group_id,
					"attendance_id":attendance_id,
					"reg_date":reg_date,
					"usr_id":usr_id, 
					"worship":worship,
					"gbs":gbs,
					"qt":qt,
					"bible":bible,
					"prayer":prayer,
					"note":note,
					"suggest":suggest,
					"report":report,
					"unusual":unusual,
					"bigo":bigo
				},
				dataType: 'json',
				success: function(json){
					if(json.error < 0){
						alert(json.message);
					}
				}
			});

			function makeVal(arry){
				var arryReturn =new Array();
				jQuery.each(arry, function(i, field){
					arryReturn[i] = field.value;
				});
                return arryReturn.join("|");
	 		}

		};

		this.insert = function(group_id)  {
			$('#attendance_box').load('../attendance/attendance_form.php?group_id='+group_id);


			$('#locationBox').html('<span class="box">'+
				'<input type="button button_xl" id="list-back" class="ui-button ui-state-default ui-corner-all" onclick="$.attendance.list();" value="출석부리스트" />' +
	            '</span>' +
				'<span class="arrow">&nbsp;</span>' +
				'<span class="box">출석부</span>');

		};

		this.detail = function(group_id)  {
			$('#attendance_box').load('../attendance/attendance.php?group_id='+group_id);
			$('#locationBox').html('<span class="box">'+
				'<a class="ui-button button_xl" href="javascript:$.attendance.list();">출석부리스트</a>' +
				'</span>'+
				'<span class="arrow">&nbsp;</span>'+
				'<span class="box">출석부</span>');
		};

		this.reportTalent = function(group_id,date)  {
			$('#attendance_box').load('../attendance/attendance_report_talent.php?group_id='+group_id+'&date='+date);
		};

		this.mergeDetail = function(root_id)  {
			$('#attendance_box').load('../attendance/attendance_merge.php?root_id='+root_id);
		};
		
		this.mergeReport = function(root_id,date)  {
			$('#attendance_box').load('../attendance/attendance_report_merge.php?root_id='+root_id+'&date='+date);
		};

		this.print = function(group_id,date)  {
			var w = 700;
			var h = 500;
			NewPopup = window.open('../attendance/attendance_report_print.php?group_id='+group_id+'&date='+date, "new", "titlebar=0, resizable=1, scrollbars=yes, width="+w+", height="+h);
		};
		
		this.one = function(week, usr_id, check_1, is_note)  {
			var className = "icon_default";
            if(check_1 == 'O'){
                className = "icon_check_1";
			}else if(check_1 == 'L'){
				className = "icon_check_2";
			}else if(check_1 == 'X'){
				className = "icon_check_3";
			}
			if(is_note == "O"){
				className = className + "_on";
			}

			$("#"+week+"_"+usr_id).removeClass();
            $("#"+week+"_"+usr_id).addClass(className);
		};
		
		this.view_chart = function(group_id)  {
			$('#attendance_box').load('../attendance/attendance_chart.php?group_id='+group_id);
		};
		
		this.view_chart_1st = function()  {
			$('#attendance_box').load('../attendance/attendance_chart_1st.php');
		};
		
		this.view_chart_2st = function()  {
			$('#attendance_box').load('../attendance/attendance_chart_2st.php');
		};
		
		this.view_chart_all = function()  {
			$('#attendance_box').load('../attendance/attendance_chart_all.php');
		};

		this.comment_insert_action = function(attendance_id) {
			var form_var = $('#attendanceCommentForm').serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(json){
					$.attendance.comment_list(attendance_id);
					$('#note').val('');
				}
			});
			return true;
		};
		
		this.comment_list = function(attendance_id) {
				$('#attendance_comment_list').load('../attendance/attendance_comment_list.php?attendance_id='+attendance_id);
		};

		this.comment_delete = function(comment_id,attendance_id) {

			if(!confirm("정말로 삭제하시겠습니까?")){
				return false;
			}else{
				$.ajax({
					type: "GET",
					url: "action.php",
					data: {'mode':'comment_delete', 'comment_id': comment_id , 'attendance_id' : attendance_id},
					dataType: 'json',
					success: function(data){
						$.attendance.comment_list(attendance_id);
					}
				});
				return false;
			}
		};
		
		this.message = function (msg){
			$('#usr_msg').fadeIn("slow");
			$('#usr_msg').html(msg);
			window.setTimeout("$('#usr_msg').fadeOut('slow');",5000);
		};
		
		this.reportTalentSave = function (){
			var group_id = $("#group_id").val();
			var reg_date = $("#reg_date").val();
			var usr_id   = makeVal($("#roll_list input[name='usr_id']").serializeArray());
			var worship  = makeVal($("#roll_list select[name='worship']").serializeArray());
			var gbs      = makeVal($("#roll_list select[name='gbs']").serializeArray());
			var note     = makeVal($("#roll_list input[name='note']").serializeArray());
					
			var attendance = makeVal($("#roll_list select[name='attendance']").serializeArray());
			var accumulateState = makeVal($("#roll_list input[name='accumulate_State']").serializeArray());
			var bible      = makeVal($("#roll_list select[name='bible']").serializeArray());
			var offertory  = makeVal($("#roll_list select[name='offertory']").serializeArray());
			var evangelize = makeVal($("#roll_list select[name='evangelize']").serializeArray());
			var sumTalent  = makeVal($("#roll_list input[name='sumTalent']").serializeArray());

			var suggest  = $("#suggest").val();
			var report   = $("#report").val();
			var unusual  = $("#unusual").val();
			var bigo     = $("#bigo").val();

			$.ajax({
				type: "POST",
				url: "action.php",
				data: {
					"mode":"report_talent_save",
                    "group_id":group_id,
					"reg_date":reg_date,
					"usr_id":usr_id, 
					"worship":worship,
					"gbs":gbs,
					
					"attendance":attendance,
					"accumulateState":accumulateState,
					"bible":bible,
					"offertory":offertory,
					"evangelize":evangelize,
                    "sumTalent":sumTalent,				
					
					"note":note,
					"suggest":suggest,
					"report":report,
					"unusual":unusual,
					"bigo":bigo,
					"quarter":1

				},
				dataType: 'json',
				success: function(json){
					if(json.error < 0){
						alert(json.message);
					}
				}
			});

			function makeVal(arry){
				var arryReturn =new Array();
				jQuery.each(arry, function(i, field){
					arryReturn[i] = field.value;
				});
                return arryReturn.join("|");
	 		}

		};

	    this.total = function()  {
			var totalUsr  = $("input[name='usr_id']").serializeArray().length;
			var worshipTotal = countVal($("input[name='worship']").serializeArray());
            $("#totalUsr").html(totalUsr);
			$("#worshipTotal").html(worshipTotal);
			function countVal(arry){				
				var c = 0;
				jQuery.each(arry, function(i, field){
					if(field.value == "O"||field.value == "L"){
						c++;
					}
				});
                return c;
	 		}
		};
		
		this.talentSum = function(usr_id) {
			var attendance = Number($("#user_"+usr_id+" select[name='attendance']").val());
			var bible      = Number($("#user_"+usr_id+" select[name='bible']").val());
			var offertory  = Number($("#user_"+usr_id+" select[name='offertory']").val());
			var evangelize = Number($("#user_"+usr_id+" select[name='evangelize']").val());
			var accumulate = Number($("#user_"+usr_id+" input[name='accumulate']").val());
            var total = attendance + bible + offertory + evangelize;

			$("#user_"+usr_id+" input[name='sumTalent']").val(total);
			$("#user_"+usr_id+" input[name='accumulate_State']").val(total+accumulate);

		};
	    //-----------------------------------------------------------------------------------------
	};
	return Attendance;
});

define([
    'jquery',
    'jquery-ui',
    'dropzone'
], function ($) {
    var Upload = function(){


        this.wrap = $("#kk_dialog");
        this.wrapHtml =
            '<div id="uploadDialog">' +
				'<form id="imageDropzone" class="dropzone" enctype="multipart/form-data" action="_imageUploadAction.php" method="POST">'+
					'<input type="hidden" name="usr_id" value=""/>' +
				'</form>' +
				'<div id="imageDropWrap" class="kkdropwrap"></div>'+
            '</div>';

        this.open = function(options){
            this.load();
            this.wrap.dialog('open');
        };

        this.close = function(options){
            this.wrap.dialog('close');
        };

        this.load = function(){
            var _this = this;
            _this.wrap.html(_this.wrapHtml);
            _this.wrap.dialog({
                title: "이미지 업로드",
                width:600,
                height:"auto",
				autoOpen:false,
                draggable:true,
                modal: true,
                resizable:false,
                open: function(event, ui) {

                },
                close : function(event, ui) {

                },
                buttons: {
                    "저장": function() {
						
                        $( this ).dialog( "close" );
                    },
                    "취소": function() {
                        $( this ).dialog( "close" );
                    }
                }
            });

			var iDropzone = null;
			iDropzone = new Dropzone("#imageDropzone", {
				url: "_imageUploadAction.php",
				maxFiles : 1,
				parallelUploads : 1,
				addRemoveLinks : true,
				acceptedFiles : ".jpg,.png"
			});

			iDropzone.on("success", function(file) {
                var data = $.parseJSON(file.xhr.response);
                //console.log(data);
				$("#imageDropzone").hide();
				$("#imageDropWrap").html('<img src="'+data.url+'"/>').show();
			});


        };
    };
    return Upload;
});

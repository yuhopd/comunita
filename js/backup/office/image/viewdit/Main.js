require.config({
    baseUrl : '../js/office/image/viewdit',
    paths: {
        'jquery' : 'lib/jquery-1.9.1',
        'iscroll-zoom' : 'lib/iscroll-zoom',
		'screenfull' : 'lib/screenfull',
		'Loader' : 'Loader',

		'Sketch' : 'lib/sketchjs/Sketch',
		'Event' : 'lib/sketchjs/Event',
		'toastmessage' : 'lib/jquery.toastmessage',

		'jquery-ui' : 'lib/jquery-ui-1.10.4.custom',
        'jquery-ui-kukudocs' : 'layout/jquery-ui-kukudocs',
		'jquery-ui-shape-editor' : 'layout/jquery-ui-shape-editor',

		'jquery-ui-touch-punch' : 'lib/jquery-ui-touch-punch',

        'Config' : 'layout/Config',
        'Util' : 'util/Util'
    },

    shim : {
        'sketch'  : {
        	deps : ['jquery']
        },
		'jquery-ui' : {
            deps : ['jquery']
        },
		'jquery-ui-kukudocs' : {
            deps : ['jquery','jquery-ui']
        },

		'jquery-ui-touch-punch' : {
            deps : ['jquery','jquery-ui']
        },

		'jquery-ui-shape-editor' : {
            deps : ['jquery','jquery-ui']
        },

		'toastmessage' : {
        	deps : ['jquery']
        }
    }
});

require([
    'jquery'
    , 'Loader'
], function($, Loader){
    $(document).ready(function() {
        var loader = new Loader();
        loader.init();
    });
});

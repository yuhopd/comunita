
(function($)
{
	$.extend(
	{
		photo : 
		{
		//-----------------------------------------------------------------------------------------
            id : 0,
			mode : "",
			usr_id  : 0,
			item_id : 0,
			listBox : "photoBox",
			
			
			list : function (json){
				var _this = this;
				_this.listBox = json.listBox;
				_this.mode    = json.mode;
				_this.item_id = json.item_id;
				_this.usr_id  = json.usr_id;
				_this.listWarp();
			},

			listWarp : function(){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../user/_photoList.php",
					data: {"mode":_this.mode, "item_id":_this.item_id, "usr_id":_this.usr_id},
					dataType: 'html',
					success: function(data){
						$("#"+_this.listBox).html(data);
					},
					complete : function(){}
				});
			},            

			deleteAction : function(id) {
				var _this = this;
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					
				}else{
					$.ajax({
						type: "GET",
						url: "../user/_photoAction.php",
						data: {"mode":"photoDelete", "id":id },
						dataType: 'json',
						success: function(data){
                            _this.listWarp();
						},
						complete : function(){	

						}
					});
				}
			},

			_ver : "1.0.1"
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

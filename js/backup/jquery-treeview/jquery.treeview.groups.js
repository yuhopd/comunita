/*
 * Async Treeview 0.1 - Lazy-loading extension for Treeview
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 *
 * Copyright (c) 2007 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id$
 *
 */

;(function($) {

function load(settings, root, child, container) {
	$.getJSON(settings.url, {root: root}, function(response) {
		function createNode(parent) {
			var excelInput = "";
            if (this.parent_id == 0){
				excelInput = "<option value='excelUpload'>액셀입력</option>";
            }

			var current = $("<li/>").attr("id", this.id || "").html(
				"<div class='tree'>"
				+ "<div class='title'><a href='#' onclick=\"javascript:$.group.edit("+this.id+"); return false;\">" + this.title + "</a></div>"




				+ "<div class='menu'>"
				+ "  <a class='ui-state-default ui-corner-all' href=\"javascript:$.group.upSequence("+this.id+");\">▲</a>"
				+ "  <a class='ui-state-default ui-corner-all' href=\"javascript:$.group.downSequence("+this.id+");\">▼</a>"

				+ "  <select nmae='groupAction_"+this.id+"' id='groupAction_"+this.id+"'>"
				+ "      <option value='none'>기능선택</option>"+excelInput
				+ "      <option value='modify'>그룹정보수정</option>"
				+ "      <option value='usrSet'>그룹회원설정</option>"
				+ "      <option value='childAdd'>하위그룹추가</option>"
				+ "      <option value='del'>그룹삭제</option>"
                + "  </select>"
				+ "  <a class='ui-state-default ui-corner-all button' href=\"javascript:$.group.listAction("+this.id+");\">실행</a>"
				+ "</div>"
				+ "<div class='clear'></div>"
				+ "</div>"
			
			).appendTo(parent);

            if (this.is_enable == "X"){
				current.find("div[class='tree']").append("<div class='enabled'><img src='../images/icon_check_3.gif' align='absmiddle' alt='미사용' title='미사용' /></div>");
            }
            if (this.use_calendar == "O"){
				current.find("div[class='tree']").append("<div class='calendar'><img src='../images/icon_check_1.gif' align='absmiddle' alt='사용' title='사용' /></div>");
            }
            if (this.use_attendance == "O"){
				current.find("div[class='tree']").append("<div class='attendance'><img src='../images/icon_check_1.gif' align='absmiddle' alt='출석부' title='출석부' /></div>");
			}else if(this.use_attendance == "T"){
				current.find("div[class='tree']").append("<div class='attendance'><img src='../images/gold.png' align='absmiddle' alt='달란트 출석부' title='달란트 출석부' /></div>");

			}
			if (this.classes) {
				current.children("span").addClass(this.classes);
			}
			if (this.expanded) {
				current.addClass("open");
			}
			if (this.hasChildren || this.children && this.children.length) {
				var branch = $("<ul/>").appendTo(current);
				if (this.hasChildren) {
					current.addClass("hasChildren");
					createNode.call({
						text:"placeholder",
						id:"placeholder",
						children:[]
					}, branch);
				}
				if (this.children && this.children.length) {
					$.each(this.children, createNode, [branch])
				}
			}
		}
		$.each(response, createNode, [child]);
        $(container).treeview({add: child});
    });
}

var proxied = $.fn.treeview;
$.fn.treeview = function(settings) {
	if (!settings.url) {
		return proxied.apply(this, arguments);
	}
	var container = this;
	load(settings, "source", this, container);
	var userToggle = settings.toggle;
	return proxied.call(this, $.extend({}, settings, {
		collapsed: true,
		toggle: function() {
			var $this = $(this);
			if ($this.hasClass("hasChildren")) {
				var childList = $this.removeClass("hasChildren").find("ul");
				childList.empty();
				load(settings, this.id, childList, container);
			}
			if (userToggle) {
				userToggle.apply(this, arguments);
			}
		}
	}));
};

})(jQuery);
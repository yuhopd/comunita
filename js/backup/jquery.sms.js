(function($)
{
	$.extend(
	{
		sms : 
		{
		//-----------------------------------------------------------------------------------------
    	send : function(phone) {
			var _this = this;
			var str = "";
			if(phone == ''){
				$("#contactListBox input:checked").each(function(i){ if(i==0){ str += $(this).val(); }else{ str += "|" + $(this).val(); }}); 
			}else{
				str = phone;
			}
			$("#dialog").dialog('option', 'title', '문자(SMS) 보내기');
			$("#dialog").dialog('option', 'width', 900);
			$("#dialog").dialog('option', 'height', "auto");
			$.ajax({
				type:"POST",
				url: "../contact/_smsForm.php",
				data: {"rcv":str },
				dataType: "html",
				success: function(data){
					$("#dialog").html(data);
				},
				complete : function(){
					$("#reserve_date").datepicker({dateFormat:'yy-mm-dd'});
				}
			});

			$("#dialog").dialog('option', 'buttons', {
				"보내기" : function() {
					_this.sendAction(); 
				} ,  

				"미리보기" : function() {

				} ,  

				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},
		
		sendAction : function() {	
			var _this = this;			
            var snd_number = $.trim($("#smsForm input[name='snd_number']").val());
            var sms_type = $("#smsForm input[name='sms_type']:checked").val();
			var sms_content = $.trim($("#sms_content").val());
			var isReserve = "";
			var isSubstitute = "";
			var phones = "";
			var reserve_date = "";

			$("#smsUsrsList input:hidden").each(function(i){ if(i==0){ phones += $(this).val(); }else{ phones += "|" + $(this).val(); }}); 
            if($("#smsForm input[name=isReserve]").is(":checked")){ 
				isReserve = "O"; 
				reserve_date = $("#smsForm input[name=reserve_date]").val()+" "+$("#smsForm select[name=reserve_time_h]").val()+":"+$("#smsForm select[name=reserve_time_m]").val()+":00";
			}else{
				isReserve = "X";
				reserve_date = "";
			}

			if($("#smsForm input[name=isSubstitute]").is(":checked")){ 
				isSubstitute = "O";
			}else{
				isSubstitute = "X";
			}

			if(sms_content == ""){
				alert("문자 메시지를 입력하세요!");
            }else if(snd_number == ""){
                alert("보내는사람 휴대폰번호를 입력하세요!");
            }else if(phones == ""){
                alert("받는사람이 없습니다.");
			}else{	
				$.ajax({
					type: "POST",
					url: "../contact/_smsAction.php",
					data: {
						"mode":"send",
						"sms_type":sms_type,
						"sms_content":sms_content,
						"snd_number":snd_number,
						"isReserve":isReserve,
						"isSubstitute":isSubstitute,
						"phones":phones,
						"reserve_date":reserve_date
					},
					dataType: 'json',
					success: function(data){
						if(data.error > 0){
							alert(data.message);
						}else{
							alert(data.message);
							$("#sub_dialog").dialog("close"); 
						}				
					},
					complete : function(){ }

				});
			}
		},

        sendDeleteUsr : function(usr_id) {
			var _this = this;
			$("#smsUsrsList div[usrId="+usr_id+"]").remove();
		},

        searchContact : function(){
			var _this = this;
			$("#sub_dialog").dialog('option', 'title', '연락처 추가');
			$("#sub_dialog").dialog('option', 'width', 350);
			$("#sub_dialog").dialog('option', 'height', "auto");
			$.ajax({
				type: "GET",
				url: "../contact/_smsContactSearch.php",
				data:{ },
				dataType: "html",
				success: function(data){
					$("#sub_dialog").html(data);
				},
				complete : function(){ 		
				}
			});

			$("#sub_dialog").dialog('option', 'buttons', {
				"추가하기" : function() {
					_this.addContact(); 
				},
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#sub_dialog').dialog('open');
		},

		_contactList : function(json) {
			$.ajax({
				type : "GET",
				url : "_smsContactList.php",
				data : json,
				dataType : "html",
				success: function(data){
					$("#smsSearchList").html(data);
				}
			});
		},

		search : function() {
			var _this = this;
			var q = $('#sms_q').val().trim();
			if(q != ""){
				var keyword = encodeURIComponent(q);
				_this._contactList({"q":keyword});
			}
		},

		addUser : function(str){
			var arryStr = str.split("@"); 
			$("#smsUsrsList").append('<div class="usrGroup" usrId="'+arryStr[0]+'" title="'+arryStr[2]+'">' +
					'<input type="hidden" name="phones[]" value="'+arryStr[0]+'" />' + arryStr[1] + 
					'<a class="remove" href="javascript:$.sms.sendDeleteUsr('+arryStr[0]+');"></a>' +
					'</div>');
			//$("#smsSearchList").find("li[userId='"+arryStr[0]+"']").remove();
		},

		addContact : function(){
			$("#smsSearchList input:checked").each(function(i){
				var str = $(this).val(); 
				var arryStr = str.split("@"); 
				$("#smsUsrsList").append('<div class="usrGroup" usrId="'+arryStr[0]+'" title="'+arryStr[2]+'">' +
						'<input type="hidden" name="phones[]" value="'+arryStr[0]+'" />' + arryStr[1] + 
						'<a class="remove" href="javascript:$.sms.sendDeleteUsr('+arryStr[0]+');"></a>' +
						'</div>');
			});          
			$("#sub_dialog").dialog("close"); 
		},

		msgStringLength : function(cnt) { 
		   //변수의 초기화
		   var sms_type = $("#smsForm input[name='sms_type']:checked").val();
		   var obj = $("#sms_content");
		   var now_str = obj.val();                     //이벤트가 발생한 컨트롤의 value값 
		   var now_len = obj.val().length;              //현재 value값의 글자 수 
		   var dest = $("#strLimit");                   //입력된 글자수를 넣어줄 id
		   
		   var max_len = cnt;                           //제한할 최대 글자 수 
		   var i = 0;                                   //for문에서 사용할 변수 
		   var cnt_byte = 0;                            //한글일 경우 2 그외에는 1바이트 수 저장 
		   var sub_cnt = 0;                             //substring 할때 사용할 제한 길이를 저장 
		   var chk_letter = "";                         //현재 한/영 체크할 letter를 저장 
		   var lmt_str = "";                            //제한된 글자 수만큼만 저장 
			
		   for (i=0; i<now_len; i++) { 
			   //1글자만 추출 
			   chk_letter = now_str.charAt(i); 

			   // 체크문자가 한글일 경우 2byte 그 외의 경우 1byte 증가 
			   if (escape(chk_letter).length > 4) { 
				   //한글인 경우 2byte (UTF-8인 경우 3byte로...)
				   cnt_byte += 2; 
			   }else{ 
				   //그외의 경우 1byte 증가 
				   cnt_byte++; 
			   } 
				
			   //만약 전체 크기가 제한 글자 수를 넘지 않으면 
			   if (cnt_byte <= max_len) { 
				   // 제한할 문자까지의 count값을 sub_cnt에 누적 
				   sub_cnt = i + 1; 
			   } 
		   } 
				
		   // 만약 전체 크기가 제한 글자 수를 넘으면     
		   if(sms_type == "SMS"){
			   if (cnt_byte > max_len) { 
				   alert("최대" + max_len + "글자 이상 쓸수 없습니다!"); 
				   lmt_str = now_str.substring(0, sub_cnt); 
				   obj.val(lmt_str); 
				   dest.html(max_len);
			   } else {
				   dest.html(cnt_byte);
			   }
			   obj.focus();
		   }else{
				dest.html(cnt_byte);
		   }

		},

        smsTypeChange : function() { 
		   var sms_type = $("#smsForm input[name='sms_type']:checked").val();

		   if(sms_type == "SMS"){
                $("#defaultLimit").html("80");
		   }else{
                $("#defaultLimit").html("무제한");
		   }

		},

		isReserve : function() { 
			var _this = this;
			if($("#smsForm input[name=isReserve]").is(":checked")){
				$("#smsForm input[name=reserve_date]").removeAttr("disabled");
				$("#smsForm select[name=reserve_time_h]").removeAttr("disabled");
				$("#smsForm select[name=reserve_time_m]").removeAttr("disabled");
			} else {
				$("#smsForm input[name=reserve_date]").attr('disabled',true);
				$("#smsForm select[name=reserve_time_h]").attr('disabled',true);
				$("#smsForm select[name=reserve_time_m]").attr('disabled',true);
			}
		},


		isSubstitute : function() { 
			var _this = this;
			if($("#smsForm input[name=isSubstitute]").is(":checked")){
				$("#smsForm a.substitute").removeClass("ui-state-disabled").addClass("ui-state-default");
			} else {
				$("#smsForm a.substitute").removeClass("ui-state-default").addClass("ui-state-disabled");
			}
		},

		addKey : function(key) { 
			var txt = $.trim($("#sms_content").val());
			
			if(key == "name"){
				$("#sms_content").val(txt + "[[=이름=]]");
			}else if(key == "position"){
				$("#sms_content").val(txt + "[[=직분=]]");
			}
		
		},
		
		_var : "1.2.10"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

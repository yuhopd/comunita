
(function($)
{
	$.extend(
	{
		message : 
		{
		//-----------------------------------------------------------------------------------------
            mode : "default",
			page : 1,
			path : "../mypage/",
			listBox : "content_wrap",

			list  : function(json){
				var _this = this;
				if(json.mode  != undefined){ _this.mode = json.mode; }
				if(json.page  != undefined){ _this.page = json.page; } 
                _this.wrapList();
			},

			wrapList  : function(){
				var _this = this;
				$.ajax({
					type: "GET",
					url: _this.path + "_messageList.php",
					data : {mode:_this.mode, page:_this.page},
					dataType: 'html',
					success: function(data){
						$("#"+_this.listBox).html(data);
					},
					complete : function(){	
						
					}
				});
			},
			
			view : function(id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: _this.path + "_messageView.php?id="+id,
					data : {"id": id},
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					},
					complete : function(){	
						$("#dialog").dialog('option', 'title', '쪽지보기' );
						$("#dialog").dialog('option', 'height', 'auto');
						$("#dialog").dialog('option', 'width', 460);
						$("#dialog").dialog('option', 'buttons', {
							"답장" : function() { 
								_this.reply();
							} ,  

							"삭제" : function() { 
								_this.deleteAction(id);
							} ,
							"보관" : function() { 
								_this.updateRead(id); 
							}  
						});
						$('#dialog').dialog('open');
					}
				});
			},

			reply : function(){
				var _this = this;
				var messageId = $("#messageId").val();
				$.ajax({
					type: "GET",
					url: _this.path + "_messageForm.php",
					data: {"mode":"reply", "messageId":messageId},
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					},
					complete : function(){	
						$("#dialog").dialog('option', 'title', '쪽지쓰기' );
						$("#dialog").dialog('option', 'height', 'auto');
						$("#dialog").dialog('option', 'width', 460);
						$("#dialog").dialog('option', 'buttons', {
							"보내기" : function() { 
								_this.sendAction();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						//$('#dialog').dialog('open');
					}
				});
			},

			form : function(){
				var _this = this;
				$.ajax({
					type: "GET",
					url: _this.path + "_messageForm.php",
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					},
					complete : function(){	
						$("#dialog").dialog('option', 'title', '쪽지쓰기' );
						$("#dialog").dialog('option', 'height', 'auto');
						$("#dialog").dialog('option', 'width', 460);
						$("#dialog").dialog('option', 'buttons', {
							"보내기" : function() { 
								_this.sendAction();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#dialog').dialog('open');
					}
				});
			},
			
			sendAction : function(){
				var _this = this;
				var form_var = $('#messageForm').serialize();
				var sendId   = $.trim($('#sendId').val());
				var readIds  = $('#readIds').val();
				var content  = $.trim($('#content').val());
				if(readIds == '') {
					alert("보낼사람을 선택하세요^^");
					return false;
				}
				if(content == '') {
					alert("내용을 입력하세요!");
					return false;
				}
				$.ajax({
					type: "POST",
					url: _this.path + "_messageAction.php",
					data: form_var,
					success: function(data){
						$('#dialog').dialog('close');
						_this.wrapList();
					}
				});
			},
			
			updateRead : function(id) {
				var _this = this;
				$.ajax({
					type: "GET",
					url: _this.path + "_messageAction.php",
					data: {"mode":"updateRead", "id":id },
					dataType: 'html',
					success: function(data){
						_this.wrapList();
					},
					complete : function(){
						$.common.reflashUsrInfo();
						$('#dialog').dialog('close');
					}
				});
			},

			deleteAction : function(id) {
				var _this = this;
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					
				}else{
					$.ajax({
						type: "GET",
						url: _this.path + "_messageAction.php",
						data: {"mode":"delete", "id":id },
						dataType: 'html',
						success: function(data){
							_this.wrapList();
						},
						complete : function(){	
							$.common.reflashUsrInfo();
							$('#dialog').dialog('close');
						}
					});
				}
			},

			/* Group Usr Manager */
			usrEdit : function() {
				var _this = this;
				var readIds = $("#readIds").val();
				var readNames = $("#readNames").val();
				$.ajax({
					type: "POST",
					url: _this.path + "_messageUsrForm.php",
					data: {"readIds":readIds, "readNames":readNames},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 450);
						$("#sub_dialog").dialog('option', 'buttons', {
							"확인" : function() { 
								_this.selectedReadUsr();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},
			
			rootGroupUsrList : function() {
				var _this = this;
				var root_id = $("#roots").val();
				$.ajax({
					type: "GET",
					url: _this.path + "_messageUsrSelectList.php",
					data: {"root_id":root_id},
					dataType: 'html',
					success: function(data){
						$("#unSelected").html(data);
					}
				});

			},		

			selectedReadUsr : function() {
				var _this = this;
				var arryReadIds = []; 
				var arryReadNames = [];
				$('#selected option').each(function(i) { 
					arryReadIds[i] = $(this).attr("value");
					arryReadNames[i] = $(this).html();
				});
				var readIds = arryReadIds.join("|");
				var readNames = arryReadNames.join("|");

				//alert(readIds+"-"+readNames);
				$("#readIds").val(readIds);
				$("#readNames").val(readNames);
				$("#sub_dialog").dialog("close"); 

			},

			_ver : "1.1.9"
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

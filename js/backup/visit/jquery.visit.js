
(function($)
{
	$.extend(
	{
		visit : 
		{
		//-----------------------------------------------------------------------------------------

            id : 0,
			mode : "",
			usr_id  : 0,
			item_id : 0,
			page : 1,

			visitUsrType : "",
			listBox : "contantBox",
			
			list : function (json){
				var _this = this;
				if(json.listBox  != undefined){ _this.listBox = json.listBox; }
				if(json.mode  != undefined){ _this.mode = json.mode; }
				if(json.usr_id  != undefined){ _this.usr_id = json.usr_id; } 
				if(json.page  != undefined){ _this.page = json.page; }
				_this.listWarp();
			},

			listWarp : function(){
				var _this = this;
				var src = "";
				if(_this.listBox == "usrVisitListBox"){
					src = "../user/_visitList.php";
				}else{
					src = "../report/_visitList.php";
				}
				$.ajax({
					type: "GET",
					url: src,
					data: {"page":_this.page,"mode":_this.mode, "usr_id":_this.usr_id},
					dataType: 'html',
					success: function(data){
						$("#"+_this.listBox).html(data);
					},
					complete : function(){}
				});
			},
			
			view : function(id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../user/_visitView.php?id="+id,
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방 보고서' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"수정" : function() { 
								_this.modify();
							},
							"닫기" : function() { 
								$(this).dialog("close");
							} ,  
							"삭제" : function() { 
								_this.deleteAction();
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

            insert : function (usr_id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../report/_visitForm.php",
					data: {"mode":"insert", "usr_id":usr_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방보고서 작성' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"보고서 작성" : function() { 
								_this.insertAction();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

			insertAction : function(){
				var _this = this,
					write_usr_id = $.trim($("#visitForm input[name='write_usr_id']").val()),
				    type    = $.trim($("#visitForm select[name='type']").val()),
				    reg_date = $.trim($("#visitForm input[name='reg_date']").val()),
				    title   = $.trim($("#visitForm input[name='title']").val()),
				    place   = $.trim($("#visitForm input[name='place']").val()),
				    content = $.trim($("#visitForm textarea[name='content']").val());

                var arryUsrId = new Array();
                $.each($("#usr_name > .usrGroup"), function() {
					arryUsrId.push($(this).attr("usrId"));
				});
				var usr_id = arryUsrId.join("|");

				var arryVisitUsrId = new Array();
                $.each($("#visit_usr_name > .usrGroup"), function() {
					arryVisitUsrId.push($(this).attr("usrId"));
				});
				var visit_usr_id = arryVisitUsrId.join("|");



				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == 0) {
					alert("심방종류를 선택하세요!");
					return false;
				}

				if(title == '') {
					alert("성경본문을 입력하세요!");
					return false;
				}

				if(content == '') {
					alert("내용을 입력하세요^^");
					return false;
				}	
				$.ajax({
					type: "POST",
					url: "../report/_visitAction.php",
					data: {
						mode: "visitInsert",
			            usr_id : usr_id,
						visit_usr_id : visit_usr_id,
						write_usr_id : write_usr_id,
						reg_date : reg_date,
						type : type,
						place : place,
						title : title,
						content : content					
					},
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
			},
			modify : function (visit_id,listBox){
				var _this = this;

				if(listBox  != undefined){ _this.listBox = listBox; }

				$.ajax({
					type: "GET",
					url: "../report/_visitForm.php",
					data: {"mode":"modify", "id":visit_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방보고서 수정' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"저장" : function() { 
								_this.modifyAction();
							} , 
								
							"삭제" : function() { 
								_this.deleteAction(visit_id);
							} , 

							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

			modifyAction : function(){
				var _this = this,
				    id      = $.trim($("#visitForm input[name='id']").val()),
					write_usr_id = $.trim($("#visitForm input[name='write_usr_id']").val()),
				    type    = $.trim($("#visitForm select[name='type']").val()),
				    reg_date   = $.trim($("#visitForm input[name='reg_date']").val()),
				    title   = $.trim($("#visitForm input[name='title']").val()),
				    place   = $.trim($("#visitForm input[name='place']").val()),
				    content = $.trim($("#visitForm textarea[name='content']").val());

				//var visit_usr_id = $("#visit_usr_name > usrGroup").attr("usrId").join("|");
                var arryUsrId = new Array();
                $.each($("#usr_name > .usrGroup"), function() {
					arryUsrId.push($(this).attr("usrId"));
				});
				var usr_id = arryUsrId.join("|");

				var arryVisitUsrId = new Array();
                $.each($("#visit_usr_name > .usrGroup"), function() {
					arryVisitUsrId.push($(this).attr("usrId"));
				});
				var visit_usr_id = arryVisitUsrId.join("|");


                
				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == 0) {
					alert("심방종류를 선택하세요!");
					return false;
				}
				if(title == '') {
					alert("제목을 이력하세요^^");
					return false;
				}
				if(content == '') {
					alert("내용을 이력하세요^^");
					return false;
				}
				$.ajax({
					type: "POST",
					url: "../report/_visitAction.php",
					data: {
						mode: "visitModify",
						id : id,
			            usr_id : usr_id,
						visit_usr_id : visit_usr_id,
						write_usr_id : write_usr_id,
						reg_date : reg_date,
						type : type,
						place : place,
						title : title,
						content : content					
					},
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
				
			},

			deleteAction : function(visit_id) {
				var _this = this;
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					
				}else{
					$.ajax({
						type: "GET",
						url: "../report/_visitAction.php",
						data: {"mode":"visitDelete", "id":visit_id },
						dataType: 'json',
						success: function(data){
							_this.listWarp();
							$('#sub_dialog').dialog('close');
						}
					});
				}
			},
        
			searchContact : function(usrType){
				var _this = this;

				_this.visitUsrType = usrType;
				$("#search_dialog").dialog('option', 'title', '연락처 추가');
				$("#search_dialog").dialog('option', 'width', 350);
				$("#search_dialog").dialog('option', 'height', "auto");
				$.ajax({
					type: "GET",
					url: "../report/_visitContactSearch.php",
					data:{ },
					dataType: "html",
					success: function(data){
						$("#search_dialog").html(data);
					},
					complete : function(){ 		
					}
				});

				$("#search_dialog").dialog('option', 'buttons', {
					"추가하기" : function() {
						_this.addContact(); 
						$(this).dialog("close"); 
					},
					"닫기" : function() { 
						$(this).dialog("close"); 
					}
				});

				$("#search_dialog").dialog("open");
			},

			search : function(page) {
				var _this = this;
				
				var q = $("#visit_qq").val();
				
			    
				if(page == undefined){ page = 1; }
				if(q != ""){
					var keyword = encodeURIComponent(q);
					_this.searchContactList({"q":keyword,"page":page});
				}
			},

			searchContactList : function(json) {
				var _this = this;
				
				
				console.log(json);
				
				$.ajax({
					type : "GET",
					url : "../report/_visitContactList.php",
					data : json,
					dataType : "html",
					success: function(data){
						
						$("#visitSearchList").html(data);
					}
				});
			},

			addContact: function() {
				var _this = this;
				var visitVal = $("#visitSearchList input[type=radio]:checked").val();
				if(visitVal == "" || !visitVal){
					alert("회원을 선택하세요!");
				}else{
					var arrayVisit = visitVal.split("@");

					if(_this.visitUsrType == "usr_id"){
						//$("#visitForm #usr_id").val(arrayVisit[0]);
						//$("#visitForm #usr_name").html(arrayVisit[1]+"<span style='color:#999;'>"+arrayVisit[2]+"</span>"); 
						$("#visitForm #usr_name").append('<div class="usrGroup" usrid="'+arrayVisit[0]+'">'+arrayVisit[1]+'<a class="remove" href="javascript:$.visit.removeUsr(\'usr_name\','+arrayVisit[0]+');"></a></div>');
					}else if(_this.visitUsrType == "visit_usr_id"){
						//$("#visitForm #visit_usr_id").val(arrayVisit[0]);
						//$("#visitForm #visit_usr_name").html(arrayVisit[1]+"<span style='color:#999;'>"+arrayVisit[2]+"</span>"); 
						$("#visitForm #visit_usr_name").append('<div class="usrGroup" usrid="'+arrayVisit[0]+'">'+arrayVisit[1]+'<a class="remove" href="javascript:$.visit.removeUsr(\'visit_usr_name\','+arrayVisit[0]+');"></a></div>');

					}
				}			
			},


			removeUsr: function(mode,id) {
				if(mode == "usr_name"){
					$("#"+mode+" div[usrid="+id+"]").remove();

				}else if(mode == "visit_usr_name"){
					$("#"+mode+" div[usrid="+id+"]").remove();

				}


			},

			_ver : "1.0.1"
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

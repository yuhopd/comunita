(function($)
{
	$.extend(
	{
		family : 
		{
		//-----------------------------------------------------------------------------------------

		page : 1,
		pagination : 30,
		
		usr_id : 0,
		listBox : "",

		list : function (json){
			var _this = this;
			_this.listBox = json.listBox;
			_this.usr_id  = json.usr_id;
			_this.listWarp();
		},

	
		listWarp : function() {
			var _this = this;
			var keyword = encodeURIComponent($.trim($('#family_q').val()));
			$.ajax({
				type : "GET",
				url : "../user/_familyList.php",
				data : {
					"q" : keyword,
					"usr_id":_this.usr_id
				},
				dataType : "html",
				success: function(data){
					$("#"+_this.listBox).html(data);
				},
				complete : function(){ }
			});
		},

		search : function(usr_id) {
			var _this = this;
			_this.list({"usr_id":usr_id , "listBox":"familySearchList"});
	    },

		deleteAction : function(usr_id,family_id) {
			var _this = this;
			if(!confirm("정말로 회원 가족관계를 삭제하시겠습니까?")){
			}else{
				$.ajax({
					type : "GET",
					url : "../user/_familyAction.php",
					data : {
						"mode": "usr_family_delete",
						"usr_id": usr_id,
						"family_id": family_id							
					},
					dataType : "json",
					success: function(data){
						_this.list({"usr_id":usr_id, "listBox": "usrFamilyList"});
					}
				});
			}
		},

		insert : function(usr_id) {

			$("#sub_dialog").dialog('option', 'title', '가족관계 추가' );
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', "auto");
			$("#sub_dialog").load("../user/_familySearch.php?usr_id="+usr_id);
			$("#sub_dialog").dialog('option', 'buttons', {});
			$('#sub_dialog').dialog('open');
		},

		insertAction : function(usr_id) {		
			var _this = this;
            var family_id = $("#familySearchList input.radio:checked").val();
		    var relations = $('#relations').val();
			if(relations == 0){
				alert("관계를 선택하세요!");
			} else if(family_id == 0 || !family_id){
				alert("가족을 선택하세요!");
			}else{
				$.ajax({
					type : "GET",
					url : "../user/_familyAction.php",
					data : {
						"mode"      : "usr_family_insert",
						"usr_id"    : usr_id,
						"family_id" : family_id,
						"relations" : relations
					},
					dataType : "json",
					success: function(data){
						if(data.error > 0){
							alert(data.message);
						}else{
							$('#family_q').val("");
							_this.list({"usr_id":usr_id, "listBox": "usrFamilyList"});
							$('#sub_dialog').dialog('close');
						}
					}
				});
			}
		},

		_var : "1.1.5"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

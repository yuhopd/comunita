(function($)
{
	$.extend(
	{
		group : 
		{
		/* GROUP manager ---------------------------------------------------------------------- */
		group_id : 0,

		list : function(group_id){
			var _this = this;
			if(group_id  != undefined){ _this.group_id = group_id; }
            _this.listWarp();
		},

		listWarp : function(){
			var _this = this;
			$("#groups").html("");
			$("#groups").treeview({
				url: "../mypage/_groupListJson.php?group_id="+_this.group_id,
				animated: "fast",
				control:"#sidetreecontrol",
				prerendered: true,
				persist: "location"
			});
		},

		insert : function(parent_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹 추가' );
			$("#dialog").load("../mypage/_groupForm.php?mode=insert&parent_id="+parent_id);
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"그룹추가" : function() { 
					_this.insertAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		insertAction : function() {
			var _this = this;
			var form_var = $('#groupForm').serialize();
			var group_title = $.trim($('#group_title').val());
			if(group_title == '') {
				alert('그룹명을 입력하세요!');
			}else{
				$.ajax({
					type: "POST",
					url: "../mypage/_groupAction.php",
					data: form_var,

					success: function(data){
						$("#dialog").dialog("close"); 
						_this.listWarp();
					}
				});
			}
		},

		edit : function(group_id, isOpen) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹수정' );
			$.ajax({
				type: "GET",
				url: "_groupForm.php",
				data: {"mode":"modify","group_id":group_id},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);
				},
				complete : function(){
					//$("#dialog select").uniform();
				}
			});



			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.editAction(group_id);
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		editAction : function(group_id) {
			var _this = this;
			var form_var = $('#groupForm').serialize();
			var group_title = $.trim($('#group_title').val());
			if(group_title == '') {
				alert('그룹명을 입력하세요!');
			} else {
				$.ajax({
					type: "POST",
					url: "../mypage/_groupAction.php",
					data: form_var+"&group_id="+group_id,
					success: function(){
						$("#dialog").dialog("close"); 
						_this.listWarp();
					}
				});
			}
		},
        
		/* Group Usr Manager */
		usrEdit : function(group_id, isOpen) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹회원 관리' );
			$("#dialog").load("../mypage/_groupUsrForm.php?group_id="+group_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrEditAction(group_id);
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}

		},

		usrEditAction : function(group_id) {
			var multipleValues0 = [];
			$('#selected_level_0 option').each(function(i) { 
				multipleValues0[i] = $(this).attr("value");
			});
			var users0 = multipleValues0.join("|");

			var multipleValues1 = [];
			$('#selected_level_1 option').each(function(i) { 
				multipleValues1[i] = $(this).attr("value");
			});
			var users1 = multipleValues1.join("|");

			var multipleValues2 = [];
			$('#selected_level_2 option').each(function(i) { 
				multipleValues2[i] = $(this).attr("value");
			});
			var users2 = multipleValues2.join("|");

			$.ajax({
				type: "POST",
				url: "../mypage/_groupAction.php",
				data: {
					"mode"  : "usr_edit", 
					"group_id" : group_id, 
					"usrs0" : users0,
					"usrs1" : users1,
					"usrs2" : users2
				},
				success: function(){
					$("#dialog").dialog("close"); 
					//$.group.list();
				}
			});
		},

        /* Attendance Manager */
        attendanceList : function(group_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 리스트' );
			$("#dialog").load("../mypage/_groupAttendanceList.php?group_id="+group_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});

			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		attendanceInsert : function(group_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 추가' );
			$("#dialog").load("../mypage/_groupAttendanceForm.php?mode=insert&group_id="+group_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"출석부 추가" : function() { 
					$.group.attendanceAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},


		attendanceModify : function(attendance_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 수정' );
			$("#dialog").load("../mypage/_groupAttendanceForm.php?mode=modify&attendance_id="+attendance_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"출석부 수정" : function() { 
					$.group.attendanceAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		attendanceAction : function(){
			var attendance_title = $('#attendance_title').val();
            if(attendance_title == ""){
				alert("출석부 이름을 입력하세요");
				return false;
			}

			var form_var = $('#groupAttendanceForm').serialize();
			var group_id = $('#group_id').val();
			$.ajax({
				type: "POST",
				url: "../mypage/_groupAction.php",
				data: form_var,
				success: function(){
					$.group.attendanceList(group_id,'O');
				}
			});
		},


		attendanceDelete : function(attendance_id,group_id) {
			if(!confirm("정말로 출석부를 삭제하시겠습니까?")){
				
			}else{
				jQuery.get('../mypage/_groupAction.php',{"mode": "deleteAttendance", "id": attendance_id },function() {
					$.group.attendanceList(group_id,'O');
				});	
			}

		},
        
		

		excelStep1 : function(group_id) {
			$("#content_wrap").load("../mypage/_excelUpload_1.php?group_id="+group_id);
		},

		excelStep2 : function(group_id,filename) {
			$("#content_wrap").load("../mypage/_excelUpload_2.php?group_id="+group_id+"&filename="+filename);
		},

        excelStep2Action : function(group_id) {
			var form_var = $('#excelForm').serialize();
			$.ajax({
				type: "POST",
				url: "../mypage/_groupAction.php",
				data: form_var,
				dataType: 'json',
				success: function(json){
					alert(json.massage);
					location.replace("../mypage/index.php");		
				}
			});

		},

		listAction : function(group_id) {
			var _this = this;
			var mode = $("#groupAction_"+group_id).val();
			if(mode == "modify"){
				_this.edit(group_id);
			} else if(mode == "usrSet"){
				_this.usrEdit(group_id);
			} else if(mode == "childAdd"){
				_this.insert(group_id);
			} else if(mode == "del"){
				_this.delete_action(group_id);
			} else if(mode == "excelUpload"){
				_this.excelStep1(group_id);
			} else {

			}
		},

		delete_action : function(group_id) {
			if(!confirm("정말로 그룹을 삭제하시겠습니까?")){
				return false;
			}else{
				jQuery.get('../mypage/_groupAction.php',{"mode": "delete", "group_id": group_id },function() {
					$.group.list();
				});
			}
			return true;
		},
		
		rootGroupUsrList : function() {
			var root_id = $("#roots").val();
			$.ajax({
				type: "GET",
				url: "../mypage/_groupUsrSelectList.php",
				data: {"root_id":root_id},
				dataType: 'html',
				success: function(data){
					$("#unSelected").html(data);
				}
			});

		},


		upSequence : function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../mypage/_groupAction.php",
				data: {"mode":"upSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		},
		
		downSequence : function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../mypage/_groupAction.php",
				data: {"mode":"downSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});

		},


		_ver : "1.0.1"
		/* GROUP manager ---------------------------------------------------------------------- */
		}
	});
})(jQuery);



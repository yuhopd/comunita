define([
    'jquery',
    'Config',
	'jquery-ui'
], function($, config){
	// kkButton
    $.widget("ui.kkButton",{
        options: {
            _id : "",
            type : "",
            callback : null
        },

        _create: function() {
            this._event();
            this._id = this.element.attr("u-id");
        },

        _init : function(){

        },

        _event : function(){
            var _this = this;
            this._on( _this.element, {
                "mousedown" : function(e) {
                    _this._trigger("callback", e, {"id":_this._id});
					return false;
                }
            });
        },

        _destroy: function() {
            this.element.remove();
        },

        _setOptions: function( options ) {
            var that = this;
            $.each( options, function( key, value ) {
                that._setOption( key, value );
            });
        },

        _setOption: function( key, value ) {
            this._super( key, value );
        },

        setActive: function() {
            this.unsetActive();
            this.element.addClass("active");
        },

        unsetActive: function() {
            this.element.removeClass("active");
        },

        setDisabled: function() {
            this.unsetDisabled();
            this.element.addClass("disabled");
        },

        unsetDisabled: function() {
            this.element.removeClass("disabled");
        }
    });

	// kkSelectBox
	$.widget("ui.kkSelectBox",{
		options:{
			id : "",
			label : "name2",
            defaultVal : "value2",
			currentVal : "value2",
            callback  : null,
			displayMode : "top",
			item: [],
            mode: "select"
		},

		_create:function(){
            var _this = this,
				lebel = "",
				strSelected = "",
                css = "",
                wrap = "";

            this.options.id = this.element.attr("u-id");
            this.options.mode = this.element.attr("mode");
            this.options.defaultVal = this.element.attr("val");
            this.options.currentVal = this.element.attr("val");
            this.options.item = config[this.options.id] || [];


			var displayModeClass = "";
			if(this.options.displayMode == "bottom"){
				displayModeClass = " ui_bottom";
			}

            if(_this.options.mode == "select"){
                var iconClassType = "kk-icon-arrow_drop_down";

                wrap = '<div ui-type="'+ _this.options.id +'" ui-item-count="'+_this.options.item.length+'" class="ui_selectbox"><div class="arrow"><i class="' + iconClassType + '"></i></div>'+
                    '<ul class="ui_select_list '+displayModeClass+'">';
                $.each(_this.options.item,function() {
                    strSelected = "";
                    if(_this.options.defaultVal == this.value){
                        lebel = this.name;
                        strSelected = "selected";
                    }
                    wrap += '<li value="'+this.value+'" '+strSelected+'>'+this.name+'</li>';
                });
                wrap += '</ul><label class="btnDropdown" ui-type="'+ this.options.type +'" >'+lebel+'</label></div>';
                this.element.html(wrap);
            } else {
                wrap = '<ul ui-type="'+ _this.options.id +'" ui-item-count="'+_this.options.item.length+'" class="'+ _this.options.id +' ui_combobox modeless'+displayModeClass+' '+ _this.options.id +'" >';
                $.each(_this.options.item,function() {
                    strSelected = "";
                    css = "";
                    if(this.css != ""){ css = 'class="'+this.css+'"'; }
                    if(_this.options.defaultVal == this.value){
                        label = this.name;
                        strSelected = "selected";
                    }

                    if(_this.options.id == "shapeBorderDash"){

						wrap += '<li value="'+this.value+'" '+css;
						if (this.type) {
							wrap += "type='" + this.type + "'";
						}
						wrap += strSelected + " ";

						if (this.key) { wrap += "key='" + this.key + "'"; }
						if (this.cmd) { wrap += "cmd='" + this.cmd + "'"; }

						var linetype = _this.getDashLineType(this.value);


						wrap += '>'+
								'<svg value="'+this.value+'"><polyline xmlns="http://www.w3.org/2000/svg" points="0,0 120,0" stroke-dasharray="'+linetype+'" stroke="#000000" stroke-width="10" value="'+this.value+'"></polyline></svg>'+
							'</li>';
					}else{
						wrap += '<li value="'+this.value+'" '+css;
						if (this.type) {
							wrap += "type='" + this.type + "'";
						}
						wrap += strSelected + " ";

						if (this.key) { wrap += "key='" + this.key + "'"; }
						if (this.cmd) { wrap += "cmd='" + this.cmd + "'"; }
						wrap += '>'+this.name+'<span value="'+this.value+'"></span></li>';
					}


                });
                wrap += '</ul>';
                this.element.append(wrap);
            }
            this.element.find("ul > li[type != 'label'], ul > li[type != 'label'] span").on("mousedown", function (e) {
                _this._toggle();
                _this.setValue(e, $(e.target).attr("value"));
                return false;
            });

            this._event();
			if( this.options.hidden ){
				this.element.hide();
			}
		},

		_init : function() {

		},

		_destroy: function() {
			this.element.remove();
		},

		_setOptions: function( options ) {
			var that = this;
			$.each( options, function( key, value ) {
				that._setOption( key, value );
			});
		},
		_setOption: function( key, value ) {
			this._super( key, value );
		},

        _event : function(){
            var _this = this;
            this._on( _this.element, {
                "mousedown" : function(e) {
                    if ($(e.target).hasClass('f_list_ol') || $(e.target).hasClass('f_list_ul')){
                        _this.element.removeClass("ui_active")
                        $("#kk_dialog").css("overflow","visible");
                    }else{
                        _this._toggle();
						return false;
                    }
                }
            });
            if(_this.mode == "select"){

                var cH = (_this.options.item.length * 25);
                var pH  = ($(window).height() - ($("#toolBar").height() + $("#statusBar").height()));
                if(cH > pH){ _this.element.find("ul").css("height", pH+"px"); }
                $(window).resize(function () {
                    pH  = ($(window).height() - ($("#toolBar").height() + $("#statusBar").height()));
                    if(cH > pH){ _this.element.find("ul").css("height", pH+"px"); }
                });
            }
        },

		//Text Color Layer 실행
		_toggle : function(){
            var _this = this;
            if(_this.options.mode == "select"){
                if(_this.element.find(".ui_selectbox").hasClass("ui_active")){
                    _this.element.find(".ui_selectbox").removeClass("ui_active");
					$("#kk_dialog").css("overflow","auto");
					$(".modeless").hide();
                }else{
                    $(".ui_active").removeClass("ui_active");
                    _this.element.find(".ui_selectbox").addClass("ui_active");
					$("#kk_dialog").css("overflow","visible");
					$(".modeless").hide();
					_this.element.find(".ui_combobox").show();
                }
            } else {
                if(_this.element.hasClass("ui_active")){
                    _this.element.removeClass("ui_active")
					$("#kk_dialog").css("overflow","visible");
					$(".modeless").hide();
                }else{
                    $(".ui_active").removeClass("ui_active");
                    _this.element.addClass("ui_active");
					$("#kk_dialog").css("overflow","auto");
					$(".modeless").hide();
					_this.element.find(".ui_combobox").show();
                }
            }
		},

		setValue : function (e, value){
            var _this = this;
            //console.log("---------------"+value);
			this.currentVal = value;
            this.element.find("li[selected]").removeAttr("selected");
			var text = this.element.find("li[value='"+value+"']").attr("selected","selected").text();
			if (!text) { text = value; }
            this.element.find("label").html(text);
            this._trigger("callback", e, {id:_this.options.id, mode:_this.options.mode,label:text,val:value});
		},

		getDashLineType : function(dash) {
			switch(dash) {
				case "solid":
					return "";
					break;
				case "dot":
				case "sysDot":
					return "5,5";
					break;
				case "sysDash":
					return "10,5";
					break;
				case "dash":
					return "15,10";
					break;
				case "dashDot":
				case "sysDashDot":
					return "15,10,5,10";
					break;
				case "lgDash":
					return "25,10";
					break;
				case "lgDashDot":
					return "25,10,5,10";
					break;
				case "lgDashDotDot":
				case "sysDashDotDot":
					return "25,10,5,10,5,10";
					break;
				default:
					return "none";
					break;
			};
		}
	});

    // kkColorPicker
    $.widget("ui.kkColorPicker",{
        options: {
            type : "",
			displayMode: "top",
            defaultColor : "000000",
            currentColor : "000000",
            noAction : false,
            colorParent : null,

            label:{
                standard: "표준 색",
                thame: "테마 색",
                default: "기본 색"
            },
            pallete : {
                defaultColor  : "000000",
                noneColor  : "transparent",
                simpleColor   : ["ffffff", "000000", "eeece1", "1f497d", "4f81bd", "c0504d", "9bbb59", "8064a2", "4bacc6", "f79646"],
                standardColor : ["f2f2f2", "808080", "ddd9c3", "c6d9f1", "dce6f2", "f2dcdb", "ebf1de", "e6e0ec", "dbeee0", "fdeada",
                    "d9d9d9", "595959", "c4bd97", "8eb4e3", "b9cde5", "e6b9b8", "d7e4bd", "ccc1da", "b7dee8", "fcd5b5",
                    "bfbfbf", "404040", "948a54", "558ed5", "95b3d7", "d99694", "c3d69b", "b3a2c7", "93cddd", "fac090",
                    "a6a6a6", "262626", "4a452a", "17375e", "376092", "953735", "77933c", "604a7b", "31859c", "e46c0a",
                    "808080", "0d0d0d", "1e1c11", "10243f", "254061", "632523", "4f6228", "403152", "215968", "984807"],
                normalColor   : ["c00000", "ff0000", "ffc000", "ffff00", "92d050", "00b050", "00b0f0", "0070c0", "002060", "7030a0"]
            },
            callback  : null
        },

        _create: function() {

            this.options.id = this.element.attr("u-id");
            this.options.defaultColor = this.element.attr("color");
            this.options.currentColor = this.element.attr("color");

			var displayModeClass = "";
			if(this.options.displayMode == "bottom"){
				displayModeClass = " ui_bottom";
			}

            var wrap = '';
                wrap += '<div class="ui_colorpicker modeless'+displayModeClass+'">';
                wrap += '<ul class="ui_sub_default_color"><li color="'+this.options.pallete.noneColor+'"><span class="ui_default_color" style="background-color:rgba(0,0,0,0);"></span>투명색</li></ul>';
                wrap += '<div class="ui_standard_color_text">'+this.options.label.thame+'</div>';
                wrap += '<ul class="ui_default_color_palette">';
                $.each(this.options.pallete.simpleColor, function(i,v) {
                    wrap += '<li style="background-color:#'+v+'" color="'+v+'"></li>';
                });
                wrap += '</ul><ul class="ui_color_palette">';

                $.each(this.options.pallete.standardColor, function(i,v) {
                    wrap += '<li style="background-color:#'+v+'" color="'+v+'"></li>';
                });
                wrap += '</ul>'+
                    '<div class="ui_standard_color_text">'+this.options.label.standard+'</div>'+
                    '<ul class="ui_standard_color_palette">';
                $.each(this.options.pallete.normalColor, function(i,v) {
                    wrap += '<li style="background-color:#'+v+'" color="'+v+'"></li>';
                });
                wrap +='</ul>';
                wrap +='</div>';

            this.element.append(wrap);




            this._event();
        },

        _init : function(){
            var _this = this;
            _this.element.find("a span.selectdColor").css("background-color","#"+_this.options.currentColor);			    // 선택된 색상 설정
            _this.element.find("li > span.defaultColor").css("background-color","#"+_this.options.pallete.defaultColor);  	// 기본 색상 설정
			//_this.element.find("ul.ui_sub_default_color > li").attr("color", "#"+_this.options.pallete.defaultColor);		// default 색상 적용
        },

        _event : function(){
            var _this = this;

            this._on( _this.element, {
                "click" : function(e) {
                    _this._toggle();
					return false;
                }
            });
            this._setAction();
        },


        _destroy: function() {
            this.element.find(".uiColorPicker").remove();
        },

        _setOptions: function( options ) {
            var that = this;
            $.each( options, function( key, value ) {
                that._setOption( key, value );
            });

        },
        _setOption: function( key, value ) {
            this._super( key, value );
        },

        //Text Color Layer 실행
        _toggle : function(){
            if(this.element.find(".ui_colorpicker").is(':hidden')){
                $(".modeless").hide();
                this.element.find(".ui_colorpicker").show();
            }else{
                this.element.find(".ui_colorpicker").hide();
            }
        },

        //리소스 삽입
        _setAction : function(){
            var _this = this;
            this.element.find("ul > li").on("mousedown", function (e) {
                var color = $(this).attr("color");
                _this.element.find(".btnCurrent").attr("value", color);

                _this._setValue(color);
                _this._toggle();
				_this._trigger("callback", e,{id:_this.options.id,color:color});
                return false;
            });
        },

        _setValue : function (color){
            var _this = this;
            _this.options.currentColor = color;
            this.element.find(".ui_semple").css("background-color","#"+color);
            this.element.find("ul > li[selected]").removeAttr("selected");
            this.element.find("ul > li[color='"+color+"']").attr("selected","selected");

        },

        //메뉴 아이템 현재값 가져오기
        getParentMenuItemValue : function(){
            return this.element.find("a  span.selectdColor").css("background-color");
        }
    });

    // kkShapePicker
    $.widget("ui.kkShspePicker",{
        options: {
            type : "format",
            defaultColor : "000000",
            currentColor : "000000",
            noAction : false,
            colorParent : null,
            item:[],
            lang:"ko",
            callback : null
        },

        _create: function() {
            var _this = this;

            this.options.id = this.element.attr("u-id");

            if(this.options.type == "format"){
                $.each(this.element.find("select > option"), function() {
                    _this.options.item.push({name:$(this).attr("name"), styleId:$(this).html()});
                });
            }
            var wrap = '';

            wrap += '<div class="ui_style_panel"><ul class="ui_shape_list">';
            if(this.options.type == "format"){
                $.each(this.options.item, function() {
                    wrap += '<li>' +
                    '<div class="title"><p pstyle="'+ this.styleId +'" style="padding:0; text-indent:0;"><span>가나다AaBbCcDd</span></p></div>' +
                    '<div class="label">'+ this.name +'</div>' +
                    '</li>';
                });
            }else if(this.options.type == "table"){
                $.each(this.options.item, function() {
                    wrap += '<li class="t_theme">' +
                    '<div class="theme '+ this.styleId +'"></div>' +
                    '</li>';
                });
            }
            wrap += '</ul>';
            wrap += '</div>';
            wrap += '<a href="#" class="more"><i class="i_icon i-down-square"></i></a>';
            wrap += '<a href="#" class="prev"><i class="arrow_icon i-arrow-up"></i></a>';
            wrap += '<a href="#" class="next"><i class="arrow_icon i-arrow-down"></i></a>';
            wrap += '<a href="#" class="close"><i class="i_icon i-close-x"></i></a>';
            this.element.html(wrap);
            this._event();
        },

        _init : function(){

        },
        _event : function(){
            var _this = this;
            this.element.find("a.more").on("click", function (e) {
                _this._toggle();
            });

            this.element.find("a.close").on("click", function (e) {
                _this._toggle();
            });

            this.element.find("a.prev").on("click", function (e) {
                _this.scrollUp();
            });

            this.element.find("a.next").on("click", function (e) {
                _this.scrollDown();
            });

            this._setAction();
        },
        _destroy: function() {

        },
        _setOptions: function( options ) {
            var that = this;
            $.each( options, function( key, value ) {
                that._setOption( key, value );
            });
        },
        _setOption: function( key, value ) {
            this._super( key, value );
        },

        //Text Color Layer 실행
        _toggle : function(){
            var _this = this;
            if(_this.element.hasClass("ui_active")){
                _this.element.removeClass("ui_active");
            }else{
                $(".ui_active").removeClass("ui_active");
                _this.element.addClass("ui_active");
            }
            _this.element.find(".ui_style_panel").scrollTop(0);
        },

        //리소스 삽입
        _setAction : function(){
            var _this = this;
            this.element.find("ul > li").on("click", function (e) {
                // var color = $(this).attr("color");
                // _this.element.find(".btnCurrent").attr("value", color);
                // _this._setValue(color);
                // _this._toggle();

                if ($(e.target).closest(".ui_style_picker").hasClass("ui_active")){
                    _this._toggle();
                }

                var pStyleValue = $(e.target).closest("li").find("p").attr("pstyle");

                _this._trigger("callback", e,{id:_this.options.id, value:pStyleValue});
                return false;
            });
        },

        _setValue : function (color){
            // var _this = this;
            // _this.options.currentColor = color;
            // this.element.find("i").css("color","#"+color);
            // this.element.find("ul > li[selected]").removeAttr("selected");
            // this.element.find("ul > li[color='"+color+"']").attr("selected","selected");
        },

        scrollUp : function (){
            var _this = this;
            var scroll = _this.element.find(".ui_style_panel").scrollTop();
            scroll =  scroll - 59;
            _this.element.find(".ui_style_panel").scrollTop(scroll);

        },

        scrollDown : function (){
            var _this = this;
            var scroll = _this.element.find(".ui_style_panel").scrollTop();
            scroll =  scroll + 59;
            _this.element.find(".ui_style_panel").scrollTop(scroll);
        },

        //메뉴 아이템 현재값 가져오기
        getParentMenuItemValue : function(){
            return this.element.find("a  span.selectdColor").css("background-color");
        }
    });

	//kkZoomSlider
	$.widget("ui.kkZoomSelect",{
		options: {
            mode : "jpg",
			min: 0.1,
			max: 4,
			value: 1,
			step:0.1,
			isMobile:false,
			callback : null
		},

		_create: function() {
            var _this = this;
            var wrap = '<div class="kk_selectbox" mode="select" val="100%" u-id="zoomRate" style="width:80px;"></div>';
            this.element.html(wrap);
            this.element.find(".kk_selectbox").kkSelectBox({
                callback:function(e,data){

                    $("#kk_zoomSlider").kkZoomSelect("pageZoom",data.val);
                }
            });
            this._event();
        },

        _init : function(){
			//this.pageZoom(1);
			//this.element.html(Math.round(100) + "%");
        },

        _event : function(){
            var _this = this;
		},

		pageZoom : function(zoom) {
            var _this = this;
			this.options.value = zoom;
            var mg_t = 0;
            var mg_l = 0;

			//this.element.find("#resizaPage").slider("value", zoom);

            var $documentView = $("#currentSlide");
            $documentView.css("transform", 'scale(' + zoom + ')');
            $documentView.css("transform-origin", '0 0');

            $documentView.attr("kk_zoom",zoom);
            var wrap_h = $documentView.height() * zoom;
			var wrap_w = Number($documentView.css("width").replace("px","")) * zoom;
            var wheight = window.innerHeight ? window.innerHeight : $(window).height();
			var win_h = wheight - $("#toolBar").height() - $("#statusBar").height();
			var win_w = $(window).width() - $("#lSideView").width();
			$documentView.parent().scrollTop(0).scrollLeft(0);

            console.log(wheight+"---------"+win_w+"--------"+wrap_w);
            //if(this.options.mode == "jpg"){

			if(win_h > wrap_h){
				mg_t = 0.5*(win_h - wrap_h);
				//$documentView.css("margin-top",(mg_t)+"px");
			}else{
				//$documentView.css("margin-top","0px");
			}

			if(win_w >= wrap_w){
				mg_l = 0.5*(win_w - wrap_w);
				//$documentView.css("margin-left",(mg_l)+"px");

				//$documentView.css("margin-right","0px");
			}else{
				//$documentView.css("margin-left","0px");
				//$documentView.css("margin-right","0px");
			}
            //$documentView.css("margin-top","0px");
			//$documentView.css("margin-left","0px");

            $documentView.css("transform","translateY("+mg_t+"px) translateX("+mg_l+"px) scale(" + zoom + ")");

			if((win_h >= wrap_h)&&(win_w >= wrap_w)){
				$(".deskView .editView").css("overflow","hidden");
			}else if((win_w >= wrap_w) && (win_h < wrap_h)){
				$(".deskView .editView").css("overflow-x","hidden");
				$(".deskView .editView").css("overflow-y","auto");

			}else if((win_w < wrap_w) && (win_h >= wrap_h)){
				$(".deskView .editView").css("overflow-x","auto");
				$(".deskView .editView").css("overflow-y","hidden");
			}else{
				$(".deskView .editView").css("overflow-x","auto");
				$(".deskView .editView").css("overflow-y","auto");
			}

            //$documentView.css("transform", 'scale(' + zoom + ') translateX(-50%)');
            //$documentView.css("transform-origin", '50% 0%');

            var rate_percent = Math.round((zoom)*100);
			_this.element.find("select").val(zoom);
            _this.element.find("label").html(rate_percent+"%");
            $("#contentView").attr("kk_zoom",zoom);
			_this.element.find("select").blur();

			//_this.element.text(Math.round(zoom*100) + "%");
			//console.log("----------"+Math.round(zoom*100)+"%");
			if(zoom == 1){
			}else{

			}
             _this._trigger("callback",{},{"zoom":zoom});
		},

		pageZoomIn : function(step) {
            console.log(this.options.value);
            console.log(this.options.step);

            if (typeof(step) != "undefined"){
                this.options.step = Number(step);
            }else{
                this.options.step = 0.1;
            }
            var z = Number(this.options.value) + Number(this.options.step);

			if(this.options.value >= this.options.max){
				z = this.options.value;
			}
            z = Number(z);
            this.pageZoom(z);
            //this.element.find("#resizaPage").slider("value", z);
		},

		pageZoomOut : function(step) {
            console.log(this.options.value);
            console.log(this.options.step);
            if (typeof(step) != "undefined"){
                this.options.step = Number(step);
            }else{
                this.options.step = 0.1;
            }
            var z = Number(this.options.value) - Number(this.options.step);
			if(this.options.value <= this.options.min){
				z = this.options.value;
			}
            z = Number(z);
            this.pageZoom(z);
            //this.element.find("#resizaPage").slider( "value", z);
		},

		pageZoomDefault : function() {
            var z = 1;
			if(this.options.value <= this.options.min){
				z = this.options.value;
			}
            z = Number(z);
            this.pageZoom(z);
            //this.element.find("#resizaPage").slider( "value", z);
		},


		_setOptions: function( options ) {
            var that = this;
            $.each( options, function( key, value ) {
                that._setOption( key, value );
            });
        },

        _setOption: function( key, value ) {
            this._super( key, value );
        },
		_ver : "1.0.1"
	});

	$.cookie = function(name, value, options) {
		if (typeof value != 'undefined') { // name and value given, set cookie
			options = options || {};
			if (value === null) {
				value = '';
				options.expires = -1;
			}
			var expires = '';
			if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
				var date;
				if (typeof options.expires == 'number') {
					date = new Date();
					date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
				} else {
					date = options.expires;
				}
				expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
			}
			// CAUTION: Needed to parenthesize options.path and options.domain
			// in the following expressions, otherwise they evaluate to undefined
			// in the packed version for some reason...
			var path = options.path ? '; path=' + (options.path) : '';
			var domain = options.domain ? '; domain=' + (options.domain) : '';
			var secure = options.secure ? '; secure' : '';
			document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
		} else { // only name given, get cookie
			var cookieValue = null;
			if (document.cookie && document.cookie != '') {
				var cookies = document.cookie.split(';');
				for (var i = 0; i < cookies.length; i++) {
					var cookie = jQuery.trim(cookies[i]);
					// Does this cookie string begin with the name we want?
					if (cookie.substring(0, name.length + 1) == (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
		}
	};
});

define([
    'jquery'
    , 'underscore'
    , 'Config'
    , 'ShapeUtil'
    , 'DefaultShape'
    , 'Loader'
	, 'SettingsDef'
    , 'jquery-ui'
    , 'jquery-ui-kukudocs'
	, 'toastmessage'
	, 'XDomainRequest'
], function ($, _, config, ShapeUtil, DefaultShape, Loader, SettingsDef) {
    var Menu = function(){
        var _menuContext = this;
        this.alreadySaved = true;
        this.isFirstLoaded = true;
		this.kkSelectable = false;
		this.isMobile = false;
		this.ratio = 1;
        this.init = function (){
			var _this = this;
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){ _this.isMobile = true; }
			this.kkRotate = 0;
			this.loader = new Loader();
			this.loader.init();
            this.documentId  = $("#currentSlide").attr("document_id");
            $(window).keydown(function(e) {
                if ((e.which == 187) &&(e.ctrlKey || e.metaKey)) {
                    _this.menuSwitch({id:"zoomin"});
                    return false;
                }
                if ((e.which == 189) &&(e.ctrlKey || e.metaKey)) {
                    _this.menuSwitch({id:"zoomout"});
                    return false;
                }
                if ((e.which == 48) &&(e.ctrlKey || e.metaKey)) {
                    _this.menuSwitch({id:"zoomdefault"});
                    return false;
                }

                if ((e.which == 80) &&(e.ctrlKey || e.metaKey)) {
					return false;
				}

				if (e.which == 34){
                    _this.menuSwitch({id:"nextPage"});
                    return false;
				}

				if (e.which == 33){
                    _this.menuSwitch({id:"prevPage"});
                    return false;
				}
     			if (e.which == 122){
                    return false;
				}

            });

			/*
			$("#editView").scroll(function (e) {
				var sPos = $(this).scrollTop();
				var zoom = $("#currentSlide").attr("kk_zoom");
				var hh = $("#currentSlide").height() * zoom;
				var wh = $(window).height() - $("#toolBar").height() - $("#statusBar").height();
				var scroll = hh - sPos;
				var current = $("#currentSlide").attr("rid");
                var $next = $("#pageWrap_"+current).next("#lSideView .slideWrap");
				var next = $next.attr("page");
				if(typeof(next) != "undefined"){
					if(wh > scroll){
						_this.menuSwitch({id:"nextPage"});
						$(this).scrollTop(0);
					}
				}
			});
			*/

			if(_this.loader.fileext == "pdf"){
				this.ratio = _this.loader.ratio;
 				$("#kk_zoomSlider").kkZoomSelect({
 					mode:"pdf",
	                callback: function( e, data ) {
						$("#currentSlide").attr("kk_zoom",data.zoom);
	                }
	            });
			}else{
				this.ratio = 1;
                $("#kk_zoomSlider").kkZoomSelect({
	                callback: function( e, data ) {
						$("#currentSlide").attr("kk_zoom",data.zoom);
					}
	            });
			}

			$("#toolBar a.kk_btn").kkButton({
				callback: function(e,data){
					//console.log("####");
					var isActive = $(e.target).hasClass("active") ? true : false;
					_this.menuSwitch(data, {isActive : isActive, node : e.target});
				}
			});

			$("#statusBar a.kk_btn").kkButton({
				callback: function(e,data){
					//console.log("####");
					var isActive = $(e.target).hasClass("active") ? true : false;
					_this.menuSwitch(data, {isActive : isActive, node : e.target});
				}
			});

            $("#closeToolbar").on("click",function(e){
				 var data = {};
                 data.id  = $(this).attr("u-id");
				 _this.menuSwitch(data);
			});


			$("#statusBar .kk_selectbox").kkSelectBox({
				displayMode: "bottom",
				callback: function(e,data){
					_this.menuSwitch(data);
				}
			});

			$("#statusBar .kk_colorpicker").kkColorPicker({
				displayMode: "bottom",
				callback: function(e,data){
					//console.log("data1 : ", data);
					_this.menuSwitch(data);
				}
			});


			$("#toolBar").find("a.kk_toggle").on("click",function(e){
				 var data = {};
                 data.id  = $(this).attr("u-id");
                 data.val = $(this).attr("state");
				 if(data.val == "on"){
					$(this).attr("state","off");
					$(this).find("i").removeClass("kk-icon-toggle-on").addClass("kk-icon-toggle-off");
					data.val = "off";
				 }else{
					$(this).attr("state","on");
					$(this).find("i").removeClass("kk-icon-toggle-off").addClass("kk-icon-toggle-on");
					data.val = "on";
				 }
				 _this.menuSwitch(data);
			});


			if(_this.isMobile){
				if($(window).width() < 480){
					$("body > div > a.kk_btn").kkButton({
						callback: function(e,data){
							_this.menuSwitch(data, {isActive : true, node : e.target});
						}
					});
				}
			}else{
				$("#editView").on('mousewheel DOMMouseScroll', function(e2){
					  if((e2.ctrlKey || e2.metaKey)){
						  var st = $(this).scrollTop();
						  if(e2.originalEvent.wheelDelta < 0) {
							  _this.menuSwitch({id:"zoomout",step:"0.03"});
							  e2.preventDefault();
						  } else {
							  _this.menuSwitch({id:"zoomin",step:"0.03"});
							  e2.preventDefault();
						  }
					  }
				});

			}

			$("#lSideView").find("a.kk_toggle").on("click",function(e){
				 var data = {};
                 data.id  = $(this).attr("u-id");
                 data.val = $(this).attr("state");
				 if(data.val == "on"){
					$(this).attr("state","off");
					$(this).find("i").removeClass("kk-icon-toggle-on").addClass("kk-icon-toggle-off");
					data.val = "off";
				 }else{
					$(this).attr("state","on");
					$(this).find("i").removeClass("kk-icon-toggle-off").addClass("kk-icon-toggle-on");
					data.val = "on";
				 }
				 _this.menuSwitch(data);
			});

			if(_this.loader.fileext == "pdf"){
        		//_this.menuSwitch({"id":"fitWidth"});
        	}else{

        	}
			//$(".pdfCanvas").kkSelectable("kkdisable");
            //this.defaultShape = new DefaultShape();
        };

        this.hideUiActiveWindow = function(){
            $(".ui_active").removeClass("ui_active");
            $(".ui_colorpicker").hide();
            $(".ui_tablepicker").hide();
        };

        this.menuSwitch = function(menu, options) {
            var _this = this;
            this.hideUiActiveWindow();
            if (options && $(options.node).hasClass("disabled")){
                return ;
            }
            switch(menu.id){
            /*-- Text Editor ------------------------------------------------------------------------------*/
    			case "bold" :
    				document.execCommand('bold', false, null);
                    break;
    			case "italic" :
    				document.execCommand('italic', false, null);
                    break;
    			case "underline" :
    				document.execCommand('underline', false, null);
                    break;
    			case "strikethrough" :
    				document.execCommand('strikethrough', false, null);
                    break;
    			case "fontColor" :
    				document.execCommand('foreColor', false, "#"+menu.color);
                    break;
    			case "fontBackgroundColor" :
    				document.execCommand('hiliteColor', false, "#"+menu.color);
                    break;
    			case "fontFamily" :
    				document.execCommand('fontName', false, menu.val);
                    break;
    			case "fontSize" :
    				document.execCommand('fontSize', false, menu.val);
                    break;
    			case "fontSizeUp" :
                    break;
    			case "fontSizeDown" :
                    break;
    			case "alignLeft" :
    				document.execCommand('justifyLeft', false, null);
                    break;
    			case "alignCenter" :
    				document.execCommand('justifyCenter', false, null);
                    break;
    			case "alignRight" :
    				document.execCommand('justifyRight', false, null);
                    break;
    			case "alignJustify" :
    				document.execCommand('justifyFull', false, null);
                    break;
    			case "lineHeight" :

                    break;
    			case "lineBefore" :

                    break;
    			case "lineAfter" :

                    break;
    			case "indentLeft" :
    				document.execCommand('outdent', false, null);
                    break;
    			case "indentRight" :
    				document.execCommand('indent', false, null);
                    break;

		    /*-- Annotation -------------------------------------------------------------------------------*/
                case "save" :
                    var annoData = "";
                    var draw = "";
        					var annoElement = "";
        					var imgElement = "";
        					var elementList = [];
        					var rotateElement = [];
        					var set_key = SettingsDef.loading.setKey;
        					var f = 0;
                            $("#lSideView .lSideViewWrapSCroll div.slideWrap").each(function(i){
        						var $slideWrap = $(this);
                                var element_id = $(this).attr("document_id");
        						var index_key = $(this).attr("index_key");
                                var isEdit = $(this).attr("isEdit");
                                var isRot = $(this).attr("isrot");
        						var rotate = $(this).find(".thumbWrap").attr("kk_rotate");
        						if(typeof(rotate) == "undefined"){ rotate = 0; }
        						if(isRot == "y"){
                                    rotateElement.push(element_id + "｜" + rotate);
                                }

                                if(isEdit == "y"){
                                    if(f > 0){ annoData += "＆"; annoElement += "|";}
        							annoElement += element_id;
                                    annoData += element_id+'|'+index_key+'|'+set_key+'＾'+isEdit;
        							elementList.push(element_id);
                                    if($(this).find(".thumb .kk_shape > .shape").length > 0){
                                        $(this).find(".thumb .kk_shape > .shape").each(function(f){
                                             if($(this).hasClass("shape")){
                                                 annoData += "＾";
                                                 var kkshape = $(this).find("kkshape");
                                                 var pos_t = Number($(this).css("top").replace("px",""));
                                                 var pos_l = Number($(this).css("left").replace("px",""))
                                                 var pos_w = Number($(this).css("width").replace("px",""));
                                                 var pos_h = Number($(this).css("height").replace("px",""));
                                                 var prst = kkshape.attr("kk-shape-name");
                                                 var stroke_color = kkshape.attr("shape-stroke-color");
                                                 var fill_color = kkshape.attr("shape-fill-color");
                                                 var stroke = kkshape.attr("shape-stroke-width");
                                                 var stroke_type = kkshape.attr("shape-stroke-type");
                                        		 var degree = ShapeUtil.getRotationDegrees($(this));
                                                 var content = $(this).find(".txBox").html();
                                                 annoData += 'sp｜'+prst+"｜"+pos_l+"｜"+pos_t+"｜"+pos_w+"｜"+pos_h+"｜"+degree+"｜"+fill_color+"｜"+stroke_color+"｜"+stroke_type+"｜"+stroke+"｜"+content;
                                             }
                                        });
            						}
        							if($(this).find(".thumb .kk_shape > .line").length > 0){
                                        $(this).find(".thumb .kk_shape > .line").each(function(f){
                                             if($(this).hasClass("line")){
                                                 annoData += "＾";
                                                 var kkshape = $(this).find("kkshape");
                                                 var pos_t = Number($(this).css("top").replace("px",""));
                                                 var pos_l = Number($(this).css("left").replace("px",""))
                                                 var pos_w = Number($(this).css("width").replace("px",""));
                                                 var pos_h = Number($(this).css("height").replace("px",""));
                                                 var prst = kkshape.attr("kk-shape-name");
                                                 var stroke_color = kkshape.attr("shape-stroke-color");
                                                 var fill_color = kkshape.attr("shape-fill-color");
                                                 var stroke = kkshape.attr("shape-stroke-width");
                                                 var stroke_type = kkshape.attr("shape-stroke-type");
                                                 var xlx = $(this).attr("xlx");

                                        		 var degree = ShapeUtil.getRotationDegrees($(this));
                                        		 degree = Math.round(degree * 180/Math.PI);
                                                 var content = "";
                                                 annoData += 'ln｜'+prst+"｜"+pos_l+"｜"+pos_t+"｜"+pos_w+"｜"+pos_h+"｜"+xlx+"｜｜"+stroke_color+"｜"+stroke_type+"｜"+stroke+"｜"+content;
                                             }
                                        });
            						}

                                    if(f > 0){ draw += "＆"; }
                                    draw += element_id+'＾'+isEdit;
                                    if($(this).find(".thumb .kk_shape > .sketch").length > 0){
                                        $(this).find(".thumb .kk_shape > .sketch").each(function(f){
                                             if($(this).hasClass("sketch")){
                                                 draw += "＾";
                                                 var kkshape = $(this).find("kkshape");
                                                 var pos_t = Number($(this).css("top").replace("px",""));
                                                 var pos_l = Number($(this).css("left").replace("px",""))
                                                 var pos_w = Number($(this).css("width").replace("px",""));
                                                 var pos_h = Number($(this).css("height").replace("px",""));
                                        		 var content = $(this).find(".kk_sketch_path").html();
                                                 draw += "draw｜draw｜"+pos_l+"｜"+pos_t+"｜"+pos_w+"｜"+pos_h+"｜0｜｜｜｜｜"+content;
                                             }
                                        });
            						}
        							f++;
                                }

        						if($(this).find(".thumb .kk_shape .masking").length > 0){
        							if(SettingsDef.loading.loadingType == "image"){
        								var _canvas =  document.createElement('canvas');
        								var _image = document.createElement('img');
        								var ctx = _canvas.getContext("2d");
        								var filepath = $slideWrap.find("img").attr("src");
        								var imageObj = new Image();
        								var _w = Number($slideWrap.attr("w"));
        								var _h = Number($slideWrap.attr("h"));
        								_canvas.width = _w;
        								_canvas.height = _h;
        								_canvas.style.width = _w + 'px';
        								_canvas.style.height = _h + 'px';



        								imageObj.src = filepath;
        								imageObj.onload = function() {


        									ctx.drawImage(imageObj, 0, 0, _w, _h);
        									$slideWrap.find(".thumb .kk_shape > .masking").each(function(f){
        										var pos_t = Number($(this).css("top").replace("px",""));
        										var pos_l = Number($(this).css("left").replace("px",""));
        										var pos_w = Number($(this).css("width").replace("px",""));
        										var pos_h = Number($(this).css("height").replace("px",""));

        										if(rotate == 90){
        											pos_w = Number($(this).css("height").replace("px",""));
        											pos_h = Number($(this).css("width").replace("px",""));
        											pos_t = Number($(this).css("top").replace("px",""))-pos_h;
        											pos_l = Number($(this).css("left").replace("px",""));
        										}else if(rotate == 180){
        											pos_w = Number($(this).css("width").replace("px",""));
        											pos_h = Number($(this).css("height").replace("px",""));
        											pos_t = Number($(this).css("top").replace("px",""))-pos_h;
        											pos_l = Number($(this).css("left").replace("px",""))-pos_w;
        										}else if(rotate == 270){
        											pos_w = Number($(this).css("height").replace("px",""));
        											pos_h = Number($(this).css("width").replace("px",""));
        											pos_t = Number($(this).css("top").replace("px",""));
        											pos_l = Number($(this).css("left").replace("px",""))-pos_w;
        										}

        										$("#lSideView .slideWrap[document_id='"+element_id+"']").attr("imgupdate","y");

        										ctx.fillStyle = "#000000";
        										ctx.globalAlpha = 1;
        										ctx.fillRect(pos_l,pos_t,pos_w,pos_h);


        										$(this).remove();
        									});
        									_image.src = _canvas.toDataURL("image/jpeg");
        									_image.style.width = _w + 'px';
        									_image.style.height = _h + 'px';
        									$slideWrap.find("img").remove();
        									$slideWrap.find(".thumbZoom").prepend(_image);

        									if($slideWrap.hasClass("current")){
        										var page = $slideWrap.attr("page");
        										_this.loader.currentPage(page);
        									}



        								};
        							}else{
        								var _canvas =  $(this).find(".thumb canvas")[0];

        								var ctx = _canvas.getContext("2d");

        								$(this).find(".thumb .kk_shape > .masking").each(function(f){
        									var pos_t = Number($(this).css("top").replace("px",""));
        									var pos_l = Number($(this).css("left").replace("px",""));
        									var pos_w = Number($(this).css("width").replace("px",""));
        									var pos_h = Number($(this).css("height").replace("px",""));


        									if(rotate == 90){
        										pos_w = Number($(this).css("height").replace("px",""));
        										pos_h = Number($(this).css("width").replace("px",""));
        										pos_t = Number($(this).css("top").replace("px",""))-pos_h;
        										pos_l = Number($(this).css("left").replace("px",""));
        									}else if(rotate == 180){
        										pos_w = Number($(this).css("width").replace("px",""));
        										pos_h = Number($(this).css("height").replace("px",""));

        										pos_t = Number($(this).css("top").replace("px",""))-pos_h;
        										pos_l = Number($(this).css("left").replace("px",""))-pos_w;
        									}else if(rotate == 270){
        										pos_w = Number($(this).css("height").replace("px",""));
        										pos_h = Number($(this).css("width").replace("px",""));

        										pos_t = Number($(this).css("top").replace("px",""));
        										pos_l = Number($(this).css("left").replace("px",""))-pos_w;
        									}


        									$("#lSideView .slideWrap[document_id='"+element_id+"']").attr("imgupdate","y");

        									ctx.fillStyle = "#000000";
        									ctx.globalAlpha = 1;
        									ctx.fillRect(pos_l,pos_t,pos_w,pos_h);

        									ctx.fillStyle = "#000000";
        									ctx.globalAlpha = 1;
        									ctx.fillRect(pos_l,pos_t,pos_w,pos_h);
        									$(this).remove();

        								});

        								if($slideWrap.hasClass("current")){
        									var page = $slideWrap.attr("page");
        									_this.loader.currentPage(page);
        								}
        							}

        						}
        					});

                            if(typeof(SettingsDef.save.saveURL) != "undefined" && SettingsDef.save.saveURL !== ""){
                                //var userinfo = localStorage.getItem("userinfo");
                                //var server = localStorage.getItem("server");

        						var setKey = SettingsDef.loading.setKey;
                                var userinfo = SettingsDef.loading.userinfo;
                                var server   = SettingsDef.loading.server;
        						var rotateStr = rotateElement.join("＾");


                                var saveUrl = SettingsDef.save.saveURL;
                                if(server != null){
                                    saveUrl = server+"/"+SettingsDef.save.saveURL;
                                }

        						var pram = {
        							"setKey": setKey,
        							"userinfo": userinfo,
        							"anno" : annoData,
        							"draw" : draw,
        							"rotate" : rotateStr
        						};


        						$.extend(true, pram, SettingsDef.save.saveParam);

                                $.ajax({
                                    type: "POST",
                                    url: saveUrl,
                                    data: pram,
                                    dataType: "html",
                                    success: function (data) {

        								window.parent.postMessage({
        									responseAnnoSave:data
        								}, '*');

        								if(data == "=SUCCESS000"){
        									$.each(elementList,function(i,v){
        										//console.log(v);
        										$("#lSideView .lSideViewWrapSCroll div.slideWrap[document_id='"+v+"']").attr("isedit","n");
        									});
        								}

                                        $().toastmessage('showSuccessToast', '저장 되었습니다.');
                                    },
                                    complete: function () {


        								if(SettingsDef.save.imgSaveURL !== undefined && SettingsDef.save.imgSaveURL !== ""){

        									var userinfo = SettingsDef.loading.userinfo;
        									var server = SettingsDef.loading.server;

        									var imgSaveURL = SettingsDef.save.imgSaveURL;
        									if(server != null){
        										imgSaveURL = server+"/"+SettingsDef.save.imgSaveURL;
        									}

        									var $imgupdate = $("#lSideView .slideWrap[imgupdate='y']");

        									if($imgupdate.length > 0){

        										var elementId = [];
        										var base64Yn = [];
        										var imageStr = [];

        										if(SettingsDef.loading.loadingType == "image"){
        											$imgupdate.each(function(i){
        												if(i > 0){ imgElement += "|";}
        												imgElement += $(this).attr("document_id");

        												elementId[i] = $(this).attr("document_id");
        												base64Yn[i] = "y";
        												imageStr[i] = $(this).find("img")[0].src;
        											});
        										}else{
        											$imgupdate.each(function(i){
        												if(i > 0){ imgElement += "|";}
        												imgElement += $(this).attr("document_id");

        												elementId[i] = $(this).attr("document_id");
        												base64Yn[i] = "y";
        												imageStr[i] = $(this).find("canvas")[0].toDataURL("image/jpeg");
        											});
        										}

        										//elementId = JSON.stringify(elementId);
        										//base64Yn = JSON.stringify(base64Yn);
        										//imageStr = JSON.stringify(imageStr);

        										var pram2 = {
        											"userinfo": userinfo,
        											"elementId" : elementId,
        											"base64Yn" : base64Yn,
        											"imageStr" : imageStr
        										};

        										$.extend(true, pram2, SettingsDef.save.imgSavePram);

        										$.ajax({
        											type: "POST",
        											url: imgSaveURL,
        											data: pram2,
        											dataType: "json",
        											success: function (data) {

        												window.parent.postMessage({
        													responseImgSave:data
        												}, '*');
        												$().toastmessage('showSuccessToast', '저장 되었습니다.');
        											},
        											complete: function () {



        												window.parent.postMessage({
        													callback:{
        														anno:annoElement,
        														img:imgElement
        													}
        												}, '*');

        											}
        										});
        									}else{
        										window.parent.postMessage({
        											callback:{
        												anno:annoElement,
        												img:imgElement
        											}
        										}, '*');
        									}

        								}else{
        									window.parent.postMessage({
        										callback:{
        											anno:annoElement,
        											img:imgElement
        										}
        									}, '*');
        								}
                                    }
                                });
                            }else{
        						window.parent.postMessage({
        							callback:{
        								anno:annoElement,
        								img:imgElement
        							}
        						}, '*');
        					}
                            break;
                        case "transfer" :
                            var data = "";
        					$("#lSideView .lSideViewWrapSCroll div.slideWrap").each(function(i){
                                var element_id = $(this).attr("document_id");
                                var isEdit = $(this).attr("isEdit");
        						if($(this).find(".thumb .kk_shape .masking").length > 0){
        							var _canvas =  $(this).find(".thumb canvas")[0];
        							$(this).find(".thumb .kk_shape > .masking").each(function(f){
        								var pos_t = Number($(this).css("top").replace("px",""));
        								var pos_l = Number($(this).css("left").replace("px",""));
        								var pos_w = Number($(this).css("width").replace("px",""));
        								var pos_h = Number($(this).css("height").replace("px",""));
        								var ctx = _canvas.getContext("2d");
        								$("#lSideView .slideWrap[document_id='"+element_id+"']").attr("imgupdate","y");
        								ctx.fillStyle = "#000000";
        								ctx.globalAlpha = 1;
        								ctx.fillRect(pos_l,pos_t,pos_w,pos_h);
        								$(this).remove();
        							});
        						}
        					});

        					if(SettingsDef.save.transferURL !== undefined && SettingsDef.save.transferURL !== ""){
        						var userinfo = SettingsDef.loading.userinfo;
        						var server = SettingsDef.loading.server;

        						var transferURL = SettingsDef.save.transferURL;
        						if(server != null){
        							transferURL = server+"/"+SettingsDef.save.transferURL;
        						}

        						var $imgupdate = $("#lSideView .slideWrap.multiselect");

        						if($imgupdate.length > 0){
        							var elementId = [];
        							var base64Yn = [];
        							var imageStr = [];
        							$imgupdate.each(function(i){
        								if(i > 0){ imgElement += "^";}
        								imgElement += $(this).attr("document_id");

        								elementId[i] = $(this).attr("document_id");
        								base64Yn[i] = "y";
        								imageStr[i] = $(this).find("canvas")[0].toDataURL("image/jpeg");
        							});

        							//elementId = JSON.stringify(elementId);
        							//base64Yn = JSON.stringify(base64Yn);
        							//imageStr = JSON.stringify(imageStr);

        							var pram = {
        								"userinfo": userinfo,
        								"elementId" : elementId,
        								"base64Yn" : base64Yn,
        								"imageStr" : imageStr
        							};

        							$.extend(true, pram, SettingsDef.save.transferPram);
        							$.ajax({
        								type: "POST",
        								url: transferURL,
        								data:pram,
        								dataType: "html",
        								success: function (data) {
        									window.parent.postMessage({
        										responseTransfer:data
        									}, '*');

        									//$().toastmessage('showSuccessToast', '저장 되었습니다.');
        								},
        								complete: function () {

        									window.parent.postMessage({
        										callback:imgElement
        									}, '*');

        								}
        							});
        						}else{
        							window.parent.postMessage({
        								callback:imgElement
        							}, '*');
        						}

        					}else{
        						window.parent.postMessage({
        							callback:imgElement
        						}, '*');
        					}
                            break;
                        case "print" :
                            window.print();
                            break;
				case "imgMasking" :

					var maskingNum = $("#kk_shape").find("div[kktype='masking']").length;
					if(SettingsDef.limit["maxMasking"] <= maskingNum){
						alert("블랙마킹은 "+SettingsDef.limit["maxMasking"]+"개까지만 생성할 수 있습니다.");
					} else {
						$("#kk_shape").kkSelectableObject({
							maxMasking : SettingsDef.limit["maxMasking"],
							maxBoxW : SettingsDef.limit["maxBoxW"],
							maxBoxH : SettingsDef.limit["maxBoxH"],
							minBoxW : SettingsDef.limit["minBoxW"],
							minBoxH : SettingsDef.limit["minBoxH"],
							callback : function(pos){
								var spos = $("#kk_shape").offset();
								var i_z = $("#currentSlide").attr("kk_zoom");
								var i_y = 0;
								var i_x = 0;
								var pos_l = i_z*pos.l+spos.left-$("#lSideView").width();
								var pos_t = i_z*pos.t+spos.top-$("#toolBar").height();
								var pos_w = i_z*(pos.w);
								var pos_h = i_z*(pos.h);

								if(pos_w > SettingsDef.limit["maxBoxW"]){
									pos_w = SettingsDef.limit["maxBoxW"];
								}else if(pos_w < SettingsDef.limit["minBoxW"]){
									pos_w = SettingsDef.limit["minBoxW"];
								}

								if(pos_h > SettingsDef.limit["maxBoxH"]){
									pos_h = SettingsDef.limit["maxBoxH"];
								}else if(pos_h < SettingsDef.limit["minBoxH"]){
									pos_h = SettingsDef.limit["minBoxH"];
								}
								$("#editView").append('<div class="kk_masking_space"></div>');
								$(".kk_masking_space").css({"top":pos_t+"px","left":pos_l+"px","width":pos_w+"px","height":pos_h+"px"}).show();
								$(".kk_masking_space").append('<div kktype="masking" class="masking kk-masking-selectabled"></div>');
								$(".kk_masking_space").find(".masking").css({"top":"0px","left":"0px","width":pos_w+"px","height":pos_h+"px"});
								$(".kk_masking_space").kkMaskingSelectable({
									maxMasking : SettingsDef.limit["maxMasking"],
									maxBoxW : SettingsDef.limit["maxBoxW"],
									maxBoxH : SettingsDef.limit["maxBoxH"],
									minBoxW : SettingsDef.limit["minBoxW"],
									minBoxH : SettingsDef.limit["minBoxH"],
									callback : function(element){
										var pos = _this.loader.mobileMeta($(".kk_masking_space"));
										$(".kk_masking_space").find(".kk-masking-selectabled").appendTo("#kk_shape");
										$("#kk_shape .kk-masking-selectabled").css({"top":pos.t,"left":pos.l,"width":pos.w,"height":pos.h,"transform":"rotate("+pos.r+"deg)","transform-origin":"0 0"});
										$("#kk_shape").find(".kk-masking-selectabled").removeClass("kk-masking-selectabled");
										$(".kk_masking_space").empty().hide();
										//$(".kk_selected_space").kkMaskingSelectable().kkMaskingSelectable("destroy");
										$("#kk_shape .masking").off("click");
										$("#kk_shape .masking").on("click",function(){
											_this.loader.maskingBoxWrap(this);
											return false;
										});
									}
								});
								$("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");

							}
						});
					}
                    break;
				case "insertShape" :
					if(_this.isMobile){
						$(".deskView").find(".kk_selected_space").css({top:"200px",left:"200px",width:"200px",height:"200px"}).show();
						$(".deskView").find(".kk_selected_space").kkMobileSelectable({
							mode: "shape",
							iscroll: _this.loader.iscroll,
							callback: function(element){
								var document_id = $("#currentSlide").attr("document_id");
								var page = $("#currentSlide").attr("page");
								var pos = _this.mobileMeta();
								var kkshape = $(".kk-mobile-shape-selectabled").find("kkshape");
								var spid = $(".kk-mobile-shape-selectabled").attr("spid");
								var prst = kkshape.attr("kk-shape-name");
								var prstgeom = kkshape.attr("kk-shape-type");
								var stroke_color = kkshape.attr("shape-stroke-color");
								var fill_color = kkshape.attr("shape-fill-color");
								var stroke = kkshape.attr("shape-stroke-width");
								var stroke_type = kkshape.attr("shape-stroke-type");
								var adj = kkshape.attr("shape-adj");
								var degree = ShapeUtil.getRotationDegrees($("#kk_selected_space"));
								degree = Math.round(degree * 180/Math.PI);

								var shp = {
									w:pos.w,
									h:pos.h,
									stroke_color:stroke_color,
									stroke_width:stroke,
									stroke_type:stroke_type,
									fill_color:fill_color
								};
								var svg = ShapeUtil.getShapeSVGMobile(prst,shp);
								element.find(".txBox .txFt").attr("contenteditable","false");
								kkshape.find("kkshapedraw").html(svg);
								$(".kk-mobile-shape-selectabled").css({"top":pos.t,"left":pos.l,"width":pos.w,"height":pos.h,"transform":"rotate("+degree+"deg)"});
								$(".kk-mobile-shape-selectabled").appendTo("#kk_shape");
								$("#kk_selected_space").kkMobileSelectable("destroy");
								$("#kk_shape .shape[spid='"+spid+"']").off("click");
								$("#kk_shape .shape[spid='"+spid+"']").on("click",function(){
									_this.loader.shapeBoxWrap(this);
									return false;
								});
							},
							resizeCallback: function(pos){
								var kkshape = $(".kk-mobile-shape-selectabled").find("kkshape");
								var prst = kkshape.attr("kk-shape-name");
								var stroke_color = kkshape.attr("shape-stroke-color");
								var fill_color = kkshape.attr("shape-fill-color");
								var stroke = kkshape.attr("shape-stroke-width");
								var stroke_type = kkshape.attr("shape-stroke-type");
								var adj = kkshape.attr("shape-adj");
								var shp = {
									w:pos.w,
									h:pos.h,
									stroke_color:stroke_color,
									stroke_width:stroke,
									stroke_type:stroke_type,
									fill_color:fill_color
								};
								var svg = ShapeUtil.getShapeSVGMobile(prst,shp);
								kkshape.find("kkshapedraw").html(svg);
							}
						});
						var pos = {t:0,l:0,w:200,h:200};
						ShapeUtil.createShapeMobile("rect",menu.val,pos, function(wrap){
							var spid = $(wrap).attr("spid");
							var key = $(wrap).attr("key");
							if(typeof key == "undefined"){
								key = 0;
							}
							$(".shape[spid='"+spid+"']").addClass("kk-mobile-shape-selectabled");
						});
					}else{
						var annoNum = $("#kk_shape").find("div[kktype='sp']").length;
						if(SettingsDef.limit["maxAnno"] <= annoNum){
							alert("주석은 "+SettingsDef.limit["maxAnno"]+"개까지만 생성할수 있습니다.");
						}else{
							_this.loader.menuTxtSelecte(false);
							$("#kk_shape").kkSelectableObject({
								maxAnno : SettingsDef.limit["maxAnno"],
								callback : function(pos){
									var spos = $("#kk_shape").offset();
									var i_z = $("#currentSlide").attr("kk_zoom");
									var i_y = 0;
									var i_x = 0;
									var pos_l = i_z*pos.l+spos.left-$("#lSideView").width();
									var pos_t = i_z*pos.t+spos.top-$("#toolBar").height();
									var pos_w = i_z*(pos.w);
									var pos_h = i_z*(pos.h);

									$(".kk_selected_space").css({"top":pos_t+"px","left":pos_l+"px","width":pos_w+"px","height":pos_h+"px"}).show();
									npos = {t:0, l:0, w:pos_w, h:pos_h, stroke_width:1, stroke_color:"rgb(61,103,142)",	fill_color:"#5B9BD5"};
									var shapeName = "rect";
									if(menu.val == "textbox"){
										shapeName = "rect";
									}else{
										shapeName = menu.val;
									}
									if(shapeName == "straightconnector1-1"){
										npos = {t:0, l:0, w:pos_w, h:pos_h, stroke_width:4, stroke_color:"rgb(61,103,142)",	fill_color:"#5B9BD5"};
										ShapeUtil.createShapeMobile("line",shapeName,npos, function(wrap){
											var spid = $(wrap).attr("spid");
											var key = $(wrap).attr("key");
											if(typeof key == "undefined"){
												key = 0;
											}
											$("#editView .shape[spid='"+spid+"']").addClass("kk-mobile-shape-selectabled");
											$("#kk_shape").kkSelectableObject("destroy");
										});

									}else if(shapeName == "straightconnector1-2"){
										npos = {t:0, l:0, w:pos_w, h:pos_h, stroke_width:4, stroke_color:"rgb(61,103,142)",	fill_color:"#5B9BD5"};
										ShapeUtil.createShapeMobile("line",shapeName,npos, function(wrap){
											var spid = $(wrap).attr("spid");
											var key = $(wrap).attr("key");
											if(typeof key == "undefined"){
												key = 0;
											}
											$("#editView .shape[spid='"+spid+"']").addClass("kk-mobile-shape-selectabled");
											$("#kk_shape").kkSelectableObject("destroy");
										});

									}else{
										ShapeUtil.createShapeMobile("rect",shapeName,npos, function(wrap){
											var spid = $(wrap).attr("spid");
											var key = $(wrap).attr("key");
											if(typeof key == "undefined"){
												key = 0;
											}
											$("#editView .shape[spid='"+spid+"']").addClass("kk-mobile-shape-selectabled");
											$("#kk_shape").kkSelectableObject("destroy");
										});
									}


									$(".kk_selected_space").kkMobileSelectable({
										mode: "shape",
										filetype:SettingsDef.loading.type,
										callback: function(element){
											var document_id = $("#currentSlide").attr("document_id");
											var page = $("#currentSlide").attr("page");
											var pos = _this.loader.mobileMeta();
											var kkshape = $(".kk-mobile-shape-selectabled").find("kkshape");
											var prst = kkshape.attr("kk-shape-name");
											var prstgeom = kkshape.attr("kk-shape-type");
											var spid = $(".kk-mobile-shape-selectabled").attr("spid");
											var stroke_color = kkshape.attr("shape-stroke-color");
											var fill_color = kkshape.attr("shape-fill-color");
											var stroke = kkshape.attr("shape-stroke-width");
											var stroke_type = kkshape.attr("shape-stroke-type");
											var degree = ShapeUtil.getRotationDegrees($(".kk_selected_space"));
											degree = Math.round(degree * 180/Math.PI);

											var shp = {
												w:pos.w,
												h:pos.h,
												stroke_color:stroke_color,
												stroke_width:stroke,
												stroke_type:stroke_type,
												fill_color:fill_color
											};

											var svg = null;
											//console.log(prst);
											if(prst == "straightconnector1-1"){
												svg = ShapeUtil.getLineSVGMobile(prst,shp);
											}else if(prst == "straightconnector1-2"){
												svg = ShapeUtil.getLineSVGMobile(prst,shp);
											}else{
												svg = ShapeUtil.getShapeSVGMobile(prst,shp);
											}

											//element.find(".txBox .txFt").attr("contenteditable","false");
											kkshape.find("kkshapedraw").html(svg);
											$(".kk-mobile-shape-selectabled").css({"top":pos.t,"left":pos.l,"width":pos.w,"height":pos.h,"transform":"rotate("+pos.r+"deg)","transform-origin":"0 0"});
											$(".kk-mobile-shape-selectabled").appendTo("#kk_shape");
											$(".kk-mobile-shape-selectabled").find(".txBody").css({"top":15+"px","bottom":15+"px","left":15+"px","right":15+"px"});

											var size = Number($(".kk-mobile-shape-selectabled").find(".txBody .txFt").css("font-size").replace("px",""));
											$(".kk-mobile-shape-selectabled").find(".txBody .txFt").css("font-size",size/pos.z);

											//$(".kk-mobile-shape-selectabled").find(".txBody").css("zoom",1/pos.z);
											$(".kk-mobile-shape-selectabled").removeClass("kk-mobile-shape-selectabled");
											$(".kk_selected_space").empty();
											$(".kk_selected_space").kkMobileSelectable("destroy");
											//$("#kk_shape .shape[spid='"+spid+"']").off("click");
											$("#kk_shape .shape").on("click",function(){
												_this.loader.shapeBoxWrap(this);
												return false;
											});
										},
										delCallback: function(element){
											$(".kk_selected_space").empty();
											$(".kk_selected_space").kkMobileSelectable("destroy");
											$("#kk_shape .shape").on("click",function(){
												_this.shapeBoxWrap(this);
												return false;
											});
										},
										resizeCallback: function(pos){
											var kkshape = $(".kk-mobile-shape-selectabled").find("kkshape");
											var prst = kkshape.attr("kk-shape-name");
											var stroke_color = kkshape.attr("shape-stroke-color");
											var fill_color = kkshape.attr("shape-fill-color");
											var stroke = kkshape.attr("shape-stroke-width");
											var stroke_type = kkshape.attr("shape-stroke-type");
											var adj = kkshape.attr("shape-adj");
											var shp = {
												w:pos.w,
												h:pos.h,
												stroke_color:stroke_color,
												stroke_width:stroke,
												stroke_type:stroke_type,
												fill_color:fill_color
											};
											if(prst == "straightconnector1-1"){
												svg = ShapeUtil.getLineSVGMobile(prst,shp);
											}else if(prst == "straightconnector1-2"){
												svg = ShapeUtil.getLineSVGMobile(prst,shp);
											}else{
												svg = ShapeUtil.getShapeSVGMobile(prst,shp);
											}

											kkshape.find("kkshapedraw").html(svg);
										}
									});
									$("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
								}
							});
						}
					}
                    break;
                case "insertLine" :
                    $("#kk_shape").kkLineSelectableObject({
        				lineType : menu.val,
        				callback : function(obj){
                            obj.element.kkLineSelectable({
        						xlx : obj.mode,
                                callback : function(element){
                                    element.on("click",function(){
                                        _this.loader.lineBoxWrap(this);
                                        return false;
                                    });
                                }
                            });
                            //$(".kk_selected_space").css({"top":pos["st"]+"px","left":pos["sl"]+"px","width":pos["w"]+"px","height":pos["h"]+"px"}).show();
                            //$(".kk_selected_space").html('<svg style="width:100%;height:100%;"><polyline xmlns="http://www.w3.org/2000/svg" points="0,0 '+pos["w"]+','+pos["h"]+'" fill="rgb(61,103,142)" stroke="rgb(61,103,142)" stroke-width="4"></polyline></svg>');
                            $("#kk_shape").kkLineSelectableObject().kkLineSelectableObject("destroy");
                            //pos.kkMobileSelectable({mode: "line"});
                        }
                    });
                    break;
                case "insertMemo" :
                    $("#kk_shape").kkSelectableObject({
                        maxAnno : SettingsDef.limit["maxAnno"],
                        callback : function(pos){


                            $("#kk_shape").append('<div class="memo" style="top:'+pos.t+'px;left:'+pos.l+'px;width:'+pos.w+'px;height:'+pos.h+'px;"></div>');

                            $("#kk_shape > .memo").kkMemoView({
                                pos : pos,
                                callback: function(){
                                    //$(".kk_selected_space").empty();
                                    //$(".kk_selected_space").kkMemoView("destroy");
                                }
                            });

                            $("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
                        }
                    });





                    break;
                case "shapeBackgroundColor" :
                    var color  = "";
                    if(menu.color == "transparent"){
                        color  = "transparent";
                        //$("#kk_sketch").sketch("tool","eraser");
                    }else{
                        color  = "#"+menu.color;
                    }

                    var targetWrap = $(".kk-mobile-shape-selectabled > .objectData");
					targetWrap.find("kkshape").attr("shape-fill-color",color);
					targetWrap.find("kkshape svg path").attr("fill",color);
					_this.saveSlide();


                    break;
				case "shapeBorderColor" :
					var mode = $("#kk_shape").attr("mode");
						targetWrap = $(".kk-mobile-shape-selectabled > .objectData");
					//}
					var color  = "";
				    if(menu.color == "transparent"){
						color  = "transparent";
					}else{
						color  = "#"+menu.color;
					}
                    if(mode == "shape"){
    					targetWrap.find("kkshape").attr("shape-stroke-color",color);
    					targetWrap.find("kkshape svg path").attr("stroke",color).css("stroke",color);
    					targetWrap.find("kkshape svg polyline").attr("stroke",color).css("stroke",color);
    					_this.saveSlide();
					}else if(mode == "line"){
						targetWrap = $(".kk-line-selectabled-box > .objectData");
    					targetWrap.find("kkshape").attr("shape-stroke-color",color);
    					targetWrap.find("kkshape svg path").attr("stroke",color).css("stroke",color);
    					targetWrap.find("kkshape svg path").attr("stroke",color).css("fill",color);
    					targetWrap.find("kkshape svg polyline").attr("stroke",color).css("stroke",color);
    					//_this.saveSlide();
                    }else if(mode == "draw"){
                        $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("borderColor",color);
                    }
                    break;
				case "shapeBorderweight" :
					var mode = $("#kk_shape").attr("mode");
					var	targetWrap = $(".kk-mobile-shape-selectabled > .objectData");

                    if(mode == "shape"){
    					targetWrap.find("kkshape").attr("shape-stroke-width",menu.val);
    					targetWrap.find("kkshape svg path").attr("stroke-width",menu.val).css("stroke-width",menu.val);
    					targetWrap.find("kkshape svg polyline").attr("stroke-width",menu.val).css("stroke-width",menu.val);
    					_this.saveSlide();
					}else if(mode == "line"){
						targetWrap = $(".kk-line-selectabled-box > .objectData");
    					targetWrap.find("kkshape").attr("shape-stroke-width",menu.val);

						//targetWrap.find("kkshape svg marker").attr({"":"",});
						targetWrap.find("kkshape svg path").attr("stroke-width",menu.val).css("stroke-width",menu.val);
    					targetWrap.find("kkshape svg polyline").attr("stroke-width",menu.val).css("stroke-width",menu.val);
    					//_this.saveSlide();
                    }else{
                        $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("lineWidth",menu.val);
                    }
                    break;
				case "shapeBorderDash" :

					var mode = $("#kk_shape").attr("mode");
					var	targetWrap = $(".kk-mobile-shape-selectabled > .objectData");
					if(mode == "line"){
						targetWrap = $(".kk-line-selectabled-box > .objectData");
					}

					var dash = ShapeUtil.getDashLineType(menu.val);
					//if(dash == ""){
						//targetWrap.find("kkshape svg path").removeAttr("stroke-dasharray");
						//targetWrap.find("kkshape svg polyline").removeAttr("stroke-dasharray");
						//targetWrap.find("kkshape svg polyline").removeAttr("stroke");
						//targetWrap.find("kkshape svg polyline").css("stroke","");
					//}else{
						//targetWrap.find("kkshape svg polyline").removeAttr("stroke");
						//targetWrap.find("kkshape svg polyline").css("stroke","");
						targetWrap.find("kkshape").attr("shape-stroke-type",menu.val);
						targetWrap.find("kkshape svg path").attr("stroke-dasharray",dash).css("stroke-dasharray",dash);
						targetWrap.find("kkshape svg polyline").attr("stroke-dasharray",dash).css("stroke-dasharray",dash);
					//}
					//_this.saveSlide();
                    break;

			/*-- Viewer -----------------------------------------------------------------------------------*/
				case "fitWidth" :
						var wW  = $(window).width() - ($("#lSideView").width()) - 20 ;
						var iW  = $("#currentSlide").width();

						$("#editView").scrollLeft(0);
						var zZ = wW / iW;
						var scale = zZ;
						if(_this.isMobile){
							_this.loader.iscroll.zoom(scale);
						}else{
							$("#kk_zoomSlider").kkZoomSelect("pageZoom",scale);
						}
						this.loader.slideResize();
                        this.getViewerMeta();
                    break;

				case "fitHeight" :
						var wH = $(window).height() - ($("#toolBar").height() + $("#statusBar").height()) - 20;
						var iH = $("#currentSlide").height();
						$("#editView").scrollTop(0);
						var zZ = wH / iH;
						var scale = zZ;
						if(_this.isMobile){
							_this.loader.iscroll.zoom(scale);
						}else{
							$("#kk_zoomSlider").kkZoomSelect("pageZoom",scale);
						}
						this.loader.slideResize();
                        this.getViewerMeta();
                    break;

				case "zoomdefault" :
						if(_this.isMobile){
							_this.loader.iscroll.zoom(1);
						}else{
							$("#kk_zoomSlider").kkZoomSelect("pageZoomDefault");
						}
                        this.getViewerMeta();
                    break;
				case "zoomin" :
						if(_this.isMobile){
							var zz = _this.loader.iscroll.scale + 0.2;
							_this.loader.iscroll.zoom(zz);
						}else{
							//$("#kk_zoomSlider").kkZoomSlider("pageZoomIn");
							$("#kk_zoomSlider").kkZoomSelect("pageZoomIn",menu.step);
						}
                        this.getViewerMeta();
                    break;
				case "zoomout" :
						if(_this.isMobile){
							var zz = _this.loader.iscroll.scale - 0.2;
							_this.loader.iscroll.zoom(zz);
						}else{
							//$("#kk_zoomSlider").kkZoomSlider("pageZoomOut");
							$("#kk_zoomSlider").kkZoomSelect("pageZoomOut",menu.step);
						}
                        this.getViewerMeta();
                    break;
				case "loupe" :
                        if($("#currentSlide").attr("href") == "" || $("#currentSlide").attr("href") == null){
                            var src = $("#currentSlide img.pdfCanvas").attr("src");
							console.log("@@@@@@@@@@@@@@"+src);
							$(".loupe img").attr("src",src);
                            $("#currentSlide").attr("href",src);
                            $("#currentSlide").loupe(true);
                        }else{
                            $("#currentSlide").attr("href","");
                            $("#currentSlide").loupe(false);
                        }
                    break;
                case "rotateRight" :
                        var wH = $(window).height() - ($("#toolBar").height() + $("#statusBar").height()) - 20;
                        var wW  = $(window).width() - ($("#lSideView").width()) - 20 ;
                        var iH = $("#currentSlide").height();
						var iW  = $("#currentSlide").width();
                        var wS = 0;
                        var iS = 0;
                        if(wH > wW){
                            wS = wW;
                            if(iH > iW){
                                iS = iW;
                            }else{
                                iS = iH;
                            }
                        }else{

                        }

                        var scale = 1;

                        $("#editView").scrollTop(0);
                        var zZ = wH / iH;
                        var scale = zZ;
                        if(_this.isMobile){
                            _this.loader.iscroll.zoom(scale);
                        }else{
                            $("#kk_zoomSlider").kkZoomSelect("pageZoom",scale);
                        }


                        _this.kkRotate = $("#lSideView .current .thumbWrap").attr("kk_rotate");

                        if(typeof(_this.kkRotate) == "undefined" || isNaN(_this.kkRotate) ){
                            _this.kkRotate = 0;
                        }else{
                            _this.kkRotate = Number(_this.kkRotate);
                        }
					    if(_this.kkRotate >= 270){
							_this.kkRotate = 0;
						}else{
							_this.kkRotate = _this.kkRotate + 90;
						}

                        if(_this.kkRotate == 270 || _this.kkRotate ==90){

                        }else{


                        }



						if(_this.loader.fileext == "pdf"){
							$("#currentSlide").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#currentSlide").css("transform-origin","50% 50%");
						}else{
                            if(_this.kkRotate == 270 || _this.kkRotate ==90){


								$("#currentSlide .pWrap").css("transform","translateX(23%) translateY(-12.5%) rotate("+ _this.kkRotate +"deg)");
								//$("#currentSlide .pWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
								$("#currentSlide .pWrap").css("transform-origin","50% 50%");

							}else{
								$("#currentSlide .pWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
								$("#currentSlide .pWrap").css("transform-origin","50% 50%");
							}

							$("#lSideView .current .thumbWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#lSideView .current .thumbWrap").css("transform-origin","50% 50%");
							$("#lSideView .current .thumbWrap").attr("kk_rotate",_this.kkRotate);

							$("#editView").css({"overflow-x":"auto","overflow-y":"auto"});

							/*

							var $lSideView = $("#lSideView");
							var $toolBar = $("#toolBar");
							var $imageImg = $(this.scroller);
							var wH = $(window).height() - $toolBar.height();
							var wW = $(window).width() - $lSideView.width();
							var pw = $imageImg.width()*this.scale;
							var ph = $imageImg.height()*this.scale;
							var sL = (wW - pw)/2;
							var sT = (wH - ph)/2;

							if((wW > pw) && (wH > ph)){
								$(this.scroller).css({"margin-top":sT+"px","margin-left":sL+"px"});
							}else if(wW > pw){
								$(this.scroller).css({"margin-top":"0px","margin-left":sL+"px"});
							}else if(wH > ph){
								$(this.scroller).css({"margin-top":sT+"px","margin-left":"0px"});
							}else{
								$(this.scroller).css({"margin-top":"0px","margin-left":"0px"});
							}
							*/


                            $("#lSideView .current").attr("isrot","y");
						}
                        this.getViewerMeta();
                    break;
                case "rotateRight180" :
                        _this.kkRotate = Number($("#lSideView .current .thumbWrap").attr("kk_rotate"));
                        if(typeof(_this.kkRotate) == "undefined" || isNaN(_this.kkRotate) ){
                            _this.kkRotate = 0;
                        }else{
                            _this.kkRotate = Number(_this.kkRotate);
                        }
					    if(_this.kkRotate >= 180){
							_this.kkRotate = 0;
						}else{
							_this.kkRotate = _this.kkRotate + 180;
						}
						if(_this.loader.fileext == "pdf"){
							$("#currentSlide").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#currentSlide").css("transform-origin","50% 50%");
						}else{
                            if(_this.kkRotate == 270 || _this.kkRotate ==90){
								$("#currentSlide .pWrap").css("transform","translateX(25%) translateY(-10%) rotate("+ _this.kkRotate +"deg)");
							}else{
								$("#currentSlide .pWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
							}
							$("#currentSlide .pWrap").css("transform-origin","50% 50%");

							$("#lSideView .current .thumbWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#lSideView .current .thumbWrap").css("transform-origin","50% 50%");
							$("#lSideView .current .thumbWrap").attr("kk_rotate",_this.kkRotate);
                            $("#lSideView .current").attr("isrot","y");
						}
                        this.getViewerMeta();
                    break;
                case "rotateRightAll" :
					    if(_this.kkRotate >= 270){
							_this.kkRotate = 0;
						}else{
							_this.kkRotate = _this.kkRotate + 90;
						}

						if(_this.loader.fileext == "pdf"){
							$("#currentSlide").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#currentSlide").css("transform-origin","50% 50%");
						}else{

							if($("#lSideView .current").find(".thumbWrap").hasClass("rotateLock")){

							}else{
								if(_this.kkRotate == 270 || _this.kkRotate ==90){
									$("#currentSlide .pWrap").css("transform","translateX(25%) translateY(-10%) rotate("+ _this.kkRotate +"deg)");
								}else{
									$("#currentSlide .pWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
								}
								$("#currentSlide .pWrap").css("transform-origin","50% 50%");
								$("#lSideView .thumbWrap:not(.rotateLock)").css("transform","rotate("+ _this.kkRotate +"deg)");
								$("#lSideView .thumbWrap:not(.rotateLock)").css("transform-origin","50% 50%");
								$("#lSideView .thumbWrap:not(.rotateLock)").attr("kk_rotate",_this.kkRotate);
								$("#lSideView .slideWrap").attr("isrot","y");
							}
						}
                        this.getViewerMeta();
                    break;
                case "rotateLeft" :
                        _this.kkRotate = Number($("#lSideView .current .thumbWrap").attr("kk_rotate"));
                        if(typeof(_this.kkRotate) == "undefined" || isNaN(_this.kkRotate) ){
                            _this.kkRotate = 0;
                        }else{
                            _this.kkRotate = Number(_this.kkRotate);
                        }

						_this.kkRotate = _this.kkRotate - 90;

						if(_this.kkRotate ==  -90){
							_this.kkRotate = 270;
						} else if( _this.kkRotate ==  -180){
							_this.kkRotate = 180;
						} else if(_this.kkRotate ==  -270){
							_this.kkRotate = 90;
						}

						if(_this.loader.fileext == "pdf"){
							$("#currentSlide").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#currentSlide").css("transform-origin","50% 50%");
						}else{
                            if(_this.kkRotate == 270 || _this.kkRotate ==90){
								$("#currentSlide .pWrap").css("transform","translateX(25%) translateY(-10%) rotate("+ _this.kkRotate +"deg)");
							}else{
								$("#currentSlide .pWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
							}
							$("#currentSlide .pWrap").css("transform-origin","50% 50%");
							$("#lSideView .current .thumbWrap").css("transform","rotate("+ _this.kkRotate +"deg)");
							$("#lSideView .current .thumbWrap").css("transform-origin","50% 50%");
							$("#lSideView .current .thumbWrap").attr("kk_rotate",_this.kkRotate);
                            $("#lSideView .current").attr("isrot","y");
						}
                        this.getViewerMeta();
                    break;
				case "nextPage" :
						_this.loader.nextPage();
                        _this.getViewerMeta();
                    break;
				case "prevPage" :
						_this.loader.prevPage();
                        _this.getViewerMeta();
                    break;
				case "thumbView" :
						if(typeof(SettingsDef.thumbnail.width) != "undefined"){
							thumbWidth = Number(SettingsDef.thumbnail.width);
						}

					    if($(".deskView").hasClass("thumbView")){
							$(".deskView").removeClass("thumbView");
							$("#lSideView").width(thumbWidth+60);
						}else{
							$(".deskView").addClass("thumbView");
							$("#lSideView").width(0);
						}

                        var scale = $("#currentSlide").attr("kk_zoom");
						var scrollW = ($(window).width() - $("#lSideView").width());
						$(".editView").css("width", scrollW + "px");

						if(_this.isMobile){
                            _this.loader.iscroll.zoom(scale);
							//_this.loader.iscroll.refresh();
						}else{
                            $("#kk_zoomSlider").kkZoomSelect("pageZoom",scale);
							_this.loader.slideResize();
                            _this.getViewerMeta();
						}
                    break;
                case "closeToolbar" :
                        var toolbarBtn = $("#closeToolbar");
                        if(toolbarBtn.hasClass("closed")){
                            toolbarBtn.removeClass("closed");
                            $("#toolBar").removeClass("closed");
                            $("#statusBar").removeClass("closed");
                        }else{
                            toolbarBtn.addClass("closed");
                            $("#toolBar").addClass("closed");
                            $("#statusBar").addClass("closed");
                        }
						var scale = $("#currentSlide").attr("kk_zoom");
						if(_this.isMobile){
							_this.loader.iscroll.zoom(scale);
						}else{
							$("#kk_zoomSlider").kkZoomSelect("pageZoom",scale);
                            _this.loader.slideResize();
                            _this.getViewerMeta();
						}
                    break;
                case "t-nav-next" :
                    $("#toolBar .buttonBar .section").css({"left":"auto","right":"80px"});
                    $("#toolBar a.next").hide();
                    $("#toolBar a.prev").show();
                    break;
                case "t-nav-prev" :
                    $("#toolBar .buttonBar .section").css({"left":"10px","right":"auto"});
                    $("#toolBar a.next").show();
                    $("#toolBar a.prev").hide();
                    break;
                case "b-nav-next" :
                    $("#statusBar .buttonBar .section").css({"left":"auto","right":"50px"});
                    $("#statusBar a.next").hide();
                    $("#statusBar a.prev").show();
                    break;
                case "b-nav-prev" :
                    $("#statusBar .buttonBar .section").css({"left":"10px","right":"auto"});
                    $("#statusBar a.next").show();
                    $("#statusBar a.prev").hide();
                    break;

			/*-- Image ------------------------------------------------------------------------------------*/
				case "imgSelecte" :
					if(_this.isMobile){
						$(".deskView").find(".kk_selected_space").css({top:"200px",left:"200px",width:"200px",height:"200px"}).show();
						$(".deskView").find(".kk_selected_space").kkMobileSelectable({
							mode: "normal",
							ratio: _this.loader.ratio,
							iscroll: _this.loader.iscroll,
							callback: function(element){
								var imgObject = element.find("img")[0];
								var pos = _this.mobileMeta();
								localStorage.setItem("cripL", pos.l);
								localStorage.setItem("cripT", pos.t);
								var mainCanvas = $("#currentSlide .pWrap canvas")[0];
								var context = mainCanvas.getContext('2d');
								context.drawImage(imgObject, pos.l*_this.ratio, pos.t*_this.ratio, pos.w*_this.ratio, pos.h*_this.ratio);
							}
						});
					}else{
						$("#kk_shape").kkSelectableObject({
							callback : function(pos){
            					var selectdiv = $('<div class="kk_selected_masking_space" style="top:'+pos['t']+'px;left:'+pos['l']+'px;width:'+pos['w']+'px;height:'+pos['h']+'px;"></div>');
            					$("#currentSlide .pWrap").append(selectdiv);
                                $("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
							}
						});
					}
                    break;
				case "imgFire" :
					if(_this.isMobile){
						var pos = _this.mobileMeta();
						var ctx = _this.loader.mainCanvas.getContext("2d");
						ctx.fillStyle = "#000000";
						ctx.fillRect(pos.l*_this.ratio,pos.t*_this.ratio,pos.w*_this.ratio,pos.h*_this.ratio);
					}else{
						_this.loader.mainCanvas= $("#currentSlide").find("canvas.pdfCanvas")[0];
						var element_id = $("#currentSlide").attr("document_id");
						var sel = $("#currentSlide .kk_selected_masking_space");
                        var zoom = $("#currentSlide").attr("kk_zoom");
						var pos_t = Number(sel.css("top").replace("px",""));
						var pos_l = Number(sel.css("left").replace("px",""));
						var pos_w = Number(sel.css("width").replace("px",""));
						var pos_h = Number(sel.css("height").replace("px",""));
						var ctx = _this.loader.mainCanvas.getContext("2d");

						_this.ratio = _this.loader.ratio;
						ctx.fillStyle = "#000000";
                        ctx.globalAlpha = 1;
						ctx.fillRect(pos_l,pos_t,pos_w,pos_h);
                        $("#currentSlide .pWrap .kk_selected_masking_space").remove();


						var thumbCanvas = $("#lSideView .slideWrap[document_id='"+element_id+"'] .thumb canvas")[0];
						var thumbContext = thumbCanvas.getContext('2d');
						thumbContext.drawImage(_this.loader.mainCanvas, 0, 0);

						var document_id = $("#currentSlide").attr("document_id");
						$("#lSideView div[document_id='"+element_id+"']").attr("imgUpdate","y");

					}
                    break;
                case "imgYmasking" :
                    if(_this.isMobile){
                        var pos = _this.mobileMeta();
                        var ctx = _this.loader.mainCanvas.getContext("2d");
                        ctx.fillStyle = "#000000";
                        ctx.fillRect(pos.l*_this.ratio,pos.t*_this.ratio,pos.w*_this.ratio,pos.h*_this.ratio);
                    }else{
						var element_id = $("#currentSlide").attr("document_id");
                        var sel = $("#currentSlide .kk_selected_masking_space");
                        var pos_t = Number(sel.css("top").replace("px",""));
                        var pos_l = Number(sel.css("left").replace("px",""));
                        var pos_w = Number(sel.css("width").replace("px",""));
                        var pos_h = Number(sel.css("height").replace("px",""));
                        var ctx = _this.loader.mainCanvas.getContext("2d");
                        _this.ratio = _this.loader.ratio;
                        ctx.fillStyle = "#ffff00";
                        ctx.globalAlpha = 0.3;
                        ctx.fillRect(pos_l,pos_t,pos_w,pos_h);
                        //$("#currentSlide .pWrap .kk_selected_space").remove();

						var thumbCanvas = $("#lSideView .slideWrap[document_id='"+element_id+"'] .thumb canvas")[0];
						var thumbContext = thumbCanvas.getContext('2d');
						thumbContext.drawImage(_this.loader.mainCanvas, 0, 0);
                    }
                    break;
				case "imgCut" :
					if(_this.isMobile){
						var pos = _this.mobileMeta();
						var ctx = _this.loader.mainCanvas.getContext("2d");
						ctx.fillStyle = "#000000";
						ctx.clearRect(pos.l*_this.ratio,pos.t*_this.ratio,pos.w*_this.ratio,pos.h*_this.ratio);
					}else{
						var sel = $("#currentSlide .kk_selected_masking_space");
						var pos_t = Number(sel.css("top").replace("px",""));
						var pos_l = Number(sel.css("left").replace("px",""));
						var pos_w = Number(sel.css("width").replace("px",""));
						var pos_h = Number(sel.css("height").replace("px",""));
						var ctx = _this.loader.mainCanvas.getContext("2d");
						_this.ratio = _this.loader.ratio;
						ctx.fillStyle = "#000000";
						ctx.clearRect(pos_l*_this.ratio,pos_t*_this.ratio,pos_w*_this.ratio,pos_h*_this.ratio);
						sel.remove();
					}
                    break;

				case "imgCrop" :
						//$(".kk_selected_space").kkCripboardSelectable().kkCripboardSelectable("destroy");
						$("#kk_shape").kkSelectableObject({
							callback : function(pos){
								var i_z = $("#currentSlide").attr("kk_zoom");
                                var spos = $("#kk_shape").offset();
								var i_y = 0;
								var i_x = 0;
                                var pos_l = i_z*pos.l+spos.left-$("#lSideView").width();
                                var pos_t = i_z*pos.t+spos.top-$("#toolBar").height();
								var pos_w = i_z*(pos.w);
								var pos_h = i_z*(pos.h);

								$("#editView").append('<div class="kk_cripboard_space"></div>');
								$(".kk_cripboard_space").css({"top":pos_t+"px","left":pos_l+"px","width":pos_w+"px","height":pos_h+"px"}).show();
								$(".kk_cripboard_space").html('<div kktype="crop" class="cripboard kk-crop-selectabled"></div>');
								$(".kk_cripboard_space").find(".cripboard").css({"top":"0px","left":"0px","width":pos_w+"px","height":pos_h+"px"});
								$(".kk_cripboard_space").kkCripboardSelectable({mode:"crop"});
								$("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
							}
						});
                    break;

				case "imgCopy" :
					var canvas = $(".pdfCanvas")[0];
					if(_this.isMobile){
						var pos = _this.mobileMeta();
						var crop_canvas = document.createElement('canvas');
						crop_canvas.width = pos.w;
						crop_canvas.height = pos.h;
						crop_canvas.getContext('2d').drawImage(_this.loader.mainCanvas, pos.l*_this.ratio, pos.t*_this.ratio, pos.w*_this.ratio, pos.h*_this.ratio, 0, 0, pos.w*_this.ratio, pos.h*_this.ratio);
						localStorage.setItem("cripL", pos.l);
						localStorage.setItem("cripT", pos.t);
						localStorage.setItem("cripW", pos.w);
						localStorage.setItem("cripH", pos.h);
						localStorage.setItem("cripImg", crop_canvas.toDataURL("image/jpeg"));
						this.loader.cripboardWrap();
					}else{
						$("#kk_shape").kkSelectableObject({
							callback : function(pos){
								var i_z = $("#currentSlide").attr("kk_zoom");
								var i_y = 0;
								var i_x = 0;
								var pos_l = i_z*(pos.l);
								var pos_t = i_z*(pos.t);
								var pos_w = i_z*(pos.w);
								var pos_h = i_z*(pos.h);
								$("#editView").append('<div class="kk_cripboard_space"></div>');
								$(".kk_cripboard_space").css({"top":pos_t+"px","left":pos_l+"px","width":pos_w+"px","height":pos_h+"px"}).show();
								$(".kk_cripboard_space").html('<div kktype="crop" class="cripboard kk-crop-selectabled"></div>');
								$(".kk_cripboard_space").find(".cripboard").css({"top":"0px","left":"0px","width":pos_w+"px","height":pos_h+"px"});
								$(".kk_cripboard_space").kkCripboardSelectable({mode:"copy"});
								$("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
							}
						});
					}
                    break;

				case "imgBlur" :
					var canvas = $(".pdfCanvas")[0];
					var sel = $("#currentSlide .kk_selected_space");
					var pos_t = Number(sel.css("top").replace("px",""));
					var pos_l = Number(sel.css("left").replace("px",""));
					var pos_w = Number(sel.css("width").replace("px",""));
					var pos_h = Number(sel.css("height").replace("px",""));
					var ctx = _this.loader.mainCanvas.getContext("2d");

					var passes = passes || 4;
					ctx.fillStyle = "#ffffff";
					ctx.globalAlpha = 0.2;
					ctx.fillRect(pos_l*_this.ratio,pos_t*_this.ratio,pos_w*_this.ratio,pos_h*_this.ratio);
					//ctx.globalAlpha = 1.0;
                    break;

				case "imgSave" :
					var canvas = $(".pdfCanvas")[0];
					var img = canvas.toDataURL("image/jpeg",1);
					var page = $("#currentSlide").attr("page");
					var total = $("body").attr("totalslide");
					var documentId = $("#currentSlide").attr("document_id");
					$.ajax({
						type: "POST",
						url: "../viewdit/_actionPdf.php",
						data: {
							"mode": "imgSave",
							"document_id" : documentId,
							"total": total,
							"page": page,
							"img" : img
						},
						dataType: "json",
						success: function (data) {
							$().toastmessage('showSuccessToast', '저장 되었습니다.');
						},
						complete: function () {
							var v = new Date();
							var src = $("#pageWrap_"+page+" img").attr("src");
							var asrc = src.split("?");
							$("#pageWrap_"+page+" img").attr("src",asrc[0]+"?v="+v.getTime());
						}
					});
                    break;

				case "imgGrayscale" :
						this.loader.filter("grayscale");
					break;

				case "imgTextBox" :
					if(_this.isMobile){
						$(".deskView").find(".kk_selected_space").kkMobileSelectable("imgTextEditor");

					}else{
						this.loader.textBoxWrap();
					}
                    break;

			/*-- Sketch -----------------------------------------------------------------------------------*/
				case "sketchAction" :
                    $("#kk_shape").kkSelectableObject({
                        callback : function(pos){
							var spos = $("#kk_shape").offset();
							var i_z = $("#currentSlide").attr("kk_zoom");
							var i_y = 0;
							var i_x = 0;
							var pos_l = i_z*pos.l+spos.left-$("#lSideView").width();
							var pos_t = i_z*pos.t+spos.top-$("#toolBar").height();
							var pos_w = i_z*(pos.w);
							var pos_h = i_z*(pos.h);

                            $("#editView .kk_selected_draw_space").show();
                            $("#editView .kk_selected_draw_space").css({"top":pos_t+"px","left":pos_l+"px","width":pos_w+"px","height":pos_h+"px"});
                            $("#editView .kk_selected_draw_space").kkDrawSelectable({
								filetype:SettingsDef.loading.type,
                                callback:function(element){
                                    $("#kk_shape .sketch").on('click',function(){
                                        _this.loader.sketchBoxWrap(this);
                                        return false;
                                    });
                                }
                            });
                            $("#kk_shape").kkSelectableObject().kkSelectableObject("destroy");
                        }
                    });

                    break;
				case "sketchBorderColor" :
					var color  = "";
				    if(menu.color == "transparent"){
						color  = "transparent";
						//$("#kk_sketch").sketch("tool","eraser");
					}else{
						color  = "#"+menu.color;
					}
                    $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("borderColor",color);
                    break;
				case "sketchBorderweight" :
					//_this.loader.sketch.style.lineWidth = menu.val;
                    $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("lineWidth",menu.val);
                    break;
				case "sketchBorderOpacity" :
                    $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("globalAlpha",menu.val);
                    break;

				case "sketchEraser" :
                    $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("eraser");
					break;
				case "sketchClear" :
                    $("#editView").find(".kk_selected_draw_space").kkDrawSelectable("clearRecording");
					break;
                case "sketchSave" :
					var path = this.loader.sketch.protoToString();
					var documentId = $("#currentSlide").attr("document_id");
					var page = $("#currentSlide").attr("page");
					//var key = e.element.attr("key");
					if(typeof key === "undefined"){
						key = 0;
					}
					break;

			/*-- ViewMode ---------------------------------------------------------------------------------*/
				case "toggleDraw" :
					if(menu.val == "off"){
						$("#kk_sketch").hide();
						$("#statusBar .buttonBar a[ref='imgDraw']").removeClass("disabled");
					}else{
						//$("#kk_sketch").show();
						_this.loader.updateState("toggleDraw");
					}
                    break;

				case "toggleEditor" :

					if(menu.val == "off"){
						$("#kk_shape").hide();
						$("#statusBar .buttonBar a[ref='imgEditor']").addClass("disabled");
					}else{
						_this.loader.updateState("toggleEditor");
						//$("#kk_shape").show();
					}
                    break;

				case "toggleShape" :
					if(menu.val == "off"){
						$("#kk_shape").hide();
						$("#statusBar .buttonBar a[ref='imgShape']").addClass("disabled");
					}else{
						_this.loader.updateState("toggleShape");
						//$("#kk_shape").show();
					}
                    break;
				case "toggleTextMasking" :
					if(menu.val == "off"){
						//$("#kk_sketch").hide();
						$("#statusBar .buttonBar a[ref='imgMasking']").addClass("disabled");
					}else{
						//$("#kk_sketch").show();
						_this.loader.updateState("toggleTextMasking");
					}
                    break;
 				case "toggleScroll" :
					if(menu.val == "off"){
						_this.loader.l_iscroll.scrollTo(0,0);
						_this.loader.l_iscroll.disable();
						var wheight = window.innerHeight ? window.innerHeight : $(window).height();
						var scrollH = (wheight - ($("#toolBar").height() + $("#statusBar").height()) - 50);
						$(".lSideViewWrapSCroll").css({"height":scrollH + "px","overflow":"auto", "padding":0});
						$('#lSideView .lSideViewWrap .lSideViewWrapSCroll').sortable("enable");
					}else{
						_this.loader.l_iscroll.enable();
						$('#lSideView .lSideViewWrap .lSideViewWrapSCroll').sortable("disable");
						$(".lSideViewWrapSCroll").css({"height":"auto","overflow":"hidden"});
					}
                    break;

            /*-- Text Masking -----------------------------------------------------------------------------*/
				case "pdfSelecte" :
						if($("a[u-id='pdfSelecte']").hasClass("current")){
							_this.loader.menuPdfSelecte(false);
						}else{
							_this.loader.menuPdfSelecte(true);
						}
                    break;
				case "txtSelecte" :
						if($("a[u-id='txtSelecte']").hasClass("current")){
							_this.loader.menuTxtSelecte(false);
						}else{
							_this.loader.menuTxtSelecte(true);
						}
                    break;
				case "maskingFire" :
					if($(".pWrap .text_layer").is(":visible")){

						var ctxt = _this.loader.ctxt;
						if($("#currentSlide").attr("imgupdate") == "y"){
							_this.ratio = 1*ctxt;
						}else{
							_this.ratio = _this.loader.ratio*ctxt;
						}

						$(".pWrap .text_layer > .ui-selected").each(function(){
							var sel = $(this);

							_this.loader.mainCanvas= $("#currentSlide").find("canvas.pdfCanvas")[0];
                            var zoom = $("#currentSlide").attr("kk_zoom");
							var pos_t = Number(sel.css("top").replace("px",""))*_this.ratio;
							var pos_l = Number(sel.css("left").replace("px",""))*_this.ratio;
							var pos_w = Number(sel.css("width").replace("px",""))*_this.ratio;
							if (typeof(sel.attr("data-canvas-width")) != "undefined"){
								pos_w = Number(sel.attr("data-canvas-width"))*_this.ratio;
							}
							var pos_h = Number(sel.css("height").replace("px",""))*_this.ratio;
							var ctx = _this.loader.mainCanvas.getContext("2d");
							ctx.fillStyle = "#000000";
							ctx.globalAlpha = 1;
							ctx.fillRect(pos_l,pos_t,pos_w,pos_h);
							sel.remove();
						});

						$("#lSideView div.slideWrap.current").attr("imgUpdate","y");
						var src = _this.loader.mainCanvas.toDataURL("image/jpeg");
						$("#lSideView div.slideWrap.current img").attr("src",src);
						$("#lSideView div.slideWrap.current").find(".text_layer").html($("#kk_text_layer").html());

					}else{

						if($("#currentSlide").attr("imgupdate") == "y"){
							_this.ratio = 1;
						}else{
							_this.ratio = _this.loader.ratio;
						}


                        var sel = $("#currentSlide .kk_selected_masking_space");
                        var zoom = $("#currentSlide").attr("kk_zoom");
                        var pos_t = Number(sel.css("top").replace("px",""))*_this.ratio;
                        var pos_l = Number(sel.css("left").replace("px",""))*_this.ratio;
                        var pos_w = Number(sel.css("width").replace("px",""))*_this.ratio;
                        var pos_h = Number(sel.css("height").replace("px",""))*_this.ratio;
                        var ctx = _this.loader.mainCanvas.getContext("2d");

                        ctx.fillStyle = "#000000";
                        ctx.globalAlpha = 1;
                        ctx.fillRect(pos_l,pos_t,pos_w,pos_h);

						$("#lSideView div.slideWrap.current").attr("imgUpdate","y");
						var src = _this.loader.mainCanvas.toDataURL("image/jpeg");
						$("#lSideView div.slideWrap.current img").attr("src",src);
						$("#lSideView div.slideWrap.current").find(".text_layer").html($("#kk_text_layer").html());
						sel.remove();
                        //$("#currentSlide .pWrap .kk_selected_space").remove();
					}

                    break;
				case "pdfMaskingSave" :
					var total = $("#lSideView").attr("totalslide");
					var documentId = $("#currentSlide").attr("document_id");
					var zoom = $("#currentSlide").attr("kk_zoom");
					var paperSizeWidth = $("#pageWrap_1").attr('w');
					var paperSizeHeight = $("#pageWrap_1").attr('h');
					var paperOrientation  = $("#currentSlide").attr("orientation");
					var textLayout = [];
					var imageStr = [];
					var pages = [];

                    var f = 0;
				    $("#lSideView .lSideViewWrapSCroll div.slideWrap").each(function(i){
						if($(this).attr("imgupdate") == "y"){
							var page = $(this).attr("page");
							pages[f] = page;
							imageStr[f] = $(this).find("img").attr("src");
							textLayout[f] = $(this).find(".text_layer").html();
							f++;
						}
					});

                    var pram = {
                        "mode" : "maskingSave",
                        "document_id" : documentId,
                        "total" : total,
						"pages" : pages,
						"width" : paperSizeWidth,
						"height" : paperSizeHeight,
						"orientation" : paperOrientation,
                        "textLayout" : textLayout,
                        "imageStr" : imageStr
                    };

                    $.extend(true, pram, SettingsDef.save.pdfSaveParam);
					$.ajax({
						type: "POST",
						url: SettingsDef.save.pdfSaveURL,
						data: pram,
						dataType: "json",
						success: function (data) {
							$().toastmessage('showSuccessToast', '저장 되었습니다.');
						},
						complete: function () {
						}
					});
                    break;
                case "pdfPageReset" :
					var current = $(".lSideViewWrapSCroll > .current").attr("page");
					_this.loader.currentPage(current,"y");
					$("#currentSlide").attr("imgupdate","n");

                    break;
				default :
                    break;
            }
        };

        this.saveSlide = function(){
			var _this = this;
			var rid = $("#currentSlide").attr("rid");
			$("#lSideView > .slideWrap[rid='"+rid+"'] > div").html($("#currentSlide").html());
			var selectabled = $("#lSideView > .slideWrap[rid='"+rid+"'] > div .kk-shape-selectabled-box");
			selectabled.find(".kk-shape-selectable").remove();
			selectabled.removeClass("kk-shape-selectabled-box");
			$("#lSideView > .slideWrap[rid='"+rid+"']").attr("edited","1");
        };

		this.mobileMeta = function(){
			var _this = this;
			var sel = $(".deskView .kk_selected_space");
			var $lSideView = $("#lSideView");
			var $toolBar = $("#toolBar");
			var $imageImg = $("#currentSlide .pWrap");

			var pos_t = Number(sel.css("top").replace("px",""));
			var pos_l = Number(sel.css("left").replace("px",""))-$lSideView.width();
			var pos_w = Number(sel.css("width").replace("px",""));
			var pos_h = Number(sel.css("height").replace("px",""));

			var i_z = 1;
			var i_y = 0;
			var i_x = 0;
			if(_this.isMobile){
				i_z = _this.loader.iscroll.scale;
				i_y = _this.loader.iscroll.y;
				i_x = _this.loader.iscroll.x;
			}else{
				i_z = $("#currentSlide").attr("kk_zoom");
				i_y = $("#editView").scrollTop();
				i_x = $("#editView").scrollLeft();
			}

			var $imageImg = $("#currentSlide .pWrap");

			var wH = $(window).height() - $toolBar.height();
			var wW = $(window).width() - $lSideView.width();
			var pw = $imageImg.width()*i_z;
			var ph = $imageImg.height()*i_z;
			var sL = (wW - pw)/2;
			var sT = (wH - ph)/2;

			var r_t = (pos_t - i_y)/i_z;
			var r_l = (pos_l - i_x)/i_z;

			if((wW > pw) && (wH > ph)){
				r_t = (pos_t - sT)/i_z;
				r_l = (pos_l - sL)/i_z;
			}else if(wW > pw){
				r_l = (pos_l - sL)/i_z;
			}else if(wH > ph){
				r_t = (pos_t - sT)/i_z;
			}

			var res = {
				t:r_t,
				l:r_l,
				w:(pos_w/i_z),
				h:(pos_h/i_z),
				z:i_z,
				iw:pos_w,
				ih:pos_h
			};
            //console.log(res);
            return res;
		};

        this.getViewerMeta = function(){
			if(SettingsDef.loading.actionCallback == "on"){
				var $current = $("#lSideView .slideWrap.current");
				var page = $current.attr("page");
				var imgupdate = $current.attr("imgupdate");
				var isedit = $current.attr("isedit");
				var rotate = $current.find(".thumbWrap").attr("kk_rotate");
				var document_id = $current.attr("document_id");
				var zoom = $("#currentSlide").attr("kk_zoom");
				var thumbView = true;
				if($(".deskView").hasClass("thumbView")){
					thumbView = false;
				}
				var meta = {
					"thumbView":thumbView,
					"zoom":zoom,
					"element":document_id,
					"rotate":rotate,
					"page":page,
					"imgupdate":imgupdate,
					"isedit":isedit
				};

				window.parent.postMessage({
					getViewerMeta:meta
				}, '*');
			}
		};


    };
    return Menu;
});

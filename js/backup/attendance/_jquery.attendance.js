(function($)
{
	$.extend(
	{
		attendance : 
		{
		//-----------------------------------------------------------------------------------------
		list : function(param)  {
			$.ajax({
				type: "GET",
				url: "attendance_list.php",
				data: param,
				dataType: 'html',
				success: function(data){
					$('#attendance_box').removeClass("mainBoxFull").addClass("mainBox").html(data);
					$('#attendance_sidebar').show();
					
				}
			});
		},
/*
		insert : function(group_id)  {
			$("#attendance_sidebar").show();
			$('#attendance_box').load('../attendance/attendance_form.php?group_id='+group_id);
			$('#locationBox').html('<span class="box">'+
				'<input type="button" id="list-back" class="ui-button ui-state-default ui-corner-all" onclick="$.attendance.list();" value="출석부리스트" />' +
	            '</span>' +
				'<span class="arrow">&nbsp;</span>' +
				'<span class="box">출석부</span>');
		},
*/
		detail : function(attendance_id)  {
			$('#attendance_box').load('../attendance/attendance.php?id='+attendance_id);

		},

		report : function(id,date)  {
			$('#attendance_box').load('../attendance/attendance_report.php?id='+id+'&date='+date);

		},

		reportTalent : function(group_id,date)  {
			$('#attendance_box').load('../attendance/attendance_report_talent.php?group_id='+group_id+'&date='+date);
		},


		mergeDetail : function(root_id)  {
			$('#attendance_box').load('../attendance/attendance_merge.php?root_id='+root_id);
		},
		
		mergeReport : function(root_id,date)  {
			$('#attendance_box').load('../attendance/attendance_report_merge.php?root_id='+root_id+'&date='+date);
		},

		print : function(id,date)  {
			var w = 700;
			var h = 500;
			NewPopup = window.open('attendance_report_print.php?id='+id+'&date='+date, "new", "titlebar=0, resizable=1, scrollbars=yes, width="+w+", height="+h);
		},

		one : function(week, usr_id, check_1, is_note)  {
			var className = "icon_default";
            if(check_1 == 'O'){
                className = "icon_check_1";
			}else if(check_1 == 'L'){
				className = "icon_check_2";
			}else if(check_1 == 'X'){
				className = "icon_check_3";
			}

			if(is_note == "O"){
				className = className + "_on";
			}

			$("#"+week+"_"+usr_id).removeClass();
            $("#"+week+"_"+usr_id).addClass(className);
		}
		,
		view_chart : function(group_id)  {
			$('#attendance_box').load('../attendance/attendance_chart.php?group_id='+group_id);
		}
		,
		view_chart_1st : function()  {
			$('#attendance_box').load('../attendance/attendance_chart_1st.php');
		}
		,
		view_chart_2st : function()  {
			$('#attendance_box').load('../attendance/attendance_chart_2st.php');
		}
		,
		view_chart_all : function()  {
			$('#attendance_box').load('../attendance/attendance_chart_all.php');
		}
		,
		check : function(date,usr_id)  {
			var group_id = $('#group_id').val();
			$('#dialog').load("check_form.php?date="+date+"&usr_id="+usr_id+"&group_id="+group_id);
		}
		,
		
		check_insert_action : function()  {
			var form_var = $('#attendanceForm').serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error == 0){						
						$.attendance.one(data.week, data.usr_id, data.check_1, data.is_note);
						$('#dialog').dialog('close');
					}else{
						$.attendance.message(data.message);
					}					
				}
			});
		}
		,

		comment_insert_action : function(attendance_id) {
			var form_var = $('#attendanceCommentForm').serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(json){
					$.attendance.comment_list(attendance_id);
					$('#note').val('');
				}
			});
			return true;
		}
		,

		comment_list : function(attendance_id) {
				$('#attendance_comment_list').load('../attendance/attendance_comment_list.php?attendance_id='+attendance_id);
		}
		,
		
		comment_delete : function(comment_id,attendance_id) {

			if(!confirm("정말로 삭제하시겠습니까?")){
				return false;
			}else{
				$.ajax({
					type: "GET",
					url: "action.php",
					data: {'mode':'comment_delete', 'comment_id': comment_id , 'attendance_id' : attendance_id},
					dataType: 'json',
					success: function(data){
						$.attendance.comment_list(attendance_id);
					}
				});
				return false;
			}


		}
		,
		message : function (msg){
			$('#usr_msg').fadeIn("slow");
			$('#usr_msg').html(msg);
			window.setTimeout("$('#usr_msg').fadeOut('slow');",5000);
		}
		,
		reportSave : function (){
			var attendance_id = $("#attendance_id").val();
			var group_id = $("#group_id").val();
			var reg_date = $("#reg_date").val();
			var usr_id   = makeVal($("#roll_list input[name='usr_id']").serializeArray());
			var worship  = makeVal($("#roll_list select[name='worship']").serializeArray());
			var gbs      = makeVal($("#roll_list select[name='gbs']").serializeArray());
			var qt       = makeVal($("#roll_list select[name='qt']").serializeArray());
			var bible    = makeVal($("#roll_list select[name='bible']").serializeArray());
			var prayer   = makeVal($("#roll_list select[name='prayer']").serializeArray());
			var note     = makeVal($("#roll_list textarea[name='note']").serializeArray());
						
			var suggest  = $("#suggest").val();
			var report   = $("#report").val();
			var unusual  = $("#unusual").val();
			var bigo     = $("#bigo").val();

            if(attendance_id == 0 || group_id == 0) {
				alert('시스템에 문제가 생겼습니다. 잠시후 다시 사용해주세요. ');
				return false;
			}

			$.ajax({
				type: "POST",
				url: "action.php",
				data: {
					"mode":"report_save",
					"attendance_id":attendance_id,
                    "group_id":group_id,
					"reg_date":reg_date,
					"usr_id":usr_id, 
					"worship":worship,
					"gbs":gbs,
					"qt":qt,
					"bible":bible,
					"prayer":prayer,
					"note":note,
					"suggest":suggest,
					"report":report,
					"unusual":unusual,
					"bigo":bigo
				},
				dataType: 'json',
				success: function(json){
					if(json.error < 0){
						alert(json.message);
					}
				}
			});

			function makeVal(arry){
				var arryReturn =new Array();
				jQuery.each(arry, function(i, field){
					arryReturn[i] = field.value;
				});
                return arryReturn.join("|");
	 		}

		},

		reportTalentSave : function (){
			var group_id = $("#group_id").val();
			var reg_date = $("#reg_date").val();
			var usr_id   = makeVal($("#roll_list input[name='usr_id']").serializeArray());
			var worship  = makeVal($("#roll_list select[name='worship']").serializeArray());
			var gas      = makeVal($("#roll_list select[name='gas']").serializeArray());
			var note     = makeVal($("#roll_list input[name='note']").serializeArray());
					
			var attendance = makeVal($("#roll_list select[name='attendance']").serializeArray());
			var accumulateState = makeVal($("#roll_list input[name='accumulate_State']").serializeArray());
			var bible      = makeVal($("#roll_list select[name='bible']").serializeArray());
			var offertory  = makeVal($("#roll_list select[name='offertory']").serializeArray());
			var evangelize = makeVal($("#roll_list select[name='evangelize']").serializeArray());
			var sumTalent  = makeVal($("#roll_list input[name='sumTalent']").serializeArray());

			var suggest  = $("#suggest").val();
			var report   = $("#report").val();
			var unusual  = $("#unusual").val();
			var bigo     = $("#bigo").val();

			$.ajax({
				type: "POST",
				url: "action.php",
				data: {
					"mode":"report_talent_save",
                    "group_id":group_id,
					"reg_date":reg_date,
					"usr_id":usr_id, 
					"worship":worship,
					"gbs":gbs,
					
					"attendance":attendance,
					"accumulateState":accumulateState,
					"bible":bible,
					"offertory":offertory,
					"evangelize":evangelize,
                    "sumTalent":sumTalent,				
					
					"note":note,
					"suggest":suggest,
					"report":report,
					"unusual":unusual,
					"bigo":bigo,
					"quarter":1

				},
				dataType: 'json',
				success: function(json){
					if(json.error < 0){
						alert(json.message);
					}
				}
			});

			function makeVal(arry){
				var arryReturn =new Array();
				jQuery.each(arry, function(i, field){
					arryReturn[i] = field.value;
				});
                return arryReturn.join("|");
	 		}

		},



	    total : function()  {
			var totalUsr  = $("#roll_list input[name='usr_id']").serializeArray().length;
			var worshipTotal = countVal($("#roll_list select[name='worship']").serializeArray());

            $("#totalUsr").html(totalUsr);
			$("#worshipTotal").html(worshipTotal);



			function countVal(arry){				
				var c = 0;
				jQuery.each(arry, function(i, field){
					if(field.value == "O"||field.value == "L"){
						c++;
					}
				});
                return c;
	 		}
		},
		
		talentSum : function(usr_id) {
			var attendance = Number($("#user_"+usr_id+" select[name='attendance']").val());
			var bible      = Number($("#user_"+usr_id+" select[name='bible']").val());
			var offertory  = Number($("#user_"+usr_id+" select[name='offertory']").val());
			var evangelize = Number($("#user_"+usr_id+" select[name='evangelize']").val());
			var accumulate = Number($("#user_"+usr_id+" input[name='accumulate']").val());
            var total = attendance + bible + offertory + evangelize;

			$("#user_"+usr_id+" input[name='sumTalent']").val(total);
			$("#user_"+usr_id+" input[name='accumulate_State']").val(total+accumulate);

		},



		comment : function(attendance_usr_id){

            var _this = this;
			
			$("#dialog").dialog('option', 'title', '의견' );
			$.ajax({
				type: "GET",
				url: "../attendance/attendance_report_comment.php?id="+attendance_usr_id,
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);
				},
				complete : function(){	
					//_this.commentList(attendance_usr_id);
				}
			});
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

        commentForm : function(attendance_usr_id){
			$("#attendanceComment").val("");
			$("#attendance_usr_id").val(attendance_usr_id);
		},


		commentInsertAction : function(attendance_usr_id,sys_id) {
			var _this = this;
			var commentText = $("#attendanceComment").val();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: {
					'mode':'insertComment',
					'attendance_usr_id' : attendance_usr_id,
					'comment' : commentText,
					'usr_id'  : sys_id
				},
				dataType: 'json',
				success: function(data){
					_this.commentList(attendance_usr_id);
					//_this.reportUsrOne(attendance_usr_id,data.usr_id);
				},
				complete : function(){	
				}
			});
			return true;
		},


		commentList : function(attendance_usr_id) {
			$('#attendanceUsrCommentList').load('attendance_report_comment_list.php?id='+attendance_usr_id);
		},
		
		
		commentDelete : function(comment_id,attendance_usr_id) {
			var _this = this;
			if(!confirm("정말로 삭제하시겠습니까?")){
				return false;
			}else{
				$.ajax({
					type: "GET",
					url: "action.php",
					data: {'mode':'deleteComment', 'comment_id':comment_id},
					dataType: 'json',
					success: function(data){
						_this.commentList(data.attendance_usr_id);
						_this.reportUsrOne(data.attendance_usr_id,data.usr_id);
					}
				});
				return false;
			}
		},


		_ver : "0.9.1"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

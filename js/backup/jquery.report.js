
(function($)
{
	$.extend(
	{
		report : 
		{
		//-----------------------------------------------------------------------------------------

            id : 0,
			mode : "",
			usr_id  : 0,
			item_id : 0,
			listBox : "visitBox",
			
			list : function (json){
				var _this = this;
				if(json.listBox != undefined){ _this.listBox = json.listBox; }
				if(json.mode    != undefined){ _this.mode = json.mode; }
				if(json.usr_id  != undefined){ _this.usr_id = json.usr_id; } 
				_this.listWarp();
			},

			listWarp : function(){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../report/_visitList.php",
					data: {"mode":_this.mode, "usr_id":_this.usr_id},
					dataType: 'html',
					success: function(data){
						$("#"+_this.listBox).html(data);
					},
					complete : function(){}
				});
			}, 
			
			view : function(id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../report/_visitView.php?id="+id,
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방 보고서' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"수정" : function() { 
								_this.modify();
							},
							"닫기" : function() { 
								$(this).dialog("close");
							} ,  
							"삭제" : function() { 
								_this.deleteAction();
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

            insert : function (usr_id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../report/_visitForm.php",
					data: {"mode":"insert", "usr_id":usr_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방보고서 작성' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"보고서 작성" : function() { 
								_this.insertAction();
							} ,  
							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

			insertAction : function(){
				var _this = this;
				var form_var = $('#visitForm').serialize();
				var usr_id  = $.trim($("#visitForm input[name='usr_id']").val());
				var type    = $.trim($("#visitForm input[name='type']").val());
				var title   = $.trim($("#visitForm input[name='title']").val());
				var place   = $.trim($("#visitForm input[name='place']").val());
				var content = $.trim($("#visitForm textarea[name='content']").val());
				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == '') {
					alert("심방종류를 이력하세요^^");
					return false;
				}

				if(title == '') {
					alert("제목을 이력하세요^^");
					return false;
				}

				if(content == '') {
					alert("내용을 이력하세요^^");
					return false;
				}	
				$.ajax({
					type: "POST",
					url: "../report/_visitAction.php",
					data: form_var,
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
			},
			modify : function (visit_id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../report/_visitForm.php",
					data: {"mode":"modify", "id":visit_id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					},
					complete : function(){	
						$("#sub_dialog").dialog('option', 'title', '심방보고서 수정' );
						$("#sub_dialog").dialog('option', 'height', 'auto');
						$("#sub_dialog").dialog('option', 'width', 500);
						$("#sub_dialog").dialog('option', 'buttons', {
							"저장" : function() { 
								_this.modifyAction();
							} , 
								
							"삭제" : function() { 
								_this.deleteAction(visit_id);
							} , 

							"닫기" : function() { 
								$(this).dialog("close"); 
							}
						});
						$('#sub_dialog').dialog('open');
					}
				});
			},

			modifyAction : function(){
				var _this = this;
				var form_var = $('#visitForm').serialize();
				var usr_id  = $.trim($("#visitForm input[name='usr_id']").val());
				var type    = $.trim($("#visitForm input[name='type']").val());
				var title   = $.trim($("#visitForm input[name='title']").val());
				var place   = $.trim($("#visitForm input[name='place']").val());
				var content = $.trim($("#visitForm textarea[name='content']").val());
				if(place == '') {
					alert("장소를 입력하세요!");
					return false;
				}
				if(type == '') {
					alert("심방종류를 이력하세요^^");
					return false;
				}
				if(title == '') {
					alert("제목을 이력하세요^^");
					return false;
				}
				if(content == '') {
					alert("내용을 이력하세요^^");
					return false;
				}
				$.ajax({
					type: "POST",
					url: "../report/_visitAction.php",
					data: form_var,
					dataType: 'json',
					success: function(data){
						$('#sub_dialog').dialog('close');
						_this.listWarp();
					}
				});
			},

			deleteAction : function(visit_id) {
				var _this = this;
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					
				}else{
					$.ajax({
						type: "GET",
						url: "../report/_visitAction.php",
						data: {"mode":"visitDelete", "id":visit_id },
						dataType: 'json',
						success: function(data){
							_this.listWarp();
							$('#sub_dialog').dialog('close');
						}
					});
				}
			},


			_ver : "1.0.1"
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

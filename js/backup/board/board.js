
(function($)
{
	$.extend(
	{
		board : 
		{
		//-----------------------------------------------------------------------------------------
			list  : function(page){
				$('#board_box').load("../board/list.php?page="+page);
			}
			,
			modify : function(id){
				$('#board_box').load("../board/form.php?mode=modify&id="+id);
			}
			,
			modify_action : function(id) {
				tinyMCE.triggerSave();
				var form_var = $('#boardForm').serialize();
				var subject = $.trim($('#subject').val());
				if(subject == '') {
					show_message('popup_msg','글 제목을 입력하세요!');
					return false;
				}

				$.ajax({
					type: "POST",
					url: "../board/action.php",
					data: form_var,
					success: function(data){
						board_list('1');
					}
				});
				return true;
			}
			,
			insert : function(){
				$('#board_box').load("../board/form.php?mode=insert");
			}
			,
			insert_action : function(){
				tinyMCE.triggerSave();
				var form_var = $('#boardForm').serialize();
				var subject = $.trim($('#subject').val());
				if(subject == '') {
					show_message('popup_msg','글 제목을 입력하세요!');
					return false;
				}

				$.ajax({
					type: "POST",
					url: "../board/action.php",
					data: form_var,
					success: function(data){
						this.list('1');
					}
				});
				return true;
			}
			,
			view : function(id){
				$('#board_box').load('../board/view.php?id='+id);
			}
			,
			delete_action : function(id) {
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					return false;
				}else{
					jQuery.get('../board/action.php',{mode:'delete', id: id },function(data) {
						board_list('1');
					});
				}
			}
			,
			file_delete : function(board_id,file_id) {
				if(!confirm("정말로 파일를 삭제하시겠습니까?")){
					return false;
				}else{
					jQuery.get('../board/action.php',{mode:'file_delete', id: file_id },function(data) {
						board_file_list(board_id);
					});
				}
			}
			,
			file_list : function(board_id){
				$('#file_list').load("../board/file_list.php?board_id="+board_id);
			}
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

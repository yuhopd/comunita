
(function($)
{
	$.extend(
	{
		board_comment : 
		{
		
			insert_action : function() {
				var board_id = $('#board_id_hidden').val();
				if($.trim($('#board_comment').val()) == ""){ 
					message("comment_message","코멘트를 입력하세요!");
					return false;
				}

				var form_var = $('#commentForm').serialize();
				$.ajax({
					type: "POST",
					url: "action.php",
					data: form_var,
					success: function(data){
						$.board_comment.list(board_id);
						$('#board_comment').val('');
					}
				});
				return true;
			}
			,

			list : function(board_id) {
				$('#comment_list_'+board_id).load('comment_list.php?board_id='+board_id);
			}
			,
			delete_action : function(id,board_id) {
				$.ajax({
					type: "GET",
					url: "action.php",
					data: {mode:'comment_delete', id: id , board_id: board_id},
					success: function(data){
						$('#comment_count_'+board_id).html('('+data+')');
						$.board_comment.list(board_id);
					}
				});
			}

        
		}
	});
})(jQuery);

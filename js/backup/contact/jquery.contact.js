(function($)
{

	

	$.extend(
	{
		contact : 
		{
		//-----------------------------------------------------------------------------------------

		usr_list_in_group : function(e)  {
			var group_id = e.attr('id');
			$('#sms_right').load('_usrList.php?group_id='+group_id);
		},

		group_list : function(params)  {
			$('#fragment-1').load('group_list.php?'+params);
		},

		sameage_list : function()  {
			$('#sameage').load('sameage_list.php');
		},
		
		class_list : function(params)  {
			$('#fragment-3').load('../sms/group_list.php?'+params);
		},

        isAllCheck : false,

		page : 1,
		pagination : 30,
		
		initialLocation : null,
		isInit : false,
		isAllCheck : false,

		group_id : 0,
		sameage_id : 0,
		position : 0,
		column : "",
		keyword : "",
		mode : "",

	/* 리스트 */

		setListOrder : function(){
			var _this = this;
			$("#contactListBox").sortable();
			$("#contactListOrderSetting").hide();
			$("#contactListOrderSave").show();
		},
		
		setListOrderSave : function(group_id){
			var _this = this;
			var str = [];
            var arry = $("#contactListBox li").attr("contact", function(i, val){
				str.push(val);
			});
			var sequence = str.join("|");
			
			$.ajax({
				type: "post",
				url: "../contact/action.php",
				data: {
					"mode" : "setGroupOrders",
					"sequence" : sequence,
					"group_id" : group_id
				},
				dataType: "json",
				success: function(data){
					//$("#mainBox").html(data);
				},
				complete : function(){
				}
			});
			
			$("#contactListBox").sortable("destroy");
			$("#contactListOrderSetting").show();
			$("#contactListOrderSave").hide();
		},


		list : function(json){
			console.log("@@@@@@@@@@@@");
			var _this = this;
			if(json.mode       != undefined){ _this.mode = json.mode; }
			if(json.position   != undefined){ _this.position = json.position; }
			if(json.group_id   != undefined){ _this.group_id = json.group_id; }
			if(json.sameage_id != undefined){ _this.sameage_id = json.sameage_id; }
			_this.page = 1;
			_this.keyword = "";
			_this.listWarp();
		},

		listWarp : function(){
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../contact/_contactList.php",
				data: {
					"mode":_this.mode,
					"column":_this.column,
					"position":_this.position,
					"sameage_id":_this.sameage_id,
					"group_id":_this.group_id,
					"q":_this.keyword,
					"page":_this.page
				},
				dataType: "html",
				success: function(data){
					$("#mainBox").html(data);
				},
				complete : function(){
				}
			});
		},

		pageList : function(page){ 
			var _this = this;
			_this.page = page;
			_this.listWarp();
		},

		prevList : function(){ 
			var _this = this;
			if(_this.page == 1){ }else{ _this.page = _this.page - 1; }
			_this.listWarp();
		},

	/* 검색 */
		search : function(){
			var _this = this;
			_this.keyword = $.trim($("#SearchContact input[name='keyword']").val());
			_this.page = 1;
			_this.listWarp();
			scrollTo(0, 0);
		},

		searchCancel : function(){
			var _this = this;
			$("#SearchContact input[name='keyword']").val("");
			_this.page = 1;
			_this.listWarp();
		},



		checkAll : function() {
			if(this.isAllCheck){
				$("input:checkbox").attr("checked",false);
				this.isAllCheck = false;
			}else{
				$("input:checkbox").attr("checked",true);
				this.isAllCheck = true;
			}
		},


		upSequence : function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../contact/action.php",
				data: {"mode":"upSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		},
		
		downSequence : function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../contact/action.php",
				data: {"mode":"downSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});

		},


	/* 회원추가 */
		usrInsert : function() {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원추가' );
			$("#dialog").dialog('option', 'width', 550);
			$("#dialog").dialog('option', 'height', "auto");
			$.ajax({
				type: "GET",
				url: "../contact/_usrForm.php",
				data: {"mode":"usr_insert"},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					//_this.usrGroupList(usr_id);
					$("#dialog select, #dialog input:checkbox, #dialog input:radio").uniform();
			        //$("#birth").datepicker({dateFormat:'yy-mm-dd', changeMonth:true, changeYear:true});
				}
			});

			$("#dialog").dialog('option', 'buttons', {
				"회원추가" : function() { 
					_this.usrInsertAction(); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});

			$('#dialog').dialog('open');
		},
	
		usrInsertAction : function() {
			var _this = this;
			var form_var = $('#userForm').serialize();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error > 0){
					    alert(data.message);
					}else{
						_this.usrModify(data.lastID);
					}
				}
			});
		},

	/* 회원수정 */
		usrModify : function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원수정' );
			$.ajax({
				type: "GET",
				url: "../contact/_usrForm.php",
				data: {"mode":"usr_modify", "usr_id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					_this.usrGroupList(usr_id);
					$("#dialog select, #dialog input:checkbox, #dialog input:radio").uniform();
			        //$("#birth").datepicker({dateFormat:'yy-mm-dd', changeMonth:true, changeYear:true});
				}
			});
			$("#dialog").dialog('option', 'width', 550);
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'buttons', {
				"회원수정" : function() { 
					_this.usrModifyAction();
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usrInfoModify : function(usr_id) {
			var _this = this;
			$("#userInfoForm input:text").removeAttr("disabled");
			$("#userInfoForm select").removeAttr("disabled");
			$("#usrInfoModifyWright").hide();
			$("#usrInfoModifySave").show();
			$("#usrInfoModifyCancel").show();
		},

		usrInfoModifyCancel : function() {
			var _this = this;
			$("#userInfoForm input:text").attr("disabled",true);
			$("#userInfoForm select").attr("disabled",true);
			$("#usrInfoModifyWright").show();
			$("#usrInfoModifySave").hide();
			$("#usrInfoModifyCancel").hide();
		},

		usrModifyAction : function() {
			var _this = this;
			var form_var = $("#userInfoForm").serialize();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.listWarp();
						$.common.usr_info(data.usr_id);
					}else{
						alert(data.message);
					}
				}
			});
		},

		usrNoteModify :  function(usr_id) {
			$("#userInfoSummary").prop("disabled",false); 
			$("#userInfoSummaryModify").hide();
			$("#userInfoSummarySave").show();

		},

		usrNoteSave : function(usr_id) {
			var _this = this;
			var summary = $("#userInfoSummary").val();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: {
					"mode":"usrSummaryModify",
					"usr_id":usr_id,
					"summary":summary				
				},
				dataType: 'json',
				success: function(data){
				    $("#userInfoSummary").prop("disabled",true); 
					$("#userInfoSummaryModify").show();
					$("#userInfoSummarySave").hide();
				}
			});
		},

	/* 회원삭제 */
		usrDeleteAction : function(usr_id) {
			var _this = this;
			if(!confirm("정말로 회원을 삭제하시겠습니까?")){

			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode"      : "usr_delete",
						"usr_id"    : usr_id
					},
					dataType : "json",
					success: function(data){
						_this.listWarp();
					}
				});
			}
		},

	/* 회원그룹설정 */
		usrGroupList : function(usr_id)  {
			$.ajax({
				type: "GET",
				url: "../contact/_usrGroupList.php",
				data: {"usr_id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#usr_group_list").html(data);				
				},
				complete : function(){					
					$("#usr_group_list select").uniform();
				}
			});

		},

		usrGroupInsertAction : function(usr_id) {
			var _this = this;
			var group_sel_id = $('#group_sel').val();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: {'mode':'usr_group_insert','group_id':group_sel_id,'usr_id':usr_id},
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.usrGroupList(usr_id);
					}else{
						alert(data.message);
					}
				}
			});
		},
	
		usrGroupDeleteAction : function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url:  "action.php",
				data: {'mode':'usr_group_delete','usr_group_id':id},
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.usrGroupList(data.usr_id);
					}else{
						alert(data.message);
					}
				}
			});
		},

	/* 회원 비밀번호 수정 */
		usrModifyPsw : function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원비밀번호 수정' );
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").load("../contact/_usrPswForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrModifyPswAction(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usrModifyPswAction : function(usr_id) {
			var _this = this;
			var form_var = $("#userPswForm").serialize();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.listWarp();
						$("#dialog").dialog("close"); 
					}else{
						alert(data.message);
					}

					
				}
			});
		},

	/* 회원 권한 설정 */
		usrModifyAuth : function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원권한 수정' );
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").load("../contact/_usrAuthForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrModifyAuthAction(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usrModifyAuthAction : function(usr_id) {
			var _this = this;
			var form_var = $("#userAuthForm").serialize();
			$.ajax({
				type: "POST",
				url: "../contact/action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.listWarp();
						$("#dialog").dialog("close"); 
					}else{
						alert(data.message);
					}

					
				}
			});
		},
		
	/* 이미지 수정 */
		usrMoodifyImage : function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원 이미지 수정' );
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").load("_usrImgForm.php?mode=usr_modify&usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"확인" : function() { 
					_this.listWarp();
					$("#dialog").dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

	/* 가족관계 설정 */
		usrFamilyList : function(json) {
			$.ajax({
				type : "GET",
				url : "_usrFamilyList.php",
				data : json,
				dataType : "html",
				success: function(data){
					$("#"+json.box).html(data);
				}
			});
		},

		usrFamilySearch : function(usr_id) {
			var _this = this;
			var q = $('#family_q').val().trim();
			if(q != ""){
				var keyword = encodeURIComponent(q);
				_this.usrFamilyList({"usr_id":usr_id ,"q":keyword, "box":"usrFamilySearchList"});
			}
		},

		usrFamilyDeleteAction : function(usr_id,family_id) {
			var _this = this;
			if(!confirm("정말로 회원 가족관계를 삭제하시겠습니까?")){
			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode": "usr_family_delete",
						"usr_id": usr_id,
						"family_id": family_id							
					},
					dataType : "json",
					success: function(data){
						_this.usrFamilyList({"usr_id":usr_id, "box": "usrFamilyList"});
					}
				});
			}
		},

		usrFamilyInsert : function(usr_id) {
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', 350);
			$("#sub_dialog").load("_usrFamilySearch.php?usr_id="+usr_id);
			$("#sub_dialog").dialog('option', 'buttons', {});
			$('#sub_dialog').dialog('open');
		},

		usrFamilyInsertAction : function(usr_id) {
			var _this = this;
            var family_id = $("#usrFamilySearchList input.radio:checked").val();
		    var relations = $('#relations').val();
			if(relations == 0){
				alert("관계를 선택하세요!");
			} else if(family_id == 0 || !family_id){
				alert("가족을 선택하세요!");
			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode"      : "usr_family_insert",
						"usr_id"    : usr_id,
						"family_id" : family_id,
						"relations" : relations
					},
					dataType : "json",
					success: function(data){
						if(data.error > 0){
							alert(data.message);
						}else{
							_this.usrFamilyList({"usr_id":usr_id, "box": "usrFamilyList"});
							$('#sub_dialog').dialog('close');
						}
					}
				});
			}
		},

		usrFamilyModify : function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원가족관계 수정' );
			$("#dialog").dialog('option', 'height', 400);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("_usrFamilyForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"확인" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		toggleAdvancedSearch : function() {
            if($("#advancedSearch").is(":visible")){
				$("#advancedSearch").hide();
			}else{
				$("#advancedSearch").show();
			}
		},


		selectAdvancedSearch :  function() {
			var column = $("#advancedSearch select[name='column']").val();

            if(column == "position"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">성도</option>' +
							'<option value="10">서리집사(남)</option>' +
							'<option value="11">서리집사(여)</option>' +
							'<option value="12">타집사</option>' +
							'<option value="13">명예집사</option>' +
							'<option value="14">안수집사</option>' +
							'<option value="15">협동집사</option>' +
							'<option value="16">공로집사</option>' +
							'<option value="17">은퇴집사</option>' +
							'<option value="18">협동은퇴집사</option>' +
							'<option value="19">전입안수집사</option>' +
							'<option value="20">권사</option>' +
							'<option value="21">은퇴권사</option>' +
							'<option value="22">명예권사</option>' +
							'<option value="23">전입권사</option>' +
							'<option value="24">시무권사</option>' +
							'<option value="25">협동권사</option>' +
							'<option value="26">전입명예권사</option>' +
							'<option value="27">전입은퇴권사</option>' +
							'<option value="30">장로</option>' +
							'<option value="32">전입장로</option>' +
							'<option value="33">협동장로</option>' +
							'<option value="35">시무장로</option>' +
							'<option value="37">은퇴장로</option>' +
							'<option value="38">전입은퇴장로</option>' +
							'<option value="39">원로장로</option>' +
							'<option value="40">사모</option>' +
							'<option value="50">교역자</option>' +
							'<option value="100">담임목사</option>' +
							'<option value="101">부목사</option>' +
							'<option value="102">목사</option>' +
							'<option value="103">영어예배목사</option>' +
							'<option value="104">협동목사</option>' +
							'<option value="105">교육목사</option>' +
							'<option value="106">찬양목사</option>' +
							'<option value="115">전임전도사</option>' +
							'<option value="116">준전임전도사</option>' +
							'<option value="117">찬양전도사</option>' +
							'<option value="118">교육전도사</option>' +
							'<option value="119">전도사</option>' +
							'<option value="120">선교사</option>' +
							'<option value="121">직원</option>' +
					'</select>');
			} else if(column == "status"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">본교회</option>' +
							'<option value="2">타교회</option>' +
							'<option value="4">비교인</option>' +
							'<option value="6">전출</option>' +
							'<option value="8">새가족</option>' +
							'<option value="10">별세</option>' +
							'<option value="11">미등록자</option>' +
					'</select>');
			} else if(column == "christen"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">등록</option>' +
							'<option value="2">학습</option>' +
							'<option value="3">유아세례</option>' +
							'<option value="4">세례</option>' +
					'</select>');

			} else {
				$("#columnInputWrap").html('<input type="text" id="columnInput" name="columnInput" value="" />');
			}
		},

		advancedSearchAction : function() {
			var _this = this;
			_this.mode = "advancedSearch";
			_this.column = $("#column").val();
			_this.keyword = $.trim($("#columnInput").val());
			_this.page = 1;
			_this.listWarp();
		},


	/* 메뉴 설정 */
        menuOn : function(usr_id) {
			$(".contact li[contact="+usr_id+"] .action").show();
		},

        menuOff : function(usr_id) {
			$(".contact li[contact="+usr_id+"] .action").hide();
		},

	/* 엑셀 설정 */
	
		_var : "1.1.5"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

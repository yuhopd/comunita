(function($)
{
	$.extend(
	{
		sms : 
		{
		//-----------------------------------------------------------------------------------------
        isAllCheck : false,

		usr_list : function(params)  {
			$('#sms_right').load('../sms/_usrList.php?'+params);
		},

		usr_list_in_group : function(e)  {
			var group_id = e.attr('id');
			$('#sms_right').load('../sms/_usrList.php?group_id='+group_id);
		},

		group_list : function(params)  {
			$('#fragment-1').load('../sms/group_list.php?'+params);
		},

		sameage_list : function()  {
			$('#sameage').load('../sms/sameage_list.php');
		},
		
		class_list : function(params)  {
			$('#fragment-3').load('../sms/group_list.php?'+params);
		},

		usr_insert : function() {
			$("#dialog").dialog('option', 'title', '회원추가' );
			$("#dialog").dialog('option', 'height', 450);
			$("#dialog").load("../sms/usr_form.php?mode=usr_insert");
			
			$("#dialog").dialog('option', 'buttons', {
				"회원추가" : function() { 
					$.sms.usr_insert_action(); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		search : function() {
            var keyword = encodeURIComponent($('#q').val());
			this.usr_list('q='+keyword);
		},
		
		usr_insert_action : function() {
			var form_var = $('#userForm').serialize();
			var str = "";
			if($('#group_id').val() != ''){
				var group_id = $('#group_id').val();
				var str = "group_id="+group_id;
			}else if($('#sameage_id').val() != ''){
				var sameage_id = $('#sameage_id').val();
				var str = "sameage_id="+sameage_id;
			}

			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error > 0){
						$.sms.message(data.message);
					}else{
						$.sms.usr_modify(data.lastID);
						//$.sms.usr_list(str);
						//$("#dialog").dialog("close"); 
					}

					
				}
			});
		},

		usr_modify : function(usr_id) {
			$("#dialog").dialog('option', 'title', '회원 수정' );
			$("#dialog").dialog('option', 'height', 450);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("usr_form.php?mode=usr_modify&usr_id="+usr_id);
			
			$("#dialog").dialog('option', 'buttons', {
				"회원수정" : function() { 
					$.sms.usr_modify_action(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usr_modify_psw : function(usr_id) {
			$("#dialog").dialog('option', 'title', '회원비밀번호 수정' );
			$("#dialog").dialog('option', 'height', 300);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("usr_psw_form.php?usr_id="+usr_id);
			
			$("#dialog").dialog('option', 'buttons', {
				"회원비밀번호 수정" : function() { 
					$.sms.usr_modify_psw_action(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usr_modify_psw_action : function(usr_id) {
			var form_var = $("#userPswForm").serialize();

			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						$.sms.usr_one(usr_id);
						$("#dialog").dialog("close"); 
					}else{
						$.sms.message(data.message);
					}

					
				}
			});
		},
		
		usr_modify_img : function(usr_id) {

			var str = "";
			if($('#group_id').val() != ''){
				var group_id = $('#group_id').val();
				var str = "group_id="+group_id;
			}else if($('#sameage_id').val() != ''){
				var sameage_id = $('#sameage_id').val();
				var str = "sameage_id="+sameage_id;
			}

			$("#dialog").dialog('option', 'title', '회원 이미지 수정' );
			$("#dialog").dialog('option', 'height', 250);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("usr_img_form.php?mode=usr_modify&usr_id="+usr_id);
			
			$("#dialog").dialog('option', 'buttons', {
				"확인" : function() { 
					$.sms.usr_one(usr_id);
					$("#dialog").dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},
		
		usr_modify_action : function(usr_id) {
			var form_var = $("#userForm").serialize();
			var str = "";
			if($('#group_id').val() != ''){
				var group_id = $('#group_id').val();
				var str = "group_id="+group_id;
			}else if($('#sameage_id').val() != ''){
				var sameage_id = $('#sameage_id').val();
				var str = "sameage_id="+sameage_id;
			}
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						$.sms.usr_one(usr_id);
						$("#dialog").dialog("close"); 
					}else{
						$.sms.message(data.message);
					}
				}
			});
		},

		usr_family_modify : function(usr_id) {
			$("#dialog").dialog('option', 'title', '회원가족관계 수정' );
			$("#dialog").dialog('option', 'height', 400);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("usr_family_form.php?usr_id="+usr_id);
			
			$("#dialog").dialog('option', 'buttons', {
				"확인" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		usr_family_list : function(json) {
			$.ajax({
				type : "GET",
				url : "_usrFamilyList.php",
				data : json,
				dataType : "html",
				success: function(data){
					$("#"+json.box).html(data);
				}
			});
		},

		usr_family_delete_action : function(usr_id,family_id) {
			var _this = this;
			if(!confirm("정말로 회원 가족관계를 삭제하시겠습니까?")){
				return false;
			}else{
				$.ajax({
					type : "GET",
					url : "../sms/action.php",
					data : {
						"mode": "usr_family_delete",
						"usr_id": usr_id,
						"family_id": family_id							
					},
					dataType : "json",
					success: function(data){
						_this.usr_family_list({"usr_id":usr_id, "box": "usrFamilyList"});

					}
				});
			}
			return true;
		},

		usr_family_insert : function(usr_id) {
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', 350);
			$("#sub_dialog").load("../sms/usr_family_search.php?usr_id="+usr_id);
			$("#sub_dialog").dialog('option', 'buttons', {});
			$('#sub_dialog').dialog('open');
		},

		usr_family_insert_action : function(usr_id) {		
			var _this = this;
            var family_id = $("#usrFamilySearchList input.radio:checked").val();
		    var relations = $('#relations').val();
			if(relations == 0){
				alert("관계를 선택하세요!");
				return false;
			} else if(family_id == 0 || !family_id){
				alert("가족을 선택하세요!");
				return false;
			}

			$.ajax({
				type : "GET",
				url : "../sms/action.php",
				data : {
					"mode"      : "usr_family_insert",
					"usr_id"    : usr_id,
					"family_id" : family_id,
					"relations" : relations
				},
				dataType : "json",
				success: function(data){
                    if(data.error > 0){
						alert(data.message);
					}else{
						_this.usr_family_list({"usr_id":usr_id, "box": "usrFamilyList"});
						$('#sub_dialog').dialog('close');
					}
				}
			});
			
		},

		usr_family_search : function(usr_id) {
			var _this = this;
			var q = $('#family_q').val().trim();
			if(q != ""){
				var keyword = encodeURIComponent(q);
				_this.usr_family_list({"usr_id":usr_id ,"q":keyword, "box":"usrFamilySearchList"});
			}
		},

        menuOn : function(usr_id) {
			$("#usr_contact_"+usr_id+" .action").show();
		},

        menuOff : function(usr_id) {
			$("#usr_contact_"+usr_id+" .action").hide();
		},

        usr_one : function(usr_id) {
			$("#usr_contact_"+usr_id).load("../sms/_usrOne.php?usr_id="+usr_id);
		},

		usr_delete_action : function(usr_id) {
			if(!confirm("정말로 회원을 삭제하시겠습니까?")){
				return false;
			}else{
				jQuery.get('../sms/action.php',{"mode": "usr_delete", "usr_id": usr_id },function() {
					//$.sms.usr_list();
					$("#usr_contact_"+usr_id).remove();
				});
			}
			return true;
		},

		send : function(phone) {
			var str = "";
			if(phone == ''){
				$("#sms_list input:checked").each(function (i){
					if(i == 0){
						str += $(this).val();
					}else{
						str += "," + $(this).val();
					}
				});	
			}else{
				str = phone;
			}

			$("#dialog").dialog('option', 'title', '문자(SMS) 보내기' );
			$("#dialog").dialog('option', 'height', 350);
			$("#dialog").load("../sms/sms_form.php?phone="+str);
			
			$("#dialog").dialog('option', 'buttons', {
				"전송하기" : function() { 
					$.sms.send_action(); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},
		
		send_action : function() {
			var form_var = $('#smsForm').serialize();
			
			if($.trim($('#sms_content').val()) == ""){
				$.sms.message("문자 메시지를 입력하세요!");
				return false;
			}
			
			$.ajax({
				type: "POST",
				url: "../sms/sms_process.php",
				data: form_var,
				dataType: 'json',
				beforeSend : function(){
					$('#sms_loading').fadeIn("slow");		
				}
				,
				success: function(json){
					if(json.error < 0){
						$('#sms_loading').fadeOut('slow');
						$.sms.message(json.message);
						window.setTimeout(function(){
							$("#dialog").dialog("close"); 
						},6000);
					}else{
						$('#sms_loading').fadeOut('slow');
						$.sms.message(json.message);
					}				
				}
			});
		},

		select_all : function() {
			if(this.isAllCheck){
				$("input:checkbox").attr("checked",false);
				this.isAllCheck = false;
			}else{
				$("input:checkbox").attr("checked",true);
				this.isAllCheck = true;
			}
		},

		usr_group_list : function(usr_id)  {
			$('#usr_group_list').load('../sms/usr_group_list.php?usr_id='+usr_id);
		},
		
		group_insert_action : function(usr_id) {
			var group_sel_id = $('#group_sel').val();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: {'mode':'usr_group_insert','group_id':group_sel_id,'usr_id':usr_id},
				dataType: 'json',
				success: function(data){
					if(data.error == 0){
						$.sms.usr_group_list(usr_id);
					}else{
						$.sms.message(data.message);
					}

					
				}
			});
		},

		group_delete_action : function(id) {
			$.ajax({
				type: "GET",
				url:  "action.php",
				data: {'mode':'usr_group_delete','usr_group_id':id},
				dataType: 'json',
				success: function(data){
					if(data.error == 0){
						$.sms.usr_group_list(data.usr_id);
					}else{
						$.sms.message(data.message);
					}

					
				}
			});
		},

		message : function (msg){
			$('#usr_msg').fadeIn("slow");
			$('#usr_msg .error_msg').html(msg);
			window.setTimeout("$('#usr_msg').fadeOut('slow');",5000);
		},

		usrImgUpload : function (usr_id){
			$("#loading")
			.ajaxStart(function(){
				$(this).show();
			})
			.ajaxComplete(function(){
				$(this).hide();
			});
			$.ajaxFileUpload({
				url:"action.php",
				secureuri:false,
				fileElementId:'usr_img',
				dataType: 'json',
				data:{
					"mode"   : "usr_img_upload",
					"usr_id" : usr_id
				},
				success: function (data, status, e){
					if(data.error > 0){
						$.sms.message(data.error);
					}else{
						$("#usr_img_thumb1234").load("usr_img.php?usr_id="+usr_id);
					}
				},
				error: function (data, status, e){
					alert(e);
				}
			});
		},
		
	    tab_sameage : function(){
			$("#groups_tab_1").removeClass("sel");
			$("#groups_tab_2").addClass("sel");
			$('#groups').hide();
			$('#sameage').show();
		},

		tab_groups : function(){
			$("#groups_tab_2").removeClass("sel");
			$("#groups_tab_1").addClass("sel");
			$('#groups').show();
			$('#sameage').hide();
		},
		
		msgStringLength : function(cnt) { 
		   //변수의 초기화
		   var obj = $("#sms_content");
		   var now_str = obj.val();                     //이벤트가 발생한 컨트롤의 value값 
		   var now_len = obj.val().length;              //현재 value값의 글자 수 
		   var dest = $("#strLimit");                   //입력된 글자수를 넣어줄 id
		   
		   var max_len = cnt;                           //제한할 최대 글자 수 
		   var i = 0;                                   //for문에서 사용할 변수 
		   var cnt_byte = 0;                            //한글일 경우 2 그외에는 1바이트 수 저장 
		   var sub_cnt = 0;                             //substring 할때 사용할 제한 길이를 저장 
		   var chk_letter = "";                         //현재 한/영 체크할 letter를 저장 
		   var lmt_str = "";                            //제한된 글자 수만큼만 저장 
			
		   for (i=0; i<now_len; i++) { 
			   //1글자만 추출 
			   chk_letter = now_str.charAt(i); 

			   // 체크문자가 한글일 경우 2byte 그 외의 경우 1byte 증가 
			   if (escape(chk_letter).length > 4) { 
				   //한글인 경우 2byte (UTF-8인 경우 3byte로...)
				   cnt_byte += 2; 
			   }else{ 
				   //그외의 경우 1byte 증가 
				   cnt_byte++; 
			   } 
				
			   //만약 전체 크기가 제한 글자 수를 넘지 않으면 
			   if (cnt_byte <= max_len) { 
				   // 제한할 문자까지의 count값을 sub_cnt에 누적 
				   sub_cnt = i + 1; 
			   } 
		   } 
				
		   // 만약 전체 크기가 제한 글자 수를 넘으면     
		   if (cnt_byte > max_len) { 
			   alert("최대" + max_len + "글자 이상 쓸수 없습니다!"); 
			   lmt_str = now_str.substring(0, sub_cnt); 
			   obj.val(lmt_str); 
			   dest.html(max_len);
		   } else {
			   dest.html(cnt_byte);
		   }
		   obj.focus();
		},
		
		_var : "0.9.1"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

(function($)
{
	$.extend(
	{
		common : 
		{
		//-----------------------------------------------------------------------------------------


			initialLocation:null,
			isInit : false,
			map:null,

			myOptions : {
			   zoom: 14, 
			   mapTypeId: google.maps.MapTypeId.ROADMAP
			},


            resize : function(){                
				var wh = $(window).height();
				var dh = $(document).height();
				var mh = $(".main_box").height();

				if(dh < wh){ var hh = dh; }else{ var hh = wh; }	
				if(mh > (hh-170)){
					$(".contents").height(mh);
				}else{
					$(".contents").height(hh-170);
				}
			},

			usr_info : function (usr_id){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../user/_usrInfo.php",
					data: {"id":usr_id},
					dataType: 'html',
					success: function(data){
						$("#usr_dialog").html(data);				
					},
					complete : function(){
						_this.usrFamilyList(usr_id);
					}
				});
				$("#usr_dialog").dialog('option', 'width', 720);
				$("#usr_dialog").dialog('option', 'height', 'auto');
				$("#usr_dialog").dialog('option', 'buttons', {
					"확인" : function() { 
						$(this).dialog("close"); 
					}
				});
				$("#usr_dialog").dialog('open');
			},

			reflashUsrInfo : function (){
				var _this = this;
				$.ajax({
					type: "GET",
					url: "../_lib/_usrInfo.php",
					dataType: 'html',
					success: function(data){
						$("#usrInfoBoxArea").html(data);				
					},
					complete : function(){
						
					}
				});
			},

			getAddressResultMap : function (){
				var _this = this;
				var geo = [];
				var geocoder = new google.maps.Geocoder();
				var address =  $("#addressResultLabel").html(); 
				if (geocoder) {
					geocoder.geocode({ 'address': address }, function (results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							geo[0] = results[0].geometry.location.lat().toFixed(3);
							geo[1] = results[0].geometry.location.lng().toFixed(3);
							//alert(geo[0]+"------"+geo[1]);
							var map2 = new google.maps.Map(document.getElementById("addressResultMap"), _this.myOptions);
							var initialLocation = new google.maps.LatLng(geo[0], geo[1]);
							var placeMarker = new google.maps.Marker({ position: initialLocation, map: map2 });
							map2.setCenter(initialLocation);
						} else {
							//console.log("Geocoding failed: " + status);
						}
					});
				}    
			},


		/* 회원그룹설정 */

			usrFamilyList : function (usr_id){
				$.ajax({
					type: "GET",
					url: "../user/_usrFamilyList.php",
					data: {"usr_id":usr_id},
					dataType: 'html',
					success: function(data){
						$("#usrDetailInfo").html(data);	
					},
					complete : function(){ 
					}
				});
			},


			usrGroupList : function(usr_id,mode)  {
				var _this = this;


				$.ajax({
					type: "GET",
					url: "../user/_usrGroupList.php",
					data: {"usr_id":usr_id,"mode":mode},
					dataType: 'html',
					success: function(data){
						if(mode == "callback"){
							$("#usrGroupCallback").html(data);
						}else{
							$("#usrDetailInfo").html(data);
						}
					},
					complete : function(){ 
					}
				});
			},

			usrGroupModify : function(id) {
				var _this = this;
				$("#sub_dialog").dialog('option', 'title', "소속정보 수정");
				$("#sub_dialog").dialog('option', 'width', 400);
				$("#sub_dialog").dialog('option', 'height', "auto");
				$("#sub_dialog").dialog('option', 'buttons', {
					"저장" : function() { 
						_this.usrGroupModifyAction();
					},
					"닫기" : function() { 
						$(this).dialog("close"); 
					}
				});

				$.ajax({
					type: "GET",
					url: "../user/_usrGroupForm.php",
					data: {'mode':'modify','id':id},
					dataType: 'html',
					success: function(data){
						$("#sub_dialog").html(data);
					}
				});
				$('#sub_dialog').dialog('open');
			},

			usrGroupModifyAction : function() {
				var _this = this;
				var form_var = $("#usrGroupForm").serialize();
				$.ajax({
					type: "POST",
					url: "../contact/action.php",
					data: form_var,
					dataType: 'json',
					success: function(data){
						if(data.error < 0){
							_this.usrGroupList(data.usr_id,"callback");
							$('#sub_dialog').dialog('close');
						}else{
							alert(data.message);
						}
					}
				});
			},
		

			usrGroupInsertAction : function(usr_id) {
				var _this = this;
				var group_sel_id = $('#group_sel').val();
				$.ajax({
					type: "POST",
					url: "../contact/action.php",
					data: {'mode':'usr_group_insert','group_id':group_sel_id,'usr_id':usr_id},
					dataType: 'json',
					success: function(data){
						if(data.error < 0){
							_this.usrGroupList(usr_id,"callback");
						}else{
							alert(data.message);
						}
					}
				});
			},
		
			usrGroupDeleteAction : function(id) {
				var _this = this;
				if(!confirm("정말로 삭제하시겠습니까?")){
				}else{
					$.ajax({
						type: "GET",
						url:  "../contact/action.php",
						data: {'mode':'usr_group_delete','usr_group_id':id},
						dataType: 'json',
						success: function(data){
							if(data.error < 0){
								_this.usrGroupList(data.usr_id,"callback");
							}else{
								alert(data.message);
							}
						}
					});
				}
			},

			error : function(msg){
                alert(msg);
			},

			msg : function(msg){
				$('#help .help_msg').html(msg);
				$('#help').fadeIn("slow", function(){
					window.setTimeout(function(){
						$('#help').fadeOut('slow');
					},5000);
				});
			},
			
			_ver : "1.1.9"
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);
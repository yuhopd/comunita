(function($)
{
    $.extend(
        {
            signin :
                {
                    //-----------------------------------------------------------------------------------------
                    usr_id : 0,

                    login : function (){
                        var uid  = $('#uid').val();
                        var psw = $('#psw').val();
                        var saveID, auto;
                        if($('#saveID').val() == "Y"){ saveID = "Y"; }else{ saveID = "N"; }
                        if($('#autoLogin').val() == "Y"){ auto = "Y"; }else{ auto = "N"; }

                        $.ajax({
                            type: "POST",
                            url: "../account/action.php",
                            data: {"mode":"signin","uid":uid,"psw":psw},
                            dataType: 'json',
                            success: function(json){
                                if(json.error > 0){
                                    alert(json.message);
                                }else{
                                    //if(saveID == "Y"){
                                    storage.set('uid', uid);
                                    storage.set('auth', json.auth);
                                    storage.set('saveId', saveID);
                                    storage.set('autoLogin', auto);
									/*
									 }else{
									 storage.remove('uid');
									 storage.remove('auth');
									 storage.remove('saveId');
									 storage.remove('autoLogin');
									 }
									 */
                                    location.replace("../main/index.php");
                                }

                            }
                        });
                    },



                    domain_insert : function(return_url) {
                        if(!return_url || return_url==''){
                            return_url = "../main/index.php";
                        }

                        var form_var = $('#domain-insert').serialize();
                        $.ajax({
                            type: "POST",
                            url: "../account/action.php",
                            data: form_var,
                            dataType: 'json',
                            success: function(json){
                                if(json.error > 0){
                                    alert(json.message);
                                }else{
                                    //location.href = return_url;
                                }
                            }
                        });
                    },


                    autologin : function(uid,psw){
                        var	return_url = "../calendar/index.php";
                        $.ajax({
                            type: "POST",
                            url: "../signin/action.php",
                            dataType: 'json',
                            data: {
                                "mode" : "signin",
                                "uid" : uid,
                                "psw" : psw
                            },
                            success: function(json){
                                if(json.error > 0){
                                    alert(json.message);
                                }else{
                                    $.cookie('AutoSignIn', 'Y', { expires: 30});
                                    $.cookie('savedID', uid, { expires: 30});
                                    $.cookie('savedPSW', psw, { expires: 30});

                                }
                                return false;
                            }
                        });
                    },

                    signup_action : function(return_url) {
                        if(!return_url || return_url==''){
                            return_url = "http://himh.net/calendar/index.php";
                        }

                        var form_var = $('#signUpForm').serialize();
                        $.ajax({
                            type: "POST",
                            url: "../signin/action.php",
                            data: form_var,
                            dataType: 'json',
                            success: function(json){
                                if(json.error > 0){
                                    alert(json.message);
                                }else{
                                    alert(json.message);
                                    location.href = return_url;
                                }
                            }
                        });
                    },

                    signup_check_action : function () {
                        var sameage = $('#sameage').val();
                        var name = $('#name').val();

                        $.ajax({
                            type: "POST",
                            url: "../signin/action.php",
                            data: {'sameage_id':sameage,'name':name,'mode':'signup_check'},
                            dataType: 'json',
                            success: function(json){
                                if(json.error > 0){
                                    $.common.error(json.message);
                                }else{
                                    $('#signin_form').load('signup_2.php?usr_id='+json.usr_id);
                                }
                            }
                        });
                    },

                    openid_form : function (){
                        $('#signin_form').load('../signin/openid_signin.php');
                        return false;
                    }
                    ,
                    signup_form : function (){
                        $('#signin_form').load('../signin/signup.php');
                        return false;
                    }
                    ,
                    signin_form : function (){
                        $('#signin_form').load('../signin/signin.php');
                        return false;
                    }
                    ,
                    sign_up_form_key : function (key){
                        $('#signin_form').load('../signin/signup.php?key='+key);
                        return false;
                    },


                    isAccount : function() {
                        var _this = this;
                        var phone = $("#phone").val();

                        $.ajax({
                            type: "POST",
                            url: "../account/action.php",
                            data: {"mode":"isAccount","phone":phone},
                            dataType: 'json',
                            success: function(json){
                                if(json.error > 0){
                                    alert(json.message);
                                }else{
                                    //$("#phoneAuthBtn").hide();
                                    _this.usr_id = json.usr_id
                                    $("#phoneAuth").show();
                                }
                            }
                        });
                    },


                    phoneAuth : function() {
                        var _this = this;
                        var phone = $.trim($("#phone").val());
                        if(phone== ""){
                            alert("휴대전화을 입력하세요");
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../account/action.php",
                                data: {"mode":"phoneAuth","phone":phone},
                                dataType: 'json',
                                success: function(json){
                                    if(json.error > 0){
                                        alert(json.message);
                                    }else{
                                        alert(json.message);
                                        $("#phoneAuth").hide();
                                        $("#phoneAuthKey").show();
                                    }
                                }
                            });
                        }
                    },

                    phoneAuthAction : function() {
                        var _this = this;
                        var key   = $.trim($("#phoneAuthKey input[name=key]").val());
                        if(key == ""){
                            alert("인증번호를 입력하세요");
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../account/action.php",
                                data: {"mode":"phoneAuthAction","usr_id":_this.usr_id,"key":key},
                                dataType: 'json',
                                success: function(json){
                                    if(json.error > 0){
                                        alert(json.message);
                                        $("#phoneAuthKey").val("");
                                    }else{
                                        alert(json.message);
                                        $("#phoneAuthUsrPsw").show();
                                    }
                                }
                            });
                        }
                    },

                    modifyPswAction : function() {
                        var _this = this;
                        var psw   = $.trim($("#phoneAuthUsrPsw input[name=psw]").val());
                        var psw_check   = $.trim($("#phoneAuthUsrPsw input[name=psw_check]").val());
                        if(psw == ""){
                            alert("비밀번호를 입력하세요");
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../account/action.php",
                                data: {"mode":"modifyPsw","usr_id":_this.usr_id,"psw":psw,"psw_check":psw_check},
                                dataType: 'json',
                                success: function(json){
                                    if(json.error > 0){
                                        alert(json.message);
                                    }else{
                                        alert(json.message);
                                        location.replace("http://himh.net");
                                    }
                                }
                            });
                        }
                    }

                    //-----------------------------------------------------------------------------------------
                }
        });
})(jQuery);

define([
	'jquery'
	, 'Common'
	, 'nestable'
	, 'multiselects'
  ], function ($, Common) {
	  var AdminGroup = function () {
		/* GROUP manager ---------------------------------------------------------------------- */
		this.group_id = 0;

		this.list = function(group_id){
			var _this = this;
			if(group_id  != undefined){ _this.group_id = group_id; }
            _this.listWarp();
		},

		this.listWarp = function(){
			var _this = this;

			$.ajax({
				type: "GET",
				url: "_settingGroupList.php",
				data: {
					
				},
				dataType: 'html',
				success: function(data){
					$('#mainBox').html(data);
				},
				complete : function(){    
					$('#adminGroupNestable').nestable({
						isDragable: false,
						group: 1
					});
					
					$("#adminGroupNestable a").on("click",function(e){
						var group = $(this).attr("group");
						var action = $(this).attr("action");

						if(action == "modify"){
							_this.edit(group);
						}else if(action == "userSettings"){
							_this.usrEdit(group);
						}else if(action == "childAdd"){
							_this.insert(group);
						}else if(action == "del"){
							_this.deleteAction(group);
						}else if(action == "excelUpload"){

						}
                        return false;
					});
				}
			});


		},

		this.insert = function(parent_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹 추가' );
			$("#dialog").load("_settingGroupForm.php?mode=insert&parent_id="+parent_id);
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"그룹추가" : function() { 
					_this.insertAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		},

		this.insertAction = function() {
			var _this = this;
			var form_var = $('#groupForm').serialize();
			var group_title = $.trim($('#group_title').val());
			if(group_title == '') {
				alert('그룹명을 입력하세요!');
			}else{
				$.ajax({
					type: "POST",
					url: "_settingGroupAction.php",
					data: form_var,

					success: function(data){
						$("#dialog").dialog("close"); 
						_this.listWarp();
					}
				});
			}
		},

		this.edit = function(group_id, isOpen) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹수정' );
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);

			$("#dialog").load("_settingGroupForm.php?mode=modify&group_id="+group_id);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.editAction(group_id);
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		this.editAction = function(group_id) {
			var _this = this;
			var form_var = $('#groupForm').serialize();
			var group_title = $.trim($('#group_title').val());
			if(group_title == '') {
				alert('그룹명을 입력하세요!');
			} else {
				$.ajax({
					type: "POST",
					url: "_settingGroupAction.php",
					data: form_var+"&group_id="+group_id,
					success: function(){
						$("#dialog").dialog("close"); 
						_this.listWarp();
					}
				});
			}
		},
        
		/* Group Usr Manager */
		this.usrEdit = function(group_id, isOpen) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '그룹회원 관리' );
			
			$.ajax({
				type: "GET",
				url: "_settingGroupUsrForm.php",
				data: {"group_id":group_id},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);
				},
				complete : function(){

					$("#dialog a.btn").on("click",function(){
						var action = $(this).attr("action");
						if(action == "rootGroupUsrList"){
							_this.rootGroupUsrList();

						}



					});


					$("#dialog a.list-group-item").on("click",function(){
						var action = $(this).attr("action");
						if(action == "groupEdit"){
							_this.groupEdit();
						}else if(action == "groupUsrEdit"){

						}else if(action == "groupAttendanceList"){
							_this.groupAttendanceList();

						}else if(action == "groupAttendanceInsert"){
							_this.groupAttendanceInsert();
						}

					});
					

					$("#unSelected").multiSelect("#selected_level_0", {trigger: "#options_right_0"});
					$("#selected_level_0").multiSelect("#unSelected", {trigger: "#options_left_0"});
			
					$("#unSelected").multiSelect("#selected_level_1", {trigger: "#options_right_1"});
					$("#selected_level_1").multiSelect("#unSelected", {trigger: "#options_left_1"});
			
					$("#unSelected").multiSelect("#selected_level_2", {trigger: "#options_right_2"});
					$("#selected_level_2").multiSelect("#unSelected", {trigger: "#options_left_2"});

				}
			});


			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrEditAction(group_id);
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}

		},

		this.usrEditAction = function(group_id) {
			var multipleValues0 = [];
			$('#selected_level_0 option').each(function(i) { 
				multipleValues0[i] = $(this).attr("value");
			});
			var users0 = multipleValues0.join("|");

			var multipleValues1 = [];
			$('#selected_level_1 option').each(function(i) { 
				multipleValues1[i] = $(this).attr("value");
			});
			var users1 = multipleValues1.join("|");

			var multipleValues2 = [];
			$('#selected_level_2 option').each(function(i) { 
				multipleValues2[i] = $(this).attr("value");
			});
			var users2 = multipleValues2.join("|");

			$.ajax({
				type: "POST",
				url: "_settingGroupAction.php",
				data: {
					"mode"  : "usr_edit", 
					"group_id" : group_id, 
					"usrs0" : users0,
					"usrs1" : users1,
					"usrs2" : users2
				},
				success: function(){
					$("#dialog").dialog("close"); 
					//$.group.list();
				}
			});
		},

        /* Attendance Manager */
        this.attendanceList = function(group_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 리스트' );
			$("#dialog").load("_settingGroupAttendanceList.php?group_id="+group_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});

			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		this.attendanceInsert = function(group_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 추가' );
			$("#dialog").load("_settingGroupAttendanceForm.php?mode=insert&group_id="+group_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"출석부 추가" : function() { 
					$.group.attendanceAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},


		this.attendanceModify = function(attendance_id,isOpen){
			$("#dialog").dialog('option', 'title', '출석부 수정' );
			$("#dialog").load("_settingGroupAttendanceForm.php?mode=modify&attendance_id="+attendance_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"출석부 수정" : function() { 
					$.group.attendanceAction();
				} ,  
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});
			if(isOpen == undefined){
				$('#dialog').dialog('open');
			}
		},

		this.attendanceAction = function(){
			var attendance_title = $('#attendance_title').val();
            if(attendance_title == ""){
				alert("출석부 이름을 입력하세요");
				return false;
			}

			var form_var = $('#groupAttendanceForm').serialize();
			var group_id = $('#group_id').val();
			$.ajax({
				type: "POST",
				url: "../mypage/_groupAction.php",
				data: form_var,
				success: function(){
					$.group.attendanceList(group_id,'O');
				}
			});
		},

		this.attendanceDelete = function(attendance_id,group_id) {
			if(!confirm("정말로 출석부를 삭제하시겠습니까?")){
				
			}else{
				jQuery.get('_settingGroupAction.php',{"mode": "deleteAttendance", "id": attendance_id },function() {
					$.group.attendanceList(group_id,'O');
				});	
			}

		},
		
		this.excelStep1 = function(group_id) {
			$("#content_wrap").load("../mypage/_excelUpload_1.php?group_id="+group_id);
		},

		this.excelStep2 = function(group_id,filename) {
			$("#content_wrap").load("../mypage/_excelUpload_2.php?group_id="+group_id+"&filename="+filename);
		},

        this.excelStep2Action = function(group_id) {
			var form_var = $('#excelForm').serialize();
			$.ajax({
				type: "POST",
				url: "../mypage/_groupAction.php",
				data: form_var,
				dataType: 'json',
				success: function(json){
					alert(json.massage);
					location.replace("../mypage/index.php");		
				}
			});

		},

		this.deleteAction = function(group_id) {
			var _this = this;
			if(!confirm("정말로 그룹을 삭제하시겠습니까?")){
				return false;
			}else{
				jQuery.get('_settingGroupAction.php',{"mode": "delete", "group_id": group_id },function() {
					_this.list();
				});
			}
			return true;
		},
		
		this.rootGroupUsrList = function() {
			var root_id = $("#roots").val();
			$.ajax({
				type: "GET",
				url: "_settingGroupUsrSelectList.php",
				data: {"root_id":root_id},
				dataType: 'html',
				success: function(data){
					$("#unSelected").html(data);
				}
			});
		},

		this.upSequence = function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_settingGroupAction.php",
				data: {"mode":"upSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		};
		
		this.downSequence = function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_settingGroupAction.php",
				data: {"mode":"downSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		};
	    //-----------------------------------------------------------------------------------------
	};
	return AdminGroup;
});

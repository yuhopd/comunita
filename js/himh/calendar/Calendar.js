define(['jquery'
    , 'Common'
	, 'fullcalendar'
	, 'locale'

  ], function ($, Common) {
	  var Calendar = function () {
		//-----------------------------------------------------------------------------------------
			this.group_id = 0;
			this.groups = "";
			this.fullCalObj = $('#full_calendar');

			this.common = null;
			
			this.list = function(eventCallback){
				var _this = this;
				var str = "";

				_this.common = new Common();

				$('#full_calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					locale: "ko",
					height: "auto",
					editable: true,
            		droppable: true,
					events: function(start, end, timezone, callback) {
						$.ajax({
							type :  "POST",
							url : '_calendarBrith.php',
							dataType : 'json',
							data : {
								// our hypothetical feed requires UNIX timestamps
								group_id: _this.group_id,
								start: start.unix(),
								end: end.unix()
							},
							success: function(doc) {
								//console.log(doc);



								callback(doc);
							}
						});
					},

					eventClick: function(event) {

		
						if(event.dayType == "birth"){
							_this.common.usr_info(event.id);
						}

						/*
						if(event.dayType == "birth"){
							$.common.usr_info(event.id);
						}else{
							_this.view(event);
						}
						*/
						return false;
					}


					//monthNamess : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
					/*
					titleFormat:{
					    month: 'yyyy년 MM월',                            
						week: "MM월 dd일{ '&#8212;'[ MM월] dd일}",
						day: 'yyyy년 MM월 dd일',                 
					},
					
					eventSources: [
							//"https://www.google.com/calendar/feeds/yuhopd%40gmail.com/public/basic",
							//"https://www.google.com/calendar/feeds/mljab4t18hug45mk7abvbte88g%40group.calendar.google.com/public/basic",
							function(start, end, callback) {
								$.ajax({
									type:"POST",
									url: "_calendarMonth.php",
									dataType: 'json',
									data: {
										group_id: _this.group_id,
										start: Math.round(start.getTime() / 1000),
										end: Math.round(end.getTime() / 1000)
									},
									success: function(doc) {
										callback(doc);
									}
								});
							}
						
					],
					

                    
					events: function(start, end, callback) {
						$.ajax({
							type:"POST",
							url: "_calendarMonth.php",
							dataType: 'json',
							data: {
								group_id: _this.group_id,
								start: Math.round(start.getTime() / 1000),
								end: Math.round(end.getTime() / 1000)
							},
							success: function(doc) {
								callback(doc);
							}
						});
					},
					
					
					eventClick: function(event) {
						if(event.dayType == "birth"){
							$.common.usr_info(event.id);
						}else{
							_this.view(event);
						}
						return false;
					},
					dayClick: function(date) {
						_this.insertForm($(this),Math.round(date.getTime() / 1000));
						//$(this).css('background-color', '#ffffcc');
					}*/
				});
			};

            this.setGroupId = function(group_id) {
				var _this = this;
				_this.group_id = group_id;
			};

            this.listFromGroup = function(group_id){
				var _this = this;
				_this.setGroupId(group_id);
				$('#full_calendar').fullCalendar('refetchEvents');
			};

            this.listFromCheck = function(){
				var _this = this;

                var str = "";
				//$("#calendarNav input:checked").each(function(i){ if(i==0){ str += $(this).val(); } else { str += "|" + $(this).val(); }});
 
				//alert(str);
				_this.groups = str;
				$('#full_calendar').fullCalendar('refetchEvents');
			};

			this.insertForm = function (wep,date) {
				var _this = this;
				wep.css('background-color', '#ffffcc');
				$("#dialog").dialog('option', 'title', '일정 등록' );
				$.ajax({
					type: "GET",
					url:  "_calendarForm.php",
					data: {"mode":"insert", "group_id":_this.group_id, "date":date},
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					}
				});

				$("#dialog").dialog('option', 'width', 550);
				$("#dialog").dialog('option', 'close', function() { wep.css('background-color', '#fff'); });
				$("#dialog").dialog('option', 'buttons', {
					"저장" : function() { 
						_this.insertAction();
					} ,  
					"취소" : function() { 
						$(this).dialog("close"); 
					}
				});
				$('#dialog').dialog('open');
			};

			this.insertAction = function () {
				var form_var = $('#calendarForm').serialize();
				if($.trim($("#calendarForm input[name=title]").val()) == "") {
					alert("제목을 입력하세요!");
				} else {
					$.ajax({
						type: "POST",
						url:  "_calendarAction.php",
						data: form_var,
						dataType: 'json',
						success: function(data){
							$('#full_calendar').fullCalendar('addEventSource', {events:[{
								'dayType':'event',
								'id':data.id,
								'title':data.title,
								'start':data.startDate,
								'end':data.endDate,
								'color':data.color,
								'allDayDefault':true
							}]});
							
							$("#dialog").dialog("close"); 
						}
					});
				}
			};

			this.modifyForm = function (event) {
				var _this = this;
				$("#dialog").dialog('option', 'title', '일정수정' );
				$.ajax({
					type: "GET",
					url:  "_calendarForm.php",
					data: {"mode":"modify", "id":event.id},
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					}
				});
				$("#dialog").dialog('option', 'width', 550);
				$("#dialog").dialog('option', 'close', function() {});
				$("#dialog").dialog('option', 'buttons', {
					"저장" : function() { 
						_this.modifyAction(event);
					} ,  
					"취소" : function() { 
						$(this).dialog("close"); 
					}
				});
			};

			this.modifyAction = function (event) {
				var form_var = $('#calendarForm').serialize();
				if($.trim($("#calendarForm input[name=title]").val()) == "") {
					alert("제목을 입력하세요!");
				} else {
					$.ajax({
						type: "POST",
						url:  "_calendarAction.php",
						data: form_var,
						dataType: 'json',
						success: function(data){
							event.title = data.title;
							event.color = data.color;
							$('#full_calendar').fullCalendar('updateEvent',event);
							$("#dialog").dialog("close"); 
						}
					});
				}
			};
			
			this.view = function (event) {
				var _this = this;
				$("#dialog").dialog('option', 'title', '일정 확인' );
				$.ajax({
					type: "GET",
					url:  "_calendarView.php",
					data: {"id":event.id},
					dataType: 'html',
					success: function(data){
						$("#dialog").html(data);
					}
				});

				$("#dialog").dialog('option', 'width', 450);
				$("#dialog").dialog('option', 'height', "auto");
				$("#dialog").dialog('option', 'close', function() {  });
				$("#dialog").dialog('option', 'buttons', {
					"수정" : function() { 
						_this.modifyForm(event);
					} ,  
					"삭제" : function() { 
						_this.deleteAction(event.id);
					} ,
					"닫기" : function() { 
						$(this).dialog("close"); 
					}

				});
				$('#dialog').dialog('open');

			};

			this.deleteAction = function (id) { 
				var _this = this;
				if(!confirm('정말로 삭제할까요?')){
				}else{
					$.ajax({
						type: "GET",
						url:  "_calendarAction.php",
						data: {mode:"delete", id:id},
						dataType: 'json',
						success: function(data){
							$('#full_calendar').fullCalendar('removeEvents',data.id);
							$("#dialog").dialog("close"); 
						}
					});
		
				}
			};

        //-----------------------------------------------------------------------------------------
	
	};
	return Calendar;
});

require.config({
    baseUrl : '../js/himh',
    paths: {
        'jquery'     : 'lib/jquery-3.1.1.min',
        'bootstrap'  : 'lib/bootstrap',
        'jquery-ui'  : 'lib/jquery-ui',

        'metisMenu'  : 'plugins/metisMenu/jquery.metisMenu',
        'slimscroll' : 'plugins/slimscroll/jquery.slimscroll.min',
        'nestable'   : 'plugins/nestable/jquery.nestable',
        'jsTree'     : 'plugins/jsTree/jsTree',
        'icheck'     : 'plugins/iCheck/icheck.min',

        'fineuploader' : 'lib/fineuploader/jquery.fineuploader-3.0',

        'Loader' : 'Loader',
        'Attendance' : 'attendance/Attendance',
        'Reservation' : 'admin/Reservation',
        'Contacts' : 'contact/Contacts',
        'Common' : 'contact/Common',
        'Family' : 'contact/Family',
        'Visit' : 'contact/Visit',
        'Sms' : 'contact/Sms',
        'Upload' : 'contact/Upload',
        'Search' : 'contact/Search',
        'Croppie' : 'lib/croppie',
        'dropzone' : 'lib/dropzone',
        'Calendar' : 'calendar/Calendar',
        'AdminGroup' : 'admin/AdminGroup',

        'fullcalendar' : 'lib/fullcalendar/fullcalendar',
        'gcal' : 'lib/fullcalendar/gcal',
        'moment' : 'lib/fullcalendar/lib/moment.min',
        'locale' : 'lib/fullcalendar/locale-all',
        'multiselects' : 'lib/jquery.multiselects',
        'fancybox' : 'lib/jquery.fancybox',
        

    },

    shim : {
		'jquery-ui' : {
            deps : ['jquery']
        },
		'bootstrap' : {
            deps : ['jquery']
        },
        'metisMenu' : {
            deps : ['jquery']
        },
        'slimscroll' : {
            deps : ['jquery']
        },
        'nestable' : {
            deps : ['jquery']
        },
        'fineuploader' : {
            deps : ['jquery']
        },

        'jsTree' : {
            deps : ['bootstrap']
        },

        'multiselects' :{
            deps : ['jquery']
        }


    }
});

require([
    'jquery'
    , 'Loader'
], function($, Loader){

	
    $(document).ready(function() {
        var loader = new Loader();
		loader.init();
    });



});

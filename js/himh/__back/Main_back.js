require.config({
    baseUrl : '../js/himh',
    paths: {
        'jquery'     : 'lib/jquery-3.1.1.min',
        'bootstrap'  : 'lib/bootstrap',
        'jquery-ui'  : 'lib/jquery-ui',

        'metisMenu'  : 'plugins/metisMenu/jquery.metisMenu',
        'slimscroll' : 'plugins/slimscroll/jquery.slimscroll.min',
        'nestable'   : 'plugins/nestable/jquery.nestable',
        'jsTree'     : 'plugins/jsTree/jsTree',


        'fineuploader' : 'lib/fineuploader/jquery.fineuploader-3.0',
        'Loader' : 'Loader',
        'Contacts' : 'contact/Contacts',
        'Attendance' : 'attendance/Attendance',
        'Common' : 'contact/Common',
        'Family' : 'contact/Family',
        'Visit' : 'contact/Visit',

        'Calendar' : 'calendar/Calendar',
        'fullcalendar' : 'lib/fullcalendar/fullcalendar',
        'gcal' : 'lib/fullcalendar/gcal',
        'moment' : 'lib/fullcalendar/lib/moment.min',
        


        'AdminGroup' : 'admin/AdminGroup'

    },

    shim : {
		'jquery-ui' : {
            deps : ['jquery']
        },
		'bootstrap' : {
            deps : ['jquery']
        },
        'metisMenu' : {
            deps : ['jquery']
        },
        'slimscroll' : {
            deps : ['jquery']
        },
        'nestable' : {
            deps : ['jquery']
        },
        'fineuploader' : {
            deps : ['jquery']
        },

        'jsTree' : {
            deps : ['bootstrap']
        }

    }
});

require([
    'jquery'
    , 'Loader'
], function($, Loader){

	
    $(document).ready(function() {
        var loader = new Loader();
		loader.init();
    });



});

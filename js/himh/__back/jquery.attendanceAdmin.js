(function($)
{
	$.extend(
	{
		attendanceAdmin :
		{
		/* GROUP manager ---------------------------------------------------------------------- */
		page : 1,
		pagination : 30,

		root_id : 0,
		keyword : "",
		year : "2017",
		mode : "",
	    /* Attendance Manager */

		list : function(json){
			var _this = this;
			if(json.root_id != undefined){ _this.root_id = json.root_id; }
			if(json.year    != undefined){ _this.year = json.year; }
			if(json.page    != undefined){ _this.page = json.page; } else { _this.page = 1; }

			_this.keyword = "";
			_this.listWarp();
		},

		listWarp : function(){
			var _this = this;
			$.ajax({
				type: "GET",
				url: "../mypage/_attendanceList.php",
				data: {
					"root_id":_this.root_id,
					"year" : _this.year,
					"q":_this.keyword,
					"page":_this.page
				},
				dataType: "html",
				success: function(data){
					$("#content_wrap").html(data);
				},
				complete : function(){
				}
			});
		},

		insert : function(){
			var _this = this;
			$("#dialog").dialog('option', 'title', '출석부 추가' );
			$("#dialog").load("../mypage/_attendanceForm.php?mode=insert");
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"출석부 추가" : function() {
					_this.insertAction();
				} ,
				"닫기" : function() {
					$(this).dialog("close");
				}
			});
			$('#dialog').dialog('open');
		},

		insertAction : function(){
			var _this = this;
			var group_id = $('#attendanceForm select[name=group_id]').val();
			var attendance_title = $('#attendanceForm input[name=title]').val();
            if(group_id == 0){
				alert("그룹을 선택하세요");
			}else if(attendance_title == ""){
				alert("출석부 이름을 입력하세요");
			}else{
				var form_var = $('#attendanceForm').serialize();
				$.ajax({
					type: "POST",
					url: "../mypage/_groupAction.php",
					data: form_var,
					success: function(){
						_this.listWarp();
						$('#dialog').dialog('close');
					}
				});
			}
		},



		modify : function(attendance_id){
			var _this = this;
			$("#dialog").dialog('option', 'title', '출석부 수정' );
			$("#dialog").load("../mypage/_attendanceForm.php?mode=modify&attendance_id="+attendance_id);
			$("#dialog").dialog('option', 'height', 'auto');
			$("#dialog").dialog('option', 'width', 600);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() {
					_this.modifyAction();
				} ,
				"닫기" : function() {
					$(this).dialog("close");
				}
			});
			$('#dialog').dialog('open');
		},

		modifyAction : function(){
			var _this = this;
			var group_id = $('#attendanceForm select[name=group_id]').val();
			var attendance_title = $('#attendanceForm input[name=title]').val();
            if(group_id == 0){
				alert("그룹을 선택하세요");
			}else if(attendance_title == ""){
				alert("출석부 이름을 입력하세요");
			}else{
				var form_var = $('#attendanceForm').serialize();
				$.ajax({
					type: "POST",
					url: "../mypage/_groupAction.php",
					data: form_var,
					success: function(){
						_this.listWarp();
						$('#dialog').dialog('close');
					}
				});
			}
		},

		deleteAction : function(attendance_id) {
			var _this = this;
			if(!confirm("정말로 출석부를 삭제하시겠습니까?")){

			}else{
				jQuery.get('../mypage/_groupAction.php',{"mode": "deleteAttendance", "id": attendance_id},function() {
					_this.listWarp();
				});
			}
		},




		attendanceDelete : function(attendance_id,group_id) {
			var _this = this;
			if(!confirm("정말로 출석부를 삭제하시겠습니까?")){

			}else{



				jQuery.get('../mypage/_groupAction.php',{"mode": "deleteAttendance", "id": attendance_id },function() {
					//$.group.attendanceList(group_id,'O');
					_this.listWarp();
				});
			}

		},

		_ver : "1.0.1"
		/* GROUP manager ---------------------------------------------------------------------- */
		}
	});
})(jQuery);

(function($)
{
	$.extend(
	{
		analysis : 
		{
		//-----------------------------------------------------------------------------------------
		list : function()  {
			$("#contantBox").load("../report/_analysisList.php");
		},

		chart : function(group_id)  {
			var worship_L = [], worship_O = [], tick = [];
			$.ajax({
				type: "GET",
				url: "../report/_analysisChartJson.php",
				data: {"group_id":group_id},
				dataType: "json",
				success: function(data){
					$("#contantBox").html(
						'<div id="quick_actions">' +
						'	<a class="button_big" href="javascript:$.analysis.list();"><span class="iconsweet">}</span>목록보기</a>' +
						'</div>' +
						'<div class="one_wrap">' +
						'   <div class="widget">' +
						'		<div class="widget_title"><span class="iconsweet">r</span><h5>'+ data.attendance.title +'</h5></div>' +
						'		<div class="widget_body">' +
						'			<div class="content_pad">' +
						'			<div id="chart_linear" class="no_overflow" style="width:100%;height:400px"></div>' +
						'			</div>' +
						'		</div>' +
						'	</div>' +
						'</div>'
					);


					var woO = data.attendance.worship;
					var woL = data.attendance.gbs;
					var woD = data.attendance.xDate;
                    
					for (var i = 0; i < 52; i++) {
						worship_O.push([i, woO[i]]);
						worship_L.push([i, woL[i]]);
						tick.push([i, woD[i]]);
					}
					var plot = $.plot($("#chart_linear"),[{"data":worship_O, "label":"예배"}, {"data":worship_L, "label":"모임"}], {
						series: {
							lines: { show: true },
							points: { show: true }
						},
						grid: {hoverable: true, clickable: true },

						yaxis: {
							autoscaleMargin: 0.02,
							position: "left"

						},

	
						xaxis: {
							
						    ticks: tick
						}


					});
				},
				complete : function(){
				
					function showTooltip(x, y, contents) {
						$('<div id="tooltip" class="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 5,
							
							padding: '6px 5px',
							'z-index': '9999',
							'background-color': '#31383a',
							'color': '#fff',
							'font-size': '11px',
							opacity: 0.85,
							'border-radius': '3px',
							'-webkit-border-radius': '3px',
							'-moz-border-radius': '3px'
						}).appendTo("body").fadeIn(200);
					}
		
					var previousPoint = null;
					
					$("#chart_linear").bind("plothover", function (event, pos, item){
						$("#x").text(pos.x.toFixed(2));
						$("#y").text(pos.y.toFixed(2));
						if ($("#chart_linear").length > 0){
							if(item) {
								if(previousPoint != item.dataIndex) {
									previousPoint = item.dataIndex;
									$("#tooltip").remove();
									var x = item.datapoint[0]; 
									var y = item.datapoint[1];
									showTooltip(item.pageX, item.pageY, item.series.label+"("+x+"주): "+y+"명");
								}
							} else {
								$("#tooltip").remove();
								previousPoint = null;
							}
						}
					});
					
					$("#chart_linear").bind("plotclick", function (event, pos, item){
						if(item) {
							$("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
							plot.highlight(item.series, item.datapoint);
						}
					});
				
				}
			});


		},

	    _ver : "1.9.1"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

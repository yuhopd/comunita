(function($)
{
	$.extend(
	{
		analysis : 
		{
		//-----------------------------------------------------------------------------------------
		list : function(usr_id)  {
			$('#analysisArea').load('../analysis/_analysisList.php');
		},

		chart : function(group_id)  {
			$('#analysisArea').load('../analysis/_analysisChart.php?group_id='+group_id);
		},

		chartAll : function()  {
			$('#analysisArea').load('../analysis/_analysisChartAll.php');
		},

	    _ver : "0.9.1"
	    //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);

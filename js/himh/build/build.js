({
    baseUrl : '../',
	paths: {
        'jquery' : 'lib/jquery-1.9.1',
        'underscore': 'lib/underscore',
        'jquery-ui' : 'lib/jquery-ui',
		'jquery-ui-kukudocs' : 'layout/jquery-ui-kukudocs',
        'jquery-ui-shape-editor' : 'layout/jquery-ui-shape-editor',
        //'jquery-ui-touch-punch' : 'lib/jquery-ui-touch-punch',
        'toastmessage' : 'lib/jquery.toastmessage',
		'XDomainRequest' : 'lib/jquery.XDomainRequest',
        'Loader' : 'Loader',
        'Define' : 'Define',
        'Menu' : 'layout/Menu',
        'Layout' : 'layout/Layout',
        'Config' : 'layout/Config',
        'Settings' : 'Settings',
		'SettingsDef' : 'SettingsDef',

		'ApiPostMessage' : 'ApiPostMessage',
        'PathMeta' : 'shape/PathMeta',
        'DefaultShape' : 'shape/DefaultShape',
        'ShapeUtil' : 'shape/ShapeUtil',

        'Loupe' : 'lib/jquery.loupe',
        'iscroll-zoom' : 'lib/iscroll-zoom',
        'fineUploader' : 'lib/fineuploader/jquery.fineuploader-3.4.1',
        'compatibility' : 'lib/compatibility',
        //'PDFJS' : 'lib/pdf',
        'PdfThumbnail' : 'lib/pdf_thumbnail_view',
        'TextLayerBuilder' : 'lib/text_layer_builder',
        'Sketch' : 'lib/sketchjs/Sketch',
        'Event' : 'lib/sketchjs/Event',
        'swiper' : 'lib/swiper/swiper',
        'tooltipster': 'lib/tooltipster.bundle'
    },

    shim : {
		/*
		'PDFJS' :{
			deps : ['compatibility']
		},
		*/

        'fineUploader' : {
            deps : ['jquery']
        },
		'jquery-ui' : {
            deps : ['jquery']
        },
		'Loupe' : {
            deps : ['jquery']
        },
		'jquery-ui-kukudocs' : {
            deps : ['jquery','jquery-ui']
        },
		'jquery-ui-shape-editor' : {
            deps : ['jquery','jquery-ui']
        },

		'toastmessage' : {
        	deps : ['jquery']
        },

		'XDomainRequest' : {
			deps : ['jquery']
		},
        'tooltipster': {
			deps : ['jquery']
		}
    },

    //optimize : 'uglify2',
    optimize : 'none',
    mainConfigFile : '../Main.js',
    name : 'Main',
    out : './HTML5MainViewer.js'
	//out : '../../../localviewer/js/HTML5MainViewer.js'
})

define(["jquery"
	, "Common"
	, "Contacts"
	, "Calendar"
	, "Attendance"
	, 'AdminGroup'
	, 'Calendar'
	, 'Visit'
	, 'Reservation'
	, 'fancybox'
	, 'bootstrap'
	, 'jquery-ui'
	, 'metisMenu'
	, 'slimscroll'
	, 'jsTree'
], function ($, Common,  Contacts, Calendar, Attendance, AdminGroup, Calendar, Visit, Reservation) {

    var Loader = function () {


		this.contacts = null;
		this.attendance = null;
		this.adminGroup = null;
		this.visit = null;
		this.calendar = null;
		this.reservation = null;

        this.init = function () {
			var _this = this;
			_this.common = new Common();
			_this.calendar = new Calendar();

			// Add body-small class if window less than 768px
			if ($(window).width() < 769) {
				$('body').addClass('body-small')
			} else {
				$('body').removeClass('body-small')
			}

			// MetisMenu
			$('#side-menu').metisMenu();


			// Close menu in canvas mode
			$('.close-canvas-menu').on('click', function () {
				$("body").toggleClass("mini-navbar");
				_this.SmoothlyMenu();
			});

			// Run menu of canvas
			$('.sidebar-collapse').slimScroll({
				height: '100%',
				railOpacity: 0.9
			});


			// Initialize slimscroll for right sidebar
			$('#page-wrapper').slimScroll({
				height: '100%',
				railOpacity: 0.4,
				wheelStep: 10
			});

			// Small todo handler
			$('.check-link').on('click', function () {
				var button = $(this).find('i');
				var label = $(this).next('span');
				button.toggleClass('fa-check-square').toggleClass('fa-square-o');
				label.toggleClass('todo-completed');
				return false;
			});

			// Append config box / Only for demo purpose
			// Uncomment on server mode to enable XHR calls
			//$.get("skin-config.html", function (data) {
			//    if (!$('body').hasClass('no-skin-config'))
			//        $('body').append(data);
			//});

			// Minimalize menu
			$('.navbar-minimalize').on('click', function (event) {
				event.preventDefault();
				$("body").toggleClass("mini-navbar");
				_this.SmoothlyMenu();

			});

			$("#side-menu a.userinfo").on('click', function(e){
				var userinfo = $(this).attr("userinfo");
				_this.common.usr_info(userinfo);
				return false;
			});


			// Full height of sidebar
			function fix_height() {
				var heightWithoutNavbar = $("body > #wrapper").height() - 61;
				$(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

				var navbarheight = $('nav.navbar-default').height();
				var wrapperHeight = $('#page-wrapper').height();

				if (navbarheight > wrapperHeight) {
					$('#page-wrapper').css("min-height", navbarheight + "px");
				}

				if (navbarheight < wrapperHeight) {
					$('#page-wrapper').css("min-height", $(window).height() + "px");
				}

				if ($('body').hasClass('fixed-nav')) {
					if (navbarheight > wrapperHeight) {
						$('#page-wrapper').css("min-height", navbarheight + "px");
					} else {
						$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
					}
				}

			}

			fix_height();

			// Fixed Sidebar
			$(window).bind("load", function () {
				if ($("body").hasClass('fixed-sidebar')) {
					$('.sidebar-collapse').slimScroll({
						height: '100%',
						railOpacity: 0.9
					});
				}
			});

			// Move right sidebar top after scroll
			$(window).scroll(function () {
				if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
					$('#right-sidebar').addClass('sidebar-top');
				} else {
					$('#right-sidebar').removeClass('sidebar-top');
				}
			});

			$(window).bind("load resize scroll", function () {
				if (!$("body").hasClass('body-small')) {
					fix_height();
				}
			});

			$("[data-toggle=popover]")
				.popover();


			// Minimalize menu when screen is less than 768px
			$(window).bind("resize", function () {
				if ($(this).width() < 769) {
					$('body').addClass('body-small')
				} else {
					$('body').removeClass('body-small')
				}
			});

			$("#SearchContact").submit(function() {
				_this.contacts.search();
				return false;
			});


			//------ Dialog ---------------------------------------------------------------------------------------------
				$("#usr_dialog").dialog({
					autoOpen: false,
					resizable: false,
					width: 400,
					height: 'auto',
					modal:true,
					open: function() { },
					close: function() { $("#usr_dialog").empty(); },
					title: "회원정보",
					position: {my:"top", at:"top"}
				});

				$("#sub_dialog").dialog({
					autoOpen: false,
					width: 400,
					height: 'auto',
					position: {my:"top", at:"top" },
					modal:true,
					open: function() { },
					close: function() { $("#sub_dialog").empty(); },
					title: ""
				});

				$("#search_dialog").dialog({
					autoOpen: false,
					width: 300,
					height: 'auto',
					position: {my:"top", at:"top" },
					modal:true,
					open: function() { },
					close: function() { $("#search_dialog").empty(); },
					title: ""
				});

				$("#dialog").dialog({
					autoOpen: false,
					resizable: true,
					position: {my:"top", at:"top" },
					width: 450,
					height: 'auto',
					modal:true,
					open: function() { },
					close: function() { $("#dialog").empty();  }
				});

			//------ Menu Tree ---------------------------------------------------------------------------------------------
				$.ajax({
					type: "GET",
					url: "_menuCalendarList.php",
					data: {},
					dataType: 'json',
					success: function(data){
						_this.calendar = new Calendar();
						$('#calendar_menu_json').jstree({
							'core' : {
								'themes' : {
									'name': 'proton',
									'responsive': true,
									"dots": true,
									"icons": true
								},
								'data' : data
							}
						}).on('changed.jstree', function (e, data) {
	
							if (typeof data.node.original.id !== "undefined") {
								_this.calendar.listFromGroup(data.node.original.id);
							}

						});
					}
				});

				$.ajax({
					type: "GET",
					url: "_menuGroupList.php",
					data: {},
					dataType: 'json',
					success: function(data){
						_this.contacts = new Contacts();
						_this.contacts.init();
						$('#group_menu_json').jstree({
							'core' : {
								'themes' : {
									'name': 'proton',
									'responsive': true,
									"dots": true,
									"icons": true
								},
								'data' : data
							}
						}).on('changed.jstree', function (e, data) {
							if(data.node.original.mode == "group"){
								if (typeof data.node.original.id !== "undefined") {
									_this.contacts.list({mode:'', position:0, sameage_id:0, group_id:data.node.original.id});
								}
							}else if(data.node.original.mode == "sameage"){
								if (typeof data.node.original.sameage_id !== "undefined") {
									_this.contacts.list({mode:'sameage', position:0, sameage_id:data.node.original.sameage_id, group_id:0});
								}
							}else if(data.node.original.mode == "position"){
								if (typeof data.node.original.position_id !== "undefined") {
									_this.contacts.list({mode:'position', position:data.node.original.position_id, sameage_id:0, group_id:0});
								}
							}
						});
					}
				});

				$.ajax({
					type: "GET",
					url: "_menuAttendanceList.php",
					data: {},
					dataType: 'json',
					success: function(data){
						$('#attendance_menu_json').jstree({
							'core' : {
								'themes' : {
									'name': 'proton',
									'responsive': true,
									"dots": true,
									"icons": true
								},
								'data' : data
							}
						}).on('changed.jstree', function (e, data) {
							_this.attendance = new Attendance();
							if (typeof data.node.original.id !== "undefined") {
								_this.attendance.list({mode:'attendance', group_id:data.node.original.id, id:data.node.original.id});
							}
						});
					}
				});

			//------ Button Event ---------------------------------------------------------------------------------------------

			$('#side-menu a').on('click', function () {
				var action = $(this).attr("action");
				if(action == "groupSettings"){
					_this.adminGroup = new AdminGroup();
					_this.adminGroup.list();
				} else if(action == "calendar"){
					$.ajax({
						type: "GET",
						url: "_calendarList.php",
						data: {},
						dataType: 'html',
						success: function(data){
							$("#mainBox").html(data);
						},
						complete : function(){
							$("#mainBox a[action='calendarGroup']").on("click",function(){
								var grouoId = $(this).attr("grouoId");
								_this.calendar.listFromGroup(grouoId);

								$("#calendarTitle").html($(this).text());
								return false;
							});

							_this.calendar.list(function(event){

						
							});

						}
					});
				}

				return false;
			});

			$('#reporttNav a').on('click', function () {
				var action = $(this).attr("action");
				if(action == "navVisitList"){
					_this.visit = new Visit();
					_this.visit.list({"mode":"normal", "listBox":"mainBox"});
				}else if(action == "groupSettings"){

				}

				return false;
			});

			$('#websiteNav a').on('click', function () {
				var action = $(this).attr("action");
				if(action == "reservation"){
					_this.reservation = new Reservation();
					_this.reservation.list({"mode":"normal", "listBox":"mainBox"});
				}else if(action == "groupSettings"){

				}

				return false;
			});

			$('#settingsNav a').on('click', function () {
				var action = $(this).attr("action");
				if(action == "groupSettings"){
					_this.adminGroup = new AdminGroup();
					_this.adminGroup.list();
				}else if(action == "groupSettings"){

				}

				return false;
			});
		};

		// For demo purpose - animation css script
		this.animationHover = function(element, animation) {
			element = $(element);
			element.hover(
				function () {
					element.addClass('animated ' + animation);
				},
				function () {
					//wait for animation to finish before removing classes
					window.setTimeout(function () {
						element.removeClass('animated ' + animation);
					}, 2000);
				});
		};

		this.SmoothlyMenu = function () {
			if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
				// Hide menu in order to smoothly turn on when maximize menu
				$('#side-menu').hide();
				// For smoothly turn on menu
				setTimeout(
					function () {
						$('#side-menu').fadeIn(400);
					}, 200);
			} else if ($('body').hasClass('fixed-sidebar')) {
				$('#side-menu').hide();
				setTimeout(
					function () {
						$('#side-menu').fadeIn(400);
					}, 100);
			} else {
				// Remove all inline style from jquery fadeIn function to reset menu state
				$('#side-menu').removeAttr('style');
			}
		}

	};
    return Loader;
});

define([
	'jquery'
	, 'Family'
	, 'Sms'
	, 'Search'
	, 'dropzone'
	, 'Croppie'
	, 'jquery-ui'
], function ($, Family, Sms, Search) {
	var Common = function () {
		//-----------------------------------------------------------------------------------------
  
		this.initialLocation = null;
		this.isInit = false;
		this.map = null;

		this.sms = null;

		this.search = null;

		this.croppie = null;

		this.myOptions = {
			zoom: 14
		};

		this.usr_info = function (usr_id){
			var _this = this;
			_this.sms = new Sms();
			$.ajax({
				type: "GET",
				url: "_usrInfo.php",
				data: {"id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#usr_dialog").html(data);				
				},
				complete : function(){
					$("#userTabs").tabs({
						activate: function( event, ui ) {
							if(ui.newPanel[0].id == "usrGroupList"){
								_this.usrGroupList(usr_id);
							}else if(ui.newPanel[0].id == "usrVisitList"){
								_this.usrVisitList(usr_id);
							}else if(ui.newPanel[0].id == "usrAttendanceList"){
								_this.usrAttendanceList(usr_id);
							}else if(ui.newPanel[0].id == "usrNote"){
								_this.usrNoteList(usr_id);
							}
						}
					});

                    var address = $.trim($("input[name='address_1']").val() + $("input[name='address_2']").val());
					if(address === ""){
						$("#userTabs").tabs( "disable", 5 );						
					}

					$(".usr_info a.btn").on("click",function(e){
						var usr_id = $(this).attr("usrid");
						var mode = $(this).attr("action");
						if(mode == "smsSend"){
							_this.sms.send("");
						}else if(mode == "usrInfoModify"){
							_this.usrInfoModify(usr_id);
						}else if(mode == "usrModifyAction"){ 
							_this.usrModifyAction(usr_id);
						}else if(mode == "usrInfoModifyCancel"){ 
							_this.usrInfoModifyCancel(usr_id);
						}else if(mode == "usrModifyPsw"){ 
							_this.usrModifyPsw(usr_id);
						}else if(mode == "usrModifyAuth"){ 
							_this.usrModifyAuth(usr_id);
						}else if(mode == "usrDeleteAction"){ 
							_this.usrDeleteAction(usr_id);
						}else if(mode == "usrImgThumbEdit"){ 
							_this.usrMoodifyImage(usr_id);
						}else if(mode == "usrImgThumbUpload"){ 
							_this.usrUploadImage(usr_id);
						}

						
					});
					_this.usrFamilyList(usr_id);
				}
			});
			$("#usr_dialog").dialog('option', 'width', 720);
			$("#usr_dialog").dialog('option', 'height', 'auto');
			$("#usr_dialog").dialog('option', 'buttons', {});
			$("#usr_dialog").dialog('open');
		};


		/* 회원추가 */
		this.usrInsert = function() {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원추가' );
			$("#dialog").dialog('option', 'width', 550);
			$("#dialog").dialog('option', 'height', "auto");
			$.ajax({
				type: "GET",
				url: "_usrForm.php",
				data: {"mode":"usr_insert"},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					//_this.usrGroupList(usr_id);
					//$("#dialog select, #dialog input:checkbox, #dialog input:radio").uniform();
			        //$("#birth").datepicker({dateFormat:'yy-mm-dd', changeMonth:true, changeYear:true});
				}
			});

			$("#dialog").dialog('option', 'buttons', {
				"회원추가" : function() { 
					_this.usrInsertAction(); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});

			$('#dialog').dialog('open');
		};
	
		this.usrInsertAction = function() {
			var _this = this;
			var form_var = $('#userForm').serialize();
			console.log(form_var);
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error > 0){
					    alert(data.message);
					}else{
						_this.usrModify(data.lastID);
					}
				}
			});
		};

		this.usrModify = function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원수정' );
			$.ajax({
				type: "GET",
				url: "_usrForm.php",
				data: {"mode":"usr_modify", "usr_id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					_this.usrGroupList(usr_id);
					//$("#dialog select, #dialog input:checkbox, #dialog input:radio").uniform();
			        //$("#birth").datepicker({dateFormat:'yy-mm-dd', changeMonth:true, changeYear:true});
				}
			});
			$("#dialog").dialog('option', 'width', 550);
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'buttons', {
				"회원수정" : function() { 
					_this.usrModifyAction();
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		};

		this.usrModifyAction = function() {
			var _this = this;
			var form_var = $("#userInfoForm").serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
	
						_this.usr_info(data.usr_id);
					}else{
						alert(data.message);
					}
				}
			});
		};

		/* 회원삭제 */
		this.usrDeleteAction = function(usr_id) {
			var _this = this;
			if(!confirm("정말로 회원을 삭제하시겠습니까?")){

			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode"      : "usr_delete",
						"usr_id"    : usr_id
					},
					dataType : "json",
					success: function(data){
						_this.listWarp();
					}
				});
			}
		};
		
		/* ------------------------------------------------------------------------------ */


		this.usrInfoModify = function(usr_id) {
			var _this = this;
			$("#userInfoForm input:text").removeAttr("disabled");
			$("#userInfoForm select").removeAttr("disabled");
			$("#usrInfoModifyWright").hide();
			$("#usrInfoModifySave").show();
			$("#usrInfoModifyCancel").show();
		};

		this.usrInfoModifyCancel = function() {
			var _this = this;
			$("#userInfoForm input:text").attr("disabled",true);
			$("#userInfoForm select").attr("disabled",true);
			$("#usrInfoModifyWright").show();
			$("#usrInfoModifySave").hide();
			$("#usrInfoModifyCancel").hide();
		};

		this.usrNoteList = function(usr_id) {
			var _this = this;
			$.ajax({
				type : "GET",
				url : "_usrNote.php",
				data : {"usr_id" : usr_id},
				dataType : "html",
				success: function(data){
					$("#usrNote").html(data);
				},
				complete : function(){
					$("#usrNote a.btn").on("click",function(){
						var usr_id = $(this).attr("usrid");
						var action = $(this).attr("action");
						if(action == "userInfoSummaryModify"){
							_this.usrNoteModify(usr_id);
						} else if(action == "userInfoSummarySave"){
							_this.usrNoteSave(usr_id);
						}
						return false;
					});
				}
			});
		};

		this.usrNoteModify = function(usr_id) {
			$("#userInfoSummary").prop("disabled",false); 
			$("#userInfoSummaryModify").hide();
			$("#userInfoSummarySave").show();
		};

		this.usrNoteSave = function(usr_id) {
			var _this = this;
			var summary = $("#userInfoSummary").val();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: {
					"mode":"usrSummaryModify",
					"usr_id":usr_id,
					"summary":summary				
				},
				dataType: 'json',
				success: function(data){
				    $("#userInfoSummary").prop("disabled",true); 
					$("#userInfoSummaryModify").show();
					$("#userInfoSummarySave").hide();
				}
			});
		};


		/* 회원그룹설정 */
		this.usrGroupList = function(usr_id)  {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_usrGroupList.php",
				data: {'usr_id':usr_id},
				dataType: 'html',
				success: function(data){
					$("#usrGroupList").html(data);										
				},
				complete : function(){
					$("#usrGroupList a.btn").on("click", function(e){
						var action = $(this).attr("action");
						var usr_id = $(this).attr("usrid");
						var usrgroupid = $(this).attr("usrgroupid");
						if(action == "usrGroupInsertAction"){
							_this.usrGroupInsertAction(usr_id);
						}else if(action == "usrGroupDeleteAction"){
							_this.usrGroupDeleteAction(usrgroupid);
							
						}else if(action == "usrGroupModify"){
							_this.usrGroupModify(usrgroupid);
						}
						return false;
					});
				}
			});
		};

		this.usrGroupModify = function(id) {
			var _this = this;
			$("#sub_dialog").dialog('option', 'title', "소속정보 수정");
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', "auto");
			$("#sub_dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrGroupModifyAction();
				},
				"닫기" : function() { 
					$(this).dialog("close"); 
				}
			});

			$.ajax({
				type: "GET",
				url: "_usrGroupForm.php",
				data: {'mode':'modify','id':id},
				dataType: 'html',
				success: function(data){
					$("#sub_dialog").html(data);
				}
			});
			$('#sub_dialog').dialog('open');
		};

		this.usrGroupModifyAction = function() {
			var _this = this;
			var form_var = $("#usrGroupForm").serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.usrGroupList(data.usr_id);
						$('#sub_dialog').dialog('close');
					}else{
						alert(data.message);
					}
				}
			});
		};
	
		this.usrGroupInsertAction = function(usr_id) {


			var _this = this;
			var group_sel_id = $('#group_sel').val();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: {'mode':'usr_group_insert','group_id':group_sel_id,'usr_id':usr_id},
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.usrGroupList(usr_id);
					}else{
						alert(data.message);
					}
				}
			});
		};
	
		this.usrGroupDeleteAction = function(id) {
			var _this = this;
			if(!confirm("정말로 삭제하시겠습니까?")){
			}else{
				$.ajax({
					type: "GET",
					url:  "action.php",
					data: {'mode':'usr_group_delete','usr_group_id':id},
					dataType: 'json',
					success: function(data){
						if(data.error < 0){
							_this.usrGroupList(data.usr_id);
						}else{
							alert(data.message);
						}
					}
				});
			}
		};

		this.usrVisitList = function(usr_id){
			var _this = this;
			var src = "";

			$.ajax({
				type: "GET",
				url: "_usrVisitList.php",
				data: {"mode":"normal", "usr_id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#usrVisitList").html(data);
				},
				complete : function(){
				
					$("#usrVisitList a.btn").on("click", function(){
						var usr_id = $(this).attr("usrid");
						var action = $(this).attr("action");
						if(action == "visitInsert"){
							_this.usrVisitInsert(usr_id);
						} else if(action == "visitView"){
							_this.usrVisitView(usr_id);
						}
						return false;
					});


					$("#usrVisitList a[action='userInfo']").on("click",function(){
						var usr_id = $(this).attr("usrid");
						_this.usr_info(usr_id);
						return false;
					});

				
				}
			});
		};
		
		this.usrVisitView = function(id){
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_visitView.php?id="+id,
				dataType: 'html',
				success: function(data){
					$("#sub_dialog").html(data);
				},
				complete : function(){	
					$("#sub_dialog").dialog('option', 'title', '심방 보고서' );
					$("#sub_dialog").dialog('option', 'height', 'auto');
					$("#sub_dialog").dialog('option', 'width', 500);
					$("#sub_dialog").dialog('option', 'buttons', {});
					$('#sub_dialog').dialog('open');
				}
			});
		};

		this.usrVisitInsert = function (usr_id){
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_visitForm.php",
				data: {"mode":"insert", "usr_id":usr_id},
				dataType: 'html',
				success: function(data){
					$("#sub_dialog").html(data);
				},
				complete : function(){	

					$("#sub_dialog").dialog('option', 'title', '심방보고서 작성' );
					$("#sub_dialog").dialog('option', 'height', 'auto');
					$("#sub_dialog").dialog('option', 'width', 500);
					$("#sub_dialog").dialog('option', 'buttons', {
						"보고서 작성" : function() { 
							_this.usrVisitInsertAction();
						} ,  
						"닫기" : function() { 
							$(this).dialog("close"); 
						}
					});
					$('#sub_dialog').dialog('open');



					$('#sub_dialog').find("a.btn").on("click",function(){
						var action = $(this).attr("action");
						var mode = $(this).attr("mode");
						if(action == "searchContact"){
							_this.search = new Search();
							_this.search.searchContact(mode);
						}
					});




				}
			});
		};

		this.usrVisitInsertAction = function(){
			var _this = this,
				write_usr_id = $.trim($("#visitForm input[name='write_usr_id']").val()),
				type    = $.trim($("#visitForm select[name='type']").val()),
				reg_date = $.trim($("#visitForm input[name='reg_date']").val()),
				title   = $.trim($("#visitForm input[name='title']").val()),
				place   = $.trim($("#visitForm input[name='place']").val()),
				content = $.trim($("#visitForm textarea[name='content']").val());

			var arryUsrId = new Array();
			$.each($("#usr_name > .usrGroup"), function() {
				arryUsrId.push($(this).attr("usrId"));
			});
			var usr_id = arryUsrId.join("|");

			var arryVisitUsrId = new Array();
			$.each($("#visit_usr_name > .usrGroup"), function() {
				arryVisitUsrId.push($(this).attr("usrId"));
			});
			var visit_usr_id = arryVisitUsrId.join("|");



			if(place == '') {
				alert("장소를 입력하세요!");
				return false;
			}
			if(type == 0) {
				alert("심방종류를 선택하세요!");
				return false;
			}

			if(title == '') {
				alert("성경본문을 입력하세요!");
				return false;
			}

			if(content == '') {
				alert("내용을 입력하세요^^");
				return false;
			}	
			$.ajax({
				type: "POST",
				url: "_visitAction.php",
				data: {
					mode: "visitInsert",
					usr_id : usr_id,
					visit_usr_id : visit_usr_id,
					write_usr_id : write_usr_id,
					reg_date : reg_date,
					type : type,
					place : place,
					title : title,
					content : content					
				},
				dataType: 'json',
				success: function(data){
					$('#sub_dialog').dialog('close');
					_this.listWarp();
				}
			});
		};

		/* 회원 비밀번호 수정 */
		this.usrModifyPsw = function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원비밀번호 수정' );
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").load("_usrPswForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrModifyPswAction(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		};

		this.usrModifyPswAction = function(usr_id) {
			var _this = this;
			var form_var = $("#userPswForm").serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.listWarp();
						$("#dialog").dialog("close"); 
					}else{
						alert(data.message);
					}

					
				}
			});
		};

		/* 회원 권한 설정 */
		this.usrModifyAuth = function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원권한 수정' );
			$("#dialog").dialog('option', 'height', "auto");
			$("#dialog").dialog('option', 'width', 450);
			$("#dialog").load("_usrAuthForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"저장" : function() { 
					_this.usrModifyAuthAction(usr_id); 
				} ,  
				"취소" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		};

		this.usrModifyAuthAction = function(usr_id) {
			var _this = this;
			var form_var = $("#userAuthForm").serialize();
			$.ajax({
				type: "POST",
				url: "action.php",
				data: form_var,
				dataType: 'json',
				success: function(data){
					if(data.error < 0){
						_this.listWarp();
						$("#dialog").dialog("close"); 
					}else{
						alert(data.message);
					}

				}
			});
		};
		
		/* 이미지 수정 */
		this.usrMoodifyImage = function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원 이미지 수정');
			$("#dialog").dialog('option', 'resizable', true);
			$("#dialog").dialog('option', 'height', 500);
			$("#dialog").dialog('option', 'width', 600);
			$.ajax({
				type: "GET",
				url: "_usrImgForm.php",
				data: {
					"mode":"usr_modify",
					"usr_id":usr_id
				},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					_this.croppie = $("#dialog").find('.user_thumb').croppie({
						viewport: {
							width: 200,
							height: 200,
							type: 'circle'
						}
					});
				}
			});

			$("#dialog").dialog('option', 'buttons', {
				"편집저장" : function() { 
					//_this.usr_info(usr_id);
					_this.croppie.croppie('result', {
						type: 'base64',
						quality:1
					}).then(function (resp) {
						_this.usrMoodifyImageAction(resp,usr_id);
					}); 
				},

				"업로드" : function() { 
					_this.usrUploadImage(usr_id);
				},
				"삭제" : function() { 

				},
				"닫기" : function() { 
					$("#dialog").dialog("close"); 
				}
			});

			$('#dialog').dialog('open');
			
		};

		this.usrMoodifyImageAction = function(base64,usr_id){
			var _this = this;
			$.ajax({
				type: "POST",
				url: "_usrImgAction.php",
				data: {
					"base64":base64,
					"usr_id":usr_id
				},
				dataType: 'html',
				success: function(data){
					$("#dialog").dialog("close"); 
				},
				complete : function(){
					_this.usr_info(usr_id);
				}
			});
		};

		this.usrUploadImage = function(usr_id) {

			var _this = this;
			$("#dialog").dialog('option', 'title', '회원 이미지 업로드' );
			$("#dialog").dialog('option', 'resizable', true);
			$("#dialog").dialog('option', 'height', 270);
			$("#dialog").dialog('option', 'width', 650);
			$.ajax({
				type: "GET",
				url: "_usrImgForm.php",
				data: {
					"mode":"usr_upload",
					"usr_id":usr_id
				},
				dataType: 'html',
				success: function(data){
					$("#dialog").html(data);				
				},
				complete : function(){
					var iDropzone = null;
					iDropzone = new Dropzone("#imageDropzone", {
						url: "_imageUploadAction.php",
						maxFiles : 1,
						parallelUploads : 1,
						addRemoveLinks : true,
						acceptedFiles : ".jpg,.png"
					});
					iDropzone.on("success", function(file) {
						_this.usrMoodifyImage(usr_id);
					});
				}
			});


			
			$("#dialog").dialog('option', 'buttons', {
				"닫기" : function() { 
					$("#dialog").dialog("close"); 
				}
			});

			$('#dialog').dialog('open');

		};

		/* 가족관계 설정 */
		this.usrFamilyList = function(usr_id) {
			var _this = this;
			$.ajax({
				type : "GET",
				url : "_usrFamilyList.php",
				data : {"usr_id" : usr_id},
				dataType : "html",
				success: function(data){
					$("#usrDetailInfo").html(data);
				},
				complete : function(){
					_this.family = new Family();
					_this.family.list({
						"usr_id"  : usr_id,
						"listBox" : "usrFamilyList",
						"callback" : function(usr_id){

							$("a[action='userinfo']").on("click",function(){
								var usr_id = $(this).attr("usrid");
								_this.usr_info(usr_id);
								return false;
							});

							$("a[action='familyDeleteAction']").on("click",function(){
								var usr_id = $(this).attr("usrid");
								var family_id = $(this).attr("familyid");
								_this.usrFamilyDeleteAction(usr_id,family_id);
								return false;
							});
						}
					});
					$("#usrDetailInfo a.btn").on("click",function(){
						var usr_id = $(this).attr("usrid");
						var action = $(this).attr("action");
						if(action == "familyInsert"){
							_this.family.insert(usr_id);
						};
						return false;
					});

				}
			});
		};

		this.usrFamilySearch = function(usr_id) {
			var _this = this;
			var q = $('#family_q').val().trim();
			if(q != ""){
				var keyword = encodeURIComponent(q);
				_this.usrFamilyList({"usr_id":usr_id ,"q":keyword, "box":"usrFamilySearchList"});
			}
		};

		this.usrFamilyDeleteAction = function(usr_id,family_id) {
			var _this = this;
			if(!confirm("정말로 회원 가족관계를 삭제하시겠습니까?")){
			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode": "usr_family_delete",
						"usr_id": usr_id,
						"family_id": family_id							
					},
					dataType : "json",
					success: function(data){
						_this.usrFamilyList(usr_id);
					}
				});
			}
		};

		this.usrFamilyInsert = function(usr_id) {
			$("#sub_dialog").dialog('option', 'width', 400);
			$("#sub_dialog").dialog('option', 'height', 350);
			$("#sub_dialog").load("_usrFamilySearch.php?usr_id="+usr_id);
			$("#sub_dialog").dialog('option', 'buttons', {});
			$('#sub_dialog').dialog('open');
		};

		this.usrFamilyInsertAction = function(usr_id) {
			var _this = this;
            var family_id = $("#usrFamilySearchList input.radio:checked").val();
		    var relations = $('#relations').val();
			if(relations == 0){
				alert("관계를 선택하세요!");
			} else if(family_id == 0 || !family_id){
				alert("가족을 선택하세요!");
			}else{
				$.ajax({
					type : "GET",
					url : "action.php",
					data : {
						"mode"      : "usr_family_insert",
						"usr_id"    : usr_id,
						"family_id" : family_id,
						"relations" : relations
					},
					dataType : "json",
					success: function(data){
						if(data.error > 0){
							alert(data.message);
						}else{
							_this.usrFamilyList({"usr_id":usr_id, "box": "usrFamilyList"});
							$('#sub_dialog').dialog('close');
						}
					}
				});
			}
		};

		this.usrFamilyModify = function(usr_id) {
			var _this = this;
			$("#dialog").dialog('option', 'title', '회원가족관계 수정' );
			$("#dialog").dialog('option', 'height', 400);
			$("#dialog").dialog('option', 'width', 350);
			$("#dialog").load("_usrFamilyForm.php?usr_id="+usr_id);
			$("#dialog").dialog('option', 'buttons', {
				"확인" : function() { 
					$(this).dialog("close"); 
				}
			});
			$('#dialog').dialog('open');
		};

		/* 출석부 설정 */
		this.usrAttendanceList = function(usr_id) {
			var _this = this;
			$.ajax({
				type : "GET",
				url : "_usrAttendanceList.php",
				data : {"usr_id" : usr_id},
				dataType : "html",
				success: function(data){
					$("#usrAttendanceList").html(data);
				},
				complete : function(){
					
				}
			});
		};

		this.getAddressResultMap = function (){
			var _this = this;
			var geo = [];
			var geocoder = new google.maps.Geocoder();
			var address =  $("#addressResultLabel").html(); 
			if (geocoder) {
				geocoder.geocode({ 'address': address }, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						geo[0] = results[0].geometry.location.lat().toFixed(3);
						geo[1] = results[0].geometry.location.lng().toFixed(3);
						//alert(geo[0]+"------"+geo[1]);
						var map2 = new google.maps.Map(document.getElementById("addressResultMap"), _this.myOptions);
						var initialLocation = new google.maps.LatLng(geo[0], geo[1]);
						var placeMarker = new google.maps.Marker({ position: initialLocation, map: map2 });
						map2.setCenter(initialLocation);
					} else {
						//console.log("Geocoding failed: " + status);
					}
				});
			}    
		};

        //-----------------------------------------------------------------------------------------
		
	}
	return Common;
});
	
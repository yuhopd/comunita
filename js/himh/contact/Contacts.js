define([
  'jquery'
  , 'Common'
  , 'Visit'
  , 'Sms'
  , 'icheck'
  , 'nestable'
], function ($, Common, Visit, Sms) {
    var Contacts = function () {
		//-----------------------------------------------------------------------------------------

        this.isAllCheck = false;

		this.page = 1;
		this.pagination = 30;
		this.initialLocation = null;
		this.isInit = false;
		this.isAllCheck = false;

		this.group_id = 0;
		this.sameage_id = 0;
		this.position = 0;
		this.column = "";
		this.keyword = "";
		this.mode = "";
		this.viewmode = "";

		this.common = null;

		this.visit = null;		

		this.family = null;

        this.init = function(){
			var _this = this;
			_this.page = 1;
			_this.keyword = "";
			_this.common = new Common();
			_this.visit = new Visit();	
			_this.sms = new Sms();
	
			_this.listWarp();
            $("#SearchContact").submit(function() {
                _this.search();
                return false;
			});
		};

		/* 리스트 */
		this.setListOrder = function(){
			var _this = this;
			$("#contactListBox").sortable();
			$("#contactListOrderSetting").hide();
			$("#contactListOrderSave").show();
		};
		
		this.setListOrderSave = function(group_id){
			var _this = this;
			var str = [];
            var arry = $("#contactListBox li").attr("contact", function(i, val){
				str.push(val);
			});
			var sequence = str.join("|");
			
			$.ajax({
				type: "post",
				url: "action.php",
				data: {
					"mode" : "setGroupOrders",
					"sequence" : sequence,
					"group_id" : group_id
				},
				dataType: "json",
				success: function(data){
					//$("#mainBox").html(data);
				},
				complete : function(){
				}
			});
			
			$("#contactListBox").sortable("destroy");
			$("#contactListOrderSetting").show();
			$("#contactListOrderSave").hide();
		};

		this.list = function(json){
			var _this = this;
			if(json.mode       != undefined){ _this.mode = json.mode; }
			if(json.position   != undefined){ _this.position = json.position; }
			if(json.group_id   != undefined){ _this.group_id = json.group_id; }
			if(json.sameage_id != undefined){ _this.sameage_id = json.sameage_id; }
			if(json.viewmode   != undefined){ _this.viewmode = json.viewmode; }

			_this.page = 1;
			_this.keyword = "";
			_this.listWarp();
		};

		this.listWarp = function(){
			var _this = this;
			$.ajax({
				type: "GET",
				url: "_contactList.php",
				data: {
					"mode":_this.mode,
					"column":_this.column,
					"position":_this.position,
					"sameage_id":_this.sameage_id,
					"group_id":_this.group_id,
					"viewmode":_this.viewmode,
					"q":_this.keyword,
					"page":_this.page
				},
				dataType: "html",
				success: function(data){
					$("#mainBox").html(data);
				},
				complete : function(){    
					$("#mainBox a.userinfo").on('click', function(e){
						var userinfo = $(this).attr("userinfo");
						_this.common.usr_info(userinfo);
						return false;
					});

					$("#mainBox a.btn").on('click', function(e){
						var action = $(this).attr("action");

						if(action == "checkAll"){
							_this.checkAll();
						} else if(action == "smsSend"){
							_this.sms.send("");
						} else if(action == "usrInsert"){
							_this.common.usrInsert();
						} else if(action == "toggleAdvancedSearch"){
							_this.toggleAdvancedSearch();
						} else if(action == "advancedSearchAction"){
							_this.advancedSearchAction();
						} else if(action == "viewmode"){ 
							var mode = $(this).attr("mode");

							_this.viewmodeList(mode);
						}

						return false;
					});

					$("#column").on("change", function(e){
						_this.selectAdvancedSearch();
					});

					$(".pagging > a").on('click', function(e){
						var page = $(this).attr("href");
						_this.pageList(page);
						return false;
					});

					$('.i-checks').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					});
				}
			});
		};

		this.viewmodeList = function(mode){
			var _this = this;
			_this.viewmode = mode;

			_this.listWarp();
		};

		this.pageList = function(page){ 
			var _this = this;
			_this.page = page;
			_this.listWarp();
		};

		this.prevList = function(){ 
			var _this = this;
			if(_this.page == 1){ }else{ _this.page = _this.page - 1; }
			_this.listWarp();
		};

		/* 검색 */
		this.search = function(){
			var _this = this;
			_this.keyword = $.trim($("#SearchContact input[name='keyword']").val());
			_this.page = 1;
			_this.listWarp();
			scrollTo(0, 0);
		};

		this.searchCancel = function(){
			var _this = this;
			$("#SearchContact input[name='keyword']").val("");
			_this.page = 1;
			_this.listWarp();
		};

		this.checkAll = function() {
			
			if(this.isAllCheck){
				$('.i-checks').iCheck("uncheck");
				this.isAllCheck = false;
			}else{
				
				$('.i-checks').iCheck("check");
				this.isAllCheck = true;
			}
		};

		this.upSequence = function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "action.php",
				data: {"mode":"upSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		};
		
		this.downSequence = function(id) {
			var _this = this;
			$.ajax({
				type: "GET",
				url: "action.php",
				data: {"mode":"downSequence","id":id},
				success: function(){
					_this.listWarp();
				}
			});
		};

		this.toggleAdvancedSearch = function() {
            if($("#advancedSearch").is(":visible")){
				$("#advancedSearch").hide();
			}else{
				$("#advancedSearch").show();
			}
		};

		this.selectAdvancedSearch = function() {
			var column = $("#advancedSearch select[name='column']").val();

            if(column == "position"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput"  class="form-control">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">성도</option>' +
							'<option value="10">서리집사(남)</option>' +
							'<option value="11">서리집사(여)</option>' +
							'<option value="12">타집사</option>' +
							'<option value="13">명예집사</option>' +
							'<option value="14">안수집사</option>' +
							'<option value="15">협동집사</option>' +
							'<option value="16">공로집사</option>' +
							'<option value="17">은퇴집사</option>' +
							'<option value="18">협동은퇴집사</option>' +
							'<option value="19">전입안수집사</option>' +
							'<option value="20">권사</option>' +
							'<option value="21">은퇴권사</option>' +
							'<option value="22">명예권사</option>' +
							'<option value="23">전입권사</option>' +
							'<option value="24">시무권사</option>' +
							'<option value="25">협동권사</option>' +
							'<option value="26">전입명예권사</option>' +
							'<option value="27">전입은퇴권사</option>' +
							'<option value="30">장로</option>' +
							'<option value="32">전입장로</option>' +
							'<option value="33">협동장로</option>' +
							'<option value="35">시무장로</option>' +
							'<option value="37">은퇴장로</option>' +
							'<option value="38">전입은퇴장로</option>' +
							'<option value="39">원로장로</option>' +
							'<option value="40">사모</option>' +
							'<option value="50">교역자</option>' +
							'<option value="100">담임목사</option>' +
							'<option value="101">부목사</option>' +
							'<option value="102">목사</option>' +
							'<option value="103">영어예배목사</option>' +
							'<option value="104">협동목사</option>' +
							'<option value="105">교육목사</option>' +
							'<option value="106">찬양목사</option>' +
							'<option value="115">전임전도사</option>' +
							'<option value="116">준전임전도사</option>' +
							'<option value="117">찬양전도사</option>' +
							'<option value="118">교육전도사</option>' +
							'<option value="119">전도사</option>' +
							'<option value="120">선교사</option>' +
							'<option value="121">직원</option>' +
					'</select>');
			} else if(column == "status"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput"  class="form-control">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">본교회</option>' +
							'<option value="2">타교회</option>' +
							'<option value="4">비교인</option>' +
							'<option value="6">전출</option>' +
							'<option value="8">새가족</option>' +
							'<option value="10">별세</option>' +
							'<option value="11">미등록자</option>' +
					'</select>');
			} else if(column == "christen"){
				$("#columnInputWrap").html('<select id="columnInput" name="columnInput"  class="form-control">' +
							'<option value="0" selected>없음</option>' +
							'<option value="1">등록</option>' +
							'<option value="2">학습</option>' +
							'<option value="3">유아세례</option>' +
							'<option value="4">세례</option>' +
					'</select>');

			} else {
				$("#columnInputWrap").html('<input type="text" id="columnInput" name="columnInput" value="" class="form-control"/>');
			}
		};

		this.advancedSearchAction = function() {
			var _this = this;
			_this.mode = "advancedSearch";
			_this.column = $("#column").val();
			_this.keyword = $.trim($("#columnInput").val());
			_this.page = 1;
			_this.listWarp();
		};

		/* 출석부 설정 */
		this.usrAttendanceList = function(usr_id) {
			var _this = this;
			$.ajax({
				type : "GET",
				url : "_usrAttendanceList.php",
				data : {"usr_id" : usr_id},
				dataType : "html",
				success: function(data){
					$("#usrAttendanceList").html(data);
				},
				complete : function(){
					
				}
			});
		};


		/* 메뉴 설정 */
		this.menuOn = function(usr_id) {
			$(".contact li[contact="+usr_id+"] .action").show();
		};

        this.menuOff = function(usr_id) {
			$(".contact li[contact="+usr_id+"] .action").hide();
		};

		this.usr_list_in_group = function(e)  {
			var group_id = e.attr('id');
			$('#sms_right').load('_usrList.php?group_id='+group_id);
		};
	
		this.group_list = function(params)  {
			$('#fragment-1').load('group_list.php?'+params);
		};
	
		this.sameage_list = function()  {
			$('#sameage').load('sameage_list.php');
		};
		
		this.class_list = function(params)  {
			$('#fragment-3').load('group_list.php?'+params);
		};
	
	
	};
	return Contacts;
});

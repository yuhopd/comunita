(function($)
{
	$.extend(
	{
		multi_select : 
		{
		//-----------------------------------------------------------------------------------------
         	leftSel     : '#select1',
         	rightSel    : '#select2',
			resultInput : '#result_input',

            go_right : function(){
				$(this.leftSel + ' option:selected').remove().appendTo(this.rightSel);  
				//this.get_result();
			}
			,
			
			go_left : function(){
				$(this.rightSel + ' option:selected').remove().appendTo(this.leftSel);  
				this.get_result();
			}
			,

			get_result : function(){
				res=new Array();
				for(var i=0;i<$(this.rightSel).length;i++){
					res[i]=$(this.rightSel).options[i].value;
				}
				res=res.join("|");
				$(this.resultInput).value=res;
			}
        //-----------------------------------------------------------------------------------------
		}
	});
})(jQuery);


var usr = {  
    


    form_edit : function (id) {
        if($('info_'+id).style.display == 'none'){
            $('arrow_'+id).className = 'arrow_on';
            $('info_'+id).style.display = 'block';
            rActionAjax("ajax_form.php", 'mode=edit&id='+id, 'info_'+id);
        }else{
            $('arrow_'+id).className = 'arrow_off';
            $('info_'+id).style.display = 'none';
        }
    }
    ,
    form_add : function () {
        if($('item_add').style.display == 'none'){
            $('item_add').style.display = 'block';
            rActionAjax("ajax_form.php", '', 'item_add');
        }else{
            $('item_add').style.display = 'none';
        }
    }
    ,
    list : function (p,b) {
        rActionAjax('ajax_list.php', 'page='+p+'&b='+b, 'item_box');
    }
    ,
    add : function () {
        fActionAjax('usr_form_insert','top_msgArea');
    }
    ,
    edit : function (id) {
        fActionAjax('usr_form_'+id,'msgArea_'+id);
    }
    ,
    del : function (id) {
        rmActionAjax('ajax_action.php', 'mode=delete&id='+id, 'top_msgArea');
    }
}

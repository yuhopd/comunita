

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `summary` varchar(100) NOT NULL,
  `group_id` int(11) NOT NULL,
  `group_title` varchar(50) NOT NULL,
  `root_id` int(11) NOT NULL,
  `root_title` varchar(50) NOT NULL,
  `year` varchar(4) NOT NULL,
  `quarter` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `every_date_type` varchar(10) NOT NULL DEFAULT 'week',  
  `every_date_week` int(3) NOT NULL DEFAULT '0',
  `template` text NOT NULL,
  `adddate` int(11) NOT NULL
);


CREATE TABLE `attendance_comment` (
  `id` int(11) NOT NULL,
  `attendance_usr_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `adddate` int(10) NOT NULL
);

CREATE TABLE `attendance_report` (
  `id` int(11) NOT NULL,
  `attendance_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `suggest` text NOT NULL,
  `report` text NOT NULL,
  `unusual` text NOT NULL,
  `bigo` text NOT NULL,
  `adddate` int(10) NOT NULL
) ;


CREATE TABLE `attendance_usr` (
  `id` int(11)  NOT NULL,
  `attendance_id` int(11)  NOT NULL,
  `group_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` text NOT NULL,
  `worship` varchar(2) NOT NULL DEFAULT 'X',
  `gbs` varchar(2) NOT NULL DEFAULT 'X',
  `qt` tinyint(1) NOT NULL DEFAULT '0',
  `bible` tinyint(1) NOT NULL DEFAULT '0',
  `prayer` tinyint(1) NOT NULL DEFAULT '0',
  `adddate` int(10) NOT NULL
) ;


CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `str_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `allDay` varchar(2) NOT NULL DEFAULT 'O',
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `completed` varchar(2) NOT NULL DEFAULT 'X',
  `reg_date` int(8)  NOT NULL
);


CREATE TABLE `domain` (
  `id` int(11) NOT NULL,
  `uid` varchar(20) NOT NULL,
  `psw` varchar(32) NOT NULL,
  `church` varchar(50) NOT NULL,
  `registration` varchar(15) NOT NULL,
  `phone_1` varchar(15) NOT NULL,
  `phone_2` varchar(15) NOT NULL,
  `etc` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `auth` varchar(2) NOT NULL DEFAULT 'O',
  `key` varchar(32) NOT NULL,
  `adddate` int(11) NOT NULL
) ;

CREATE TABLE `group` (
  `id` int(11)  NOT NULL,
  `domain_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `year` varchar(4) NOT NULL,
  `class` tinyint(2) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `summary` text NOT NULL,
  `is_enable` varchar(4) NOT NULL DEFAULT 'O',
  `use_attendance` varchar(4) NOT NULL DEFAULT 'X',
  `use_calendar` varchar(4) NOT NULL DEFAULT 'X',
  `color` varchar(10) NOT NULL DEFAULT '#cccccc',
  `sequence` int(11) NOT NULL,
  `reg_date` int(11) NOT NULL
) ;

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `mode` varchar(10) NOT NULL DEFAULT 'usr',
  `item_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `summary` tinytext NOT NULL,
  `rename` varchar(20) NOT NULL,
  `ext` varchar(8) NOT NULL,
  `path` varchar(20) NOT NULL,
  `adddate` int(11) NOT NULL
);


CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` text,
  `adddate` int(11) NOT NULL,
  `lastUpdate` int(11) NOT NULL
);

CREATE TABLE `reservation_files` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `path` varchar(10) NOT NULL,
  `name` varchar(260) NOT NULL,
  `rename` varchar(50) NOT NULL,
  `ext` varchar(5) NOT NULL,
  `size` int(11) DEFAULT NULL,
  `adddate` int(11) NOT NULL
);


CREATE TABLE `reservation_usr` (
  `id` int(11)  NOT NULL,
  `reservation_id` int(11)  NOT NULL,
  `usr_id` int(11)  NOT NULL,
  `s_date` date NOT NULL,
  `s_time` time NOT NULL,
  `e_date` date NOT NULL,
  `e_time` time NOT NULL,
  `note` text NOT NULL,
  `adddate` int(10) NOT NULL
);


CREATE TABLE `sameage` (
  `id` int(10)  NOT NULL,
  `domain_id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `gisu` tinyint(2) NOT NULL,
  `year` varchar(4) NOT NULL
) ;

CREATE TABLE `usr` (
  `id` int(11)  NOT NULL,
  `domain_id` int(11) NOT NULL DEFAULT '1',
  `sameage_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `psw` varchar(32) NOT NULL,
  `groups` text NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `zip_code` varchar(7) NOT NULL,
  `address_1` varchar(150) NOT NULL,
  `address_2` varchar(100) NOT NULL,
  `summary` text NOT NULL,
  `car_number` varchar(10) NOT NULL,
  `car_type` varchar(20) NOT NULL,
  `birth` date NOT NULL,
  `lunar` varchar(2)  NOT NULL DEFAULT 'X',
  `district` varchar(20) NOT NULL,
  `householder` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `christen`  varchar(2)  NOT NULL DEFAULT '0',
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `level` tinyint(2) NOT NULL DEFAULT '1',
  `auth` varchar(2)  NOT NULL DEFAULT 'X',
  `key` varchar(32) NOT NULL,
  `gender` varchar(2)  NOT NULL DEFAULT 'M',
  `register` date NOT NULL,
  `point` tinyint(3) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `reg_date` int(11) NOT NULL
);


CREATE TABLE `usr_admin_auth` (
  `id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `media` varchar(2) NOT NULL DEFAULT 'X',
  `board` varchar(2) NOT NULL DEFAULT 'X',
  `ebook` varchar(2) NOT NULL DEFAULT 'X',
  `qt` varchar(2) NOT NULL DEFAULT 'X',
  `desk` varchar(2) NOT NULL DEFAULT 'X',
  `website` varchar(2) NOT NULL DEFAULT 'X',
  `member` varchar(2) NOT NULL DEFAULT 'X'
) ;

CREATE TABLE `usr_attendance` (
  `id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `attendance_id` int(11) NOT NULL,
  `level` varchar(2)  NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL,
  `duty` varchar(10) NOT NULL
);


CREATE TABLE `usr_family` (
  `id` int(11)  NOT NULL,
  `usr_id` int(11) NOT NULL,
  `family_id` int(11) NOT NULL,
  `relations` varchar(2)  NOT NULL DEFAULT '0',
  `adddate` int(11) NOT NULL
);


CREATE TABLE `usr_group` (
  `id` int(10)  NOT NULL,
  `usr_id` int(10)  NOT NULL,
  `group_id` int(10)  NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `duty` varchar(10) NOT NULL
) ;


CREATE TABLE `usr_img` (
  `id` int(11)  NOT NULL,
  `usr_id` int(11) NOT NULL,
  `path` varchar(20) NOT NULL,
  `rename` varchar(50) NOT NULL,
  `ext` varchar(5) NOT NULL,
  `adddate` int(11) NOT NULL
);


CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `usr_id` varchar(255) NOT NULL,
  `visit_usr_id` varchar(255) NOT NULL,
  `write_usr_id` int(11) NOT NULL,
  `reg_date` date NOT NULL,
  `type` varchar(50) NOT NULL,
  `place` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `adddate` int(11) NOT NULL
);


--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `send_id` int(11) NOT NULL,
  `read_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `adddate` int(11) NOT NULL,
  `is_read` varchar(2) NOT NULL DEFAULT 'X'
)
<?php

require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");

switch ($_REQUEST[mode])
{
    /******************************************************************************
     * show waring message bar for delete 1 favorites :: delFav
     * requred params(_GET) :: mode, type, sn
     ******************************************************************************/
    case "signin":
        if($_POST['uid'] == ''){
            $r['error']   = 1;
            $r['message'] = "핸드폰번호(이메일)를 입력하세요!";
            print json_encode($r);
            exit;
        }

        if($_POST['psw'] == ''){
            $r['error']   = 1;
            $r['message'] = "비밀번호를 입력하세요!";
            print json_encode($r);
            exit;
        }

        $phone = Common::getPhone($_POST[uid]);


        $psw   = md5($_POST['psw']);
        $query = "SELECT * FROM `usr` WHERE `email`='{$_POST[uid]}' OR `phone`='{$phone}' AND `auth`='O' ";

        //echo $query;
        $res = $db->query($query);
        $row = $res->fetch(PDO::FETCH_ASSOC);
        if($row['psw']==$psw){
            $_SESSION['sys_id']       = $row['id'];        // member serial number.
            $_SESSION['sys_domain']   = $row['domain_id'];
            $_SESSION['sys_email']    = $row['email'];     // member ID (email)
            $_SESSION['sys_nickname'] = $row['name'];      // sum of point
            $_SESSION['sys_name']     = $row['name'];
            $_SESSION['sys_level']    = $row['level'];
            $r['error']   = -1;
            $r['message'] = "로그인 성공하였습니다.";
            print json_encode($r);
            exit;
        }else{
            $r['error']   = 2;
            $r['message'] = "아이디와 비밀번호가 일치하지 않습니다!";
            print json_encode($r);
            exit;
        }


        break;


    /******************************************************************************
     * 교회등록
     * requred params(_GET) :: mode, type, sn
     ******************************************************************************/
    case "domainInsert":
        if($_POST['church'] == '' && $_POST['manager'] && $_POST['church_tel'] && $_POST['manager_tel'] && $_POST['email']){
            $r["error"]    = 100;
            $r["message"]  = "필수값 미입력";
            print json_encode($r);
            exit;
        }


        $time = time();
        $key = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $query  = "INSERT INTO `domain` (`church`, `manager`, `church_tel`, `manager_tel`, `email`, `auth`, `key`,`adddate`) VALUES ('{$_POST[church]}', '{$_POST[manager]}', '{$_POST[church_tel]}', '{$_POST[manager_tel]}', '{$_POST[email]}', 'O', '{$key}', '{$time}')";
        echo $query;
        $res = $db->query($query);
        if($res) {
            $r['error']   = -1;
            $r['message'] = "교회가 등록 되었습니다.";
        }
        else {
            $r['error']   = 101;
            $r['message'] = "???";
        }

        print json_encode($r);


        exit;
        break;

    /******************************************************************************
     * 핸드폰 인증번호 전송 :: phoneAuth
     * requred params(_POST) :: usr_id, phone
     ******************************************************************************/
    case "phoneAuth":
        $key = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $phone = Common::getPhone($_POST['phone']);

        $arryUsr = array(
            "phone"    => $phone,
            "key"      => $key,
            "auth"     => "X"
        );

        $usr = UsrManager::modifyUsrPhoneAuth($arryUsr);
        if($usr){
            UsrManager::sendPhoneAuth($phone,$key);
            $r["error"]    = -1;
            $r["message"]  = "휴대폰 번호으로 인증번호를 전송했습니다.";
            print json_encode($r);
            exit;
        }else{
            $r["error"]    = 100;
            $r["message"]  = "휴대폰 번호가 존재하지 않습니다.";
            print json_encode($r);
            exit;
        }
        break;

    /******************************************************************************
     * 핸드폰 인증 :: phoneAuthAction
     * requred params(_POST) :: usr_id, key, phone
     ******************************************************************************/
    case "phoneAuthAction":
        $usr = UsrManager::getUsr($_POST['usr_id']);
        if($usr['key'] == $_POST['key']){
            $arryUsr = array(
                "id"       => $_POST['usr_id'],
                "auth"     => "O"
            );
            $usr = UsrManager::modifyUsrAuth($arryUsr);

            //UsrManager::sendUsrInfo($_POST['usr_id']);
            $r["error"]    = -1;
            $r["usr_id"]   = $_POST['usr_id'];
            $r["uid"]      = $usr['usr_id'];
            $r["message"]  = "휴대폰 번호으로 인증되었습니다. ";
            print json_encode($r);
            exit;
        }else{
            $r["error"]    = 100;
            $r["message"]  = "휴대폰 번호으로 인증 실패했습니다.";
            print json_encode($r);
            exit;
        }
        break;

    /******************************************************************************
     * 회원 인증 (핸드폰) :: checkAccount
     * requred params(_POST) :: name, phone, sn
     ******************************************************************************/
    case "isAccount":
        if($_POST['phone'] == ''){
            $r["error"]    = 100;
            $r["message"]  = "휴대전화 번호를 입력하세요!";
            print json_encode($r);
            exit;
        }
        $phone = Common::getPhone($_POST[phone]);
        $usr = UsrManager::isAccount($phone);

        if($usr){
            $r["error"]    = -1;
            $r["usr_id"]   = $usr['id'];
            $r["message"]  = "등록된 계정입니다. ";
            print json_encode($r);
            exit;
        }else{
            $r["error"]    = 100;
            $r["message"]  = "휴대폰번호가 존재하지 않습니다.";
            print json_encode($r);
            exit;
        }
        break;



    /******************************************************************************
     * 비밀번호변경
     * requred params(_GET) :: friend_id
     ******************************************************************************/
    case "modifyPsw":
        if($_POST['psw'] == ''){
            $r["error"]    = 100;
            $r["message"]  = "변경할 비밀번호를 입력하세요!";
            print json_encode($r);
            exit;
        }

        if($_POST['psw_check'] == ''){
            $r["error"]    = 100;
            $r["message"]  = "비밀번호 확인을 입력하세요!";
            print json_encode($r);
            exit;
        }

        if($_POST['psw'] != $_POST['psw_check']){
            $r["error"]    = 100;
            $r["message"]  = "변경할 비밀번호가 일치 하지 않습니다!";
            print json_encode($r);
            exit;
        }

        $new_psw = md5($_POST['psw']);
        $query  = "UPDATE `usr` SET `psw` = '$new_psw' WHERE id='{$_POST[usr_id]}'";
        $result = $db->query($query);

        $r["error"]    = -1;
        $r["message"]  = "비밀번호가 변경되었습니다.";
        print json_encode($r);
        exit;
        break;



}

?>
<?include"../_lib/_inner_footer.php";?>
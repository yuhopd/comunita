<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>HIMH | Login</title>

    <link href="../css/himh.css" rel="stylesheet">

    <script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="../js/jquery.signin.js" charset="utf-8"></script>
    <script type="text/javascript" src="../js/lostorage.js" charset="utf-8"></script>
    <script type="text/javascript">
        $(function() {
            $("#signUpCheckForm").submit(function() {
                $.signin.signup_check_action();
                return false;
            });

            $("#signInForm").submit(function() {
                $.signin.login();
                return false;
            });
            $("#uid").val(storage.get('uid'));
        });
    </script>
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Welcome to HIMH</h3>
    
            <p>Login in. To see it in action.</p>

            <form class="m-t" role="form" id="signInForm">
            <input type="hidden" name="mode" value="signin" />

                <div class="form-group">
                    <input name="uid" id="uid" type="text" class="form-control" placeholder="아이디(휴대전화)" required="">
                </div>
                <div class="form-group">
                    <input  name="psw" id="psw" type="password" class="form-control" placeholder="비밀번호" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="../account/findAccount.php"><small>아이디/비밀번호 찾기</small></a>
            </form>
            <p class="m-t"> <small>HIMH &copy; 2019</small> </p>
        </div>
    </div>
</body>
</html>
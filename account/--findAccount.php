<?php
$html_js = <<<EOF
	<script src="../js/jquery.signin.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		$(function() {

		});
	</script>
EOF;

$html_css = <<<EOF
	<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
EOF;
require_once("../_lib/__login_header.php");
?>



<div id="login">
	<img src="../images/logo.png" class="logo"/>
	<form id="phoneAuthForm">
		<div class="box" id="phoneCheck">
			<div class="summary">회원가입확인은 휴대전화번호로 확인할 수 있습니다. </div>
			<input placeholder="휴대전화" name="phone" type="text" id="phone" value=""  class="auth" style="width:120px;"  />
			<a href="javascript:$.signin.isAccount();" class="authBtn" >계정확인</a>
		</div>

        <div class="box" id="phoneAuth" style="display:none;">
			<div class="summary">회원가입되어 있는 계정입니다. <br /> 휴대폰 인증이 필요합니다. </div>
			<a href="javascript:$.signin.phoneAuth();" class="authBtn2" id="phoneAuthKeyBtn" >인증번호 보내기</a>
		</div>


        <div class="box" id="phoneAuthKey" style="display:none;">
			<div class="summary">회원가입되어 있는 계정입니다. <br /> 휴대폰 인증이 필요합니다. </div>
			<input placeholder="인증번호" name='key' value="" type='text' class="auth" maxlength="6" style="width:120px;" />
			<a href="javascript:$.signin.phoneAuthAction();" class="authBtn" id="phoneAuthKeyBtn" >인증번호 확인</a>
		</div>

		<div class="box" id="phoneAuthUsrPsw" style="display:none;" >
			<div class="summary">새로운 비밀번호를 입력하세요. </div>
			<div class="box">
				<input placeholder="비밀번호" name='psw' id='psw' value="" type='text' class="auth" style="width:120px;" />
			</div>
			<div class="box">
				<input placeholder="비밀번호 확인" name='psw_check' id='psw_check' value="" type='text' class="auth" style="width:120px;" />
			</div>
			<a href="javascript:$.signin.modifyPswAction();" class="authBtn" >비밀번호 변경</a>
		</div>

	</form>
</div>


<?php
require_once("../_lib/__login_footer.php");
?>
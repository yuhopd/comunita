<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");

if($_COOKIE["savedID"]){
    $saveEmail = $_COOKIE["savedID"];
}else{
	$saveEmail = "";
}
if(!$_SESSION['sys_id']){
?>
<div class="login_wrap">
    <div class="tabs">
		<ul>
			<li class="sel" onclick="$.signin.signin_form();">로그인</li>
			<!-- <li><a href="#" onclick="javascript: $.signin.openid_form(); return false;">오픈 ID</a></li> -->
			<li onclick="$.signin.signup_form();">회원가입</li>
		</ul>
		<div class="clear"></div>
    </div>
	<div class="login_form">
		<form id="signInForm">
			<input type="hidden" name="mode" value="signin" />
			<ul>
				<li>
					<div>
						<label class="desc" for="uid" >이메일<!--  <span class="req"> *</span>  --></label>
						<input name='email' id='w_email' value="<?=$saveEmail?>" class="field text" style="width:180px;" />
					</div>
				</li>
				<li>
					<div>
						<label class="desc" for="psw" >비밀번호<!--  <span class="req"> * </span> --></label>
						<input type="password" name="psw" id="psw" value="" class="field text" style="width:180px;" />
					</div>
				</li>
				<li>
					<div style="margin-top:5px;" >
						<input type="submit" value="로그인" class="ui-state-default ui-corner-all signin_button"  /> 
					</div>
				</li>
				<li>
					<div style="margin-top:5px;">
					<input name="saveID" id="saveID" value="Y" type="checkbox" <?php if($_COOKIE["saveID"] == "Y"){?> checked <?php } ?> /> <label for="saveID" class="text">이메일 저장</label>
					&nbsp;&nbsp;&nbsp;
					<input name="AutoSignIn" id="AutoSignIn" value="Y" type="checkbox" <?php if($_COOKIE["AutoSignIn"] == "Y"){?> checked <?php } ?> /> <label for="AutoSignIn" class="text">자동 로그인</label>
					</div>
				</li>
				<!--li>
					<div>
						<a href="#" class="">회원가입</a> | <a href="#">이메일/비밀번호 찾기</a>
					</div>
				</li -->
			</ul>
		</form>
	</div>
</div>
<?php
}else{
	$usr = UsrManager::getUsr($_SESSION['sys_id']);
?>

<div class="login_wrap">

	<div class="usr_info">
		<?php
		$query = "SELECT `rename`,`exp` FROM `usr_img` WHERE `usr_id`='{$usr[id]}'";
		$res   = $db->query($query);
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
		?>
			<div class="thumb"><a href="#" onClick="javascript:usr_info(<?=$_GET[id]?>); return false;"><img src="../box_data/usr_img/<?=$row['rename']?>.<?=$row['exp']?>"></a></div>
		<?php }else{ ?>
			<div class="thumb"><a href="#" onClick="javascript:usr_info(<?=$_GET[id]?>); return false;"><img src="../images/view_usr_photo.gif"></a></div>
		<?php } ?>
		<div class="info" style="width:150px;">
			<p class="name"><?=$usr['name']?></p>
			<?php if($_SESSION['sys_id']){?>
			<p class="email"><?=$usr['email']?></p>
			<p class="phone"><?=$usr['phone']?></p>
			<?php } ?>
			<?php if($usr['birth'] != '0000-00-00'){ ?>
			<p class="birth"><?=$usr['birth']?></p>
			<?php } ?>
		</div>
	</div>
	<div>
	<span class="groups ui-corner-all" <?=$sameage_sel?>><?=$usr['sameage']?></span>
	<?php
	$query0 = "SELECT g.`id`, g.`title` FROM `group` AS g LEFT JOIN `usr_group` AS ug ON g.id = ug.group_id WHERE ug.usr_id='{$usr[id]}'";
	$res0   = $db->query($query0);
	while($res0->fetchInto($row0,DB_FETCHMODE_ASSOC)){
		if($row0['id'] == $_GET['group_id']){
			$sel = "style='color:#ff0000;'";
		}else{
			$sel = "";
		}
	?>
	<span class="groups ui-corner-all"><?=$row0['title']?></span>
	<?php } ?>
	</div>
</div>


</body>

</html>
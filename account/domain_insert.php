<?php
error_reporting(E_ALL);

ini_set("display_errors", 1);

require_once("./_sign_inner_header.php");
require_once("../_classes/class.DomainManager.php");

$isAuth = 'X';

$pl    = new DomainManager(null);
?>

    <script type="text/javascript">
        $(function() {
            $("#domain-insert").submit(function() {
                $.signin.domain_insert();
                return false;
            });
        });
    </script>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>교회 등록</h3>
            <p>교회정보를 입력해 주세요.</p>
            <form class="m-t" role="form" id="domain-insert">
                <h4 class="text-left">* 교회 정보</h4>
                <div class="form-group">
                    <input id='w_church' name='church' value="1111" type="text" class="form-control" placeholder="교회 이름" required="">
                </div>
                <div class="form-group">
                    <input id='w_phone_1' name="church_tel" value="1111" type="text" class="form-control" placeholder="교회 전화번호" required="">
                </div>
                <h4 class="text-left m-t-md">* 관리자 정보</h4>
                <div class="form-group">
                    <input id='w_manager' name='manager' value="1111" type="text" class="form-control" placeholder="담당자 이름" required="">
                </div>
                <div class="form-group">
                    <input id='w_phone_2' name="manager_tel" value="1111" type="text" class="form-control" placeholder="담당자 전화번호" required="">
                </div>
                <div class="form-group">
                    <input id='w_email' name="email" type="email" value="1111@naver.com" class="form-control" placeholder="담당자 이메일(아이디)" required="">
                </div>
                <div class="form-group">
                    <input id='w_psw' name='psw' value="1111" type='password' class="form-control" placeholder="비밀번호" required="">
                </div>
                <div class="form-group">
                    <input id='w_psw_check' name='psw_check' value="1111" type='password'  class="form-control" placeholder="비밀번호 확인" required="">
                </div>
                <input id='w_mode' name='mode' value="domainInsert" type='hidden'  class="form-control">
                <input id='w_uid' name='uid' value="1111" type='hidden'  class="form-control">


                <div class="form-group">
                    <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">회원가입</button>

                <p class="text-muted text-center"><small>이미 등록하셨나요?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="../account/">로그인 하기</a>
            </form>
            <p class="m-t"> <small>HIMH &copy; 2018</small> </p>
        </div>
    </div>



<?php
require_once("./_sign_inner_footer.php");
?>
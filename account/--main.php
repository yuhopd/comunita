<div class="main">

	<div class="top">
		mypdf는 온라인 문서관리 서비스입니다.
	</div>

	<div class="m_item">
		<div class="thumb">
			<img src="../images/main_item_01.gif" />
		</div>
		<div class="content">
			<div class="title">다양한 오피스문서를 PDF로 변환합니다.</div>
			<div class="summary">doc, ppt, xls등 MS오피스 및 오픈오피스 문서를 PDF파일로 변환해줍니다. </div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="m_item">
		<div class="thumb">
			<img src="../images/main_item_02.gif" />
		</div>
		<div class="content">
			<div class="title">오피스문서를 핸드폰으로 볼 수 있습니다.</div>
			<div class="summary">별도의 뷰어프로그램 없이도 웹브라우져에서 문서를 바로 확인할 수있습니다. (풀브라우징이 지원되는 디바이스면 문서를 볼 수 있습니다.)</div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="m_item">
		<div class="thumb">
			<img src="../images/main_item_03.gif" />
		</div>
		<div class="content">
			<div class="title">친구들과 문서를 공유할 수 있습니다.</div>
			<div class="summary">친구들과 폴더를 공유할 수 있고, 문서를 업로드하면 메세지를 남길 수 있어서 문서를 통한 협업이 가능합니다.</div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="m_item">
		<div class="thumb">
			<img src="../images/main_item_04.gif" />
		</div>
		<div class="content">
			<div class="title">언제 어디서나 접속해서 확인할 수 있습니다.</div>
			<div class="summary">인터넷이 가능한 곳은 어디서나 mypdf를 통해 문서관리, 공유, 웹하드 기능을 사용하실 수 있습니다.</div>
		</div>
		<div class="clear"></div>
	</div>
    <br />
</div>
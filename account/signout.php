<?php
$html_title = "nizBox : Sign Out";
$html_js = <<<EOF
EOF;
session_start();
session_unset();
session_destroy();
?>
<html>
<head><title><?=$html_title?></title>
<script language="javascript">
	function goURL(){
		window.location.href = "../";
	}
</script>
</head>
<body onLoad="goURL();">
</body>
</html>
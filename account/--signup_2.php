<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");

$isAuth = 'X';
if($_GET['key'] || $_GET[key] != ''){
	$query =  "SELECT `email`, `name` FROM `usr` WHERE `key` = '{$_GET[key]}' AND `auth` = 'X'";
	$res   =& $db->query($query);
	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
	?>
		<script language="Javascript" type="text/JavaScript">
			$('#w_psw').focus();
		</script>
	<?php
		$isAuth = 'O';
		if($row['email'] == $row['name']){ $row['name'] = ''; }
		$txt_title = "회원가입 인증";
	}else{

		$txt_title = "회원가입";
		$_GET['key'] = "";
	}
	$res->free();
}else{
    $txt_title = "회원가입";
}


$usr = UsrManager::getUsr($_GET['usr_id']);
?>
<script type="text/javascript">
	$(function() {	
		$("#signUpForm").submit(function() {
			$.signin.signup_action();
			return false;
		});
	});
</script>
<div class="login_wrap">
    <div class="tabs">
		<ul>
			<li onclick="javascript: return false;"><a href="#">로그인</a></li>
			<!-- <li><a href="#" onclick="javascript: $.signin.openid_form(); return false;">오픈 ID</a></li> -->
			<li><a href="#">가입확인하기</a></li>
			<li class="sel" ><a href="#"><?=$txt_title?></a></li>
		</ul>
		<div class="clear"></div>
    </div>

	<div class="login_form">
		<form id="signUpForm">
			<input type="hidden" name="mode" value="signup" />
			<input type="hidden" name="usr_id" value="<?=$usr['id']?>" />
			<ul>
				<li>
					<div>
						<label for="w_email" class="desc">이메일</label>
						<input name='email' id='w_email' class="field text" value="<?=$usr[email]?>" type='text' style="width:180px;" />
					</div>
				</li>
				<li>
					<div>
						<label for="w_psw" class="desc">비밀번호</label>
						<input name='psw' id='w_psw' value="" type='password' class="field text" style="width:180px;" /><br />
						<span class="help">영문과 숫자 6자 이상</span>
					</div>
				</li>
				<li>
					<div>
						<label for="w_psw_check" class="desc">비밀번호 확인</label>
						<input name='psw_check' id='w_psw_check' value="" type='password' class="field text" style="width:180px;" />
					</div>
				</li>
				<li>
					<div>
						<label for="w_name" class="desc">이름</label>
						<input name='name' id='w_name' value="<?=$usr[name]?>" type='text' class="field text" style="width:180px;" />
					</div>
				</li>
				<li>
					<div>
						<label for="w_phone" class="desc">핸드폰</label>
						<input name='phone' id='w_phone' value="<?=$usr[phone]?>" type='text' class="field text" style="width:180px;" /><br />
						<span class="help">'-' 없이 입력 (01000000000)</span>
					</div>
				</li>
				<li>
					<div style="margin-top:5px;">
						<input type="submit" value="<?=$txt_title?>" class="ui-state-default ui-corner-all button_l" />
					</div>
				</li>
			</ul>
		</form>
	</div>
</div>


<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
require_once("../_lib/_inner_header.php");
require_once('../_classes/class.openid.php');
// EXAMPLE
if ($_POST['openid_action'] == "login"){ // Get identity from user and redirect browser to OpenID Server
	$openid = new SimpleOpenID;
	$openid->SetIdentity($_POST['openid_url']);
	$openid->SetTrustRoot('http://' . $_SERVER["HTTP_HOST"]);
	$openid->SetRequiredFields(array('email','fullname'));
	$openid->SetOptionalFields(array('dob','gender','postcode','country','language','timezone'));
	if ($openid->GetOpenIDServer()){
		$openid->SetApprovedURL('http://' . $_SERVER["HTTP_HOST"] . "/signin/index.php?mode=openid");  	// Send Response from OpenID server to this script
		$openid->Redirect(); 	// This will redirect user to OpenID Server
	}else{
		$error = $openid->GetError();
		// echo "ERROR CODE: " . $error['code'] . "<br>";
		// echo "ERROR DESCRIPTION: " . $error['description'] . "<br>";
	}
	exit;
}
else if($_GET['openid_mode'] == 'id_res'){ 	// Perform HTTP Request to OpenID server to validate key
	$openid = new SimpleOpenID;
	$openid->SetIdentity($_GET['openid_identity']);
	$openid_validation_result = $openid->ValidateWithServer();
	if ($openid_validation_result == true){ 		// OK HERE KEY IS VALID

		// ------------------------------------------------------------------------------

		$query = "SELECT id, email, psw, name, level FROM `usr` WHERE email='$_GET[openid_sreg_email]'";
		$res   =& $db->query($query);
		if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){



			$_SESSION['sys_id']       = $row['id'];    // member serial number.
			$_SESSION['sys_email']    = $row['email']; // member ID (email)
			$_SESSION['sys_nickname'] = $row['name'];
			$_SESSION['sys_name']     = $row['name'];
			$_SESSION['sys_level']    = $row['level'];
            ?>
			<script language="Javascript" type="text/JavaScript">
				location.replace("../mybox/index.php");
            </script>
			<?php
			exit;
		}else{
			$md5_email = md5('yuho'.$_GET['openid_sreg_email'].round(10,99));
			$query  = "INSERT INTO `usr` (`email`, `psw`, `name`, `level`, `auth`, `openid`, `key`, `reg_date`) VALUES  ('$_GET[openid_sreg_email]','$psw','$_GET['openid_sreg_fullname']',1,'X','O','$md5_email',UNIX_TIMESTAMP())";
			$result = $db->query($query);


			$query = "SELECT id, email, psw, name, level FROM `usr` WHERE email='$_GET[openid_sreg_email]'";
			$res   =& $db->query($query);
			if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
				$_SESSION['sys_id']       = $row['id'];      // member serial number.
				$_SESSION['sys_email']    = $row['email'];   // member ID (email)
				$_SESSION['sys_nickname'] = $row['name'];
				$_SESSION['sys_name']     = $row['name'];
				$_SESSION['sys_level']    = $row['level'];
				?>
				<script language="Javascript" type="text/JavaScript">
					location.replace("../mybox/index.php");
				</script>
				<?php
				exit;
			}
		}
        // ------------------------------------------------------------------------------

	}else if($openid->IsError() == true){			// ON THE WAY, WE GOT SOME ERROR
		$error = $openid->GetError();
		// echo "ERROR CODE: " . $error['code'] . "<br>";
		// echo "ERROR DESCRIPTION: " . $error['description'] . "<br>";
	}else{											// Signature Verification Failed
		// echo "INVALID AUTHORIZATION";
	}
}else if ($_GET['openid_mode'] == 'cancel'){ // User Canceled your Request
	// echo "USER CANCELED REQUEST";
}
?>

<div class="signin">
	<form action="<?echo 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"]; ?>" method="post" onsubmit="this.login.disabled=true;">
	<input type="hidden" name="openid_action" value="login">
	<input type="hidden" name="mode" value="openid">
		<h2 class="title_signin">
			<a class="submenu" href="#" onclick="return sign_in_form();">로그인</a>
			<span class="sel">오픈 ID</span>
			<a class="submenu" href="#" onclick="return sign_up_form();">회원가입</a>
		</h2>

		<p>
			<label for="open_id" class="openid_caption">오픈 ID</label>
			<input type="text" name="openid_url" id="open_id" class="openid_input" value="<?=$savedOpenID?>" />
		</p>

        <p id="sigin_msg" class="sigin_msg" style="display:none;"></p>
		<p>
			<input class="openid_button" name="login" type="submit" value="로그인" />
		</p>
	</form>
		<p class="openid_box">
			<input name="saveID" id="saveID" value="Y" type="checkbox" <?=$isChecked?>/> <label for="saveID" class="text">오픈 아이디 저장하기 </label>
		</p>
		<p class="openid_box">
			<a href="http://www.myid.net" target="_blank">오픈 아이디 만들기..</a></label>
		</p>
		<p class="openid_box">
			<input class="guest_button"  type="button" value="둘러보기..." onclick="guest_sign_in_action();" />
		</p>
</div>

<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
$html_js = <<<EOF
	<script src="../js/jquery.signin.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		$(function() {
			$("#signInForm").submit(function() {
				$.signin.login();
				return false;
			});

			$("#uid").val(storage.get('uid'));

		});
	</script>
EOF;

$html_css = <<<EOF
	<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
EOF;
require_once("../_lib/__login_header.php");
?>

	<div id="login">
    	<img src="../images/logo.png" class="logo"/>
		<form id="signInForm">
			<input type="hidden" name="mode" value="signin" />
        	<div class="input_box"> <span class="iconsweet">a</span><input placeholder="아이디(휴대전화)" name="uid" type="text" id="uid" value="" /></div>
            <div class="input_box"> <span class="iconsweet">y</span><input placeholder="비밀번호" name="psw" type="password" id="psw" /></div>
            <div> <a class="forgot_password" href="../account/findAccount.php">아이디/비밀번호 찾기</a> <input type="submit" value="로그인" /></div>
		</form>
	</div>

<?php
require_once("../_lib/__login_footer.php");
?>
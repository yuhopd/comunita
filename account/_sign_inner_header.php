<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>HIMH | Login</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="../js/jquery.signin.js" charset="utf-8"></script>
    <script type="text/javascript" src="../js/lostorage.js" charset="utf-8"></script>
    <script type="text/javascript">
        $(function() {
            $("#signUpCheckForm").submit(function() {
                $.signin.signup_check_action();
                return false;
            });
        });
    </script>
</head>

<body class="gray-bg">
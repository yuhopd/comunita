var debug = require('debug')('0-node:server');
var http = require('http');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var fs = require('fs-extra');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var router = require('./routes/Router');
var admin = require('./routes/Admin');
var cms = require('./routes/Cms');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(process.cwd() , 'public')));

app.use(express.static('www'));
// app.use(express.static(path.join(__dirname, 'public')));
var localConfig = fs.readFileSync(path.resolve('_conf/local.json'));
global.config = JSON.parse(localConfig);

// console.log(global.config);

var session = require('express-session');
app.use(session({
    secret: '@#@$MYSIGN#@$#$',
    resave: false,
    saveUninitialized: true
}));
app.use('/', router);


var port = normalizePort(process.env.PORT || '8001');
app.set('port', port);

console.log('port', port);

var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.log(error);
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
}

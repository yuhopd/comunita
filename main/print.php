
<?php
require_once("../_lib/common_function.php");
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.MessageManager.php");
session_start();
$html_metas["title"] = (isset($custom_title)) ? $custom_title : "HIMH";
$html_metas["description"] = (isset($custom_description)) ? $custom_description : "himh.net, iles anytime, anywhere.";
$html_metas["keywords"] = (isset($custom_keywords)) ? $custom_keywords : "CRM, Online web office";
$html_metas["author"] = (isset($custom_author)) ? $custom_author : "himh";
if(!$_SESSION['sys_id']){ header("Location: ../account/index.php"); }
$db =  DBConnection::get()->handle();
header("Content-type: text/html; charset=utf-8");
if($_SESSION['sys_id']){
	$strNoMessageRead = "";
	$noMessageRead = MessageManager::getNoReadCount($_SESSION['sys_id']);
    if($noMessageRead > 0){
		$strNoMessageRead = "(".$noMessageRead.")";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>HIMH</title>
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="YES" />
    
    <meta name="description" content="<?=$html_metas['description']?>" />
    <meta name="keywords" content="<?=$html_metas['keywords']?>" />
    <meta name="author" content="<?=$html_metas['author']?>" />

    <link rel="stylesheet" type="text/css" href="../css/himh.css" />

</head>
<body class="print">
<?php
$usr = UsrManager::getUsr($_GET['id']);
$phone = Common::getPhone($usr['phone']);



$usrStatus    = UsrManager::$usrStatus;
$usrStatusVal = array_values($usrStatus);
$usrStatusKey = array_keys($usrStatus);

$usrPosition    = UsrManager::$usrPosition;
$usrPositionVal = array_values($usrPosition);
$usrPositionKey = array_keys($usrPosition);

$usrChristen    = UsrManager::$usrChristen;
$usrChristenVal = array_values($usrChristen);
$usrChristenKey = array_keys($usrChristen);

$address = $usr['address_1']." ".$usr['address_2'];
?>
<div class="printWrap">
<div class="usr_info">
    <div class="photo">
		<div id="usrImgThumb">
		<?php
		$thumb = UsrManager::getUsrThumb($_GET['id']);
		if($thumb){
			if($thumb['width'] > $thumb['height']){ $_class = "landscape-lg"; }else{ $_class = "portrait-lg"; }	
		?>
			<a class="<?=$_class?>" data-fancybox="gallery" href="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>.<?=$thumb['ext']?>"><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>_small.<?=$thumb['ext']?>?v=<?=time()?>" /></a>
		<?php }else{ ?>
			<div class="thumb-lg" ><img src="../css/images/member.png" /></div>
		<?php } ?>
		</div>
		<?php if($thumb){ ?>
		<a class="btn btn-primary btn-xs" action="usrImgThumbEdit" usrid="<?=$_GET['id']?>" style="margin-top:10px;">이미지편집</a>
		<?php } else { ?>
		<a class="btn btn-primary btn-xs" action="usrImgThumbUpload" usrid="<?=$_GET['id']?>" style="margin-top:10px;">이미지업로드</a>
		<?php } ?>
    </div>
	<form id="userInfoForm">
	<input type="hidden" name="usr_id" value="<?=$usr['id']?>" />
	<input type="hidden" name="mode" value="usr_modify" />
	<table class="info">
		<tr>
            <td class="label">이름</td>
			<td class="value"><input type="text" name="name" value="<?=$usr['name']?>" disabled="disabled" /></td>
			<td class="label">생년월일</td>
			<td class="value">
				<input id="birth" name="birth" type="text" style="width:80px; margin-right:5px;" value="<?=$usr['birth']?>" disabled="disabled" />
				<select id="lunar" name="lunar" disabled="disabled" >
					<option value="X" <?php if($usr['lunar'] == "X"){?> selected <?php } ?>>양력</option>
					<option value="O" <?php if($usr['lunar'] == "O"){?> selected <?php } ?>>음력</option>
				</select>
			</td>
			<td rowspan="7" class="toolArea">

				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="ustInfoPrint" target="_blank" href="print.php?id=<?=$usr['id']?>"><i class="fa fa-print"></i>&nbsp;&nbsp;인쇄</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="smsSend"  href="#"><i class="fa fa-commenting-o"></i>&nbsp;&nbsp;문자</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrInfoModify" href="#" id="usrInfoModifyWright"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;수정</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrModifyAction" href="#"  id="usrInfoModifySave" style="display:none;"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;저장</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrInfoModifyCancel" href="#" id="usrInfoModifyCancel" style="display:none;"><i class="fa fa-ban"></i>&nbsp;&nbsp;취소</a>
				<!-- a class="btn" href="javascript:;"><span class="iconsweet">V</span> 출력</a //-->
				<?php if($_SESSION['sys_level'] > 2){ ?>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrModifyPsw" href="#"><i class="fa fa-user"></i>&nbsp;&nbsp;계정</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrModifyAuth" href="#"><i class="fa fa-lock"></i>&nbsp;&nbsp;권한</a>
				<a class="btn btn-xs btn-default" usrid="<?=$usr['id']?>" action="usrDeleteAction" href="#"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;삭제</a>
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td class="label">휴대폰</td>
			<td class="value"><input type="text" name="phone" value="<?=$phone?>" disabled="disabled" /></td>
			<td class="label">성별</td>
			<td class="value">
				<select id="gender" name="gender" disabled="disabled" >
					<option value="M" <?php if($usr['gender'] == "M"){?> selected <?php } ?>>남자</option>
					<option value="F" <?php if($usr['gender'] == "F"){?> selected <?php } ?>>여자</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="label">전화</td>
			<td class="value"><input type="text" name="tel" value="<?=$usr['tel']?>" disabled="disabled" /></td>
			<td class="label">세대주</td>
			<td class="value"><input type="text" name="householder" value="<?=$usr['householder']?>" disabled="disabled" /></td>
		</tr>
		<tr>
			<td class="label">이메일</td>
			<td class="value"><input type="text" name="email" value="<?=$usr['email']?>" disabled="disabled" /></td>
			<td class="label">차량번호</td>
			<td class="value"><input type="text" name="car_number" value="<?=$usr['car_number']?>" disabled="disabled" /></td>
		</tr>
		<tr>
			<td class="label">동기(기수)</td>
			<td class="value"><select id="sameage" name="sameage_id" style="width:150px;" disabled="disabled">
					<option value="0">선택하세요.</option>
					<?php
					$sameage = UsrManager::getSameageList();
                    for($i = 0; $i < count($sameage); $i++){
					?>
						<option value="<?=$sameage[$i]['id']?>" <?php if($sameage[$i]['id'] == $usr['sameage_id']){?> selected <?php } ?>><?=$sameage[$i]['title']?> (<?=$sameage[$i]['gisu']?> 기)</option>
					<?php } ?>
				</select>
			</td>
			<td class="label">직분</td>
			<td class="value">
            <select id="position" name="position" disabled="disabled">
				<?php for ($i=0 ; $i<count($usrPositionKey); $i++){ ?>
					<option value="<?=$usrPositionKey[$i]?>" <?php if($usrPositionKey[$i] == $usr['position']){?> selected <?php } ?>><?=$usrPositionVal[$i]?></option>
				<?php } ?>
			</select>
			</td>

		</tr>

		<tr>
			<td class="label">구분</td>
			<td class="value">
				<select name="status" disabled="disabled">
					<?php for ($i=0 ; $i<count($usrStatusKey); $i++){ ?>
						<option value="<?=$usrStatusKey[$i]?>" <?php if($usrStatusKey[$i] == $usr['status']){?> selected <?php } ?>><?=$usrStatusVal[$i]?></option>
					<?php } ?>
				</select>
			</td>
			<td class="label">신급</td>
			<td class="value">
				<select name="christen" disabled="disabled">
					<?php for ($i=0 ; $i<count($usrChristenKey); $i++){ ?>
						<option value="<?=$usrChristenKey[$i]?>" <?php if($usrChristenKey[$i] == $usr['christen']){?> selected <?php } ?>><?=$usrChristenVal[$i]?></option>
					<?php } ?>
				</select>
			</td>
		</tr>

			<!-- td class="label">결혼관계</td>
			<td class="value"><input type="text" name="name" value="<?=$usr['name']?>" /></td -->
			<!-- td class="label">Twitter</td>
			<td class="value"><div class="val">&nbsp;</div></td -->
			<!-- td class="label">세례여부</td>
			<td class="value"><div class="val">&nbsp;</div></td -->
			<!-- td class="label">Facebook</td>
			<td class="value"><div class="val">&nbsp;</div></td -->
			<!-- td class="label">기타</td>
			<td class="value"><div class="val">&nbsp;</div></td -->
		<tr>
			<td class="label">주소</td>
			<td class="address" colspan="3">
				<input type="text" name="address_1" value="<?=$usr['address_1']?>" style="width:190px;" disabled="disabled" />
				<input type="text" name="address_2" value="<?=$usr['address_2']?>" style="width:180px;" disabled="disabled" />
			</td>
		</tr>
		<tr>
			<td class="label">등록일</td>
			<td class="address" colspan="3">
				<input name="register" type="text" value="<?=$usr['register']?>" disabled="disabled" />
			</td>
		</tr>
	</table>
	</form>
</div>

<?php
$_GET['usr_id'] = $_GET['id'];
?>

<div id="usrDetailInfo">
    <div class="ibox">
    <div class="ibox-title">
        <h5>가족관계</h5>
                    </div>
    <div class="ibox-content">
    
<?php
require_once("_familyList.php");
?>
</div>
</div>
</div>
<div id="usrGroupList">
<?php
require_once("_usrGroupList.php");
?>
</div>
<div id="usrVisitList">
<?php
$_GET['usr_id'] = $_GET['id'];
require_once("_usrVisitList.php");
?>
</div>
<div id="usrAttendanceList"></div>
<div id="usrNote"></div>

<!-- div id="usrPhotoList"></div -->
<div id="usrMap"></div>		

</div>
      
</body>
</html>

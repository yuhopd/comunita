<?php
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.VisitManager.php");
$usrPosition = UsrManager::$usrPosition;
if($_GET['mode'] == "modify"){
	$visit    = VisitManager::getItem($_GET['id']);
	$usr      = UsrManager::getUsr($visit['usr_id']);
	$visitUsr = UsrManager::getUsr($visit['visit_usr_id']);
	$writeUsr = UsrManager::getUsr($visit['write_usr_id']);
	$_GET['usr_id'] = $visit['usr_id'];

}else{
	$usr      = UsrManager::getUsr($_GET['usr_id']);
	$visitUsr = UsrManager::getUsr($_SESSION['sys_id']);
	$writeUsr = UsrManager::getUsr($_SESSION['sys_id']);
	$_GET['id'] = 0;
}
?>
<script type="text/javascript">
	$(document).ready(function() {


	});
</script>

<form id="visitForm" class="form-horizontal">
<?php if($_GET['mode'] == "modify"){ ?>
	<input type='hidden' name='id' value="<?=$_GET['id']?>">
	<input type='hidden' name='mode' value="visitModify" />
<?php }else{ ?>
	<input type='hidden' name='mode' value="visitInsert" />
<?php } ?>

		<div class="form-group"> 
			<label class="col-md-3 control-label" >피심방자</label>
			<div class="col-md-9">
				<div class="input-group">
					<input type='hidden' id="usr_id" name="usr_id" value="<?=$usr['id']?>" />
					<div class="inputValue form-control usrGroupBoxList" id="usr_name">
						<?php
						if($visit['usr_id'] != ""){
						$list = array_unique(explode("|",$visit['usr_id']));
							for($i=0; $i<count($list); $i++ ) {
								$visitUsr = UsrManager::getUsr($list[$i]);
							?>
							<div class="usrGroup" usrId="<?=$visitUsr['id']?>">
								<a href="#" action="userInfo" usrId="<?=$visitUsr['id']?>"><?=$visitUsr['name']?></a>
								
								<a class="remove" action="removeUsr" mode="usr_name" usrId="<?=$visitUsr['id']?>"><i class="fa fa-times"></i></a>
							</div>
							<?php
							}
						unset($list);
						}
						?>
					
					
					</div>

					<span class="input-group-btn">
						<a action="searchContact" mode="usr_id"  class="btn btn-primary">선택</a>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">심방자</label>
			<div class="col-md-9">
				<div class="input-group">
					<input type='hidden' id="visit_usr_id" name="visit_usr_id" value="<?=$visitUsr['id']?>" />
					<div class="inputUsrValue form-control usrGroupBoxList" id="visit_usr_name">
						<?php
						if($visit['visit_usr_id'] != ""){
						$list = array_unique(explode("|",$visit['visit_usr_id']));
							for($i=0; $i<count($list); $i++ ) {
								$visitUsr = UsrManager::getUsr($list[$i]);
							?>
							<div class="usrGroup" usrId="<?=$visitUsr['id']?>">
								<a href="#" action="userInfo" usrId="<?=$visitUsr['id']?>"><?=$visitUsr['name']?></a>
								<a class="remove" action="removeUsr" mode="visit_usr_name" usrId="<?=$visitUsr['id']?>"><i class="fa fa-times"></i></a>
							</div>
							<?php
							}
						unset($list);
						}
						?>
					</div>
			        <span class="input-group-btn">
						<a action="searchContact" mode="visit_usr_id" class="btn btn-primary">선택</a>
					</span>
				</div>
			</div>	
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">작성자</label>
			<div class="col-md-9">
			<input type='hidden' id="write_usr_id" name="write_usr_id" value="<?=$writeUsr['id']?>" />
				<div class="inputValue form-control">
					<a href="#" action="userInfo" usrId="<?=$writeUsr['id']?>"><?=$writeUsr['name']?></a> <span style="color:#999;"><?=$usrPosition[$writeUsr['position']]?></span>
				</div>
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">심방종류</label>
			<div class="col-md-9">
			<select id="type" name="type" class="form-control">
				<option value="0">선택하세요.</option>
				<?php
				$visitType    = VisitManager::$arryVisitType;
				$visitTypeVal = array_values($visitType);
				$visitTypeKey = array_keys($visitType);
				for($i = 0; $i < count($visitTypeKey); $i++){
				?>
					<option value="<?=$visitTypeKey[$i]?>" <?php if($visitTypeKey[$i] == $visit['type']){?> selected <?php } ?>><?=$visitTypeVal[$i]?></option>
				<?php
				}
				?>
			</select>
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">일시</label>
			<div class="col-md-9">
				<input type="text" name="reg_date" value="<?=$visit['reg_date']?>" id="datepicker" class="form-control" />
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">장소</label>
			<div class="col-md-9">
				<input type="text" id="place" name="place"  value="<?=$visit['place']?>" class="form-control"/>
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">성경본문</label>
			<div class="col-md-9">
				<input type="text" id="title" name="title"  value="<?=$visit['title']?>" class="form-control" />
			</div>
		</div>
		<div class="form-group"> 
			<label class="col-md-3 control-label">내용</label>
			<div class="col-md-9">
				<textarea id="content" name="content" style="height:200px;" class="form-control" ><?=$visit['content']?></textarea>
			</div>
		</div>
		<div class="form-group"> 
			<div class="photoBoxList">
				<div id="photoBox" class="thumbList"></div>
				<div id="photoBoxUploader" class="uploadBtn"></div>
			</div>
		</div>
</form>

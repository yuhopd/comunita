<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.GroupManager.php");



if($_GET['mode'] == 'modify'){
	$usrGroup = UsrManager::getUsrGroup($_GET['id']);

	$usr = UsrManager::getUsr($usrGroup['usr_id']);
	$group = GroupManager::getGroup($usrGroup['group_id']);

}
?>

<div class="settings">
	<form id="usrGroupForm">
	<input type="hidden" name="mode" value="usrGroupModify">
	<input type="hidden" name="id" value="<?=$_GET['id']?>">
	<ul class="form" style="padding:0;">
		<li>
		    <div class="formInput">
				<label for="group_title">소속명</label>
				<div class="value"><?=$group['title']?></div>
			</div>
		</li>
		<li>
		    <div class="formInput">
				<label for="group_title">이름</label>
				<div class="value"><?=$usr['name']?></div>
			</div>
		</li>
		<li>
		    <div class="formInput">
			<label for="level">권한</label>
			<select name="level" style="width:200px;">
				<option value="0" <?php if($usrGroup['level'] == 0){?> selected <?php } ?>>그룹회원</option>
				<option value="1" <?php if($usrGroup['level'] == 1){?> selected <?php } ?>>그룹리더</option>
				<option value="2" <?php if($usrGroup['level'] == 2){?> selected <?php } ?>>그룹관리자</option>
			</select>
			</div>
		</li>
		<li>
		    <div class="formInput">
			<label for="duty">직책</label>
			<input type="text" name="duty" value="<?=$usrGroup['duty']?>" style="width:200px;" />
			</div>
		</li>
	</ul>
	</form>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
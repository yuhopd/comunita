<?php
require_once("../_lib/_inner_header.php");
require_once('../_classes/class.GroupManager.php');
$root_id = GroupManager::getRootIdFromUsr($_SESSION['sys_id']);
if($_SESSION['sys_level'] > 2 ){
    $query1 = "SELECT * FROM `group` WHERE `parent_id`='0' ORDER BY `sequence` ASC";
}else{
    $query1 = "SELECT * FROM `group` WHERE `id`='$root_id'";
}
$res1 = $db->query($query1);
$row1 = $res1->fetchAll(PDO::FETCH_ASSOC);
if($row1){ ?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>그룹관리</h2>
		<ol class="breadcrumb">
			<li>
				<?=$group['title']?>
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">
			<a class="btn btn-default btn-sm" href="">전체보기</a>
            <a class="btn btn-default btn-sm" href="javascript:$.attendance.panoramaView();">전체보기</a>
		</div>
	</div>
</div>


<div class="row wrapper wrapper-content animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
        <div class="dd" id="adminGroupNestable">
        <ol class="dd-list">
            <?php 
            foreach($row1 as $v1){ ?>
                <li class="dd-item" group="<?=$v1['id']?>">
                    <div class="dd-handle" group-id="<?=$v1['id']?>">
                        <?=$v1['title']?>
                        <?php if($v1['use_calendar'] == "O"){ ?>
                            <span class="label"><i class="fa fa-calendar"></i></span>
                        <?php } ?>
                        <?php if($v1['use_attendance'] == "O"){ ?>
                            <span class="label"><i class="fa fa-address-book-o"></i></span>
                        <?php } ?>
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                
                                <li><a href="#" group="<?=$v1['id']?>" action="modify">그룹정보수정</a></li>
                                <li><a href="#" group="<?=$v1['id']?>" action="userSettings">그룹회원설정</a></li>
                                <li><a href="#" group="<?=$v1['id']?>" action="childAdd">하위그룹추가</a></li>
                                <li><a href="#" group="<?=$v1['id']?>" action="del">그룹삭제</a></li>
                            </ul>
                        </div>
                    
                    </div>

                    

                    <?php
                    $query2 = "SELECT * FROM `group` WHERE `parent_id`='{$v1[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
                    $res2   = $db->query($query2);
                    $row2   = $res2->fetchAll(PDO::FETCH_ASSOC);
                    if($row2){ ?>
                    <ol class="dd-list">
                    <?php
                    foreach($row2 as $v2){?>
                        <li class="dd-item" group="<?=$v2['id']?>"><div class="dd-handle" group-id="<?=$v2['id']?>">
                        <?=$v2['title']?>
                            
                            <?php if($v2['use_calendar'] == "O"){ ?>
                            <span class="label"><i class="fa fa-calendar"></i></span>
                            <?php } ?>
                            <?php if($v2['use_attendance'] == "O"){ ?>
                            <span class="label"><i class="fa fa-address-book-o"></i></span>
                            <?php } ?>
                            
                            <div class="btn-group pull-right">
                                <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    
                                    <li><a href="#" group="<?=$v2['id']?>" action="modify">그룹정보수정</a></li>
                                    <li><a href="#" group="<?=$v2['id']?>" action="userSettings">그룹회원설정</a></li>
                                    <li><a href="#" group="<?=$v2['id']?>" action="childAdd">하위그룹추가</a></li>
                                    <li><a href="#" group="<?=$v2['id']?>" action="del">그룹삭제</a></li>
                                </ul>
                            </div>
                        </div>
                        <?php                                        
                            $query3  = "SELECT * FROM `group` WHERE `parent_id`='{$v2[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
                            $res3    = $db->query($query3);		
                            $row3    = $res3->fetchAll(PDO::FETCH_ASSOC);
                            if($row3){ ?>
                            <ol class="dd-list">
                            <?php
                            foreach($row3 as $v3){?>
                                <li class="dd-item" group="<?=$v3['id']?>"><div class="dd-handle" group-id="<?=$v3['id']?>"><?=$v3['title']?>
                                    <?php if($v3['use_calendar'] == "O"){ ?>
                                    <span class="label"><i class="fa fa-calendar"></i></span>
                                    <?php } ?>
                                    <?php if($v3['use_attendance'] == "O"){ ?>
                                    <span class="label"><i class="fa fa-address-book-o"></i></span>
                                    <?php } ?>
                                    <div class="btn-group pull-right">
                                        <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            
                                            <li><a href="#" group="<?=$v3['id']?>" action="modify">그룹정보수정</a></li>
                                            <li><a href="#" group="<?=$v3['id']?>" action="userSettings">그룹회원설정</a></li>
                                            <li><a href="#" group="<?=$v3['id']?>" action="childAdd">하위그룹추가</a></li>
                                            <li><a href="#" group="<?=$v3['id']?>" action="del">그룹삭제</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php                                        
                                    $query4  = "SELECT * FROM `group` WHERE `parent_id`='{$v3[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
                                    $res4    = $db->query($query4);		
                                    $row4    = $res3->fetchAll(PDO::FETCH_ASSOC);
                                    if($row4){ ?>
                                    <ol class="dd-list">
                                    <?php
                                    foreach($row4 as $v4){?>
                                        <li class="dd-item" group="<?=$v4['id']?>"><div class="dd-handle" group-id="<?=$v4['id']?>"><?=$v4['title']?>
                                            
                                            <?php if($v4['use_calendar'] == "O"){ ?>
                                            <span class="label"><i class="fa fa-calendar"></i></span>
                                            <?php } ?>
                                            <?php if($v4['use_attendance'] == "O"){ ?>
                                            <span class="label"><i class="fa fa-address-book-o"></i></span>
                                            <?php } ?>
                                            <div class="btn-group pull-right">
                                                <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                   
                                                    <li><a href="#" group="<?=$v4['id']?>" action="modify">그룹정보수정</a></li>
                                                    <li><a href="#" group="<?=$v4['id']?>" action="userSettings">그룹회원설정</a></li>
                                                    <li><a href="#" group="<?=$v4['id']?>" action="childAdd">하위그룹추가</a></li>
                                                    <li><a href="#" group="<?=$v4['id']?>" action="del">그룹삭제</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php                                        
                                            $query5  = "SELECT * FROM `group` WHERE `parent_id`='{$v4[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
                                            $res5    = $db->query($query5);		
                                            $row5    = $res5->fetchAll(PDO::FETCH_ASSOC);
                                            if($row5){ ?>
                                            <ol class="dd-list">
                                            <?php
                                            foreach($row5 as $v5){?>
                                                <li class="dd-item" group="<?=$v5['id']?>"><div  class="dd-handle" group-id="<?=$v5['id']?>"><?=$v5['title']?>
                                                    
                                                    <?php if($v5['use_calendar'] == "O"){ ?>
                                                    <span class="label"><i class="fa fa-calendar"></i></span>
                                                    <?php } ?>
                                                    <?php if($v5['use_attendance'] == "O"){ ?>
                                                    <span class="label"><i class="fa fa-address-book-o"></i></span>
                                                    <?php } ?>

                                                    <div class="btn-group pull-right">
                                                        <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            
                                                            <li><a href="#" group="<?=$v4['id']?>" action="modify">그룹정보수정</a></li>
                                                            <li><a href="#" group="<?=$v4['id']?>" action="userSettings">그룹회원설정</a></li>
                                                            <li><a href="#" group="<?=$v4['id']?>" action="childAdd">하위그룹추가</a></li>
                                                            <li><a href="#" group="<?=$v4['id']?>" action="del">그룹삭제</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                </li>
                                            <?php
                                            }
                                            ?>
                                            </ol>
                                            <?php
                                            }
                                            ?>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                    </ol>
                                    <?php
                                    }
                                    ?>
                                </li>
                            <?php
                            }
                            ?>
                            </ol>
                            <?php
                            }
                            ?>
                        </li>
                    <?php
                    }
                    ?>
                    </ol>
                    <?php
                    }
                    ?>
                </li>
            <?php
            }
            ?>
        </ol>
        </div>
        <?php
        }
        ?>

        </div>
    </div>
</div>

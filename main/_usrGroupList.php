<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.GroupManager.php");
?>
<?php
#-- last edit 2006.07.31
	$properties = array(
		"usr_id"   => $_GET['usr_id'],
		"type"     => "usr_group_list"
	);
	$pl    = new UsrPhoneList($properties);
	$list  = $pl->getList('X');

	if($list != false){
		$count = count($list);
	}else{
		$count = 0;
	}
?>
<?php if($_GET['mode'] != "callback"){ ?>

<div id="usrGroupCallback">
<?php } ?>

<div class="ibox ">
	<div class="ibox-title">
		<h5>소속정보</h5>
		<div class="ibox-tools">
			<select id="group_sel" name="group_sel" style="width:150px;">
				<option value="0">선택하세요.</option>
				<?php
				$query1 = "SELECT * FROM `group` WHERE `parent_id`='0' AND `is_enable`='O' ORDER BY `sequence` ASC";
				$res1   = $db->query($query1);
				while($row1 = $res1->fetch(PDO::FETCH_ASSOC)){
					?>
					<option value="<?=$row1['id']?>" ><?=$row1['title']?></option>
					<?php
					$query2  = "SELECT * FROM `group` WHERE `parent_id`='{$row1[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
					$res2    = $db->query($query2);
					while($row2 = $res2->fetch(PDO::FETCH_ASSOC)){?>
						<option value="<?=$row2['id']?>" >--<?=$row2['title']?></option>
						<?php
						$query3  = "SELECT * FROM `group` WHERE `parent_id`='{$row2[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
						$res3    = $db->query($query3);
						while($row3 = $res3->fetch(PDO::FETCH_ASSOC)){ ?>
							<option value="<?=$row3['id']?>" >----<?=$row3['title']?></option>
							<?php
							$query4  = "SELECT * FROM `group` WHERE `parent_id`='{$row3[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
							$res4    = $db->query($query4);
							while($row4 = $res4->fetch(PDO::FETCH_ASSOC)){ ?>
								<option value="<?=$row4['id']?>" >------<?=$row4['title']?></option>
								<?php
								$query5  = "SELECT * FROM `group` WHERE `parent_id`='{$row4[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
								$res5    = $db->query($query5);
								while($row5 = $res5->fetch(PDO::FETCH_ASSOC)){ ?>
									<option value="<?=$row5['id']?>" >--------<?=$row5['title']?></option>
									<?php
									$query6  = "SELECT * FROM `group` WHERE `parent_id`='{$row5[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
									$res6    =& $db->query($query6);
									while($row6 = $res6->fetch(PDO::FETCH_ASSOC)){ ?>
										<option value="<?=$row6['id']?>" >--------<?=$row6['title']?></option>
									<?php
									}
								}
							}
						}
					}
				}
				?>
			</select>
			<a class="btn btn-primary btn-xs" usrid="<?=$_GET['usr_id']?>" action="usrGroupInsertAction" href="#">추가</a>
		</div>
	</div>
	<div class="ibox-content">
		<ul class="sortable-list connectList agile-list row">
				<?php for($i=0; $i<$count; $i++ ) {

					if($list[$i]['parent_id'] == 0){
						unset($root);
					}else{
						$root = GroupManager::getParentGroup($list[$i]['group_id']);
					}

				?>
            	<div class="col-md-4">
				<li class="<?php if($list[$i]['parent_id'] == 0){?>success-element <?php }else{ ?>info-element <?php } ?>">
					<?=$list[$i]['title']?>
					<div class="agile-detail">
						<a href="#" action="usrGroupDeleteAction" usrgroupid="<?=$list[$i]['id']?>" class="pull-right btn btn-xs btn-white">삭제</a>						
						<a href="#" action="usrGroupModify" usrgroupid="<?=$list[$i]['id']?>" class="pull-right btn btn-xs btn-white">수정</a>

						<?php if($root){ ?><?=$root['title']?><?php } else { ?><br> <?php } ?>
						<?php if($list[$i]['level'] > 0){ ?>
							<small><?=$list[$i]['duty']?></small>
						<?php } ?>
						
					</div>
				</li>
				</div>
				<?php } ?>
			</div>
		</ul>
	</div>

</div>




<?php if($_GET['mode'] == "callback"){ ?>
</div>

<?php } ?>

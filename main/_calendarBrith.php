<?php
//-----------------기본정보를 얻어온다.--------------------------------------//
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.CalendarManager.php");
require_once("../_classes/class.GroupManager.php");
if(!$_POST['group_id']){ $_POST['group_id'] = 0; }

$gColor = GroupManager::getUseCalendarGroupColor();


    $birth = CalendarManager::getUsrFromBirth("-".$checkBirth,$_POST['group_id']);

    if($_POST['group_id'] == 0){
        $bgColor = "#585858";
    }else{
        $bgColor = $gColor[$birth[$b]['group_id']];
    }

    $year1  = date("Y",$_POST['start']);
	$month1 = date("n",$_POST['start']);

	$month[] = $month1;
	$year[] = $year1;

	if($month1 == 12){
		$month2 = 1;
		$year2  = $year1+1;
	}else{
		$month2 = $month1 + 1;
		$year2  = $year1;
	}
	$year[]  = $year2;
	$month3 = date("n",$_POST['end']);
	if($month2 == $month3){
		$month[] = $month2;
	}else{
		if($month2 == 12){
			$month3 = 1;
			$year[]  = $year2+1;
		}else{
			$month3 = $month2 + 1;
			$year[]  = $year2;
		}
		$month[] = $month2;
		$month[] = $month3;
	}

	for($m=0;$m<=count($month);$m++){
		for($i=0;$i<=31;$i++){
			$reday = sprintf("%02d", $i);
			$remonth = sprintf("%02d", $month[$m]);
			$checkBirth = $remonth."-".$reday;
			$birth = CalendarManager::getUsrFromBirth("-".$checkBirth,$_POST['group_id']);

            if($_POST['group_id'] == 0){
				$bgColor = "#585858";
			}else{
				$bgColor = $gColor[$birth[$b]['group_id']];
			}


			for($b=0;$b<count($birth);$b++){
				$r[] = array(
					'dayType' => 'birth',
					'id'      => $birth[$b]['id'],
					'title'   => $birth[$b]['name']." 생일",
					'start'   => date(r,mktime(0,0,0,$month[$m],$i,$year[$m])),
					'textColor'   => $bgColor,
                    'color'   => "transparent",
                    'allDay'  => true
				);
			}
		}
	}

print json_encode($r);
?>
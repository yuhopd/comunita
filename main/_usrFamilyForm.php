<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.UsrManager.php");

$usr = UsrManager::getUsr($_GET['usr_id']);
$birth = str_replace ('-','.',substr($usr['birth'],-5));
?>

<script type="text/javascript">
	$(function() {
		$.contact.usrFamilyList({"box":"usrFamilyList", "usr_id":<?=$_GET['usr_id']?>});
	});
</script>
<form id="usrFamilyForm">
	<input type="hidden" id="usr_id" name="usr_id" value="<?=$_GET['usr_id']?>">
	<ul>
		<li>
		    <div>
				<span for="name" class="desc">이름 :</span>
				<span class="value"><?=$usr['name']?></span>
				<div class="tools">
					<a href="javascript:$.contact.usrFamilyInsert(<?=$_GET['usr_id']?>);" class="ui-state-default ui-corner-all button_s" >관계추가</a>
				</div>
				<div class="clear"></div>
			</div>
		</li>
		<li>
			<div id="usrFamilyList" class="familyList">
			<?php require_once("_usrFamilyList.php"); ?>
			</div>
		</li>
	</ul>
</form>
<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.UsrManager.php");
?>
<div class="form-horizontal">
	<div class="form-group"> 
		<div class="col-md-12">
			<div class="input-group">
				<input type="text" name="visit_q" id="visit_qq" value="<?=$_GET['q']?>" class="form-control input-sm"/>
				<span class="input-group-btn"><a class="btn btn-sm btn-primary" action="visitSearch" href="#" >검색</a></span>
			</div>
		</div>
	</div>
	<div class="form-group"> 
		<div class="col-md-12">
			<div id="visitSearchList" class="smsContact form-control" style="height:200px;"></div>
		</div>
	</div>
</div>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.GroupManager.php");


switch ($_REQUEST[mode])
{
    /******************************************************************************
    * 회원추가
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
    case "usr_insert":
		if($_POST['name'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "이름을 입력하세요!";
			print json_encode($r);
			exit;
		}



		$arryUsr = array(
			"domain_id"   => $_POST['domain_id'],
			"sameage_id"  => $_POST['sameage_id'],
			"email"       => $_POST['email'],
			"name"        => $_POST['name'],
			"tel"         => $_POST['tel'],
			"phone"       => $_POST['phone'],
			"address_1"   => $_POST['address_1'],
			"address_2"   => $_POST['address_2'],
			"summary"     => $_POST['summary'],
			"car_number"  => $_POST['car_number'],
			"car_type"    => $_POST['car_type'],
			"birth"       => $_POST['birth'],
			"lunar"       => $_POST['lunar'],
			"status"      => $_POST['status'],
			"householder" => $_POST['householder'],
			"christen"    => $_POST['christen'],
			"position"    => $_POST['position'],
			"gender"      => $_POST['gender'],
			"register"    => $_POST['register']
		);

        $lastID = UsrManager::insert($arryUsr);
		$r["lastID"]   = $lastID;
		$r["error"]    = -1;
		$r["message"]  = "입력되었습니다. ";
		print json_encode($r);

	break;
    /******************************************************************************
    * 회원정보 변경
    * requred params(_POST) :: usr_id, name, phone, email
    ******************************************************************************/
    case "usr_modify":
		if($_POST['name'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "이름을 입력하세요!";
			print json_encode($r);
			exit;
		}
		$arryUsr = array(
			"domain_id"   => $_POST['domain_id'],
			"sameage_id"  => $_POST['sameage_id'],
			"email"       => $_POST['email'],
			"name"        => $_POST['name'],
			"tel"         => $_POST['tel'],
			"phone"       => $_POST['phone'],
			"address_1"   => $_POST['address_1'],
			"address_2"   => $_POST['address_2'],
			"summary"     => $_POST['summary'],
			"car_number"  => $_POST['car_number'],
			"car_type"    => $_POST['car_type'],
			"birth"       => $_POST['birth'],
			"lunar"       => $_POST['lunar'],
			"status"      => $_POST['status'],
			"householder" => $_POST['householder'],
			"christen"    => $_POST['christen'],
			"position"    => $_POST['position'],
			"gender"      => $_POST['gender'],
			"register"    => $_POST['register'],
			"usr_id"      => $_POST['usr_id']
		);

        $lastID = UsrManager::modify($arryUsr);
		$r["usr_id"]   = $_POST['usr_id'];
		$r["error"]    = -1;
		$r["message"]  = "수정되었습니다.";
		print json_encode($r);

	break;


    /******************************************************************************
    * 회원정보 변경
    * requred params(_POST) :: usr_id, name, phone, email
    ******************************************************************************/
    case "usr_psw_modify":
		if($_POST['email'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "이메일을 입력하세요!";
			print json_encode($r);
			exit;
		} else if($_POST['psw'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "비밀번호를 입력하세요!";
			print json_encode($r);
			exit;
		} else if($_POST['psw_confirm'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "비밀번호 확인을 입력하세요!";
			print json_encode($r);
			exit;
		} else if($_POST['psw_confirm'] != $_POST['psw']){
			$r["error"]    = 1;
			$r["message"]  = "비밀번호와 비밀번호 확인이 일치하지 않습니다.";
			print json_encode($r);
			exit;
		}
        $psw = md5($_POST['psw']);

		$query  = "UPDATE `usr` SET `email` = '{$_POST[email]}', `psw` = '$psw', `auth` = 'O' WHERE `id` = '{$_POST[usr_id]}'";
		$res   =& $db->query($query);

		$r["error"]    = -1;
		$r["message"]  = "계정내용이 변경되었습니다.";
		print json_encode($r);

	break;

	/******************************************************************************
    * 회원정보 변경
    * required params(_POST) :: usr_id, name, phone, email
    ******************************************************************************/
    case "usr_auth_modify":
		if($_POST['email'] == ''){
			$r["error"]    = 1;
			$r["message"]  = "이메일이 입력되어야 권한을 바꿀수 있습니다!";
			print json_encode($r);
			exit;
		}

		$query  = "UPDATE `usr` SET `level` = '{$_POST[level]}', `auth` = 'O' WHERE `id` = '{$_POST[usr_id]}'";
		$res   =& $db->query($query);

		$r["error"]    = -1;
		$r["message"]  = "회원권한이 변경되었습니다.";
		print json_encode($r);

	break;

    /******************************************************************************
    * 회원그룹 추가
    * requred params(_POST) :: usr_id, group_id
    ******************************************************************************/
    case "usr_group_insert":
		if($_POST['group_id'] == 0){
			$r["error"]    = 1;
			$r["message"]  = "그룹을 선택하세요!";
			print json_encode($r);
			exit;
		}

		$query = "INSERT INTO `usr_group` (usr_id, group_id) VALUES ('$_POST[usr_id]', '$_POST[group_id]')";
		$res   =& $db->query($query);

		UsrManager::updateGroups($_POST['usr_id']);
		$r["error"]    = -1;
		$r["message"]  = "그룹이 추가 되었습니다.";
		print json_encode($r);
	break;

    /******************************************************************************
    * 회원그룹 추가
    * requred params(_GET) :: usr_group_id
    ******************************************************************************/
    case "usr_group_delete":
		$res = $db->query("SELECT `usr_id` FROM `usr_group` WHERE id='{$_GET[usr_group_id]}' ");
		$row = $res->fetch(PDO::FETCH_ASSOC);

		$usr_id = $row['usr_id'];

		$db->query("DELETE FROM `usr_group` WHERE id='{$_GET[usr_group_id]}'");

		UsrManager::updateGroups($usr_id);
    	$r["usr_id"]   = $usr_id;
		$r["error"]    = -1;
		$r["message"]  = "그룹이 삭제 되었습니다.";
		print json_encode($r);
	break;


    /******************************************************************************
    * 회원 삭제
    * requred params(_GET) :: usr_group_id
    ******************************************************************************/
    case "usr_delete":
		$db->query("DELETE FROM `usr` WHERE `id`='{$_GET[usr_id]}' LIMIT 1");
		$db->query("DELETE FROM `usr_box` WHERE `usr_id`='{$_GET[usr_id]}'");
		$db->query("DELETE FROM `usr_group` WHERE `usr_id`='{$_GET[usr_id]}'");
		$db->query("DELETE FROM `usr_img` WHERE `usr_id`='{$_GET[usr_id]}'");

		$r["usr_id"]   = $_GET['usr_id'];
		$r["error"]    = -1;
		$r["message"]  = "그룹이 삭제 되었습니다.";
		print json_encode($r);
	break;

	/******************************************************************************
    * 회원 가족 관계 삭제
    ******************************************************************************/
    case "usr_family_delete":
		UsrManager::delFamily($_GET['usr_id'],$_GET['family_id']);
		$r["error"]    = -1;
		$r["message"]  = "가족관계가 삭제되었습니다. ";
		print json_encode($r);
		exit;
	break;

	/******************************************************************************
    * 회원 가족 관계 추가
    ******************************************************************************/
    case "usr_family_insert":
        $usr = UsrManager::getUsr($_GET['usr_id']);
        $family = UsrManager::getUsr($_GET['family_id']);

        if($_GET['relations'] == 1 && $usr['gender'] == $family['gender']){
			$r["error"]    = 1;
		    $r["message"]  = "같은 성별의사람입니다. 부부로 설정할 수 없습니다.";
		    print json_encode($r);
			exit;
		}

		UsrManager::addFamily($_GET['usr_id'],$_GET['family_id'],$_GET['relations']);

		$r["error"]    = -1;
		$r["message"]  = "가족관계가 설정 되었습니다. ";
		print json_encode($r);
		exit;

	break;

    /******************************************************************************
    * 회원 이미지 업로드
    * requred params(_GET) :: friend_id
    ******************************************************************************/
    case "usr_img_upload":


		$img_path = "/home/himh/public_html/data/usr_img";
		$error    = "";
		$msg      = "";
		$fileElementName = "usr_img";
		if(!empty($_FILES[$fileElementName]['error'])) {
			switch($_FILES[$fileElementName]['error']) {
				case '1':
					$msg = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
					break;
				case '2':
					$msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
					break;
				case '3':
					$msg = 'The uploaded file was only partially uploaded';
					break;
				case '4':
					$msg = 'No file was uploaded.';
					break;
				case '6':
					$msg = 'Missing a temporary folder';
					break;
				case '7':
					$msg = 'Failed to write file to disk';
					break;
				case '8':
					$msg = 'File upload stopped by extension';
					break;
				case '999':
				default:
					$msg = 'No error code avaiable';
			}
			$r["error"]    = $_FILES[$fileElementName]['error'];
			$r["message"]  = $msg;
			print json_encode($r);
			exit;
		} elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {
			$msg = 'No file was uploaded..';

			$r["error"]    = 101;
			$r["message"]  = $msg;
			print json_encode($r);
			exit;
		} else {

				$allowedExt = array("jpg","gif","png");
				$upfileExt  = strtolower(substr(strrchr($_FILES[$fileElementName]['name'], "."),1));
				// $isImg = $db->getOne("SELECT count(*) FROM `usr_img` WHERE `usr_id`='{$_GET[usr_id]}'");
				$reNameCode = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

				if(!@in_array($upfileExt,$allowedExt)){
				    $r["error"]    = 100;
					$r["message"]  = "jpg 이미지 파일만 업로드 할 수 있습니다!";
					print json_encode($r);
					exit;
				}

				$arryUpload = array(
					"id"            => $_POST['usr_id'],
					"file_rename"   => $reNameCode,
					"file_ext"      => $upfileExt
				);
				$upload = new UsrManager($arryUpload);
				$upload_tmp_name = $_FILES[$fileElementName]['tmp_name'];
				if($upload->save_file_to_storage($upload_tmp_name)){
					$upload->updateUsrImg();
					$error = -1;
					$msg = "uploaded..";
				}else{
					$error = 100;
					$msg = "@@@@@@@";
				}
		}

	    $r["error"]    = $error;
		$r["message"]  = $msg;
		$r["code"]     = $reNameCode;
		print json_encode($r);
		exit;


	break;
	/******************************************************************************
    * upSequence
    * requred params(_POST) :: menu_id , title , summary
    ******************************************************************************/
    case "upSequence":
        UsrManager::upSequence($_GET['id']);
    break;

	/******************************************************************************
    * downSequence
    * requred params(_POST) :: menu_id , title , summary
    ******************************************************************************/
    case "downSequence":
        UsrManager::downSequence($_GET['id']);
    break;


	/******************************************************************************
    * usrGroupModify
    * requred params(_POST) :: level , duty , id
    ******************************************************************************/
    case "usrGroupModify":

		$arryUsrGroup = array(
			"level"    => $_POST['level'],
			"duty"     => $_POST['duty'],
			"id"       => $_POST['id']
		);

		UsrManager::modifyUsrGroup($arryUsrGroup);
        $usrGroup = UsrManager::getUsrGroup($_POST['id']);

		$r["usr_id"]   = $usrGroup['usr_id'];
		$r["error"]    = -1;
		$r["message"]  = "소속정보가 수정되었습니다.";
		print json_encode($r);
		exit;

	break;


	/******************************************************************************
    * downSequence
    * requred params(_POST) :: menu_id , title , summary
    ******************************************************************************/
    case "setGroupOrders":
		$arryUsr = explode("|",$_POST["sequence"]);

		GroupManager::setGroupOrders($_POST['group_id'],$arryUsr);
		$r["error"]    = -1;
		$r["message"]  = "순서가 업데이트 되었습니다. ";
		print json_encode($r);
		exit;


    break;


	/******************************************************************************
    * usrGroupModify
    * requred params(_POST) :: level , duty , id
    ******************************************************************************/
    case "usrSummaryModify":
		$arryUsrSummary = array(
			"summary"  => $_POST['summary'],
			"usr_id"   => $_POST['usr_id']
		);

		UsrManager::modifyUsrSummary($arryUsrSummary);


		$r["usr_id"]   = $_POST['usr_id'];
		$r["error"]    = -1;
		$r["message"]  = "노트가 수정되었습니다.";
		print json_encode($r);
		exit;

	break;



    /******************************************************************************/
}


?>
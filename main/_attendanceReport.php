<?php
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_lib/_inner_header.php");
require_once("./_attendance_date.php");

$setting    = AttendanceManager::getAttendanceSetting($_GET['id']);
$group      = GroupManager::getGroup($setting['group_id']);
$groupOwner = UsrManager::getUsr($group['usr_id']);
$thisWeek   = date("W");
$thisYear   = date("Y");
$arryDate   = explode("-",$_GET["date"]);
$report     = AttendanceManager::getReport($setting['id'],$_GET['date']);
?>



<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2><?=$group['title']?> 출석부</h2>
		<ol class="breadcrumb">
			<li><?=$group['title']?></li>
			<li>
			<?=$arryDate[0]?>년 <?=$arryDate[1]?>월 <?=$arryDate[2]?>일
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">

			<a class="btn btn-default btn-sm" href="javascript:$.attendance.list({id:<?=$_GET['id']?>,group_id:<?=$setting['group_id']?>});">보고서 목록</a>
			<a class="btn btn-default btn-sm" href="attendanceReportPrint.php?id=<?=$_GET['id']?>&date=<?=$_GET['date']?>" target="_blank">프린트</a>
			<a class="btn btn-primary btn-sm" href="javascript:$.attendance.reportSave();">보고서 저장</a>

		</div>
	</div>

</div>


<div class="wrapper wrapper-content animated fadeInUp">
	<div class="ibox">
		<div class="ibox-content">

        <form id="attendanceReportForm">
		<div class="row">
            <div class="col-lg-12">
				<div class="m-b-md">
					<h2><?=$group['title']?> 출석부</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Status:</dt> 
					<dd>
						<?php if($report){ ?>
							<span class="label label-primary">완료</span>
						<?php }else{ ?>
							<span class="label label-default">대기</span>
						<?php } ?>
					</dd>

					<dt>모임 일자:</dt> <dd><b><?=$arryDate[0]?></b>년 <b><?=$arryDate[1]?></b>월 <b><?=$arryDate[2]?></b>일</dd>
					<!-- dt>모임</dt> <dd><?=$setting["group_title"]?></dd -->
					<!-- dt>리더 이름(작성자)</dt>
					<dd><b><?=$groupOwner['name']?></b></dd -->
				</dl>
				<div class="m-b-md">
					<h2> 모임 출결 및 신앙 생활 점검 </h2>	
				</div>

				<input type="hidden" id="attendance_id" name="attendance_id" value="<?=$setting['id']?>" />
				<input type="hidden" id="group_id" name="group_id" value="<?=$setting['group_id']?>" />
				<input type="hidden" id="reg_date" name="reg_date" value="<?=$_GET['date']?>" />




				<table class="table table-hover">
					<?php
					//$atCount =  AttendanceManager::getAttendanceUsrCount($setting['id'],$_GET['date']);
					if($setting["year"] < $thisYear){
						$list = AttendanceManager::getUsrListFromAttendance($setting['id']);
					}else{
						$list = AttendanceManager::getUsrListFromGroup($setting['group_id']);
					}
					for($i=0;$i<count($list);$i++){
						//$usr = AttendanceManager::getAttendanceFromUsr($list[$i]['id'],$setting['id'], $_GET['date']);
						if($i == 0){
						?>
						<tr>			
							<th class="col-md-1"></th>
							<th class="col-md-6">이름</th>
							<th class="col-md-1 text-center">예배</th>
							<th class="col-md-1 text-center">모임</th>
							<th class="col-md-1 text-center">QA</th>
							<th class="col-md-1 text-center">말씀</th>
							<th class="col-md-1 text-center">기도</th>
						</tr>
						<?php } ?>
						<tr usrId="<?=$list[$i]['id']?>">
							<input type="hidden" name="usr_id" value="<?=$list[$i][id]?>" />
							<input type="hidden" name="worship" value="<?php if(!$usr['worship']){ echo"X"; }else{ echo $usr['worship']; }?>" />
							<input type="hidden" name="gbs" value="<?php if(!$usr['gbs']){ echo"X"; }else{ echo $usr['gbs']; }?>" />
							<input type="hidden" name="qt" value="<?=$usr['qt']?>" />
							<input type="hidden" name="bible" value="<?=$usr['bible']?>" />
							<input type="hidden" name="prayer" value="<?=$usr['prayer']?>" />
							<input type="hidden" name="note" value="<?=$usr['note']?>" />
							<td class="text-center">
								<?php 
								$thumb = UsrManager::getUsrThumb($list[$i]['id']);
								if($thumb){
									if($thumb['width'] > $thumb['height']){
										$_class = "landscape-sm";
									}else{
										$_class = "portrait-sm";
									}    
								?>
									<span class="<?=$_class?>"><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>.<?=$thumb['ext']?>" title="<?=$_SESSION['sys_id']?>" /></span>
								<?php } else { ?>
									<span class="thumb-sm"><img src="../images/member.png" /></span>
								<?php } ?>
							</td>
							<td>
								<a href="#" action="userInfo" usrId="<?=$list[$i]['id']?>"><?=$list[$i]['name']?></a>
							</td>
							<td class="text-center">
								<a href="#" action="update" mode="worship" usrId="<?=$list[$i]['id']?>" val="<?=$usr['worship']?>">
								<?php if($usr['worship'] == "O"){ ?>
									<i class="fa fa-check-circle-o text-success fa-2x"></i>
								<?php }else if($usr['worship'] == "L"){ ?>
									<i class="fa fa-clock-o text-info fa-2x"></i>
								<?php }else if($usr['worship'] == "X"){ ?>
									<i class="fa fa-times-circle-o text-danger fa-2x"></i>
								<?php } else{ ?>
									<i class="fa fa-edit fa-2x"></i>
								<?php } ?>
								</a>
							</td>
							<td class="text-center">
								<a href="#" action="update" mode="gbs" usrId="<?=$list[$i]['id']?>" val="<?=$usr['gbs']?>">
								<?php if($usr['gbs'] == "O"){ ?>
									<span class="text-success"><i class="fa fa-check-circle-o fa-2x"></i></span>
								<?php }else if($usr['gbs'] == "L"){ ?>
									<span class="text-warning"><i class="fa fa-clock-o fa-2x"></i></span>
								<?php }else if($usr['gbs'] == "X"){ ?>
									<span class="text-danger"><i class="fa fa-times-circle-o fa-2x"></i></span>
								<?php } else{ ?>
									<span class="text-plain"><i class="fa fa-edit fa-2x"></i></span>
								<?php } ?>
								</a>
							</td>
							<td class="text-center">
								<a href="#" action="update" mode="qt" usrId="<?=$list[$i]['id']?>" val="<?=$usr['qt']?>">
								<?php if(!$usr['qt']){ echo"0"; }else{ echo $usr['qt']; }?>
								</a>
							</td>
							<td class="text-center">
								<a href="#" action="update" mode="bible" usrId="<?=$list[$i]['id']?>" val="<?=$usr['bible']?>">
								<?php if(!$usr['bible']){ echo"0"; }else{ echo $usr['bible']; }?>
								</a>
							</td>
							<td class="text-center">
								<a href="#" action="update" mode="prayer" usrId="<?=$list[$i]['id']?>" val="<?=$usr['prayer']?>">
								<?php if(!$usr['prayer']){ echo"0"; }else{ echo $usr['prayer']; }?>
								</a>
							</td>
						</tr>

						<?php
						if(trim($usr['note']) !== ""){
						?>
						<tr>
							<td colspan="6" class="note" onclick="$.attendance.check('<?=$_GET['date']?>',<?=$list[$i]['id']?>);" ><?=nl2br($usr['note'])?></td>
						</tr>
						<?php
						}
						?>
					<?php
					$c++;
					}
					?>
					<tr>
						<td colspan="7" class="text-center">
							총 <span id="totalUsr"><?=$c?></span>명 중 <b id="worshipTotal"></b> 명
						</td>
					</tr>
				</table>

				<div class="m-b-md">
					<h2>모임 시 특이사항 및 건의사항</h2>
				</div>
				<div id="suggest" class="bg-muted" style="padding:5px; font-size:12px; min-height:80px; line-height:180%;" contenteditable="true"><?=nl2br($report["suggest"])?> </div>
				<br><br>
				<div class="m-b-md">
					<h2>리더 개인상황 (운영에 어려운점 포함)</h2>
				</div>
				<div id="report" class="bg-muted" style="padding:5px; font-size:12px; min-height:80px; line-height:180%;" contenteditable="true"><?=nl2br($report["report"])?> </div>
				<br><br>
				<div class="m-b-md">
					<h2>주중 행사계획 :</h2>
				</div>
				<div id="unusual" class="bg-muted" style="padding:5px; font-size:12px; min-height:80px; line-height:180%;" contenteditable="true"><?=nl2br($report["unusual"])?> </div>
				<br><br>
				<div class="m-b-md">
				<h2>모임 내 특별사항 :</h2>
				</div>
				<div id="bigo" class="bg-muted" style="padding:5px; font-size:12px; min-height:80px; line-height:180%;" contenteditable="true"><?=nl2br($report["bigo"])?> </div>
				<br><br>
				<div style="padding:20px 0;" class="text-center">
					<a class="btn btn-primary btn-lg" onclick="$.attendance.reportSave();">보고서 저장</a>
				</div>


			</div>
		</div>
		</form>
		</div>
	</div>
</div>

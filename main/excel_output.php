<?php
require_once("../_lib/_inner_header.php");
//require_once("Spreadsheet/Excel/Writer.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.ContactList.php");
$reNameCode = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);
header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8");
header( "Content-Disposition: attachment; filename = ".$reNameCode.".xls" );
header( "Content-Description: PHP4 Generated Data" );




$res1 = $db->query("SELECT `id`, `title`, `gisu` FROM `sameage`");
while($res1->fetchInto($row, DB_FETCHMODE_ASSOC)){
	$sameage[$row['id']]['title'] = $row['title'];
	$sameage[$row['id']]['gisu']  = $row['gisu'];
}


if($_GET['mode'] == "advancedSearch"){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
		"isAllSelect"=> "O",
		"type"     => "advancedSearch",
        "column"   => $_GET['column'],
        "keyword"  => $_GET['q']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$totalItemCount = $pl->getTotalCount();
	$totalUsr = count($list);
	$title_text = "상세검색 (".$totalItemCount.")";
} else if($_GET['mode'] == 'group'){
	$properties = array(
		"isAllSelect"=> "O",
		"type"       => "group",
		"group_id"   => $_GET['group_id']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$group_title = $db->getOne("SELECT `title` FROM `group` WHERE id = '{$_GET[group_id]}'");
	$totalUsr = count($list);
	$title_text = $group_title." 주소록 ( ".$totalUsr." )";

} else if($_GET['mode'] == 'sameage'){
	$properties = array(
		"isAllSelect"=> "O",
		"type"        => "normal",
		"sameage_id"  => $_GET['sameage_id']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$totalUsr = count($list);
	$title_text = $sameage[$_GET['sameage_id']]['title']." 주소록 ( ".$totalUsr." )";

} else if($_GET['mode'] == 'position'){
	$properties = array(
		"isAllSelect"=> "O",
		"type"      => "position",
		"position"  => $_GET['position']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$totalUsr = count($list);
	$title_text = $sameage[$_GET['sameage_id']]['title']." 주소록 ( ".$totalUsr." )";


} else if($_GET['mode'] == 'search'){

	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
		"type"     => "search",
        "keyword"  => $_GET['q'],
		"page"     => $_GET['page'],
		"perPage"  => 20
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$totalItemCount = $pl->getTotalCount();
	if($list != false){ $count = count($list); }else{ $count = 0; }
	$title_text = "검색어: <b>".$_GET['q']."</b> (".$totalItemCount.")";

}else{

	$properties = array(
		"isAllSelect"=> "O",
		"type"        => "normal"
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('O');
	$totalUsr = count($list);
	$title_text = "주소록 ( ".$totalUsr." )";

}


$arryUsrPosition = UsrManager::$usrPosition;
$arryUsrStatus   = UsrManager::$usrStatus;
$arryUsrChristen = UsrManager::$usrChristen;


$EXCEL_STR = "<table border='1'>
<tr>
   <td colspan='16'>".$title_text."</td>
</tr>
<tr>
   <td>번호</td>
   <td>기수</td>
   <td>기이름</td>
   <td>이름</td>
   <td>생년월일</td>
   <td>성별</td>
   <td>예배출석일수</td>
   <td>모임출석일수</td>
   <td>휴대폰</td>
   <td>전화</td>
   <td>이메일</td>
   <td>차량번호</td>
   <td>직분</td>
   <td>구분</td>
   <td>신급</td>
   <td>주소</td>
</tr>";

for($i=0;$i<count($list);$i++){
	if($list[$i]['lunar'] == "O"){ $lunar="(음)"; } else { $lunar="(양)"; }
	if($list[$i]['gender'] == "F"){ $gender="여"; } else { $gender="남"; }
	$attendanceCount = UsrManager::getAttendanceCount($list[$i]['id']);
	$attendanceGbsCount = UsrManager::getAttendanceGbsCount($list[$i]['id']);

	$phone = Common::getPhone($list[$i]['phone']);
	$tel = Common::getPhone($list[$i]['tel']);

	$EXCEL_STR .= "
	<tr>
	   <td>".$list[$i]['id']."</td>
	   <td>".$sameage[$list[$i]['sameage_id']]['gisu']."</td>
	   <td>".$sameage[$list[$i]['sameage_id']]['title']."</td>
	   <td>".$list[$i]['name']."</td>
	   <td>".$list[$i]['birth']." ".$lunar."</td>
	   <td>".$gender."</td>
	   <td>".$attendanceCount."</td>
	   <td>".$attendanceGbsCount."</td>
	   <td>".$phone."</td>
	   <td>".$tel."</td>
	   <td>".$list[$i]['email']."</td>
	   <td>".$list[$i]['car_number']."</td>
	   <td>".$arryUsrPosition[$list[$i]['position']]."</td>
	   <td>".$arryUsrStatus[$list[$i]['status']]."</td>
	   <td>".$arryUsrChristen[$list[$i]['christen']]."</td>
	   <td>".$list[$i]['address_1']." ".$list[$i]['address_2']."</td>
	</tr>";
}

$EXCEL_STR .= "</table>";



echo "<meta content=\"application/vnd.ms-excel; charset=UTF-8\" name=\"Content-type\"> ";
echo $EXCEL_STR;

?>

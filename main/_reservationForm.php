<?php
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.ReservationManager.php");
?>
<script type="text/javascript">
	$(document).ready(function() {


	});
</script>

<form id="reservationForm" class="form-horizontal">
<?php if($_GET['mode'] == "modify"){ ?>
	<input type='hidden' name='id' value="<?=$_GET['id']?>">
	<input type='hidden' name='mode' value="reservationModify" />
<?php }else{ ?>
	<input type='hidden' name='mode' value="reservationInsert" />
<?php } ?>
		<div class="form-group"> 
			<label class="col-md-3 control-label">작성자</label>
			<div class="col-md-9">
			<input type='hidden' id="write_usr_id" name="write_usr_id" value="<?=$writeUsr['id']?>" />
				<div class="inputValue form-control">
					<a href="#" action="userInfo" usrId="<?=$writeUsr['id']?>"><?=$writeUsr['name']?></a> <span style="color:#999;"><?=$usrPosition[$writeUsr['position']]?></span>
				</div>
			</div>
		</div>

		<div class="form-group"> 
			<label class="col-md-3 control-label">제목</label>
			<div class="col-md-9">
				<input type="text" name="reg_date" value="<?=$reservation['reg_date']?>" id="datepicker" class="form-control" />
			</div>
		</div>

		<div class="form-group"> 
			<label class="col-md-3 control-label">사진</label>
			<div class="col-md-9 photoBoxList">
				<div id="photoBox" class="thumbList"></div>
				<div id="photoBoxUploader" class="uploadBtn"></div>
			</div>
		</div>

		<div class="form-group"> 
			<label class="col-md-3 control-label">내용</label>
			<div class="col-md-9">
				<textarea id="content" name="content" style="height:200px;" class="form-control" ><?=$reservation['content']?></textarea>
			</div>
		</div>
		<div class="form-group"> 
			<div class="photoBoxList">
				<div id="photoBox" class="thumbList"></div>
				<div id="photoBoxUploader" class="uploadBtn"></div>
			</div>
		</div>
</form>

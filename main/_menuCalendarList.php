<?php
require_once('../_classes/class.GroupManager.php');


$list[0]['id'] = 0;
$list[0]['mode'] = "calendar";
$list[0]['text'] = "전체";
$list[0]['icon'] = "fa fa-calendar";
$list[0]['color'] = "";

$groups = GroupManager::getUseCalendarGroup();
for($g=0; $g<count($groups); $g++){
    $i = $g+1;
    $list[$i]['id'] = $groups[$g]['id'];
    $list[$i]['mode'] = "calendar";
    $list[$i]['text'] = $groups[$g]['title'];
    $list[$i]['icon'] = "fa fa-calendar";
    $list[$i]['color'] = $groups[$g]['color'];
}
$r = json_encode($list);
echo $r;
?>
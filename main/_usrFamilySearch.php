<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.UsrManager.php");

$usr = UsrManager::getUsr($_GET['usr_id']);
$birth = str_replace ('-','.',substr($usr['birth'],-5));
?>

<script type="text/javascript">
	$(function() {
		$.contact.usrFamilyList({"usr_id":<?=$_GET['usr_id']?>});
	});
</script>


	<ul>
		<li style="margin-bottom:5px;">
			<div class="search">
				<input type="hidden" id="usr_id" name="usr_id" value="<?=$_GET['usr_id']?>">
				<input type="text" name="family_q" id="family_q" value="<?=$_GET['q']?>" class="text" onkeydown="if(event.keyCode == '13'){ $.contact.usrFamilySearch(); }" style="width:150px;"/>
				<span class="ui-state-default ui-corner-all button_s" onclick="$.contact.usrFamilySearch(<?=$_GET['usr_id']?>);" >검색</span>
			</div>
		</li>
		<li>
			<div id="usrFamilySearchList" class="familyList" style="height:200px;"></div>
		</li>
		<li style="margin-top:5px;">
			<select id="relations" name="relations" >
			    <option value="0" >가족관계선택</option>
			    <option value="1" >부부</option>
				<option value="2" >부모/자녀</option>
				<option value="3" >형제/자매</option>
				<option value="4" >조부모/손자손녀</option>
				<option value="9" >친척</option>
			</select>
			<span class="ui-state-default ui-corner-all button_s" onclick="$.contact.usrFamilyInsertAction(<?=$_GET['usr_id']?>);" >가족관계 추가</span>
		</li>
	</ul>



<?php
require_once("../_lib/_inner_footer.php");
?>
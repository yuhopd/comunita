<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
$usr = UsrManager::getUsr($_GET['usr_id']);

?>
<form id="userAuthForm" class="form-horizontal">
<input type="hidden" name="usr_id" value="<?=$_GET['usr_id']?>">
<input type="hidden" name="mode" value="usr_auth_modify">

<div class="form-group">
	<label for="name" class="col-md-3 control-label">이름</label>
	<div class="col-md-9">
		<div class="value"><?=$usr['name']?></div>
	</div>
</div>
<div class="form-group">
	<label for="email" class="col-md-3 control-label">이메일</label>
	<div class="col-md-9">
		<input id="email" name="email" type="text" value="<?=$usr['email']?>" class="form-control" />
	</div>
</div>
<div class="form-group">
	<label for="level"  class="col-md-3 control-label">권한</label>
	<div class="col-md-9">
		<select id="level" name="level" class="form-control">
			<option value="0" <?php if($usr['level'] == "0"){?> selected <?php } ?>>대기</option>
			<option value="1" <?php if($usr['level'] == "1"){?> selected <?php } ?>>회원</option>
			<option value="2" <?php if($usr['level'] == "2"){?> selected <?php } ?>>리더</option>
			<option value="3" <?php if($usr['level'] == "3"){?> selected <?php } ?>>관리자</option>
			<option value="4" <?php if($usr['level'] == "4"){?> selected <?php } ?>>시스템 관리자</option>
		</select>
	</div>
</div>

</form>
<div id="usr_msg" class="ui-state-error ui-corner-all" style="display:none; padding:3px;"> 
	<span class="gtd-ui-icon ui-icon ui-icon-alert left" style="margin-right: 0.3em;" ></span> 
	<span class="error_msg"><strong>Alert:</strong> Sample ui-state-error style.</span>
</div>


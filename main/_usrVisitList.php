<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.VisitManager.php");
require_once("../_classes/class.VisitList.php");
if(!$_GET['page']){ $_GET['page']=1; }
$properties = array(
    "usr_id"   => $_GET['usr_id'],
	"page"     => $_GET['page'],
	"perPage"  => 20
);

$vt          = new VisitList($properties);
$totalitems  = $vt->getTotalCount();
$vlist        = $vt->getList('X');
$tot         = count($list);


$visitType = VisitManager::$arryVisitType;
$usrPosition = UsrManager::$usrPosition;
?>



<div class="ibox ">
	<div class="ibox-title">
		<h5>심방기록</h5>
		<div class="ibox-tools">
			<a href="#" usrid="<?=$_GET['usr_id']?>" action="visitInsert" class="btn btn-primary btn-xs">추가</a>  
		</div>
	</div>
	<div class="ibox-content"  id="usrVisitListBox">

<?php

if($totalitems) {
?>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th><small>방문일</small></th>
			<th><small>종류</small></th>
			<th><small>피심방자</small></th>
			<th><small>심방자</small></th>
			<th><small>장소</small></th>
			<th><small>성경본문</small></th>
		</tr>
	</thead>
	<tbody>
	<?php for($i=0;$i<count($vlist);$i++){
		$usr = UsrManager::getUsr($vlist[$i]['usr_id']);
		$visit_usr = UsrManager::getUsr($vlist[$i]['visit_usr_id']);
		if($i%2 == 1){ $odd = " odd"; } else { $odd = " even"; }
		?>
		<tr>
			<td><a href="#" action="visitView" usrid="<?=$vlist[$i]['id']?>" class="btn" ><small><?=$vlist[$i]['reg_date']?></small></a></td>
			<td><?=$visitType[$vlist[$i]['type']]?></td>
			<td><a href="#" action="userInfo" usrId="<?=$usr['id']?>"><?=$usr['name']?></a> <small><?=$usrPosition[$usr['position']]?></small></td>
			<td><a href="#" action="userInfo" usrId="<?=$visit_usr['id']?>"><?=$visit_usr['name']?></a> <small><?=$usrPosition[$visit_usr['position']]?></small></td>

			<td><small><?=$vlist[$i]['place']?></small></td>
			<td><small><?=$vlist[$i]['title']?></small></td>
		</tr>
	<?php
	}
	?>
	</tbody>
</table>
<?php
}else{
?>
<div style="margin:50px;text-align:center;">심방기록이 없습니다.</div>
<?php
}
?>


</div>


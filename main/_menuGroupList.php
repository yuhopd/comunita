<?php

require_once('../_lib/_inner_header.php');
require_once('../_classes/class.GroupManager.php');
require_once('../_classes/class.UsrManager.php');

$list = array();

$row0['mode'] = "group";
$row0['text'] = "소속";
$row0['icon'] = "fa fa-folder";
$list[0] = $row0;

$root_id = GroupManager::getRootIdFromUsr($_SESSION['sys_id']);
if($_SESSION['sys_level'] > 2 ){
	$query1 = "SELECT * FROM `group` WHERE `parent_id`='0' AND `is_enable`='O'  ORDER BY `sequence` ASC";
}else{
	$query1 = "SELECT * FROM `group` WHERE `id`='$root_id'";
}
$res1 = $db->query($query1);
$a=0;
while($row1 = $res1->fetch(PDO::FETCH_ASSOC)){
	$row1['mode'] = "group";
	$row1['text'] = $row1['title'];
	$row1['icon'] = "fa fa-folder";
    $list[0]['children'][]= $row1;
	$query2  = "SELECT * FROM `group` WHERE `parent_id`='{$row1[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
    $res2    = $db->query($query2);
	$b=0;
    while($row2 = $res2->fetch(PDO::FETCH_ASSOC)){
		$row2['mode'] = "group";
		$row2['text'] = $row2['title'];
		$row2['icon'] = "fa fa-folder";
		$list[0]['children'][$a]['children'][] = $row2;
		$query3  = "SELECT * FROM `group` WHERE `parent_id`='{$row2[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
		$res3    = $db->query($query3);		
		$c=0;
		while($row3 = $res3->fetch(PDO::FETCH_ASSOC)){
			$row3['mode'] = "group";
			$row3['text'] = $row3['title'];
			$row3['icon'] = "fa fa-folder";
			$list[0]['children'][$a]['children'][$b]['children'][] = $row3;
			$query4  = "SELECT * FROM `group` WHERE `parent_id`='{$row3[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
			$res4    = $db->query($query4);
			$d=0;
			while($row4 = $res4->fetch(PDO::FETCH_ASSOC)){
				$row4['mode'] = "group";
				$row4['text'] = $row4['title'];
				$row4['icon'] = "fa fa-folder";
				$list[0]['children'][$a]['children'][$b]['children'][$c]['children'][] = $row4;
				$query5  = "SELECT * FROM `group` WHERE `parent_id`='{$row4[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
				$res5    = $db->query($query5);
				$e=0;
				while($row5 = $res5->fetch(PDO::FETCH_ASSOC)){
					$row5['mode'] = "group";
					$row5['text'] = $row5['title'];
					$row5['icon'] = "fa fa-folder";
					$list[0]['children'][$a]['children'][$b]['children'][$c]['children'][$d]['children'][] = $row5;
					$query6  = "SELECT * FROM `group` WHERE `parent_id`='{$row5[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
					$res6    = $db->query($query6);
					$f=0;
					while($row6 = $res6->fetch(PDO::FETCH_ASSOC)){
						$row6['mode'] = "group";
						$row6['text'] = $row6['title'];
						$row6['icon'] = "fa fa-folder";
						$list[0]['children'][$a]['children'][$b]['children'][$c]['children'][$d]['children'][$e]['children'][] = $row6;
						$f++;
						unset($row6);
					}
					$e++;
					unset($row5);
				}
				$d++;
				unset($row4);
			}
			$c++;
			unset($row3);
		}
		$b++;
		unset($row2);
    }
	$a++;
	unset($row1);
}



$row00['text'] = "동기";
$row00['icon'] = "fa fa-users";
$list[1] = $row00;

$query = "SELECT * FROM `sameage` ORDER BY `gisu` DESC";
$res   =& $db->query($query);
while($rowAA = $res->fetch(PDO::FETCH_ASSOC)){
	$rowA['mode'] = "sameage";
	$rowA['text'] = $rowAA['title'];
	$rowA['icon'] = "fa fa-users";

	$rowA['sameage_id'] = $rowAA['id'];	
	$list[1]['children'][]= $rowA;

	unset($rowA);
}
$usrPosition = UsrManager::$usrPosition;


$usrPositionVal = array_values($usrPosition);
$usrPositionKey = array_keys($usrPosition);

$row000['text'] = "직분";
$row000['icon'] = "fa fa-folder";
$list[2] = $row000;

for ($i=0 ; $i < count($usrPositionKey) ; $i++){
	$rowB['mode'] = "position";
	$rowB['text'] = $usrPositionVal[$i];
	$rowB['icon'] = "fa fa-folder";
	$rowB['position_id'] = $usrPositionKey[$i];

	$list[2]['children'][]= $rowB;
}
//print_r($list);
$r = json_encode($list);
if($_GET['callback'] != null){
	echo $_GET['callback']. "(".$r.")";
}else{
	echo $r;
}

?>
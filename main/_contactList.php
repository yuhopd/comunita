<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.ContactList.php");
require_once("../_classes/class.UsrManager.php");

if(!$_GET['page']){ $_GET['page']=1; }



if($_GET['mode'] == "advancedSearch"){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
		"type"     => "advancedSearch",
        "column"   => $_GET['column'],
        "keyword"  => $_GET['q'],
		"page"     => $_GET['page'],
		"perPage"  => 24
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('O');
	$totalItemCount = $pl->getTotalCount();
	if($list != false){ $count = count($list); }else{ $count = 0; }
	$title = "상세검색 (".$totalItemCount.")";
    $excel_out = "../contact/excel_output.php?mode=advancedSearch&column={$properties[column]}&q={$properties[keyword]}";
	$paging = Common::getPaging($totalItemCount, $properties['perPage'],  "%d");

} else if($_GET['q'] && $_GET['q'] != ''){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
		"type"     => "search",
        "keyword"  => $_GET['q'],
		"page"     => $_GET['page'],
		"perPage"  => 24
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('O');
	$totalItemCount = $pl->getTotalCount();

	if($list != false){ $count = count($list); }else{ $count = 0; }
	$title = "검색어: <b>".$_GET['q']."</b> (".$totalItemCount.")";
    $excel_out = "../contact/excel_output.php?mode=search&q={$properties[keyword]}";
	$paging = Common::getPaging($totalItemCount, $properties['perPage'], "%d");

}else if($_GET['sameage_id'] && $_GET['sameage_id'] != 0){
	$properties = array(
		"sameage_id" => $_GET['sameage_id'],
		"page"     => $_GET['page'],
		"perPage"    => 48
	);
	$pl    = new ContactList($properties);

	$totalItemCount = $pl->getTotalCount();
    if($totalItemCount > 50){
		$list  = $pl->getList('O');
	}else{
		$list  = $pl->getList('X');
	}

	if($list != false){ $count = count($list); }else{ $count = 0; }
	
	
	$query = "SELECT `title` FROM `sameage` WHERE `id`='{$_GET['sameage_id']}'";
	$res   = $db->query($query);
	$row   = $res->fetch(PDO::FETCH_ASSOC);

	$title = $row['title'];

	$title = $title." (".$count.")";
	$sameage_sel = "selected";
    $excel_out = "../contact/excel_output.php?mode=sameage&sameage_id=".$_GET['sameage_id'];

	$paging = Common::getPaging($totalItemCount, $properties['perPage'], "%d");

}else if($_GET['group_id'] && $_GET['group_id'] != 0){
	$properties = array(
		"type"       => "group",
		"group_id"   => $_GET['group_id'],
		"page"     => $_GET['page'],
		"perPage"    => 48
	);

	$pl    = new ContactList($properties);

	$totalItemCount = $pl->getTotalCount();

	if($totalItemCount > 50){
		$list  = $pl->getList('O');
		$paging = Common::getPaging($totalItemCount, $properties['perPage'], "%d");

	}else{
		$list  = $pl->getList('X');
		$paging = "";
	}
	if($list != false){ $count = count($list); }else{ $count = 0; }
	$res = $db->query("SELECT `title` FROM `group` WHERE `id`='{$_GET['group_id']}'");
	$row = $res->fetch(PDO::FETCH_ASSOC);


	$title = $row['title']." (".$totalItemCount.")";

	$excel_out = "../contact/excel_output.php?mode=group&group_id=".$_GET['group_id'];


}else if($_GET['mode'] == "position"){
	$properties = array(
		"type"       => "position",
		"position"   => $_GET['position'],
		"page"       => $_GET['page'],
		"perPage"    => 24
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	$totalItemCount = $pl->getTotalCount();
	if($list != false){ $count = count($list); }else{ $count = 0; }

	$title = UsrManager::$usrPosition[$properties['position']];
	$title = $title." (".$totalItemCount.")";
    $excel_out = "../contact/excel_output.php?mode=position&position=".$_GET['position'];
	$paging = "";


} else {
	$properties = array(
		"usr_id"   => $_GET['usr_id'],
		"page"     => $_GET['page'],
		"perPage"  => 24
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('O');
	$totalItemCount = $pl->getTotalCount();
	if($list != false){ $count = count($list); }else{ $count = 0; }
	$title = "전체 리스트 (".$totalItemCount.")";

    $excel_out = "../contact/excel_output.php?mode=all";
	$paging = Common::getPaging($totalItemCount, $properties['perPage'],"%d");

}
$usrStatus    = UsrManager::$usrStatus;
$usrStatusVal = array_values($usrStatus);
$usrStatusKey = array_keys($usrStatus);

$usrPosition    = UsrManager::$usrPosition;
$usrPositionVal = array_values($usrPosition);
$usrPositionKey = array_keys($usrPosition);

$usrChristen    = UsrManager::$usrChristen;
$usrChristenVal = array_values($usrChristen);
$usrChristenKey = array_keys($usrChristen);
?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>주소록</h2>
		<ol class="breadcrumb">
			<li>
				<?=$title?>
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">
			<a class="btn btn-default btn-sm" action="viewmode" mode="card"><i class="fa fa-th-large"></i></a>
			<a class="btn btn-default btn-sm" action="viewmode" mode="table"><i class="fa fa-th-list"></i></a>

			
			<a class="btn btn-default btn-sm" action="usrInsert">회원추가</a>
			<a class="btn btn-default btn-sm" href="<?=$excel_out?>">엑셀출력</a>
			<?php if($_GET['group_id'] && $_GET['group_id'] != 0){ ?>
			<a id="contactListOrderSetting" class="btn btn-default btn-sm" href="javascript:$.contact.setListOrder();">순서설정</a>
			<a id="contactListOrderSave" style="display:none;" class="btn btn-default btn-sm" href="javascript:$.contact.setListOrderSave(<?=$_GET['group_id']?>);">순서저장</a>
			<?php } ?>
			<a id="contactListOrderCancel" style="display:none;" class="btn btn-default btn-sm" href="javascript:$.contact.setListOrderCancel();">순서취소</a>
			<a class="btn btn-default btn-sm" action="toggleAdvancedSearch" href="#">상세검색</a>

			<a class="btn btn-default btn-sm" href="#" action="checkAll">전체선택</a>
			<a class="btn btn-primary btn-sm" href="#" action="smsSend">문자보내기</a>
		</div>
	</div>
</div>

<div id="advancedSearch" class="wrapper wrapper-content" <?php if($_GET['mode'] != "advancedSearch"){?> style="display:none;" <?php } ?>>
	<div clsss="ibox">
		<div class="ibox-content form-horizontal">
			<div class="form-group">
				<div class="col-md-3">
					<select id="column" name="column"  class="form-control">
						<option value="name" <?php if($_GET['column'] == "name"){?>selected<?php } ?>>이름</option>
						<option value="birth" <?php if($_GET['column'] == "birth"){?>selected<?php } ?>>생년월일</option>
						<option value="phone" <?php if($_GET['column'] == "phone"){?>selected<?php } ?>>연락처(휴대폰/전화)</option>
						<option value="email" <?php if($_GET['column'] == "email"){?>selected<?php } ?>>이메일</option>
						<option value="car_number" <?php if($_GET['column'] == "car_number"){?>selected<?php } ?>>차량번호</option>
						<option value="status" <?php if($_GET['column'] == "status"){?>selected<?php } ?>>구분</option>
						<option value="christen" <?php if($_GET['column'] == "christen"){?>selected<?php } ?>>신급</option>
						<option value="address" <?php if($_GET['column'] == "address"){?>selected<?php } ?>>주소</option>
						<option value="register" <?php if($_GET['column'] == "register"){?>selected<?php } ?>>등록일</option>
					</select>
				</div>
				<?php if($_GET['column'] == "status"){ ?>
				<div class="col-md-6" id="columnInputWrap">
					<select id="columnInput" name="columnInput" class="form-control">
						<option value="0" <?php if($_GET['q'] == "0"){?>selected<?php } ?>>없음</option>
						<option value="1" <?php if($_GET['q'] == "1"){?>selected<?php } ?>>본교회</option>
						<option value="2" <?php if($_GET['q'] == "2"){?>selected<?php } ?>>타교회</option>
						<option value="4" <?php if($_GET['q'] == "4"){?>selected<?php } ?>>비교인</option>
						<option value="6" <?php if($_GET['q'] == "6"){?>selected<?php } ?>>전출</option>
						<option value="8" <?php if($_GET['q'] == "8"){?>selected<?php } ?>>새가족</option>
						<option value="10" <?php if($_GET['q'] == "10"){?>selected<?php } ?>>별세</option>
						<option value="11" <?php if($_GET['q'] == "11"){?>selected<?php } ?>>미등록자</option>
					</select>
				</div>
				<?php }else if($_GET['column'] == "christen"){ ?>
				<div class="col-md-6"  id="columnInputWrap">
					<select id="columnInput" name="columnInput" class="form-control">
						<option value="0" <?php if($_GET['q'] == "0"){?>selected<?php } ?>>없음</option>
						<option value="1" <?php if($_GET['q'] == "1"){?>selected<?php } ?>>등록</option>
						<option value="2" <?php if($_GET['q'] == "2"){?>selected<?php } ?>>학습</option>
						<option value="3" <?php if($_GET['q'] == "3"){?>selected<?php } ?>>유아세례</option>
						<option value="4" <?php if($_GET['q'] == "4"){?>selected<?php } ?>>세례</option>
					</select>
				</div>
				<?php }else{ ?>
				<div class="col-md-6" id="columnInputWrap">
					<input type="text" id="columnInput" name="columnInput" value="<?=$_GET['q']?>"  class="form-control"/>
				</div>
				<?php } ?>
				
			
				<div class="col-md-3">
					<a href="#" action="advancedSearchAction" class="btn btn-primary">검색</a>
					<a href="#" action="toggleAdvancedSearch" class="btn btn-default">취소</a>				
				</div>
			</div>
			<span class="help-block m-b-none">생년월일 검색은 ","(콤마)로 구분하면 중복검색이 가능합니다. (ex : 1978,1979,1980 )</span>


		</div>
	</div>
</div>


<?php
if($_GET['viewmode'] == "table"){
?>
<br><br>
<div class="ibox">

    <div class="ibox-content">       
        <div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th></th>
						<th>이름</th>
						<th>직분</th>
						<th>성별</th>
						<th>생년월일</th>
						<th>구분</th>
						<th>연락처</th>
						<th>그룹</th>
					</tr>
				</thead>
				<tbody>
					<?php
					for($i=0; $i<$count; $i++ ) {
						if($i%2 == 1){ $item_odd = " odd"; }else{ $item_odd = ""; }
						$phone = Common::getPhone($list[$i]['phone'],$list[$i]['tel']);
						if($list[$i]['position'] == 0){
							$position = "";
						}else{
							$position = "<span class='position'>".UsrManager::$usrPosition[$list[$i]['position']]."</span>";
						}
						$birth = str_replace ('-','.',substr($list[$i]['birth'],-5));
					?>
					<tr>
						<td><input type="checkbox" class="i-checks" value="<?=$list[$i]['id']?>" /></td>
						<td><a userinfo = "<?=$list[$i]['id']?>" class="userinfo" ><?=$list[$i]['name']?></a></td>
						<td><?=$position?></td>
						<td><?php if($list[$i]['gender'] == "M"){?>남<?php }else{ ?>여<?php } ?></td>
						<td><?php if($birth != '00.00'){?><?=$birth?><?php if($list[$i]['lunar'] == "O"){ ?>(음)<?php } ?><?php } ?></td>
						<td><?=$usrPositionVal[$list[$i]['position']]?></td>
						<td><?=$list[$i]['phone']?></td>
						<td><?php 
							$sameage = UsrManager::getSameage($list[$i]['sameage_id']);
							if($sameage){ ?>
								<span class="label label-success <?=$sameage_sel?>"><?=$sameage['title']?></span>
							<?php } ?>
							<?php
							$groups = UsrManager::getGroups($list[$i]['groups']);
							//$groups = UsrManager::getGroupList($list[$i]['id']);
							if($groups != false){
								for($f=0;$f<count($groups);$f++){
									if($groups[$f]['id'] == $_GET['group_id']){ $sel = "selected"; }else{ $sel = ""; }
								?>
									<span class="label <?=$sel?>" ><?=$groups[$f]['title']?></span>
								<?php
								}
							}
							unset($groups);									
							?>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="pagging"><?=$paging?></div>
</div>

<?php }else{ ?>


<div class="row wrapper wrapper-content animated fadeInRight">

	<div class="grid" >
		<?php
		for($i=0; $i<$count; $i++ ) {
			if($i%2 == 1){ $item_odd = " odd"; }else{ $item_odd = ""; }
			$phone = Common::getPhone($list[$i]['phone'],$list[$i]['tel']);
			if($list[$i]['position'] == 0){
				$position = "";
			}else{
				$position = "<span class='position'>".UsrManager::$usrPosition[$list[$i]['position']]."</span>";
			}
			$birth = str_replace ('-','.',substr($list[$i]['birth'],-5));
		?>
			<div class="grid-item">

				<div class="contact-box">
					<input type="checkbox" class="i-checks" value="<?=$list[$i]['id']?>" />
					
					<div class="col-sm-5 userthumb">
						<a userinfo = "<?=$list[$i]['id']?>" class="userinfo" >
							<?php
							$thumb = UsrManager::getUsrThumb($list[$i]['id']);
							if($thumb){
								if($thumb['width'] > $thumb['height']){
									$_class = " landscape";
								}else{
									$_class = " portrait";
								}
							?>
								<div class="<?=$_class?>"><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>_small.<?=$thumb['ext']?>?v=<?=time()?>"/></div>
							<?php
							}else{
							?>
								<div class="thumb"><img alt="image" class="img-responsive" src="../css/images/member.png"></div>
							<?php
							}
							?>
						</a>
					</div>
					<div class="col-sm-7">
					<a userinfo = "<?=$list[$i]['id']?>" class="userinfo" ><h3><strong><?=$list[$i]['name']?></strong> <small><?=$position?></small> <small><?php if($birth != '00.00'){?>
						<?=$birth?><?php if($list[$i]['lunar'] == "O"){ ?>(음)<?php } ?>
						<?php } ?></small></h3></a>
						<?php if($phone !== "--"){ ?>
						<p><i class="fa fa-phone"></i> <?=$phone?></p>
						<?php } ?>
						<div class="groupBox">
							<?php 
							$sameage = UsrManager::getSameage($list[$i]['sameage_id']);
							if($sameage){ ?>
								<span class="label label-success <?=$sameage_sel?>"><?=$sameage['title']?></span>
							<?php } ?>
							<?php
							$groups = UsrManager::getGroups($list[$i]['groups']);
							//$groups = UsrManager::getGroupList($list[$i]['id']);
							if($groups != false){
								for($f=0;$f<count($groups);$f++){
									if($groups[$f]['id'] == $_GET['group_id']){ $sel = "selected"; }else{ $sel = ""; }
								?>
									<span class="label <?=$sel?>" ><?=$groups[$f]['title']?></span>
								<?php
								}
							}
							unset($groups);									
							?>
						</div>
					</div>
					<div class="clearfix"></div>
					
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>

<?php
}
?>
<br><br><br>
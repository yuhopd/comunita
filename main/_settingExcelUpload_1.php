<?php
require_once ("../_lib/_inner_header.php");
require_once ("../_classes/class.GroupManager.php");

$row = GroupManager::getGroup($_GET['group_id']);
?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#excelUploader').fineUploader({
			request: {
				endpoint: "../admin/_excelUploadAction.php",
				params: {
					"group_id"  : $("#excelUploadForm select[name=group_id]").val()
				}
		    },
			multiple: false,
			validation: {
				allowedExtensions: ['xls','xlsx'],
				forceMultipart:true,
				sizeLimit: 1024000000
			},
			text: {
				uploadButton: '엑셀파일업로드'
			},
			debug: true
		}).on('complete', function(event, id, fileName, responseJSON) {
			$.group.excelStep2(responseJSON.group_id, responseJSON.filename);
		    $('#excelUploader .qq-upload-list').hide();
		});


	});
</script>

<div class="one_wrap">
	<!--Calendar-->
	<div class="widget">
		<div class="widget_title"><span class="iconsweet">]</span><h5>그룹엑셀 입력 Step1</h5></div>
		<div class="widget_body">
			<div class="content_pad">
				<form id="excelUploadForm">
				<input type="hidden" name="group_id" value="<?=$_GET['group_id']?>">
				<ul class="form">
					<li>
						<label for="group_title">그룹명</label>
                        <select name="group_id" style="width:200px;">
						<?php
						$groups = GroupManager::getAllRootIds();
						for($g=0; $g<count($groups); $g++){
						?>
							<option value="<?=$groups[$g]['id']?>" <?php if($groups[$g]['id'] == $_GET['group_id']){ ?> selected="selected" <?php } ?> ><?=$groups[$g]['title']?></option>
						<?php
						}
						?>
						</select>
						<span class="help">엑셀입력은 기본그룹만 선택할 수 있습니다.</span>
					</li>
					<li>
						<label>&nbsp;</label>
					    <div id="excelUploader" style="display:inline-block;"></div>
               		</li>
				</ul>
				</form>
			</div>
		</div>
	</div>
</div>

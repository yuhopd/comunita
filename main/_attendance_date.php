<?php
$arryDate = getSundaysFromYear(date("Y"),0);
$startTimestamp = mktime(0, 0, 0, 1, 1, date("Y"));
$startDayFromWeek = date("w",$startTimestamp);
if($startDayFromWeek > 0){
	$gap = (7-$startDayFromWeek) + $week;
	$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
}
$mon = 0;
for($i=0;$i<26;$i++){
	$weekday_1st[$i] =  date("j",strtotime("+{$i} week",$startTimestamp));
	if($mon != date("n",strtotime("+{$i} week",$startTimestamp))){
        $mon =  date("n",strtotime("+{$i} week",$startTimestamp));
		$arryMonth_1st[$i] = $mon." 월";
	}else{
		$arryMonth_1st[$i] = "";
	}
}
$weekday_str_1st = implode("|",$weekday_1st);
$arryMonth_str_1st = implode("|",$arryMonth_1st);

$mon = 0;
for($i=26;$i<52;$i++){
	$weekday_2st[$i] =  date("j",strtotime("+{$i} week",$startTimestamp));
	
	if($mon != date("n",strtotime("+{$i} week",$startTimestamp))){
        $mon =  date("n",strtotime("+{$i} week",$startTimestamp));
		$arryMonth_2st[$i] = $mon." 월";
	}else{
		$arryMonth_2st[$i] = "";
	}
}

$weekday_str_2st = implode("|",$weekday_2st);
$arryMonth_str_2st = implode("|",$arryMonth_2st);



?>
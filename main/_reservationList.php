<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.ReservationList.php");
if(!$_GET['page']){ $_GET['page']=1; }

if($_GET['q'] && $_GET['q'] != ''){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
        "keyword"  => $_GET['q'],
	    "type"     => "search",	
		"page"     => $_GET['page'],
		"perPage"  => 20
	);

	$vt          = new ReservationList($properties);
	$totalitems  = $vt->getTotalCount();
	$vlist       = $vt->getList('O');
	if($list != false){
		$count1 = count($vlist);
	}else{
		$count1 = 0;
	}

	$title = "검색어: <b>".$_GET['q']."</b> (".$totalitems.")";
	$paging = Common::getPaging($totalitems, $properties['perPage'], "%d");
	

} else {

	$properties = array(
		"usr_id"   => $_GET['usr_id'],
		"page"     => $_GET['page'],
		"perPage"  => 20
	);
	$title = "전체 (".$properties['page']." 페이지)";

	$vt          = new ReservationList($properties);
	$totalitems  = $vt->getTotalCount();
	$vlist       = $vt->getList('O');
	if($list != false){
		$count1 = count($vlist);
	}else{
		$count1 = 0;
	}
	$paging = Common::getPaging($totalitems, $properties['perPage'], "%d");

}

$usrPosition = UsrManager::$usrPosition;
?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>회의실</h2>
		<ol class="breadcrumb">
			<li>
				<?=$title?>
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action row">
		    <div class="col-sm-6">
				<a class="btn btn-default btn-sm" href="#" action="visitInsert">회의실 추가</a>
				<?php if($_GET['q'] && $_GET['q'] != ''){ ?>
				<a class="btn btn-default btn-sm" href="#" action="visitSearchCancel">검색취소</a>
				<?php } ?>
			</div>
			<div class="input-group col-sm-6">
				<input type="search" placeholder="피심방자 / 심방자 / 방문일" id="visit_q" name="visit_q" value="<?=$_GET['q']?>" class="input-sm form-control"> 
				<span class="input-group-btn"><button type="button" class="btn btn-sm btn-primary">검색</button></span>
			</div>
		</div>
	</div>
</div>

<?php
if($totalitems) {
?>

<div class="row wrapper wrapper-content animated fadeInRight">
	<div class="ibox">
		<div class="ibox-content">


			<table class="table table-hover">
				<tr>
					<th >방문일</th>
					<th>종류</th>
					<th>피심방자</th>
					<th>심방자</th>
					<th>장소</th>
					<th >성경본문</th>
					<th>action</th>
				</tr>
				<?php
				for($i=0;$i<count($vlist);$i++){
					$extraUsr = "";
					$extraVisitUsr = "";
					

					$usr_id_list = array_unique(explode("|",$vlist[$i]['usr_id']));
					if(count($usr_id_list) > 1){
						$extraUsr = " 외".(count($usr_id_list)-1)."명";
					}

					$visit_usr_id_list = array_unique(explode("|",$vlist[$i]['visit_usr_id']));
					if(count($visit_usr_id_list) > 1){
						$extraVisitUsr = " 외".(count($visit_usr_id_list)-1)."명";
					}

					$usr = UsrManager::getUsr($usr_id_list[0]);
					$visit_usr = UsrManager::getUsr($visit_usr_id_list[0]);

					

					if($i%2 == 1){ $odd = " odd"; } else { $odd = " even"; }
					?>
					<tr listBox="mainBox">
						<td><?=$vlist[$i]['reg_date']?></td>
						<td><?=$visitType[$vlist[$i]['type']]?></td>
						<td><a href="#" action="userInfo" usrId="<?=$usr['id']?>"><?=$usr['name']?></a> <small><?=$usrPosition[$usr['position']]?></small> <?=$extraUsr?></td>
						<td><a href="#" action="userInfo" usrId="<?=$visit_usr['id']?>"><?=$visit_usr['name']?></a> <small><?=$usrPosition[$visit_usr['position']]?></small> <?=$extraVisitUsr?></td>
						<td><?=$vlist[$i]['place']?></td>
						<td><?=$vlist[$i]['title']?></td>
						<td>
							<a class="btn btn-default btn-xs" href="#" visitId="<?=$vlist[$i]['id']?>" action="visitView">보기</a>
							<a class="btn btn-default btn-xs" href="#" visitId="<?=$vlist[$i]['id']?>" action="visitEdit">수정</a>
						</td>
					</tr>
				<?php
					unset($usr_id_list);
					unset($visit_usr_id_list);
				}
				?>
			</table>

			<div class="pagging"><?=$paging?></div>
			<br /><br />
		</div>
	</div>

</div>
<?php
}
?>

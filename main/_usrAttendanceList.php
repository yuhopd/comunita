<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.AttendanceManager.php");
$usr = UsrManager::getUsr($_GET['usr_id']);
$note = AttendanceManager::getNoteFromUsr($usr['id']);
?>

<div class="ibox ">
	<div class="ibox-title">
		<h5>소속정보</h5>
		<div class="ibox-tools"></div>
	</div>
	<div class="ibox-content">
<?php
if($note){
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th class="col-md-7"><small>날짜</small></th>
			<th class="col-md-1"><small>예배</small></th>
			<th class="col-md-1"><small>순</small></th>
			<th class="col-md-1"><small>QT</small></th>
			<th class="col-md-1"><small>말씀</small></th>
			<th class="col-md-1"><small>기도</small></th>
		</tr>
	</thead>
	<tbody>

	<?php
    for($i=0;$i<count($note);$i++){
    ?>
		<tr>
			<td><small><?=$note[$i]['date']?></small></td>
			<td>
				<?php if($note[$i]['worship'] == "O"){ ?>
					<span class="text-success"><i class="fa fa-check-circle-o"></i></span>
				<?php }else if($note[$i]['worship'] == "L"){ ?>
					<span class="text-warning"><i class="fa fa-exclamation"></i></span>
				<?php } else{ ?>
					<span class="text-danger"><i class="fa fa-times-circle-o"></i></span>
				<?php } ?>
			</td>
			<td>
				<?php if($note[$i]['gbs'] == "O"){ ?>
					<span class="text-success"><i class="fa fa-check-circle-o"></i></span>
				<?php }else if($note[$i]['gbs'] == "L"){ ?>
					<span class="text-warning"><i class="fa fa-exclamation"></i></span>
				<?php } else{ ?>
					<span class="text-danger"><i class="fa fa-times-circle-o"></i></span>
				<?php } ?>
			<td><small><?php if(!$note[$i]['qt']){ echo "0"; }else{ echo $note[$i]['qt']; }?></small></td>
			<td><small><?php if(!$note[$i]['bible']){ echo "0"; }else{ echo $note[$i]['bible']; }?></small></td>
			<td><small><?php if(!$note[$i]['prayer']){ echo "0"; }else{ echo $note[$i]['prayer']; }?></small></td>
		</tr>
        <?php if(trim($note[$i]['note']) !== ""){ ?>
        <tr>
			<td colspan="6" ><?=$note[$i]['note']?></td>
		</tr>
		<?php } ?>
	<?php
	}
	?>
    
<?php
}
?>
	</tbody>
</table>

<?php
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.VisitManager.php");
$usrPosition = UsrManager::$usrPosition;

	$visit    = VisitManager::getItem($_GET['id']);
	$usr      = UsrManager::getUsr($visit['usr_id']);
	$visitUsr = UsrManager::getUsr($visit['visit_usr_id']);

?>

	<input type='hidden' id="visit_id" name="visit_id" value="<?=$visit['id']?>" />
    <ul class="form">
		<li>
			<span class="desc" style="width:60px;">피심방자</span>
			<div class="value" style="display:inline-block;"><?=$usr['name']?> <span style="color:#999;display:inline-block;"><?=$usrPosition[$usr['position']]?></span></div>

			<span class="desc" style="width:60px;">심방자</span>
			<div class="value" style="display:inline-block;"><?=$visitUsr['name']?> <span style="color:#999;display:inline-block;"><?=$usrPosition[$visitUsr['position']]?></span></div>

			<input type='hidden' id="usr_id" name="usr_id" value="<?=$usr['id']?>" />
			<input type='hidden' id="visit_usr_id" name="visit_usr_id" value="<?=$visitUsr['id']?>" />
		</li>
		<li>
			<label class="desc" style="width:60px;">일시</label>
			<div class="value"><?=$visit['reg_date']?></div>
		</li>
		<li>
			<label class="desc" style="width:60px;">심방종류</label>
			<div class="value"><?=$visit['type']?></div>
		</li>
		<li>
			<label class="desc" style="width:60px;">장소</label>
			<div class="value"><?=$visit['place']?></div>
		</li>
		<li>
			<label class="desc" style="width:60px;">제목</label>
			<div class="value"><?=$visit['title']?></div>
		</li>
		<li>
        	<label class="desc" style="width:60px;">내용</label>
            <textarea id="content" name="content" class="textarea" style="height:200px;width:350px;"><?=$visit['content']?></textarea>
		</li>
	</ul>
<?php
require_once('../_lib/_inner_footer.php');
?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");

$_GET['year'] = date("Y");
if($_SESSION['sys_level'] > 3){
	$root = GroupManager::getAttendanceAdminGroups();
	//echo $root;
	$list = array();
	for($i=0; $i<count($root); $i++){
        
        $list[$i] = $root[$i];
		$list[$i]['text'] = $root[$i]['title'];
		$list[$i]['icon'] = "fa fa-address-book-o";
		$attendance = AttendanceManager::getAttendanceSettingList($root[$i]['id'],$_GET['year']);
		if($attendance){
			for($f=0; $f<count($attendance); $f++){
                $list[$i]['children'][$f] = $attendance[$f];
				$list[$i]['children'][$f]['text'] =  $attendance[$f]['title'];
				$list[$i]['children'][$f]['icon'] = "fa fa-address-book-o";
				$parent = GroupManager::getParentGroup($attendance[$f]['group_id']);
				if($f%2 == 1){ $odd = "odd"; }else{  $odd = "even"; }
                //$list[$i]['children'][$f]['parent'] = $parent;
                //
			}
		}
	}
	$r = json_encode($list);
	echo $r;
}else{
	$group = GroupManager::getGroupUseAttendanceFromUsr($_SESSION['sys_id']);
	for($i=0; $i<count($group); $i++){

		$list[$i]['group_id'] = $group[$i]['id'];
		$list[$i]['text'] = $group[$i]['title'];
		
		
		$attendance = AttendanceManager::getAttendanceSettingListFromGroup($group[$i]['id']);
		$list[$i]['id'] = $attendance[0]['id'];
		
        /*
		if($attendance){
			for($f=0; $f<count($attendance); $f++){
				$list[$i]['children'][$f] = $attendance[$f];
				$parent = GroupManager::getParentGroup($attendance[$f]['group_id']);
				$list[$i]['children'][$f]['parent'] = $parent;
			}
		}*/
	}

	
	$r = json_encode($list);
	echo $r;
}
?>
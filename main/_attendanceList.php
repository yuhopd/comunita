<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");
$attendance = AttendanceManager::getAttendanceSetting($_GET['attendance_id']);
$group      = GroupManager::getGroup($attendance['group_id']);
$arryDate   = Common::getWeekDayFromYear($attendance['year'],$attendance['every_date_week']);

if($attendance['year'] == (string)date("Y")){
	$startDayFromWeek = (int) date("W")+1;
} else {
	$startDayFromWeek = 52;
}

?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2>출석부</h2>
		<ol class="breadcrumb">
			<li>
				<?=$group['title']?>
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">
			<a class="btn btn-default btn-sm" href="#" action="panoramaView">전체보기</a>
		</div>
	</div>

</div>


<div class="row wrapper wrapper-content animated fadeInRight">
	
<div class="project-list">


<div class="ibox">
    <div class="ibox-content">


		<table class="table table-hover">
			<tbody>

		<?php
		for($i=$startDayFromWeek;$i>0;$i--){
			if($arryDate[$i]){
				$thisDate = explode("-",$arryDate[$i]);
				$row1 = AttendanceManager::getReport($attendance['id'],$thisDate[0]."-".$thisDate[1]."-".$thisDate[2]);
				if($row1){
					$adddate = "Created ".date("m/d ah:i", $row1['adddate']);
				} else {
					$adddate = "&nbsp;";
				}
				?>

			<tr>
				<td class="project-status">
					<?php if($row1){ ?>
					<span class="label label-primary" report="<?=$arryDate[$i]?>">완료</span>
					<?php }else{ ?>
						<span class="label label-default" report="<?=$arryDate[$i]?>">대기</span>
					<?php } ?>
				</td>
				<td class="project-title">
					<a href="project_detail.html"><?=$thisDate[0]?>년 <?=$thisDate[1]?>월 <?=$thisDate[2]?>일</a>
					<br/>
					<small><?=$adddate?></small>
				</td>
				<td class="project-completion">
					<?php if($row1){ ?>
					<small>Completion with: 100%</small>
					<div class="progress progress-mini">
						<div style="width: 100%;" class="progress-bar"></div>
					</div>
					<?php } ?>
				</td>
				<td class="project-people">
				</td>
				<td class="project-actions">
				<?php if($row1){ ?>
					<a href="#" group="<?=$attendance['group_id']?>" attendance="<?=$attendance['id']?>" date="<?=$arryDate[$i]?>" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> 보기/수정 </a>
				<?php } else { ?>
					<a href="#" group="<?=$attendance['group_id']?>" attendance="<?=$attendance['id']?>" date="<?=$arryDate[$i]?>" class="btn btn-success btn-sm"><i class="fa fa-folder"></i> 작성 </a>
				
				<?php } ?>
				</td>
			</tr>
			<?php
			}
		}
		?>

			</tbody>
		</table>
	</div>
</div>
</div>


</div>

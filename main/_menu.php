<?php
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.AttendanceManager.php");
$thumb = UsrManager::getUsrThumb($_SESSION['sys_id']);

$usr = UsrManager::getUsr($_SESSION['sys_id']);
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo">중곡동교회</div>

                <div class="profile-element row">

                    <div class="col-md-4">
                        <?php if($thumb){ 
                            if($thumb['width'] > $thumb['height']){ $_class = " landscape-md"; }else{ $_class = " portrait-md"; }    
                        ?>
                            <span class="<?=$_class?>"><a href="#" class="userinfo" userinfo="<?=$_SESSION['sys_id']?>"><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>.<?=$thumb['ext']?>"  class="img-circle" style="width:50px;height:50px;" /></a></span>
                        <?php } else { ?>
                            <span class="thumb"><a href="#" class="userinfo" userinfo="<?=$_SESSION['sys_id']?>"><img src="../images/memder.png" class="img-circle"  style="width:50px;height:50px;"/></a></span>
                        <?php } ?>
                    </div>
                    <div class="col-md-8">
                        <a href="#" class="userinfo" userinfo="<?=$_SESSION['sys_id']?>">
                            <strong class="font-bold"><?=$_SESSION['sys_name']?></strong>
                        </a>
                        <br>
                        <?php
                        $groups = UsrManager::getGroups($usr['groups']);
                        if($groups != false){
                            for($f=0;$f<count($groups);$f++){
                                if($groups[$f]['id'] == $_GET['group_id']){ $sel = "selected"; }else{ $sel = ""; }
                            ?>
                                <span class="label <?=$sel?>" ><?=$groups[$f]['title']?></span>
                            <?php
                            }
                        }
                        unset($groups);									
                        ?>

                    </div>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">메인</span></a>
            </li>
            <li>
                <a href="#" action="calendar"><i class="fa fa-calendar"></i> <span class="nav-label">달력</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse"><li id="calendar_menu_json" class="jstree_menu_json"></li></ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-address-card-o"></i> <span class="nav-label">주소록</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse"><li id="group_menu_json" class="jstree_menu_json"></li></ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">출석부</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse"><li id="attendance_menu_json" class="jstree_menu_json"></li></ul>
            </li>
            <?php if($_SESSION['sys_level'] > 3){?>
            <li>
                <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">심방</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" id="reporttNav">
                    <li><a href="#" action="navVisitList">심방보고서</a></li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">홈페이지관리</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" id="websiteNav">
                    <li><a href="#" action="groupSettings">배너</a></li>
                    <li><a href="#" action="attendanceSettings">페이지</a></li>
                    <li><a href="#" action="reservation">예약관리</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">설정</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" id="settingsNav">
                    <li><a href="#" action="groupSettings">그룹관리</a></li>
                    <li><a href="#" action="attendanceSettings">출석부관리</a></li>
                    <li><a href="#" action="userSettings">회원관리</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

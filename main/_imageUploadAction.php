<?php
require_once("../_classes/class.UsrManager.php");

$src_path   = "../../data/usr_img";
$pathinfo = pathinfo($_FILES['file']['name']);
if(is_null($path) || $path == ''){
	$path = date("ym");
}
if(!is_dir($src_path."/".$path)){
	mkdir ($src_path."/".$path, 0777);
}


// max file size in bytes
$file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);

//specify what percentage you are resizing to
$percent_resizing = 200;
$file_ext = "jpg";

$target_storage = $src_path."/".$path."/".$file_rename.".".$file_ext;
$target_thumb_storage = $src_path."/".$path."/".$file_rename."_small.".$file_ext;
$target_src_storage = $src_path."/".$path."/".$file_rename."_src.".$file_ext;

$upload_tmp_name = $target_storage;


if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_tmp_name)) {


	$exif = exif_read_data($upload_tmp_name, 0, true);
	if($exif['IFD0']['Orientation'] == 6 || $exif['IFD0']['Orientation'] == 3 || $exif['IFD0']['Orientation'] == 8){
		if($file_ext=="jpg"){
			 $source = imagecreatefromjpeg($upload_tmp_name);
		}else if($file_ext=="png"){
			 $source = imagecreatefrompng($upload_tmp_name);
		}else{
			 $source = imagecreatefromgif($upload_tmp_name);
		}

		if($exif['IFD0']['Orientation'] == 6){
			$rotate = imagerotate($source, 270, 0);
		}else if($exif['IFD0']['Orientation'] == 3){
			$rotate = imagerotate($source, 180, 0);
		}else if($exif['IFD0']['Orientation'] == 8){
			$rotate = imagerotate($source, 90, 0);
		}
		if($file_ext=="jpg"){
			 imagejpeg($rotate, $upload_tmp_name, 100);
		}else if($file_ext=="png"){
			 imagepng($rotate, $upload_tmp_name);
		}else{
			 imagegif($rotate, $upload_tmp_name);
		}
		imagedestroy($rotate);
	}



	list($width, $height, $type, $attr)= getimagesize($upload_tmp_name);
	if($width < $height){
		$reWidth = $percent_resizing;
		$reHeight = round((($height/$width)*$percent_resizing));
	}else{
		$reHeight = $percent_resizing;
		$reWidth = round((($width/$height)*$percent_resizing));
	}
	$dst_img = imagecreatetruecolor($reWidth,$reHeight);
	if($file_ext == "jpg"){
		 $src_img = imagecreatefromjpeg($upload_tmp_name);
	}else if($file_ext == "png"){
		 $src_img = imagecreatefrompng($upload_tmp_name);
	}else{
		 $src_img = imagecreatefromgif($upload_tmp_name);
	}

	imagecolorallocate($dst_img, 255, 255, 255);
	imagecopyresized($dst_img ,$src_img , 0, 0, 0, 0, $reWidth, $reHeight, $width ,$height);
	imageinterlace($dst_img);
	if($file_ext=="jpg"){
		 imagejpeg($dst_img, $target_thumb_storage, 100);
	}else if($file_ext=="png"){
		 imagepng($dst_img, $target_thumb_storage);
	}else{
		 imagegif($dst_img, $target_thumb_storage);
	}
	imagedestroy($dst_img);
	$arryUpload = array(
		"usr_id" => $_POST['usr_id'],
		"rename" => $file_rename,
		"path" => $path,
		"ext" => $file_ext
	);
	UsrManager::updateUsrImgUpload($arryUpload);
	
	$r['url'] = $target_thumb_storage;
	$r["error"] = -1;
	$r["msg"] = "File is valid, and was successfully uploaded.";
} else {
	$r["error"] = 500;
	$r["msg"] = "Possible file upload attack!";
}




print json_encode($r);
?>
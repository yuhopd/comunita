<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.ContactList.php");
require_once("../_classes/class.UsrManager.php");

if($_GET['q'] && $_GET['q'] != ''){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
		"type"       => "family_search",
		"usr_id"     => $_GET['usr_id'],
        "keyword"    => $_GET['q']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	if($list != false){ $count = count($list); }else{ $count = 0; }
} else {
	$properties = array(
		"type"     => "family_list",
		"usr_id"   => $_GET['usr_id']
	);
	$pl    = new ContactList($properties);
	$list  = $pl->getList('X');
	if($list != false){ $count = count($list); }else{ $count = 0; }
}

for($i=0; $i<$count; $i++) {
	$relations = UsrManager::getRelations($_GET['usr_id'],$list[$i]['id']);
	$family = UsrManager::getFamilyRelations($_GET['usr_id'],$list[$i]['id'],$relations);
	$phone = Common::getPhone($list[$i]['phone']);
	$birth = str_replace ('-','.',substr($list[$i]['birth'],-5));

	if($_GET['q'] && $_GET['q'] != ''){ ?>
		<div class="forum-item row">
			<div class="col-sm-1">
				<input class="radio" type="radio" name="family_id" value="<?=$list[$i]['id']?>" />
			</div>
			<div class="col-sm-3">
				<?php
				$thumb = UsrManager::getUsrThumb($list[$i]['id']);
				if($thumb){
					if($thumb['width'] > $thumb['height']){
						$_class = "landscape-md";
					}else{
						$_class = "portrait-md";
					}	
				?>
					<div class="<?=$_class?>"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>" ><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>_small.<?=$thumb['ext']?>?v=<?=time()?>" /></a></div>
				<?php }else{ ?>
					<div class="thumb-md"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>"><img src="../css/images/member.png" /></a></div>
				<?php } ?>
			</div>
			<div class="col-sm-7">
				<div>
					<span class="name"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>"><?=$list[$i]['name']?></a></span>
					<?php if($birth != '00.00'){?>
					<small class="birth"><?=$birth?></small>
					<?php } ?>
				</div>
				<small class="phone"><?=$phone?></small>
			</div>
		</div>

	<?php } else { ?>
		<div class="forum-item row">
			<div class="col-sm-2">
				<?php
				$thumb = UsrManager::getUsrThumb($list[$i]['id']);
				if($thumb){
					if($thumb['width'] > $thumb['height']){
						$_class = "landscape-md";
					}else{
						$_class = "portrait-md";
					}	
				?>
					<div class="<?=$_class?>"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>" ><img src="../data/usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>_small.<?=$thumb['ext']?>?v=<?=time()?>" /></a></div>
				<?php }else{ ?>
					<div class="thumb-md"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>"><img src="../css/images/member.png" /></a></div>
				<?php } ?>
			</div>
			<div class="col-sm-6">
				<div>
					<span class="name"><a href="#" action="userinfo" usrid="<?=$list[$i]['id']?>"><?=$list[$i]['name']?></a></span>
					<?php if($birth != '00.00'){?>
					<small class="birth"><?=$birth?></small>
					<?php } ?>
				</div>
				<small class="phone"><?=$phone?></small>
			</div>
			<div class="col-sm-2 text-center">
				<span class="label label-default"><?=$family?></span>			
			</div>				
			<div class="col-sm-2 text-center">
				<a class="btn btn-default" href="#" action="familyDeleteAction" usrid="<?=$_GET['usr_id']?>" familyid="<?=$list[$i]['id']?>"><i class="fa fa-trash"></i></a>
			</div>
		</div>

	
	<?php } ?>

<?php
}
?>


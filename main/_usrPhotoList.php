<?php
require_once("../_lib/_inner_header.php");
?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#usrPhotoUploader').fineUploader({
			request: {
				endpoint: "../user/_visitImgAction.php",
				params: {
					"mode": "usr",
					"usr_id"  : <?=$_GET['usr_id']?>,
				}
		    },
			multiple: false,
			validation: {
				allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
				forceMultipart:true,
				sizeLimit: 5120000 // 50 kB = 50 * 1024 bytes
			},
			text: {
				uploadButton: '이미지 추가'
			},
			debug: true
		}).on('complete', function(event, id, fileName, responseJSON) {
			$.photo.list({"mode":"usrImg","usr_id":<?=$_GET['usr_id']?>,"listBox":"usrPhotoBox"});
		    $('#usrPhotoUploader .qq-upload-list').hide();
		});

		$.photo.list({"mode":"usrImg","usr_id":<?=$_GET['usr_id']?>,"listBox":"usrPhotoBox"});
	});
</script>

<div class="toolbar">
	<div class="subject">사진</div>
	<div class="tool">
		<div id="usrPhotoUploader" class="uploadBtn" ></div>
	</div>
</div>
<div class="photoBoxList" style="margin-top:10px;">
	<div id="usrPhotoBox" class="thumbList"></div>
</div>

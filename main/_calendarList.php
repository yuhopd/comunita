<?php
require_once('../_classes/class.GroupManager.php');
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>달력</h2>
        <ol class="breadcrumb">
            <li class="active" id="calendarTitle">전체</li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">

            <div class="ibox float-e-margins">
                <div class="ibox-content" id="full_calendar">
                </div>
            </div>

    </div>
</div>

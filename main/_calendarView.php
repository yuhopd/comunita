<?php
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.CalendarManager.php");

$row = CalendarManager::getEvent($_GET['id']);

$sDate = explode(" ",$row['str_date']);
$eDate = explode(" ",$row['end_date']);
$startDate = $sDate[0];
$endDate   = $eDate[0];

$_GET['group_id'] = $row['group_id'];

$adddate = date("Y/m/d H:i", $row['adddate']);
$content = nl2br($row['content']);
?>
<script type="text/javascript">
$(function() {
	$("#dialog").dialog('option', 'title', "일정보기" );
});
</script>
<ul class="form">
	<li>
		<label class="desc mini">일시</label>
		<div class="value"><?=$startDate?></div>
		<!-- input type="text" name="str_time" value="AM 00:00" id="strTime" class="text" style="width:60px; margin-left:5px;" disabled  //-->
		<div class="bar">-</div>
		<div class="value"><?=$endDate?></div>
		<!-- input type="text" name="end_time" value="AM 00:00" id="endTime" class="text" style="width:60px; margin-left:5px;" disabled //-->
		<label><input type="checkbox" name="allDay" value="O" checked="checked" style="margin-left:5px;" readonly /> 종일</label>
	</li>
	<li>
		<label class="desc mini">제목</label>
		<div class="value" style="width:350px;"><?=$row['title']?></div>
	</li>
	<li>
		<label class="desc mini">내용</label>
		<div class="value" style="width:350px;min-height:100px;"><?=$row['content']?></div>
	</li>
</ul>

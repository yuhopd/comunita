<?php
require_once("../_lib/_inner_header.php");

$arryDate = getSundaysFromYear(date("Y"),0);
$startTimestamp = mktime(0, 0, 0, 1, 1, date("Y"));
$startDayFromWeek = date("w",$startTimestamp);
if($startDayFromWeek > 0){
	$gap = (7-$startDayFromWeek) + $week;
	$startTimestamp = strtotime("+{$gap} day",$startTimestamp);
}
$mon = 0;
for($i=0;$i<26;$i++){
	$weekday_1st[$i] =  date("j",strtotime("+{$i} week",$startTimestamp));
	if($mon != date("n",strtotime("+{$i} week",$startTimestamp))){
        $mon =  date("n",strtotime("+{$i} week",$startTimestamp));
		$arryMonth_1st[$i] = $mon." 월";
	}else{
		$arryMonth_1st[$i] = "";
	}
}
$weekday_str_1st = implode("|",$weekday_1st);
$arryMonth_str_1st = implode("|",$arryMonth_1st);

$mon = 0;
for($i=26;$i<52;$i++){
	$weekday_2st[$i] =  date("j",strtotime("+{$i} week",$startTimestamp));
	
	if($mon != date("n",strtotime("+{$i} week",$startTimestamp))){
        $mon =  date("n",strtotime("+{$i} week",$startTimestamp));
		$arryMonth_2st[$i] = $mon." 월";
	}else{
		$arryMonth_2st[$i] = "";
	}
}

$weekday_str_2st = implode("|",$weekday_2st);
$arryMonth_str_2st = implode("|",$arryMonth_2st);



require_once("../_classes/class.AttendanceManager.php");
$res = $db->query("SELECT `title` FROM `attendance` WHERE id='{$_GET[attendance_id]}'");
$row = $res->fetch(PDO::FETCH_ASSOC);
$group_title = $row["title"];

$thisWeek = date("W");

$arryDate = getSundaysFromYear(date("Y"),0);
$startDayFromWeek = date("W");
if($startDayFromWeek > 51){
	$startDayFromWeek = 0;
}


?>


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2> 출석부</h2>
		<ol class="breadcrumb">
			<li><?=$group_title?></li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">
			<a class="btn btn-default btn-sm" href="javascript:$.attendance.list({});">보고서 목록</a>
			<a class="btn btn-default btn-sm" href="../attendance/excelOutput.php?attendance_id=<?=$_GET['attendance_id']?>" target="_blank">엑셀출력</a>
		</div>
	</div>
</div>


<div class="wrapper wrapper-content animated fadeInUp">
	<div class="ibox">
		<div class="ibox-content">


			<div class="rollbookBox">
				<input type="hidden" name="group_id" id="group_id" value="<?=$_GET[group_id]?>" />
				<div class="roll_left">
					<?php
					$c = 0;
					$list = AttendanceManager::getUsrListFromAttendance($_GET['attendance_id']);
					?>
					<table class="roll" width="120">
						<tr><td><?=$group_title?></td></tr>
						<tr><td>회원</td></tr>
						<?php
						for($i=0;$i<count($list);$i++){
							$usr_arry[$c] = $list[$i]['id'];
						?>
						<tr>
							<td><a href="javascript:$.common.usr_info('<?=$list[$i]['id']?>');" ><?=$list[$i]['name']?></a></td>
						</tr>
						<?php
							$c++;
						}
						?>
					</table>
				</div>
				<div class="roll_right">
					<table>
						<tr>
							<?php 
							$tmp = 0;

							$mm = 0;
							$month = "";

							$col = [];
							for($j=0; $j <= count($arryDate); $j++){
								$strDate = explode("-",$arryDate[$j]);
								if($month != $strDate[1]){
									$month = $strDate[1];
									$mm = $tmp;
									$col[] = $tmp;
									$tmp = 1;
								}else{
									$tmp++;
								}
							}
							for($z=0; $z < count($col) -1 ; $z++){
								?>
									<th align="center" colspan="<?=$col[$z+1]?>" <?=$thisWeek_class?> ><?=$z+1?></th>
								<?php	
							}
							?>
						</tr>
						<tr>
						<?php		
						for($j=0; $j < count($arryDate); $j++){
							$strDate = explode("-",$arryDate[$j]);
							$thisWeek_class = ($thisWeek == ($j+1))? " class='thisweek'" : ""; 
						?>
							<td align="center" <?=$thisWeek_class?>><?=$strDate[2]?></td>
						<?php } ?>
						</tr>
					<?php 
					for($i = 0; $i<$c; $i++){
						$query = "SELECT `id`, `worship`, `date` FROM `attendance_usr` WHERE `usr_id`='{$usr_arry[$i]}'";	
						$res   = $db->query($query);
						while($row = $res->fetch(PDO::FETCH_ASSOC)){
						     $arryUsrWorship[$row['date']] = $row['worship'];
							 $arryUsrAttID[$row['date']] = $row['id'];
						}
					?>
						<tr>
						<?php
						for($j=0; $j<count($arryDate); $j++){
							$strDate = explode("-",$arryDate[$j]);
							$check_class = "fa fa-2x fa-edit";
							if($arryUsrWorship[$arryDate[$j]]){
								if($arryUsrWorship[$arryDate[$j]] == 'O'){
									$check_title = "출석";
									$check_class = "fa fa-check-circle-o";
								} else if($arryUsrWorship[$arryDate[$j]] == 'L'){
									$check_title = "지각";
									$check_class = "fa fa-clock-o";
								} else if($arryUsrWorship[$arryDate[$j]] == 'X'){
									$check_title = "결석";
									$check_class = "fa fa-times-circle-o";
								} else {
									$check_title = "입력";
									$check_class = "fa fa-edit";	
								}
							}

							$thisWeek_tag = ($thisWeek == $j)? " thisweek" : ""; 
							$item_odd = ($j%2 == 1)? " class='odd {$thisWeek_tag}'" : "";
						?>
							<td<?=$item_odd?> align="center" <?=$thisWeek_tag?>>
								<a href="#" class="check" mm="<?=$arryDate[$j]?>" dd="<?=$usr_arry[$i]?>" ><i class="<?=$check_class?>"></i></a>
							</td>
						<?php 
						}
						?>
						</tr>
					<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

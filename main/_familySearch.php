<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
$usr = UsrManager::getUsr($_GET['usr_id']);
$birth = str_replace ('-','.',substr($usr['birth'],-5));
?>
<div class="ibox ">
	<div class="ibox-content">
	<ul style="list-style: none;padding: 0;">
		<li style="margin-bottom:5px;">
			<div class="search">
				<input type="hidden" id="usr_id" name="usr_id" value="<?=$_GET['usr_id']?>">
				<input type="text" name="family_q" id="family_q" value="<?=$_GET['q']?>" class="text"style="width:150px;"/>
				<a href="#" class="btn btn-primary btn-xs" action="familySearch" usrid="<?=$_GET['usr_id']?>">검색</a>
			</div>
		</li>
		<li>
			<div id="familySearchList" class="familyList" style="height:200px;background-color:#fff;"></div>
		</li>
		<li style="margin-top:5px;">
			<select id="relations" name="relations" >
			    <option value="0" >가족관계선택</option>
			    <option value="1" >부부</option>
				<option value="2" >부모/자녀</option>
				<option value="3" >형제/자매</option>
				<option value="4" >조부모/손자손녀</option>
				<option value="5" >시부모/사위/며느리</option>
				<option value="9" >친척</option>
			</select>
			<a href="#" class="btn btn-primary btn-xs" action="familyInsertAction" usrid="<?=$_GET['usr_id']?>">가족관계 추가</a>
		</li>
	</ul>

	</div>
</div>



	
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.AttendanceManager.php");
$usr = UsrManager::getUsr($_GET['usr_id']);
if($usr['address_1']) {
	$addr1 = $usr['address_1'];
	$addr2 = $usr['address_2'];
	$addressResult = $addr1." ".$addr2;
?>
<script type="text/javascript">
	$(document).ready(function() {
		$.common.getAddressResultMap();
	});
</script>
<?php
}else{
	$addressResult = "입력된 주소가 없습니다. ";
}
?>


<div id="addressResultLabel" style="margin:10px;"><?=$addressResult?></div>
<div id="addressResultMap" style="height:300px;background-color:#fff;margin:10px;"></div>
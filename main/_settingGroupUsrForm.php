<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");

$query = "SELECT * FROM `group` WHERE id='$_GET[group_id]'";
$res   = $db->query($query);
$row   = $res->fetch(PDO::FETCH_ASSOC);



$res1 = $db->query("SELECT `id`, `title`, `gisu` FROM `sameage`");
while($row1 = $res1->fetch(PDO::FETCH_ASSOC)){
	$sameage[$row1['id']]['title'] = $row1['title'];
	$sameage[$row1['id']]['gisu']  = $row1['gisu'];
}

	$root_id = GroupManager::getRootId($_GET['group_id']);


	$properties1 = array(
		"type"     => "allWithRootGroup",
		"root_id"  => $root_id,
		"browse"   => 2
	);

	$pl1    = new UsrPhoneList($properties1);
	$allUsrlist  = $pl1->getList('X');


	$properties2 = array(
		"browse"   => 2,
		"group_level" => 0,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl2    = new UsrPhoneList($properties2);
	$groupUsrlist_0  = $pl2->getList('X');


	$properties3 = array(
		"browse"   => 2,
		"group_level" => 1,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl3    = new UsrPhoneList($properties3);
	$groupUsrlist_1  = $pl3->getList('X');

	$properties4 = array(
		"browse"   => 2,
		"group_level" => 2,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl4    = new UsrPhoneList($properties4);
	$groupUsrlist_2  = $pl4->getList('X');

    $rootGroups = GroupManager::getAttendanceAdminGroups();

?>



<div class="row">
	<form id="groupUsrForm" class="form-horizontal">
		<div class="col-md-12">

			<input type="hidden" name="mode" value="usr_edit">
			<input type="hidden" id="group_id" name="group_id" value="<?=$_GET['group_id']?>">

			<div class="form-group">
				<label for="group_title" class="col-md-3 control-label">그룹명</label>
				<div class="col-md-9">	
					<input id="group_title" value="<?=$row['title']?>" class="form-control" readonly />
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
			
					<div class="input-group" style="margin-bottom:10px;">
						<select name="roots" id="roots" size="1" class="input-xs form-control input-s-xs inline">
							<?php for($f=0; $f<count($rootGroups); $f++ ) { ?>
								<option value="<?=$rootGroups[$f]['id']?>" <?php if($rootGroups[$f]['id'] == $root_id){ ?> selected <?php } ?> ><?=$rootGroups[$f]['title']?></option>
							<?php } ?>
						</select>
						<span class="input-group-btn ">
							<a href="#" action="rootGroupUsrList" class="btn btn-primary btn-xs">변경</a>
						</span>
					</div>

					<div id="groupUsrListBox">
						<select name="left[]" id="unSelected" multiple="multiple" size="19" class="form-control">
						<?php
						$countAll = count($allUsrlist);
						if($allUsrlist){
						for($i=0; $i<$countAll; $i++ ) { ?>
							<option value="<?=$allUsrlist[$i]['id']?>"><?=$allUsrlist[$i]['name']?>(<?=$sameage[$allUsrlist[$i]['sameage_id']]['title']?>)</option>
						<?php
						}}
						?>
						</select>
					</div>
				</div>
				<div class="col-md-2 text-center">
					<br />
					<a id="options_right_2" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-right"></i></a>

					<a id="options_left_2" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-left"></i></a>
					<br /><br /><br />
					<a id="options_right_1" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-right"></i></a>

					<a id="options_left_1" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-left"></i></a>
					<br /><br /><br />
					<a id="options_right_0" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-right"></i></a>

					<a id="options_left_0" class="btn btn-block btn-default btn-xs"><i class="fa fa-arrow-left"></i></a>
				</div>
				<div class="col-md-5">
					<div style="margin-bottom:14px;">	
						<label>관리자</label>
						<select name="right2[]" id="selected_level_2" multiple="multiple" size="3" class="form-control">
						<?php
						$count_2 = count($groupUsrlist_2);
						if($groupUsrlist_2){
						for($i=0; $i<$count_2; $i++ ) { ?>
							<option value="<?=$groupUsrlist_2[$i]['id']?>"><?=$groupUsrlist_2[$i]['name']?>(<?=$sameage[$groupUsrlist_2[$i]['sameage_id']]['title']?>)</option>
						<?php }
						}
						?>
						</select>
					</div>
					<div  style="margin-bottom:14px;">
					
						<label>그룹리더</label>
						<select name="right1[]" id="selected_level_1" multiple="multiple" size="3" class="form-control">
						<?php
						$count_1 = count($groupUsrlist_1);
						if($groupUsrlist_1){
						for($i=0; $i<$count_1; $i++ ) { ?>
							<option value="<?=$groupUsrlist_1[$i]['id']?>"><?=$groupUsrlist_1[$i]['name']?>(<?=$sameage[$groupUsrlist_1[$i]['sameage_id']]['title']?>)</option>
						<?php
						}}
						?>
						</select>
					</div>
					<div>
						<label>그룹회원</label>
						<select name="right0[]" id="selected_level_0" multiple="multiple" size="8" class="form-control">
						<?php
						$count_0 = count($groupUsrlist_0);
						if($groupUsrlist_0){
						for($i=0; $i<$count_0; $i++ ) { ?>
							<option value="<?=$groupUsrlist_0[$i]['id']?>"><?=$groupUsrlist_0[$i]['name']?>(<?=$sameage[$groupUsrlist_0[$i]['sameage_id']]['title']?>)</option>
						<?php }}?>
						</select>
					</div>
				</div>

			</div>
		</div>
	</form>	
</div>

<?php
require_once("../_lib/_inner_footer.php");
?>
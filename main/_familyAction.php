<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.VisitManager.php");
require_once("../_classes/class.PhotoManager.php");
require_once('../_lib/_docs_log.inc');

switch ($_REQUEST[mode])
{
	/******************************************************************************
    * 회원 가족 관계 삭제
    ******************************************************************************/
    case "usr_family_delete":
		UsrManager::delFamily($_GET['usr_id'],$_GET['family_id']);
		$r["error"]    = -1;
		$r["message"]  = "가족관계가 삭제되었습니다. ";
		print json_encode($r);
		exit;
	break;

	/******************************************************************************
    * 회원 가족 관계 추가
    ******************************************************************************/
    case "usr_family_insert":
        $usr = UsrManager::getUsr($_GET['usr_id']);
        $family = UsrManager::getUsr($_GET['family_id']);

        if($_GET['relations'] == 1 && $usr['gender'] == $family['gender']){
			$r["error"]    = 1;
		    $r["message"]  = "같은 성별의사람입니다. 부부로 설정할 수 없습니다.";
		    print json_encode($r);
			exit;
		}

		UsrManager::addFamily($_GET['usr_id'],$_GET['family_id'],$_GET['relations']);

		$r["error"]    = -1;
		$r["message"]  = "가족관계가 설정 되었습니다. ";
		print json_encode($r);
		exit;

	break;
}

?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.VisitManager.php");
require_once("../_classes/class.PhotoManager.php");



require_once('../_lib/_docs_log.inc');

switch ($_REQUEST[mode])
{
    /******************************************************************************
    * 회원정보 변경
    * requred params(_POST) :: friend_id
    ******************************************************************************/
    case "visitInsert":
		$visit = array(
			'usr_id'       => $_POST['usr_id'],
			'visit_usr_id' => $_POST['visit_usr_id'],
			'reg_date'     => $_POST['reg_date'],
			'type'         => $_POST['type'],
			'place'        => $_POST['place'],
			'title'        => $_POST['title'],
			'content'      => $_POST['content']
		);

		$lastID = VisitManager::insert($visit);
		PhotoManager::update($lastID);
		$r["error"]    = -1;
		$r["msg"]      = "등록되었습니다.";
		$r["lastID"]   = $lastID;
		print json_encode($r);

	break;

    /******************************************************************************
    * 회원 이미지 업로드
    * requred params(_GET) :: friend_id
    ******************************************************************************/
    case "visitModify":
		$visit = array(
			'visit_id'     => $_POST['id'],
			'usr_id'       => $_POST['usr_id'],
			'visit_usr_id' => $_POST['visit_usr_id'],
			'reg_date'     => $_POST['reg_date'],
			'type'         => $_POST['type'],
			'place'        => $_POST['place'],
			'title'        => $_POST['title'],
			'content'      => $_POST['content']
		);

		$lastID = VisitManager::modify($visit);
		$r["error"]    = -1;
		$r["msg"]      = "수정되었습니다.";
		$r["lastID"]   = $lastID;
		print json_encode($r);
	break;

    /******************************************************************************
    * 친구 승인 거부
    * requred params(_GET) :: friend_id
    ******************************************************************************/
    case "visitDelete":
		$row = VisitManager::getItem($_GET['id']);
		VisitManager::del($row['id']);

		$r["usr_id"] = $row["usr_id"];
		$r["error"]    = -1;
		$r["message"]  = "삭제되었습니다.";
		print json_encode($r);
	break;
}

?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
$usr = UsrManager::getUsr($_GET['usr_id']);
?>
<div class="ibox ">
	<div class="ibox-title">
		<h5>노트</h5>
		<div class="ibox-tools">
			<a href="#" usrid="<?=$_GET['usr_id']?>" action="userInfoSummaryModify" id="userInfoSummaryModify" class="btn btn-primary btn-xs">수정</a>
			<a href="#" usrid="<?=$_GET['usr_id']?>" action="userInfoSummarySave" id="userInfoSummarySave" style="display:none;" class="btn btn-primary btn-xs">저장</a>			
		</div>
	</div>
	<div class="ibox-content">
		<textarea id="userInfoSummary" name="userInfoSummary" class="form-control" disabled="disabled" style="width:100%; height:250px;"><?=nl2br($usr['summary']);?></textarea>	
	</div>
</div>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");
$res = $db->query("SELECT `phone` FROM `usr` WHERE `id`='{$_SESSION['sys_id']}'");
$row = $res->fetch(PDO::FETCH_ASSOC);
$sys_phone  = $row['phone'];

$reserve_date = date("Y-m-d");
$time_h = date("G");
$time_m = date("i");
//-----------------------------------------------------------------------------
?>
<table class="sms">
	<tr>
		<td class="smsBody">
		<form id="smsForm" >
			<ul class="form">
				<li style="padding:5px 5px 5px 20px; color:#999; line-height:150%;">
					SMS는 최대80자 / LMS는 무제한 입니다.<br/> 
					장문 문자를 보낼 때 LMS를 이용하세요.
				</li>
				<li>
         			<label style="width:70px;" for="snd_number">문자종류</label>
					<label style="width:50px;">SMS <input type="radio" class="i-checks"  name="sms_type" value="SMS" onclick="$.sms.smsTypeChange();" checked="checked" /></label>
					<label style="width:50px;"><input type="radio" class="i-checks"  name="sms_type" value="LMS" onclick="$.sms.smsTypeChange();"/> LMS</label>
				</li>
				<li>
					<label style="width:70px;" for="snd_number">보내는 사람</label>
					<input type="text"  class="form-control"name="snd_number" value="<?=$sys_phone?>" style="width:150px;" />
				</li>
				<li>
					<label style="width:70px;">내용<br /><br />(<span id="strLimit">0</span> / <span id="defaultLimit">80</span>)</label>
					<textarea name="sms_content" id="sms_content" class="textarea" style="width:230px; height:100px;"></textarea>
				</li>
				<li>
					<label style="width:70px;">약속말</label>
					<a action="addKey" mode="name" href="#" class="btn btn-default btn-xs" >이름</a>
					<a action="addKey" mode="position" href="#" class="btn btn-default btn-xs" >직분</a>
				</li>
				<li class="form-group">
					<label style="width:70px;">예약 <input type="checkbox" name="isReserve" class="i-checks" id="isReserve" value="O"/></label>
					<input type="text" class="form-control" name="reserve_date" id="reserve_date" value="<?=$reserve_date?>" style="width:85px;" disabled="disabled" />
					<select name="reserve_time_h" disabled="disabled" class="form-control" style="width:80px;">
						<option value="00" <?php if($time_h == "00"){?>selected="selected"<?php } ?> >AM 00시</option>
						<option value="01" <?php if($time_h == "01"){?>selected="selected"<?php } ?> >AM 01시</option>
						<option value="02" <?php if($time_h == "02"){?>selected="selected"<?php } ?> >AM 02시</option>
						<option value="03" <?php if($time_h == "03"){?>selected="selected"<?php } ?> >AM 03시</option>
						<option value="04" <?php if($time_h == "04"){?>selected="selected"<?php } ?> >AM 04시</option>
						<option value="05" <?php if($time_h == "05"){?>selected="selected"<?php } ?> >AM 05시</option>
						<option value="06" <?php if($time_h == "06"){?>selected="selected"<?php } ?> >AM 06시</option>
						<option value="07" <?php if($time_h == "07"){?>selected="selected"<?php } ?> >AM 07시</option>
						<option value="08" <?php if($time_h == "08"){?>selected="selected"<?php } ?> >AM 08시</option>
						<option value="09" <?php if($time_h == "09"){?>selected="selected"<?php } ?> >AM 09시</option>
						<option value="10" <?php if($time_h == "10"){?>selected="selected"<?php } ?> >AM 10시</option>
						<option value="11" <?php if($time_h == "11"){?>selected="selected"<?php } ?> >AM 11시</option>
						<option value="12" <?php if($time_h == "12"){?>selected="selected"<?php } ?> >PM 12시</option>
						<option value="13" <?php if($time_h == "13"){?>selected="selected"<?php } ?> >PM 01시</option>
						<option value="14" <?php if($time_h == "14"){?>selected="selected"<?php } ?> >PM 02시</option>
						<option value="15" <?php if($time_h == "15"){?>selected="selected"<?php } ?> >PM 03시</option>
						<option value="16" <?php if($time_h == "16"){?>selected="selected"<?php } ?> >PM 04시</option>
						<option value="17" <?php if($time_h == "17"){?>selected="selected"<?php } ?> >PM 05시</option>
						<option value="18" <?php if($time_h == "18"){?>selected="selected"<?php } ?> >PM 06시</option>
						<option value="19" <?php if($time_h == "19"){?>selected="selected"<?php } ?> >PM 07시</option>
						<option value="20" <?php if($time_h == "20"){?>selected="selected"<?php } ?> >PM 08시</option>
						<option value="21" <?php if($time_h == "21"){?>selected="selected"<?php } ?> >PM 09시</option>
						<option value="22" <?php if($time_h == "22"){?>selected="selected"<?php } ?> >PM 10시</option>
						<option value="23" <?php if($time_h == "23"){?>selected="selected"<?php } ?> >PM 11시</option>
					</select>

					<select name="reserve_time_m" disabled="disabled" class="form-control" style="width:50px;" >
						<option value="00">00분</option>
						<option value="05">05분</option>
						<option value="10">10분</option>
						<option value="15">15분</option>
						<option value="20">20분</option>
						<option value="25">25분</option>
						<option value="30">30분</option>
						<option value="35">35분</option>
						<option value="40">40분</option>
						<option value="45">45분</option>
						<option value="50">50분</option>
						<option value="55">55분</option>
					</select>
				</li>
			</ul>
		</form>
		</td>
		<td class="reserve">

			<h3>받는사람</h3>

			<div id="smsUsrsList" class="usrGroupBoxList" style="height:250px;">
				<?php
				if($_POST['rcv']){
				$list = array_unique(explode("|",$_POST['rcv']));

					for($i=0; $i<count($list); $i++ ) {
						$usr = UsrManager::getUsr($list[$i]);
					?>
					<div class="usrGroup" usrId="<?=$usr['id']?>" title="<?=$usr['phone']?>">
						<input type="hidden" name="phones[]" value="<?=$usr['id']?>" />
						<?=$usr['name']?> <a class="remove" action="sendDeleteUsr" usrId="<?=$usr['id']?>"><i class="fa fa-times"></i></a>
					</div>
					<?php
					}
				}
				?>
			</div>

		</td>
		<td class="search">

			<div class="search">
				<input type="text" name="sms_q" id="sms_q" value="<?=$_GET['q']?>" class="text" style="width:150px;" placeholder="연락처 검색"/>
				<a href="#" action="smsSearch" class="btn btn-xs btn-primary">검색</a>
			</div>
			<div id="smsSearchList" class="familyList" style=" height:250px;"></div>

		</td>
	</tr>
</table>

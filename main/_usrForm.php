<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrManager.php");

$usrPosition = array('선택하세요!','서리집사','안수집사','장로','권사');

if($_GET['mode'] == "usr_modify"){
	$usr = UsrManager::getUsr($_GET['usr_id']);
}

$usrStatus    = UsrManager::$usrStatus;
$usrStatusVal = array_values($usrStatus);
$usrStatusKey = array_keys($usrStatus);

$usrPosition    = UsrManager::$usrPosition;
$usrPositionVal = array_values($usrPosition);
$usrPositionKey = array_keys($usrPosition);

$usrChristen    = UsrManager::$usrChristen;
$usrChristenVal = array_values($usrChristen);
$usrChristenKey = array_keys($usrChristen);


?>


<form id="userForm" class="form-horizontal">
	<?php if($_GET['mode'] == "usr_modify"){ ?>
	<input type="hidden" name="usr_id" value="<?=$_GET['usr_id']?>">
	<?php } ?>
	<input type="hidden" name="mode" value="<?=$_GET['mode']?>">
	
		<div class="form-group">
			<label class="col-md-3 control-label" for="name">이름</label>
			<div class="col-md-9">
				<input id="name" name="name" type="text" value="<?=$usr['name']?>" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="tel">전화</label>
			<div class="col-md-9">
				<input id="tel" name="tel" type="text" value="<?=$usr['tel']?>" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="phone">핸드폰</label>
			<div class="col-md-9">
				<input id="phone" name="phone" type="text" value="<?=$usr['phone']?>"  class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="sameage">동기(기수)</label>
			<div class="col-md-9">
				<select id="sameage" name="sameage_id"  class="form-control" style="width:150px;">
					<option value="0">선택하세요.</option>
					<?php
					$sameage = UsrManager::getSameageList();
                    for($i = 0; $i < count($sameage); $i++){
					?>
						<option value="<?=$sameage[$i]['id']?>" <?php if($sameage[$i]['id'] == $usr['sameage_id']){?> selected <?php } ?>><?=$sameage[$i]['title']?> (<?=$sameage[$i]['gisu']?> 기)</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="email">이메일</label>
			<div class="col-md-9">
				<input id="email" name="email" type="text" value="<?=$usr['email']?>"  class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="address">주소</label>
			<div class="col-md-9">
				<input id="address" name="address_1" type="text" value="<?=$usr['address_1']?>"  class="form-control"/>
				<input id="address" name="address_2" type="text" value="<?=$usr['address_2']?>"  class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="birth">생년월일</label>
			<div class="col-md-9">
				<input id="birth" name="birth" type="text"  style="width:80px; float:left; margin-right:10px;" value="<?=$usr['birth']?>"  class="form-control"/>

				<input type="checkbox" id="lunar" name="lunar" value="O" <?php if($usr['lunar'] == "O"){ ?>checked="checked" <?php } ?> /> <label class="col-md-3 control-label" for="lunar">음력</label>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="status">성도구분</label>
			<div class="col-md-9">
            <select id="status" name="status" class="form-control">
				<?php for ($i=0 ; $i<count($usrStatusKey); $i++){ ?>
					<option value="<?=$usrStatusKey[$i]?>" <?php if($usrStatusKey[$i] == $usr['status']){?> selected <?php } ?>><?=$usrStatusVal[$i]?></option>
				<?php } ?>
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="householder">세대주</label>
			<div class="col-md-9">
				<input name="householder" type="text" value="<?=$usr['householder']?>"  class="form-control"/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="christen">신급</label>
			<div class="col-md-9">
            <select id="christen" name="christen" class="form-control">
				<?php for ($i=0 ; $i<count($usrChristenKey); $i++){ ?>
					<option value="<?=$usrChristenKey[$i]?>" <?php if($usrChristenKey[$i] == $usr['christen']){?> selected <?php } ?>><?=$usrChristenVal[$i]?></option>
				<?php } ?>
			</select>
			</div>
		</div>


		<div class="form-group">
			<label class="col-md-3 control-label" for="position">직분</label>
			<div class="col-md-9">
            <select id="position" name="position" class="form-control">
				<?php for ($i=0 ; $i<count($usrPositionKey); $i++){ ?>
					<option value="<?=$usrPositionKey[$i]?>" <?php if($usrPositionKey[$i] == $usr['position']){?> selected <?php } ?>><?=$usrPositionVal[$i]?></option>
				<?php } ?>
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="gender">성별</label>
			<div class="col-md-9">
            <select id="gender" name="gender" class="form-control">
				<option value="M" <?php if($usr['gender'] == "M"){?> selected <?php } ?>>남자</option>
				<option value="F" <?php if($usr['gender'] == "F"){?> selected <?php } ?>>여자</option>
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="car_number">차량번호</label>
			<div class="col-md-9">
				<input name="car_number" type="text" value="<?=$usr['car_number']?>" class="form-control" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="summary">메모</label>
			<div class="col-md-9">
            <textarea id="summary" name="summary" style="height:50px;" class="form-control"><?=$usr['summary']?></textarea>
			</div>
		</div>

		<?php if($_GET['mode'] == "usr_modify"){ ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="groups">그룹</label>
			<div class="col-md-9" id="usr_group_list"></div>
		</div>
		<?php }else{ ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="groups">그룹</label>
			<div class="col-md-9"><div class="value">그룹추가는 일단 회원추가를 한 다음,<br /> 회원 수정할 때 사용할 수 있습니다. </div></div>
		</div>
		<?php } ?>
</form>

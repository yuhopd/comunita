<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.CalendarManager.php");
require_once("../_classes/class.GroupManager.php");


switch ($_REQUEST['mode'])
{

    /******************************************************************************
    * 일정 추가
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
    case "insert":

		$gColor = GroupManager::getUseCalendarGroupColor();

		$arryCalendar = array(
		    'domain_id'   => 1,
		    'usr_id'      => $_POST['usr_id'],
			'group_id'    => $_POST['group_id'],
			'title'       => $_POST['title'],
			'content'     => $_POST['content'],
			'str_date'    => $_POST['str_date'],
			'end_date'    => $_POST['end_date'],
			'group_id'    => $_POST['group_id'],
			'allDay'      => $_POST['allDay']
		);

        $sDate = explode("-",$_POST['str_date']);
        $startDate = mktime(0, 0, 0, $sDate[1], $sDate[2], $sDate[0]);
		$eDate = explode("-",$_POST['end_date']);
		$endDate = mktime(0, 0, 0, $eDate[1], $eDate[2], $eDate[0]);

		$lastId = CalendarManager::insert($arryCalendar);
		if($lastId){
			$r['id']        = $lastId;
			$r['title']     = $_POST['title'];
			$r['startDate'] = $startDate;
			$r['endDate']   = $endDate;
			$r['color']     = $gColor[$_POST['group_id']];
			$r["error"]     = -1;
			$r["msg"]       = "등록되었습니다.";
			print json_encode($r);
		} else {
			$r["error"]    = 10;
			$r["msg"]      = "등록실패하였습니다.";
			print json_encode($r);
		}
	break;

    /******************************************************************************
    * 보고서 추가 (달란트)
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
    case "modify":
		$gColor = GroupManager::getUseCalendarGroupColor();
		$arryCalendar = array(
		    'domain_id'   => 1,
		    'id'          => $_POST['id'],
		    'usr_id'      => $_POST['usr_id'],
			'group_id'    => $_POST['group_id'],
			'title'       => $_POST['title'],
			'content'     => $_POST['content'],
			'str_date'    => $_POST['str_date'],
			'end_date'    => $_POST['end_date'],
			'group_id'    => $_POST['group_id'],
			'allDay'      => $_POST['allDay']
		);

        $sDate = explode("-",$_POST['str_date']);
        $startDate = mktime(0, 0, 0, $sDate[1], $sDate[2], $sDate[0]);
		$eDate = explode("-",$_POST['end_date']);
		$endDate = mktime(0, 0, 0, $eDate[1], $eDate[2], $eDate[0]);

		$result = CalendarManager::modify($arryCalendar);
		if($result){
			$r['id']        = $_POST['id'];
			$r['title']     = $_POST['title'];
			$r['startDate'] = $startDate;
			$r['endDate']   = $endDate;
			$r['color']     = $gColor[$_POST['group_id']];
			$r["error"]     = -1;
			$r["msg"]       = "수정되었습니다.";
			print json_encode($r);
		} else {
			$r["error"]     = 10;
			$r["msg"]       = "등록실패하였습니다.";
			print json_encode($r);
		}
	break;

    /******************************************************************************
    * 보고서 추가 (달란트)
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
    case "delete":
		$event = CalendarManager::del($_GET['id']);

        $sDate = explode("-",$event['str_date']);
        $startDate = mktime(0, 0, 0, $sDate[1], $sDate[2], $sDate[0]);
		$eDate = explode("-",$event['end_date']);
		$endDate = mktime(0, 0, 0, $eDate[1], $eDate[2], $eDate[0]);

		$r['id']        = $_GET['id'];
		$r['title']     = $event['title'];
		$r['startDate'] = $startDate;
		$r['endDate']   = $endDate;
		$r["error"]     = -1;
		$r["msg"]       = "삭제되었습니다.";
		print json_encode($r);
	break;
}
require_once("../_lib/_inner_footer.php");
?>
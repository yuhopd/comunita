
<?php
require_once("../_lib/common_function.php");
require_once("../_classes/class.DBConnection.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.MessageManager.php");
session_start();
$html_metas["title"] = (isset($custom_title)) ? $custom_title : "HIMH";
$html_metas["description"] = (isset($custom_description)) ? $custom_description : "himh.net, iles anytime, anywhere.";
$html_metas["keywords"] = (isset($custom_keywords)) ? $custom_keywords : "CRM, Online web office";
$html_metas["author"] = (isset($custom_author)) ? $custom_author : "himh";
if(!$_SESSION['sys_id']){ header("Location: ../account/index.php"); }
$db =  DBConnection::get()->handle();
header("Content-type: text/html; charset=utf-8");
if($_SESSION['sys_id']){
	$strNoMessageRead = "";
	$noMessageRead = MessageManager::getNoReadCount($_SESSION['sys_id']);
    if($noMessageRead > 0){
		$strNoMessageRead = "(".$noMessageRead.")";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>HIMH</title>
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="YES" />
    
    <meta name="description" content="<?=$html_metas['description']?>" />
    <meta name="keywords" content="<?=$html_metas['keywords']?>" />
    <meta name="author" content="<?=$html_metas['author']?>" />

    <link rel="stylesheet" type="text/css" href="../css/himh.css" />
	<script type="text/javascript" src="../js/build/require-min.js" data-main="../js/himh/Main"></script>
</head>
<body class="fixed-sidebar pace-done">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <div id="wrapper">
        <?php include "_menu.php"; ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i></a>
                        <form role="search" class="navbar-form-custom" id="SearchContact">
                            <input type="search" name="keyword" placeholder="연락처 검색" class="form-control"  />
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="../account/signout.php">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper" id="mainBox"></div>
        </div>
    </div>
	<div id="usr_dialog"></div>
	<div id="sub_dialog"></div>
	<div id="dialog"></div>
	<div id="search_dialog"></div>
	<div id="kk_dialog"></div>
</body>
</html>

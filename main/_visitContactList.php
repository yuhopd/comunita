<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.ContactList.php");
require_once("../_classes/class.UsrManager.php");

$_GET['q'] =  rawurldecode($_GET['q']);
$properties = array(
	"type"       => "search",
	"usr_id"     => $_GET['usr_id'],
	"keyword"    => $_GET['q']
);
$pl    = new ContactList($properties);
$list  = $pl->getList('X');
if($list != false){ $count = count($list); }else{ $count = 0; }
?>

<?php
for($i=0; $i<$count; $i++) {
	$birth = str_replace ('-','.',$list[$i]['birth']);

	$phone = Common::getPhone($list[$i]['phone'],$list[$i]['tel']);

	if($list[$i]['position'] == 0){
		$position = "";
		$positionStr = "";
	}else{
		$position = "<span class='position'>".UsrManager::$usrPosition[$list[$i]['position']]."</span>";
		$positionStr = UsrManager::$usrPosition[$list[$i]['position']];
	}
?>

	<div class="card" userId="<?=$list[$i]['id']?>" >
		<a href="#" action="visitAddUser" meta="<?=$list[$i]['id']?>@<?=$list[$i]['name']?>@<?=$positionStr?>@<?=$phone?>" class="btn btn-xs btn-default pull-right" style="margin-right:10px;">추가</a>

		<?php
		$thumb = UsrManager::getUsrThumb($list[$i]['id']);
		if($thumb){
			if($thumb['width'] > $thumb['height']){
				$_class = " landscape";
			}else{
				$_class = " portrait";
			}
		?>
			<div class="thumb-sm"><a class="userinfo" href="#" usrId="<?=$list[$i]['id']?>"><img src="../../../usr_img/<?=$thumb['path']?>/<?=$thumb['rename']?>.<?=$thumb['ext']?>" /></a></div>
		<?php } else { ?>

			<div class="thumb-sm"><img alt="image" class="img-responsive" src="../img/member.png"></div>
		<?php
		}
		?>

		<div class="info">
			<span class="name"><a class="userinfo" href="#" usrId="<?=$list[$i]['id']?>"><?=$list[$i]['name']?><?=$position?></a></span>
			<?php if($birth != '00.00'){?>
			<span class="birth"><?=$birth?><?php if($list[$i]['lunar'] == "O"){ ?>(음)<?php } ?></span>
			<?php } ?>
		</div>
	</div>
<?php
}
?>

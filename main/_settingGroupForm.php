<?php
require_once("../_lib/_inner_header.php");
if($_GET['mode'] == 'modify'){
	$query = "SELECT * FROM `group` WHERE id='$_GET[group_id]'";
	$res   = $db->query($query);
	$row   = $res->fetch(PDO::FETCH_ASSOC);
}
?>



<div class="row">
	<div class="col-md-12">

		<form id="groupForm" class="form-horizontal">
			<?php if($_GET['mode'] == 'modify'){ ?>
			<input type="hidden" name="mode" value="modify">
			<input type="hidden" name="parent_id" value="<?=$row['parent_id']?>">
			<?php }else{ ?>
			<input type="hidden" name="mode" value="insert">
			<input type="hidden" name="parent_id" value="<?=$_GET['parent_id']?>">
			<?php } ?>

		
			<div class="form-group">
				<label for="group_title" class="col-md-3 control-label">그룹명</label>
				<div class="col-md-9">	
					<input type="text" id="group_title" name="title" value="<?=$row['title']?>" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label for="is_enable" class="col-md-3 control-label">사용</label>
				<div class="col-md-9">
					<select id="is_enable" name="is_enable" class="form-control">
						<option value="X" <?php if($row['is_enable'] == "X"){?> selected <?php } ?>>사용 안 함</option>
						<option value="O" <?php if($row['is_enable'] == "O"){?> selected <?php } ?>>사용 함</option>
						<option value="G" <?php if($row['is_enable'] == "G"){?> selected <?php } ?>>그룹으로 사용</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="use_attendance" class="col-md-3 control-label">출석부관리</label>
				<div class="col-md-9">
				<select id="use_attendance" name="use_attendance" class="form-control">
					<option value="X" <?php if($row['use_attendance'] == "X"){?> selected <?php } ?>>사용 안 함</option>
					<option value="O" <?php if($row['use_attendance'] == "O"){?> selected <?php } ?>>출석부 관리 그룹</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="use_calendar" class="col-md-3 control-label">달력</label>
				<div class="col-md-9">
					<select id="use_calendar" name="use_calendar" class="form-control">
						<option value="X" <?php if($row['use_calendar'] == "X"){?> selected <?php } ?>>사용 안 함</option>
						<option value="O" <?php if($row['use_calendar'] == "O"){?> selected <?php } ?>>사용함</option>
					</select>
				</div>
			</div>
			<div class="form-group">	    
				<label for="group_summary" class="col-md-3 control-label">설명</label>
				<div class="col-md-9">
				<textarea id="group_summary" name="summary" class="form-control"><?=$row['summary']?></textarea>
				</div>
			</div>

		</form>
	</div>
</div>



<?php
require_once('../_lib/_inner_header.php');
require_once("../_classes/class.CalendarManager.php");
require_once("../_classes/class.GroupManager.php");
if(!$_GET['group_id']){ $_GET['group_id'] = 0; }
if($_GET['mode'] == "modify"){
	$row = CalendarManager::getEvent($_GET['id']);
	$sDate = explode(" ",$row['str_date']);
	$eDate = explode(" ",$row['end_date']);
	$startDate = $sDate[0];
	$startTime = explode(":",$sDate[1]);
	$startTimeH = $startTime[0];
	$startTimeM = $startTime[1];
	$endDate   = $eDate[0];
	$endTime = explode(":",$eDate[1]);
	$endTimeH = $endTime[0];
	$endTimeM = $endTime[1];
	$_GET['group_id'] = $row['group_id'];
}else{
	$startDate = date("Y-m-d",$_GET['date']);
	$startTime = date("H:i:00",$_GET['date']);
	$startTime = explode(":",$startTime);
	$startTimeH = $startTime[0];
	$startTimeM = $startTime[1];
	$endDate   = date("Y-m-d",$_GET['date']);
	$endTime   = date("H:i:00",$_GET['date']);
	$endTime = explode(":",$endTime);
	$endTimeH = $endTime[0];
	$endTimeM = $endTime[1];
}

$arryTimeH = CalendarManager::$detaultTimeH;
$arryTimeHVal = array_values($arryTimeH);
$arryTimeHKey = array_keys($arryTimeH);


$arryTimeM = CalendarManager::$detaultTimeM;
$arryTimeMVal = array_values($arryTimeM);
$arryTimeMKey = array_keys($arryTimeM);

?>
<script type="text/javascript">
	$(function() {
		$("#strDate").datepicker({
			dateFormat:'yy-mm-dd',
            onClose: function( selectedDate ) {
                $("#endDate").datepicker( "option", "minDate", selectedDate );
            }
        });
        $("#endDate").datepicker({
			dateFormat:'yy-mm-dd',
            onClose: function( selectedDate ) {
                $("#strDate").datepicker( "option", "maxDate", selectedDate );
            }
        });
	});
</script>
<form id="calendarForm">
<?php if($_GET['mode'] == "modify"){ ?>
	<input type='hidden' name='id' value="<?=$_GET['id']?>">
<?php } ?>
	<input type='hidden' name='mode' value="<?=$_GET['mode']?>">
	<input type='hidden' name='usr_id' value="<?=$_SESSION['sys_id']?>">
    <ul class="form">
		<li>
			<label class="desc mini">선택</label>
			<select name="group_id">
				<option value="0" <?php if($_GET['group_id'] == 0){ ?> selected <?php } ?> >전체</option>
				<?php
				$groups = GroupManager::getUseCalendarGroup();
				for($g=0; $g<count($groups); $g++){ ?>
				<option value="<?=$groups[$g]['id']?>" <?php if($_GET['group_id'] == $groups[$g]['id']){ ?> selected <?php } ?> ><?=$groups[$g]['title']?></option>
				<?php }	?>
			</select>

			<label style="display:inline;"><input type="checkbox" name="allDay" value="O" <?php if($row['allDay'] == "O"){?> checked="checked" <?php } ?> onclick="$.cal" /> 종일</label>
		</li>
		<li>
			<label class="desc mini">일시</label>
			<input type="text" name="str_date" value="<?=$startDate?>" id="strDate" class="text" style="width:70px;"/>
			<select name="startTimeH">
				<?php for($i = 0; $i < count($arryTimeHKey); $i++){ ?>
					<option value="<?=$arryTimeHKey[$i]?>" <?php if($arryTimeHKey[$i] == $startTimeH){?> selected <?php } ?>><?=$arryTimeHVal[$i]?></option>
				<?php } ?>
			</select>
			<select name="startTimeM">
				<?php for($i = 0; $i < count($arryTimeMKey); $i++){ ?>
					<option value="<?=$arryTimeMKey[$i]?>" <?php if($arryTimeMKey[$i] == $startTimeM){?> selected <?php } ?>><?=$arryTimeMVal[$i]?></option>
				<?php } ?>
			</select>
			<div class="bar">-</div>
			<input type="text" name="end_date" value="<?=$endDate?>" id="endDate" class="text" style="width:70px;" />
			<select name="endTimeH">
				<?php for($i = 0; $i < count($arryTimeHKey); $i++){ ?>
					<option value="<?=$arryTimeHKey[$i]?>" <?php if($arryTimeHKey[$i] == $endTimeH){?> selected <?php } ?>><?=$arryTimeHVal[$i]?></option>
				<?php } ?>
			</select>
			<select name="endTimeM">
				<?php for($i = 0; $i < count($arryTimeMKey); $i++){ ?>
					<option value="<?=$arryTimeMKey[$i]?>" <?php if($arryTimeMKey[$i] == $endTimeM){?> selected <?php } ?>><?=$arryTimeMVal[$i]?></option>
				<?php } ?>
			</select>
		</li>
		<li>
        	<label class="desc mini">제목</label>
            <input type="text" name="title" class="text" style="width:420px;" value="<?=$row['title']?>" />
		</li>
		<li>
        	<label class="desc mini">내용</label>
            <textarea name="content" class="textarea" style="height:80px;width:420px;"><?=$row['content']?></textarea>
		</li>
	</ul>

</form>

<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.Common.php");
require_once("../_classes/class.UsrManager.php");
require_once("../_classes/class.SMSManager.php");
require_once('../_lib/nusoap_tong.php');

require_once('../_lib/_docs_log.inc');

switch ($_REQUEST['mode'])
{

    /******************************************************************************
    * 문자보내기
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
	/*
    case "send":
		$sms_id="yuho";          //고객님께서 부여 받으신 sms_id
		$sms_pwd="265265";       //고객님께서 부여 받으신 sms_pwd
	
		$callbackURL = "sms.tongkni.co.kr";
		$userdefine = $sms_id;	//예약취소를 위해 넣어주는 구분자 정의값, 사용자 임의로 지정해주시면 됩니다. 영문으로 넣어주셔야 합니다. 사용자가 구분할 수 있는 값을 넣어주세요.
		$canclemode = "1";      //예약 취소 모드 1: 사용자정의값에 의한 삭제.  현제는 무조건 1을 넣어주시면 됩니다.

		//구축 테스트 주소와 일반 웹서비스 선택
		if (substr($sms_id,0,3) == "bt_"){
			$webService = "http://webservice.tongkni.co.kr/sms.3.bt/ServiceSMS_bt.asmx?WSDL";
		}else{
			$webService = "http://webservice.tongkni.co.kr/sms.3/ServiceSMS.asmx?WSDL";
		}

    	//+) funcMode는 메소드실행 후 반환값에 따라 다른 메시지를 띄우기 위해서 쓰입니다.

		$sms = new SMS($webService); //SMS 객체 생성
	
		$domain_id = 1;
		$from_phone = $_POST['snd_number'];
		$content    = $_POST['sms_content'];
        $arryUsr = array_unique(explode("|",$_POST['phones']));

		for($i=0;$i<count($arryUsr);$i++){
			$usr = UsrManager::getUsr($arryUsr[$i]);
			$position  = Common::getUsrPosition($usr['position']);
            $recontent = str_replace("[[=이름=]]",$usr['name'],$content);
			$recontent = str_replace("[[=직분=]]",$position,$recontent);
            if($_POST['isReserve'] == "O"){
				$reserveDateTime = explode(" ",$_POST['reserve_date']);
				$reserve_date = str_replace("-","",$reserveDateTime[0]);
				$reserve_time = str_replace(":","",$reserveDateTime[1]);
				$result=$sms->SendSMSReserve($$sms_id,$sms_pwd,$from_phone,$usr['phone'],$recontent,$reserve_date,$reserve_time,$userdefine);// 8개의 인자로 함수를 호출합니다.
			}else{
				$result=$sms->SendSMS($sms_id,$sms_pwd,$from_phone,$usr['phone'],$recontent);// 5개의 인자로 함수를 호출합니다.
			}
		}

		$r["error"]    = -1;
		$r["message"]  = "전송되었습니다. ";
		print json_encode($r);

	break;
	*/
    /******************************************************************************
    * 문자보내기
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/

    case "send2":
		$domain_id = 1;
		$from_phone = $_POST['snd_number'];
		$subject    = $_POST['sms_subject'];
		$sms_content = $_POST['sms_content'];

		$last_id = null;
		$sms_type = 1;
        $arryUsr = array_unique(explode("|",$_POST['phones']));
		
		for($i=0;$i<count($arryUsr);$i++){
			$usr = UsrManager::getUsr($arryUsr[$i]);
			
			$position  = Common::getUsrPosition($usr['position']);		
			$content = str_replace("[[=이름=]]",$usr['name'], $sms_content);
			$content = str_replace("[[=직분=]]",$position,$content);
			
			if($_POST['isReserve'] == "O"){
				if($_POST['sms_type'] == "LMS"){
					$result = SMSManager::reserveSendLMS($domain_id, $usr['phone'], $from_phone, "", $content, $_POST['reserve_date'], $sms_type);
				}else{
					$result = SMSManager::reserveSend($domain_id, $usr['phone'], $from_phone, $content, $_POST['reserve_date'], $sms_type);					
				}

			}else{
				if($_POST['sms_type'] == "LMS"){
					$result = SMSManager::sendLMS($domain_id, $usr['phone'], $from_phone, "", $content, $sms_type);
				}else{
					$result = SMSManager::send($domain_id, $usr['phone'], $from_phone, $content, $sms_type);
				}
			}
		}

		$r["error"]    = -1;
		$r["message"]  = "전송되었습니다. ";
		print json_encode($r);

	break;




	case "send":
	
		$a_url = "https://auth.supersms.co:7000/auth/v3/token";			
		$response = exec('curl -X POST -H "X-IB-Client-Id:himh_rest" -H "X-IB-Client-Passwd:9345HMQ06525EBJ0UBW4" -H "Accept: application/json" https://auth.supersms.co:7000/auth/v3/token');
		$auth = json_encode($response);
		$key = $auth['schema']." ".$auth['accessToken'];
		echo $key;
		$domain_id = 1;
		$from_phone = $_POST['snd_number'];
		$subject    = $_POST['sms_subject'];
		$sms_content = $_POST['sms_content'];

		$last_id = null;
		$sms_type = 1;
        $arryUsr = array_unique(explode("|",$_POST['phones']));
		
		for($i=0;$i<count($arryUsr);$i++){

			$usr = UsrManager::getUsr($arryUsr[$i]);
			
			$position  = Common::getUsrPosition($usr['position']);		
			$content = str_replace("[[=이름=]]",$usr['name'], $sms_content);
			$content = str_replace("[[=직분=]]",$position,$content);
			$to_phone = "82".substr($usr['phone'],1);
			$to_phone = str_replace ('-','',$to_phone);

		
			$data = array(
				"title" => "",
				"from" =>  $from_phone,
				"text" => $content,
				"destinations" => array(
					array("to" =>  $to_phone)

				),
				"ttl" => 100
			);

			//$data = json_decode($data_json);	
			$url = "https://sms.supersms.co:7020/sms/v3/multiple-destinations";
			$key = $auth['schema']." ".$auth['accessToken'];
			echo $key;
			$ch = curl_init();                                 //curl 초기화
			curl_setopt($ch, CURLOPT_URL, $url);               //URL 지정하기
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"Authorization: ".$key,
				"Accept: application/json",
				"content-type: application/json"
			));


			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    //요청 결과를 문자열로 반환 
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);      //connection timeout 10초 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   //원격 서버의 인증서가 유효한지 검사 안함
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);       //POST data
			curl_setopt($ch, CURLOPT_POST, true);              //true시 post 전송 
			$response = curl_exec($ch);
			curl_close($ch);
			 
			//return $response;

		}

		$r["error"]    = -1;
		$r["message"]  = "전송되었습니다. ";
		print json_encode($r);

	break;

    /******************************************************************************
    * 회원추가
    * requred params(_POST) :: name, phone, email
    ******************************************************************************/
    case "reserve":
		$domain_id = 1;
		$from_phone = $_POST['snd_number'];
		$content    = $_POST['sms_content'];
        $arryPhones = array_unique(explode("|",$_POST['phones']));

		for($i=0;$i<count($arryPhones);$i++){
			$result = SMSManager::send($domain_id, $arryPhones[$i], $from_phone, $content, $type=4);
		}

		$r["lastID"]   = $lastID;
		$r["error"]    = -1;
		$r["message"]  = "전송되었습니다. ";
		print json_encode($r);

	break;




    /******************************************************************************/
}

require_once("../_lib/_inner_footer.php");
?>

<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceList.php");
require_once("../_classes/class.AttendanceManager.php");
if(!$_GET['page']){ $_GET['page']=1; }
if(!$_GET['root_id']){ $_GET['root_id']=0; }
if(!$_GET['year']){ $_GET['year'] = "2017"; }
if($_GET['q'] && $_GET['q'] != ''){
	$_GET['q'] =  rawurldecode($_GET['q']);
	$properties = array(
	    "keyword"  => $_GET['q'],
		"page"     => $_GET['page'],
		"perPage"  => 20
	);
	$al     = new AttendanceList($properties);
	$totalitems  = $al->getTotalCount();
	$list   = $al->getList();

	if($list != false){ $count = count($list); }else{ $count = 0; }
	$title = "검색어: <b>".$_GET['q']."</b> (".$totalItemCount.")";

	$paging = Common::getPaging($totalItemCount, $properties['perPage'], "javascript:$.contact.pageList(%d);");
} else {
	$properties = array(
		"root_id"  => $_GET['root_id'],
		"page"     => $_GET['page'],
		"year"     => $_GET['year'],
		"perPage"  => 20
	);

	$al     = new AttendanceList($properties);
	$totalitems  = $al->getTotalCount();
	$list   = $al->getList();
	$tot    = count($list);

    if($_GET['root_id'] == 0){
		$title = "전체 (".$totalitems.")";
	}else{
		$root = GroupManager::getGroup($_GET['root_id']);
		$title = $root['title']." (".$totalitems.")";
	}

	$paging = getPaging($totalitems, $properties['perPage'], "javascript:$.attendanceAdmin.list({page:%d});");
}
?>
<div id="quick_actions">
	<a class="button_small whitishBtn" href="javascript:$.attendanceAdmin.insert();"><span class="iconsweet">+</span>출석부추가</a>
	<?php
	$groups = GroupManager::getAttendanceAdminGroups();
	for($g=0; $g<count($groups); $g++){
	?>
		<a class="button_small whitishBtn" href="javascript:$.attendanceAdmin.list({'root_id':<?=$groups[$g]['id']?>});"><span class="iconsweet">l</span><?=$groups[$g]['title']?></a>
	<?php
	}
	?>
</div>
<div id="contantBox">
	<div class="one_wrap">
		<div class="widget">
			<div class="widget_title"><span class="iconsweet">f</span><h5>출석부 관리</h5></div>
			<div class="widget_body">
				<div class="content_pad">
				<?php if($list){ ?>
					<div class="location">
						<div class="subject"><?=$title?></div>
						<div class="tool"></div>
					</div>

					<div class="project_sort">
						<ul class="project_list">
							<?php
							for($i=0; $i<count($list); $i++) {
								$root = GroupManager::getGroup($list[$i]['root_id']);

								//$root = GroupManager::getAttendanceAdminGroupId($list[$i]['group_id']);
							?>
							<li data-type="incomplete" >
								<span class="project_badge <?php if($list[$i]['parent_id'] == 0){?> blue<?php }else{ ?> grey<?php } ?> iconsweet"> C </span>
								<a class="project_title" href="#"><?=$list[$i]['title']?></a>
								<p><?=$list[$i]['year']?></p>
								<?php if($root){ ?>
								<a class="assigned_user" href="#"><?=$root['title']?></a>
								<?php } ?>
								<a href="javascript:$.attendanceAdmin.modify(<?=$list[$i]['id']?>);" class="modify iconsweet">C</a>
								<a href="javascript:$.attendanceAdmin.deleteAction('<?=$list[$i]['id']?>');" class="remove iconsweet">X</a>
							</li>
							<?php } ?>
						</ul>
					</div>


					<div class="pagging"><?=$paging?></div>
				<?php } ?>


				</div>
			</div>
		</div>
	</div>
</div>

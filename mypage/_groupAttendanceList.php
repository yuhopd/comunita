<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");

$group = GroupManager::getGroup($_GET['group_id']);
$root  = GroupManager::getRootGroup($_GET['group_id']);
$list = AttendanceManager::getAttendanceSettingListFromGroup($_GET['group_id']);
?>

<ul class="groupMenu">
	<li>
		<a href="javascript:$.group.edit(<?=$_GET['group_id']?>, 'O');">그룹설정</a>
	</li>
	<li>
		<a href="javascript:$.group.usrEdit(<?=$_GET['group_id']?>, 'O');">그룹회원관리</a>
	</li>
	<li class="active">
		<a href="javascript:$.group.attendanceList(<?=$_GET['group_id']?>, 'O');">출석부관리</a>
	</li>
	<li>
		<a href="javascript:$.group.attendanceInsert(<?=$_GET['group_id']?>, 'O');">출석부추가</a>
	</li>
</ul>
<form>
	<ul>
		<li>
			<div>
				<span class="desc" for="attendance_title" style="width:50px;">그룹:</span>
				<div class="value"><?=$group['title']?></div>
			</div>
		</li>
	</ul>
</form>
<div class="settings" style="width:400px;height:200px;overflow:auto;">
    <?php if($list){ ?>
	<ul class="attendance">
	<?php
	for($i=0; $i<count($list); $i++) {

	?>
		<li>
			<span class="icon48x48 attendance48x48"></span>
			<div class="meta">
				<div class="name"><?=$list[$i]['title']?> <span class="year">(<?=$list[$i]['year']?>)</span></div>
				<div class="summary"><?=$list[$i]['summary']?></div>
			</div>
			<div class="action">
			    <a href="javascript:$.group.attendanceModify(<?=$list[$i]['id']?>,'O');" class="ui-state-default ui-corner-all">수정</a>
				<a href="javascript:$.group.attendanceDelete(<?=$list[$i]['id']?>,<?=$_GET['group_id']?>);" class="ui-state-default ui-corner-all">삭제</a>
			</div>

		</li>
	<?php
	}
	?>
	</ul>
	<?php } ?>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
require_once('../_classes/class.GroupManager.php');

$html_js = <<<EOF
	<script src="../js/jquery.admin.group.js" type="text/javascript"></script>
	<script src="../js/jquery.admin.usr.js" type="text/javascript"></script>
	<script src="../js/jquery.cookie.js" type="text/javascript"></script>
	<script src="../js/jquery-treeview/jquery.treeview.min.js" type="text/javascript"></script>
	<script src="../js/jquery-treeview/jquery.treeview.groups.js" type="text/javascript"></script>
	<script src="../js/jquery.multiselects.js" type="text/javascript"></script>
	<script src="../js/jquery.attendanceAdmin.js" type="text/javascript"></script>
EOF;

$html_css = <<<EOF
	<link href="../js/jquery-treeview/jquery.treeview.css" rel="stylesheet"  />
EOF;

require_once("../_lib/__header.php");
if(!$_SESSION['sys_id']){ header("Location: ../account/index.php"); }
$domain = GroupManager::getDomain($_SESSION['sys_domain']);
?>
	<script type="text/javascript">
		$(function() {
			<?php if($_GET['mode'] == "message"){ ?>
				$.message.list({});
			<?php } else { ?>
                $.group.list(0);
			<?php } ?>
		});
	</script>
	<nav id="secondary_nav">
	    <div id="usrInfoBoxArea">
        <?php if($_SESSION['sys_id']){ require_once("../_lib/_usrInfo.php"); } ?>
		</div>
		<h2>My Page</h2>
		<ul class="secondaryNav">
			<li><a href="javascript:$.message.list({});"><span class="iconsweet">A</span>쪽지</a></li>
			<li><a href="javascript:;"><span class="iconsweet">R</span>달력 관리</a></li>
		</ul>

		<h2>관리자 환경</h2>
		<ul class="secondaryNav">
		    <li><a href="../mypage/index.php"><span class="iconsweet">f</span>그룹관리</a></li>
		    <li><a href="javascript:$.group.excelStep1(0);"><span class="iconsweet">]</span>엑셀입력</a></li>
			<li><a href="javascript:$.attendanceAdmin.list({});"><span class="iconsweet">C</span>출석부 관리</a></li>
			<li><a href="javascript:;"><span class="iconsweet">}</span>동기(기수) 관리</a></li>
		</ul>
	</nav>
	<div id="content_wrap">
		<div id="quick_actions">
			<a class="button_big" href="javascript:$.group.insert(0);"><span class="iconsweet">+</span>기본그룹추가</a>
			<?php
			$groups = GroupManager::getAllRootIds();
			for($g=0; $g<count($groups); $g++){
			?>
				<a class="button_small whitishBtn" href="javascript:$.group.list(<?=$groups[$g]['id']?>);"><span class="iconsweet">l</span><?=$groups[$g]['title']?></a>
			<?php
			}
			?>
		</div>
		<div id="contantBox">
			<div class="one_wrap">
				<div class="widget">
					<div class="widget_title"><span class="iconsweet">f</span><h5>그룹관리</h5></div>
					<div class="widget_body">
						<div class="content_pad">
							<ul id="groups"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
require_once("../_lib/__footer.php");
?>
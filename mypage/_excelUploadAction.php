<?php
require_once("../_lib/_docs_log.inc");
require_once('../_classes/class.Upload.php');

$allowedExtensions = array("xls","xlsx");
$file_rename = date('ymdHms').substr(microtime(mktime(date("s"))),2,-16).rand(0,9).rand(0,9);
$sizeLimit   = 1024 * 1024 * 1024;
$path        = "/home/himh/public_html/data/excel";

$uploader    = new qqFileUploader($allowedExtensions, $sizeLimit, $file_rename, "");
$result      = $uploader->handleUpload($path);

$r["path"]	   = $path;
$r["group_id"] = $_GET['group_id'];
$r["filename"] = $file_rename.".".$result['ext'];
$r["error"]    = -1;
$r["message"]  = "파일업로드 성공...";
print json_encode($r);
?>
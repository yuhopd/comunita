<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrList.php");

$auth[1] = "회원";
$auth[2] = "리더";
$auth[3] = "관리자";
$auth[4] = "수퍼관리자";


if(!$_GET['page']){ $_GET['page'] = 1; }
if(!$_GET['level']){$_GET['level'] = 2; }
$properties = array(
	"level"     => $_GET['level'],
	"page"      => $_GET['page'],
	"perPage"   => 30
);

$ul  = new UsrList($properties);
$list = $ul->getList();
$totalitems = $ul->getTotalCount();
$tot = ($list==false)? 0 : count($list);


$paging = getPaging($totalitems, 10, "javascript:usr_list('page=%d&level={$_GET[level]}');");
?>
<div style="margin:10px 10px 10px 20px; font-size:12px;">
    <?php if($_GET['level'] == 1){ ?>
	<strong>회원</strong> | 
	<?php }else{ ?>
	<a href="#" onclick="usr_list('level=1');" >회원</a> | 
    <?php } ?>
	
    <?php if($_GET['level'] == 2){ ?>
	<strong>리더</strong> | 
	<?php }else{ ?>
	<a href="#" onclick="usr_list('level=2');" >리더</a> |
    <?php } ?>

    <?php if($_GET['level'] == 3){ ?>
	<strong>관리자</strong> | 
	<?php }else{ ?>
	<a href="#" onclick="usr_list('level=3');" >관리자</a> |
    <?php } ?>
	
    <?php if($_GET['level'] == 4){ ?>
	<strong>슈퍼관리자</strong>
	<?php }else{ ?>
	<a href="#" onclick="usr_list('level=4');" >슈퍼관리자</a>
    <?php } ?>
    <div class="clear"></div>
</div>

<table class="user">
<thead>
	<tr>
		<th class="no">No</th>
		<th class="name">이름</th>
		<th class="phone">연락처</th>
		<th class="email">email</th>
		<th class="level">권한</th>
		<th class="action">기능</th>
	</tr>
</thead>
<tbody>
<?php
for($i=0; $i < $tot; $i++){
?>
	<tr>
		<td><?=$list[$i]['id']?></td>
		<td><a href="#"><?=$list[$i]['name']?></a></td>
		<td><?=$list[$i]['phone']?></td>
		<td><?=$list[$i]['email']?></td>
		<td><?=$auth[$list[$i]['level']]?></td>
		<td>
			<a href="#" class='ui-state-default ui-corner-all'  onclick="$.sms.usr_modify('<?=$list[$i]['id']?>');" >수정</a>
			<a href="#" class='ui-state-default ui-corner-all'  onclick="$.usr.psw_edit('<?=$list[$i]['id']?>');" >비번수정</a>
			<a href="#" class='ui-state-default ui-corner-all'  onClick="return usr_delete_action('<?=$list[$i]['id']?>');">삭제</a>
		</td>
	</tr>
<?php
}
?>
</tbody>
</table>
<div class="pagging"><?=$paging?></div>

<div class="item_button_box">
	<div class="main_top">
	     <input type="button" onclick="usr_insert();" value="유저추가" />
	</div>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");

$properties = array(
	"type"   =>  "normal"
);
$pl    = new UsrPhoneList($properties);
$list  = $pl->getList('X');



if($_GET['mode'] == 'modify'){
	$query = "SELECT * FROM `group` WHERE id='$_GET[group_id]'";
	$res   =& $db->query($query);
	if($res->fetchInto($row,DB_FETCHMODE_ASSOC)){
	}
}
?>


<ul class="groupMenu">
	<li class="active">
		<a href="javascript:$.group.edit(<?=$_GET['group_id']?>, 'O');">그룹설정</a>
	</li>
	<li>
		<a href="javascript:$.group.usrEdit(<?=$_GET['group_id']?>, 'O');">그룹회원관리</a>
	</li>
	<li>
		<a href="javascript:$.group.attendanceList(<?=$_GET['group_id']?>, 'O');">출석부관리</a>
	</li>
	<li>
		<a href="javascript:$.group.attendanceInsert(<?=$_GET['group_id']?>, 'O');">출석부추가</a>
	</li>
</ul>

<div class="settings">
	<form id="groupForm">
	<?php if($_GET['mode'] == 'modify'){ ?>
	<input type="hidden" name="mode" value="modify">
	<input type="hidden" name="parent_id" value="<?=$row['parent_id']?>">
	<?php }else{ ?>
	<input type="hidden" name="mode" value="insert">
	<input type="hidden" name="parent_id" value="<?=$_GET['parent_id']?>">
	<?php } ?>

	<ul class="form">
		<li>
		    <div class="formInput">
				<label for="group_title">그룹명</label>
				<input type="text" id="group_title" name="title" value="<?=$row['title']?>"  style="width:250px;" />
			</div>
		</li>
		<li>
		    <div class="formInput">
			<label for="is_enable">사용</label>
			<select id="is_enable" name="is_enable" style="width:250px;">
				<option value="X" <?php if($row['is_enable'] == "X"){?> selected <?php } ?>>사용 안 함</option>
				<option value="O" <?php if($row['is_enable'] == "O"){?> selected <?php } ?>>사용 함</option>
				<option value="G" <?php if($row['is_enable'] == "G"){?> selected <?php } ?>>그룹으로 사용</option>
			</select>
			</div>
		</li>
		<li>
		    <div class="formInput">
			<label for="use_attendance">출석부관리</label>
			<select id="use_attendance" name="use_attendance" style="width:250px;">
				<option value="X" <?php if($row['use_attendance'] == "X"){?> selected <?php } ?>>사용 안 함</option>
				<option value="O" <?php if($row['use_attendance'] == "O"){?> selected <?php } ?>>출석부 관리 그룹</option>
			</select>
			</div>
		</li>
		<li>
			<div class="formInput">
			<label for="use_calendar">달력</label>
			<select id="use_calendar" name="use_calendar" style="width:250px;">
				<option value="X" <?php if($row['use_calendar'] == "X"){?> selected <?php } ?>>사용 안 함</option>
				<option value="O" <?php if($row['use_calendar'] == "O"){?> selected <?php } ?>>사용함</option>
			</select>
			</div>
		</li>
		<!-- li>
			<label for="usr_id">그룹관리자</label>
			<div class="form_input">
				<select id="usr_id" name="usr_id" class="field select" style="width:200px;">
					<option value="0">관리자를 선택하세요!</option>
					<?php for($i=0 ; $i<count($list); $i++){ ?>
					<option value="<?=$list[$i]["id"]?>" <?php if($row['usr_id'] == $list[$i]["id"]){?> selected <?php } ?>><?=$list[$i]["name"]?> (<?=$list[$i]['sameage']?>)</option>
					<?php } ?>
				</select>
			</div>
		</li -->
		<li>
		    <div class="formInput">
			<label for="group_summary">설명</label>
			<textarea id="group_summary" name="summary" style="width:250px;" ><?=$row['summary']?></textarea>
			</div>
		</li>
	</ul>
	</form>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
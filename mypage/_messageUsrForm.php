<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");

if($_POST['readIds']){
	$arryReadIds = explode("|", $_POST['readIds']);
	$arryReadNames = explode("|", $_POST['readNames']);
}
 

$rootGroups = GroupManager::getAllRootIds(1);

?>
<script type="text/javascript">
    $(function() {
		$("#unSelected").multiSelect("#selected", {trigger: "#options_right"});
		$("#selected").multiSelect("#unSelected", {trigger: "#options_left"});
		$.message.rootGroupUsrList();
	});
</script>


<div class="settings">

	<form id="groupUsrForm">
		<ul>
			<li>
				<div>
				<table>
					<tr>
						<td>
							<div id="groupUsrListBox">
							<select name="left[]" id="unSelected" multiple="multiple" style="width:180px;" size="10">
							</select>
							</div>
							<select name="roots" id="roots" style="width:120px;" size="1">
								<?php for($f=0; $f<count($rootGroups); $f++ ) { ?>
									<option value="<?=$rootGroups[$f]['id']?>" <?php if($rootGroups[$f]['id'] == $root_id){ ?> selected <?php } ?> ><?=$rootGroups[$f]['title']?></option>
								<?php } ?>
							</select>
							<input type="button" onclick="$.message.rootGroupUsrList();" class="ui-state-default ui-corner-all button_s" value="변경" />

						</td>
						<td style="padding:5px;" valign="center">
							<span id="options_right" class="ui-state-default ui-corner-all button_s">&gt;</span>
							<br />
							<span id="options_left" class="ui-state-default ui-corner-all button_s">&lt;</span>
						</td>
						<td valign="top">
							<strong>받는사람</strong>
							<select name="right[]" id="selected" multiple="multiple" style="width:180px;" size="10">
								<?php for($j=0; $j<count($arryReadIds); $j++ ) { ?>
									<option value="<?=$arryReadIds[$j]?>" ><?=$arryReadNames[$j]?></option>
								<?php } ?>
							</select>
						 </td>
					</tr>
				</table>
				</div>
			</li>
		
		</ul>

	</form>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
<?php
require_once("../_lib/_inner_header.php");

require_once("../_classes/class.AttendanceManager.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.UsrManager.php");
//require_once("../_lib/_docs_log.inc");

switch ($_REQUEST['mode'])
{
	/******************************************************************************
    * Group Delete
    * requred params(_GET) :: group_id
    ******************************************************************************/  
    case "delete":
		$result = GroupManager::del($_GET['group_id']);
	
    break;
    
	/******************************************************************************
    * Group Insert
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "insert":

		$group = array(
			"parent_id"      => $_POST['parent_id'], 
			"usr_id"         => $_POST['usr_id'], 
			"depth"          => $_POST['depth'],
			"title"          => $_POST['title'],
			"summary"        => $_POST['summary'],
			"is_enable"      => $_POST['is_enable'],
			"use_calendar"   => $_POST['use_calendar'],
			"use_attendance" => $_POST['use_attendance']
		);	

        $lastID = GroupManager::insert($group);

		echo $lastID;
    break;
	/******************************************************************************
    * Group Edit
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "modify":

		$group = array(
			"group_id"       => $_POST['group_id'] , 
			"usr_id"         => $_POST['usr_id'] , 
			"title"          => $_POST['title'] ,
			"summary"        => $_POST['summary'] ,
			"is_enable"      => $_POST['is_enable'] ,
			"use_calendar"   => $_POST['use_calendar'],
			"use_attendance" => $_POST['use_attendance']
		);	

        $lastID = GroupManager::modify($group);

    break;

	/******************************************************************************
    * Group Edit
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "usr_edit":
		GroupManager::usr_edit($_POST['usrs0'], $_POST['group_id'], 0);
		GroupManager::usr_edit($_POST['usrs1'], $_POST['group_id'], 1);
		GroupManager::usr_edit($_POST['usrs2'], $_POST['group_id'], 2);


    break;

	/******************************************************************************
    * Group Edit
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "eaxelInsert":


		for($i =0 ; $i<count($_POST['name']); $i++ ){
			$usr = array(
				"sameage_id"  => $_POST['sameage_id'][$i] , 
				"email"       => $_POST['email'][$i] , 
				"name"        => $_POST['name'][$i] ,
				"phone"       => $_POST['phone'][$i] ,
				"address"     => $_POST['address'][$i] ,
				"birth"       => $_POST['birth'][$i] ,
				"gender"      => $_POST['gender'][$i]
			);
			//echo $_POST['name'][$i];
			$lastID = UsrManager::addUsr($usr);
			UsrManager::addGroupUsr($_POST['group_id'],$lastID);
			unset($usr);
		}

		$r["error"]    = -1;
		$r["massage"]  = "저장 되었습니다.";
		print json_encode($r);
    break;

	/******************************************************************************
    * Group Insert Attendance
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "insertAttendance":

		$attendance = array(
			"title"            => $_POST['title'], 
			"summary"          => $_POST['summary'], 
			"group_id"         => $_POST['group_id'],
			"group_title"      => $_POST['group_title'],
			"root_id"          => $_POST['root_id'],
			"root_title"       => $_POST['root_title'],
			"year"             => $_POST['year'],
			"every_date_type"  => $_POST['every_date_type'],
			"every_date_week"  => $_POST['every_date_week']
		);	

        $lastID = AttendanceManager::insertAttendanceSetting($attendance);
		$r["lastID"]   = $lastID;
		$r["error"]    = -1;
		$r["massage"]  = "추가 되었습니다.";
		print json_encode($r);
    break;


	/******************************************************************************
    * Group Modify Attendance
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "modifyAttendance":

		$attendance = array(
			"id"               => $_POST['attendance_id'], 
			"title"            => $_POST['title'], 
			"summary"          => $_POST['summary'], 
			"group_id"         => $_POST['group_id'],
			"group_title"      => $_POST['group_title'],
			"root_id"          => $_POST['root_id'],
			"root_title"       => $_POST['root_title'],
			"year"             => $_POST['year'],
			"every_date_type"  => $_POST['every_date_type'],
			"every_date_week"  => $_POST['every_date_week']
		);	

        AttendanceManager::modifyAttendanceSetting($attendance);
		
		$r["error"]    = -1;
		$r["massage"]  = "추가 되었습니다.";
		print json_encode($r);
    break;


	/******************************************************************************
    * Group Delete Attendance
    * requred params(_POST) :: group_id , title , summary 
    ******************************************************************************/  
    case "deleteAttendance":

        $res = AttendanceManager::deleteAttendance($_GET['id']);
		if($res){
			$r["error"]    = -1;
			$r["massage"]  = "삭제 되었습니다.";
		}else{
			$r["error"]    = 100;
			$r["massage"]  = "일시적으로 시스템에 문제가 생겼습니다. 잠시후 다시 시도해보세요!";
		}
		print json_encode($r);
    break;


	/******************************************************************************
    * Up Sequence
    * requred params(_GET) :: id
    ******************************************************************************/
    case "upSequence":
        GroupManager::upSequence($_GET['id']);
    break;

	/******************************************************************************
    * Down Sequence
    * requred params(_GET) :: id
    ******************************************************************************/
    case "downSequence":
        GroupManager::downSequence($_GET['id']);
    break;
    /******************************************************************************/
}

require_once("../_lib/_inner_footer.php");
?>
<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");

if($_GET['mode'] == 'modify'){
	$attendance = AttendanceManager::getAttendanceSetting($_GET['attendance_id']);
	$group = GroupManager::getGroup($attendance['group_id']);
	$root  = GroupManager::getGroup($attendance['root_id']);
}else{

}
?>

<form id="attendanceForm">
	<?php if($_GET['mode'] == 'modify'){?>
	<input type="hidden" name="mode" value="modifyAttendance" />
	<input type="hidden" name="attendance_id" value="<?=$attendance['id']?>" />
	<input type="hidden" name="group_id" value="<?=$group['id']?>" />
    <input type="hidden" name="group_title" value="<?=$group['title']?>" />
    <input type="hidden" name="root_id" value="<?=$root['id']?>" />
    <input type="hidden" name="root_title" value="<?=$root['title']?>" />

	<?php }else{ ?>
	<input type="hidden" name="mode" value="insertAttendance" />
	<?php } ?>

	<ul class="form_fields_container">
		<li>
			<label for="attendance_title">그룹</label>
			<div class="form_input">
				<select name="group_id" style="width:150px;">
					<option value="0">선택하세요.</option>
					<?php
					$query1 = "SELECT * FROM `group` WHERE `parent_id`='0' AND `is_enable`='O' ORDER BY `sequence` ASC";
					$res1   =& $db->query($query1);
					while($res1->fetchInto($row1,DB_FETCHMODE_ASSOC)){
						?>
						<option value="<?=$row1['id']?>" <?php if($row1['id'] == $group['id']){?> selected="selected" <?php } ?> ><?=$row1['title']?></option>
						<?php
						$query2  = "SELECT * FROM `group` WHERE `parent_id`='{$row1[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
						$res2    =& $db->query($query2);
						while($res2->fetchInto($row2,DB_FETCHMODE_ASSOC)){?>
							<option value="<?=$row2['id']?>" <?php if($row2['id'] == $group['id']){?> selected="selected" <?php } ?> >--<?=$row2['title']?></option>
							<?php
							$query3  = "SELECT * FROM `group` WHERE `parent_id`='{$row2[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
							$res3    =& $db->query($query3);
							while($res3->fetchInto($row3,DB_FETCHMODE_ASSOC)){ ?>
								<option value="<?=$row3['id']?>" <?php if($row3['id'] == $group['id']){?> selected="selected" <?php } ?> >----<?=$row3['title']?></option>
								<?php
								$query4  = "SELECT * FROM `group` WHERE `parent_id`='{$row3[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
								$res4    =& $db->query($query4);
								while($res4->fetchInto($row4,DB_FETCHMODE_ASSOC)){ ?>
									<option value="<?=$row4['id']?>" <?php if($row4['id'] == $group['id']){?> selected="selected" <?php } ?> >------<?=$row4['title']?></option>
									<?php
									$query5  = "SELECT * FROM `group` WHERE `parent_id`='{$row4[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
									$res5    =& $db->query($query5);
									while($res5->fetchInto($row5,DB_FETCHMODE_ASSOC)){ ?>
										<option value="<?=$row5['id']?>" <?php if($row5['id'] == $group['id']){?> selected="selected" <?php } ?> >--------<?=$row5['title']?></option>
										<?php
										$query6  = "SELECT * FROM `group` WHERE `parent_id`='{$row5[id]}' AND `is_enable`='O' ORDER BY `sequence` ASC";
										$res6    =& $db->query($query6);
										while($res6->fetchInto($row6,DB_FETCHMODE_ASSOC)){ ?>
											<option value="<?=$row6['id']?>" <?php if($row6['id'] == $group['id']){?> selected="selected" <?php } ?> >--------<?=$row6['title']?></option>
										<?php
										}
									}
								}
							}
						}
					}
					?>
				</select>
			</div>
		</li>
		<li>
			<label for="attendance_title">출석부 이름</label>
			<div class="form_input">
				<input type="text" name="title" value="<?=$attendance['title']?>"/>
			</div>
		</li>
		<li>
			<label for="attendance_summary">설명</label>
			<div class="form_input">
				<textarea name="summary"  class="field textarea"  style="width:250px;height:30px;"><?=$attendance['summary']?></textarea>
			</div>
		</li>
		<li>
			<label for="is_enable">사용연도</label>
			<div class="form_input">
				<select name="year" class="field select" style="width:80px;">
					<option value="2018" <?php if($attendance['year'] == "2018"){?> selected <?php } ?> >2018년</option>					
					<option value="2017" <?php if($attendance['year'] == "2017"){?> selected <?php } ?> >2017년</option>
					<option value="2016" <?php if($attendance['year'] == "2016"){?> selected <?php } ?> >2016년</option>
					<option value="2015" <?php if($attendance['year'] == "2015"){?> selected <?php } ?> >2015년</option>
					<option value="2014" <?php if($attendance['year'] == "2014"){?> selected <?php } ?> >2014년</option>
					<option value="2013" <?php if($attendance['year'] == "2013"){?> selected <?php } ?> >2013년</option>
					<option value="2012" <?php if($attendance['year'] == "2012"){?> selected <?php } ?> >2012년</option>
					<option value="2011" <?php if($attendance['year'] == "2011"){?> selected <?php } ?> >2011년</option>
				</select>
			</div>
		</li>
		<li>
			<label for="every_date_type">반복</label>
			<div class="form_input">
				<select name="every_date_type" class="field select" style="width:100px;">
					<option value="week"  <?php if($attendance['every_date_type'] == "week"){?> selected <?php } ?>>매주</option>
					<option value="month" <?php if($attendance['every_date_type'] == "month"){?> selected <?php } ?> disabled="disabled" >매월</option>
					<option value="year"  <?php if($attendance['every_date_type'] == "year"){?> selected <?php } ?> disabled="disabled" >매년</option>
				</select>
				<select name="every_date_week" class="field select" style="width:100px;">
					<option value="0" <?php if($attendance['every_date_week'] == "0"){?> selected <?php } ?>>일요일</option>
					<option value="1" <?php if($attendance['every_date_week'] == "1"){?> selected <?php } ?>>월요일</option>
					<option value="2" <?php if($attendance['every_date_week'] == "2"){?> selected <?php } ?>>화요일</option>
					<option value="3" <?php if($attendance['every_date_week'] == "3"){?> selected <?php } ?>>수요일</option>
					<option value="4" <?php if($attendance['every_date_week'] == "4"){?> selected <?php } ?>>목요일</option>
					<option value="5" <?php if($attendance['every_date_week'] == "5"){?> selected <?php } ?>>금요일</option>
					<option value="6" <?php if($attendance['every_date_week'] == "6"){?> selected <?php } ?>>토요일</option>
				</select>
			</div>
		</li>
	</ul>
</form>

<?php
require_once("../_lib/_inner_footer.php");
?>

<?php
require_once ("../_lib/_inner_header.php");
require_once ("../_classes/class.ExcelReader.php");
if(file_exists("/home/himh/public_html/data/excel/".$_GET['filename'])){
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('EUCKR');
	$data->read("/home/himh/public_html/data/excel/".$_GET['filename']);

	for ($i = 0; $i < $data->sheets[0]['numRows']; $i++) {
		$list[$i]['sameage_id'] = $data->sheets[0]['cells'][$i][1];
		$list[$i]['name']       = $data->sheets[0]['cells'][$i][2];
		$list[$i]['phone']      = $data->sheets[0]['cells'][$i][3];
		$list[$i]['email']      = $data->sheets[0]['cells'][$i][4];
		$list[$i]['birth']      = $data->sheets[0]['cells'][$i][5];
		$list[$i]['gender']     = $data->sheets[0]['cells'][$i][6];
		$list[$i]['address']    = $data->sheets[0]['cells'][$i][7];
	}
	$totalItems = $data->sheets[0]['numRows'];
}else{
	$totalItems = 0;
}

?>
    <form name="excelForm" id="excelForm" method="POST" >
	<input type="hidden" name="mode" value="eaxelInsert" />
	<input type="hidden" name="group_id" value="<?=$_GET['group_id']?>" />
	<table class="data_table">
		<colgroup>
			<col style="width:50px; text-align:center;" ></col>
			<col style="width:80px; text-align:center;" ></col>
			<col style="width:100px; text-align:center;" ></col>
			<col style="width:150px; text-align:center;" ></col>
			<col style="width:80px; text-align:center;" ></col>
			<col style="width:30px; text-align:center;" ></col>
			<col style="width:100px; text-align:center;" ></col>
		</colgroup>
		<thead>
			<tr>
				<th>기수</th>
				<th>이름</th>
				<th>연락처</th>
				<th>이메일</th>
				<th>생일</th>
				<th>성별</th>
				<th>주소</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if ($totalItems == 0){
		//---------------------------------------------------------------------
		?>
			<tr <?=$str_bgcolor?>>
				<td colspan="6" align="center">데이타가 없습니다. </td>
			</tr>
		<?php
		}else{
			//---------------------------------------------------------------------
			for ($i =2; $i < $totalItems ; $i++) {

				if(in_array($listNib[$i]['serial_num'] , $resultNib)){
					$str_bgcolor = " bgcolor='#ffdd77' ";
				}else{
					$str_bgcolor = " bgcolor='#ffffff' ";
				}
				?>
				<tr <?=$str_bgcolor?>>
					<td><input type="text" name="sameage_id[]" value="<?=_encode($list[$i]['sameage_id']);?>" style="width:50px;" /></td>
					<td><input type="text" name="name[]" value="<?=_encode($list[$i]['name']);?>"  style="width:80px;" /></td>
					<td><input type="text" name="phone[]" value="<?=_encode($list[$i]['phone']);?>" style="width:100px;" /></td>
					<td><input type="text" name="email[]" value="<?=_encode($list[$i]['email']);?>" style="width:150px;" /></td>
					<td><input type="text" name="birth[]" value="<?=_encode($list[$i]['birth']);?>" style="width:80px;"  /></td>
					<td><input type="text" name="gender[]" value="<?=_encode($list[$i]['gender']);?>" style="width:30px;" /></td>
					<td><input type="text" name="address[]" value="<?=_encode($list[$i]['address']);?>" style="width:100px;" /></td>
				</tr>
            <?php
			}
			//---------------------------------------------------------------------
		}
		?>
		</tbody>
	</table>
<input type="button" class="ui-state-default ui-corner-all button_m" onclick="$.group.excelStep2Action(<?=$_GET['group_id']?>);" value="저 장" />
    </form>

<?php
require_once ("../_lib/_inner_footer.php");
?>
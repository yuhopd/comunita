<?php
require_once ("../_lib/_inner_header.php");
require_once ("../_classes/class.MessageList.php");
if(!$_GET['page']){ $_GET['page']=1; }
$properties = array(
    "type"     => $_GET['mode'],
    "read_id"  => $_SESSION['sys_id'],
	"page"     => $_GET['page'],
	"perPage"  => 10
);

$msg = new MessageList($properties);
$totalitems  = $msg->getTotalCount();
$list        = $msg->getList();
$tot         = count($list);
$paging = Common::getPaging($totalitems, $properties['perPage'], "javascript:$.message.list({mode:'".$_GET['mode']."', page:%d});");
?>




<div id="quick_actions">
	<a class="button_big" href="javascript:$.message.list({mode:'default',page:1});"><span class="iconsweet">L</span>받은쪽지함</a>
	<a class="button_big" href="javascript:$.message.list({mode:'noRead',page:1});"><span class="iconsweet">=</span>안읽은쪽지함</a>
	<a class="button_big" href="javascript:$.message.form();" ><span class="iconsweet">+</span>쪽지쓰기</a>
</div>



<div class="one_wrap">
	<div class="widget">
		<div class="widget_title"><span class="iconsweet">f</span><h5>쪽지 (<?=$totalitems?>)</h5></div>
		<div class="widget_body">
			<div class="content_pad">
				<table class="activity_datatable" width="100%">
					<tr>
						<th class="date" >날짜</th>
						<th class="owner">글쓴이</th>
						<th class="subject">제목</th>
					</tr>
					<?php
					if($totalitems > 0){
						$today = time();
						for($i = 0; $i < count($list) ; $i++){
							if($list[$i]['adddate'] < ($today-(24*60*60))){
								$reg_date = date("Y년 n월 j일", $list[$i]['adddate']);
							}else{
								$reg_date = date("h시 i분", $list[$i]['adddate']);
							}
							$item_odd = ($i%2 == 1) ? " odd" : "";

							if($list[$i]['is_read'] == "X"){
								$isRead = " read";
							}else{
								$isRead = "";
							}
						?>

						<tr class="message<?=$item_odd?>" >
							<td class="date"><?=$reg_date?></td>
							<td class="owner"><?=$list[$i]['send_name']?></td>
							<td class="subject <?=$isRead?>"><a href="javascript:$.message.view('<?=$list[$i]['id']?>');"> &nbsp;<?=$list[$i]['content']?></a></td>
						</tr>
						<?php
						}
					} else {
					?>
						<tr class="message" >
							<?php if($_GET['mode'] == "default"){ ?>
							<td style="text-align:center;" colspan="3">쪽지가 없습니다.</td>
							<?php } else { ?>
							<td style="text-align:center;" colspan="3">안읽은 쪽지가 없습니다.</td>
							<?php } ?>
						</tr>
						<?php
					}
					?>
				</table>

				<div class="pagging">
					<?=$paging?>
				</div>

			</div>
		</div>
	</div>
</div>



<?php
require_once ("../_lib/_inner_footer.php");
?>
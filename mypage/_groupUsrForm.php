<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.UsrPhoneList.php");
require_once("../_classes/class.GroupManager.php");

$res1 = $db->query("SELECT `id`, `title`, `gisu` FROM `sameage`");
while($res1->fetchInto($row, DB_FETCHMODE_ASSOC)){
	$sameage[$row['id']]['title'] = $row['title'];
	$sameage[$row['id']]['gisu']  = $row['gisu'];
}

$root_id = GroupManager::getRootId($_GET['group_id']);
$query = "SELECT * FROM `group` WHERE id='$_GET[group_id]'";
$res   =& $db->query($query);
$res->fetchInto($row,DB_FETCHMODE_ASSOC);
	$properties1 = array(
		"type"     => "allWithRootGroup",
		"root_id"  => $root_id,
		"browse"   => 2
	);

	$pl1    = new UsrPhoneList($properties1);
	$allUsrlist  = $pl1->getList('X');


	$properties2 = array(
		"browse"   => 2,
		"group_level" => 0,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl2    = new UsrPhoneList($properties2);
	$groupUsrlist_0  = $pl2->getList('X');


	$properties3 = array(
		"browse"   => 2,
		"group_level" => 1,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl3    = new UsrPhoneList($properties3);
	$groupUsrlist_1  = $pl3->getList('X');

	$properties4 = array(
		"browse"   => 2,
		"group_level" => 2,
		"type"     => "group_list_level",
		"group_id" => $_GET['group_id']
	);

	$pl4    = new UsrPhoneList($properties4);
	$groupUsrlist_2  = $pl4->getList('X');

    $rootGroups = GroupManager::getAttendanceAdminGroups();

?>
<script type="text/javascript">
	$(function() {
		$("#unSelected").multiSelect("#selected_level_0", {trigger: "#options_right_0"});
		$("#selected_level_0").multiSelect("#unSelected", {trigger: "#options_left_0"});

		$("#unSelected").multiSelect("#selected_level_1", {trigger: "#options_right_1"});
		$("#selected_level_1").multiSelect("#unSelected", {trigger: "#options_left_1"});

		$("#unSelected").multiSelect("#selected_level_2", {trigger: "#options_right_2"});
		$("#selected_level_2").multiSelect("#unSelected", {trigger: "#options_left_2"});
	});
</script>

<ul class="groupMenu">
	<li>
		<a href="javascript:$.group.edit(<?=$_GET['group_id']?>, 'O');">그룹설정</a>
	</li>
	<li class="active">
		<a href="javascript:$.group.usrEdit(<?=$_GET['group_id']?>, 'O');">그룹회원관리</a>
	</li>
	<li>
		<a href="javascript:$.group.attendanceList(<?=$_GET['group_id']?>, 'O');">출석부관리</a>
	</li>
	<li>
		<a href="javascript:$.group.attendanceInsert(<?=$_GET['group_id']?>, 'O');">출석부추가</a>
	</li>
</ul>

<div class="settings">

	<form id="groupUsrForm">
		<input type="hidden" name="mode" value="usr_edit">
		<input type="hidden" id="group_id" name="group_id" value="<?=$_GET['group_id']?>">
		<ul class="form">
			<li>
				<label for="group_title">그룹명</label>
				<div id="group_title" class="value" style="width:250px;"><?=$row['title']?></div>
			</li>
			<li>
				<table>
					<tr>
						<td rowspan="3">
							<select name="roots" id="roots" style="width:120px;" size="1">
								<?php for($f=0; $f<count($rootGroups); $f++ ) { ?>
									<option value="<?=$rootGroups[$f]['id']?>" <?php if($rootGroups[$f]['id'] == $root_id){ ?> selected <?php } ?> ><?=$rootGroups[$f]['title']?></option>
								<?php } ?>
							</select>
							<a href="javascript:$.group.rootGroupUsrList();" class="ui-state-default ui-corner-all button_s">변경</a>
                            <br /><br />
							<div id="groupUsrListBox">
								<select name="left[]" id="unSelected" multiple="multiple" style="width:180px;" size="16">
								<?php
								$countAll = count($allUsrlist);
								if($allUsrlist){
								for($i=0; $i<$countAll; $i++ ) { ?>
									<option value="<?=$allUsrlist[$i]['id']?>"><?=$allUsrlist[$i]['name']?>(<?=$sameage[$allUsrlist[$i]['sameage_id']]['title']?>)</option>
								<?php
								}}
								?>
								</select>
							</div>
						</td>
						<td style="padding:5px 10px;">
						    <br /><br />
							<span id="options_right_2" class="ui-state-default ui-corner-all button_s">&gt;</span>
							<br />
							<span id="options_left_2" class="ui-state-default ui-corner-all button_s">&lt;</span>
						</td>
						<td valign="top">
							<div class="label">관리자</div>
							<select name="right2[]" id="selected_level_2" multiple="multiple" style="width:180px;" size="3">
							<?php
							$count_2 = count($groupUsrlist_2);
							if($groupUsrlist_2){
							for($i=0; $i<$count_2; $i++ ) { ?>
								<option value="<?=$groupUsrlist_2[$i]['id']?>"><?=$groupUsrlist_2[$i]['name']?>(<?=$sameage[$groupUsrlist_2[$i]['sameage_id']]['title']?>)</option>
							<?php }
							}
							?>
							</select>
						 </td>
					</tr>
					<tr>
						<td style="padding:5px 10px;">
						    <br /><br />
							<span id="options_right_1" class="ui-state-default ui-corner-all button_s">&gt;</span>
							<br />
							<span id="options_left_1" class="ui-state-default ui-corner-all button_s">&lt;</span>
						</td>
						<td valign="top">
							<div class="label">그룹리더</div>
							<select name="right1[]" id="selected_level_1" multiple="multiple" style="width:180px;" size="3">
							<?php
							$count_1 = count($groupUsrlist_1);
							if($groupUsrlist_1){
							for($i=0; $i<$count_1; $i++ ) { ?>
								<option value="<?=$groupUsrlist_1[$i]['id']?>"><?=$groupUsrlist_1[$i]['name']?>(<?=$sameage[$groupUsrlist_1[$i]['sameage_id']]['title']?>)</option>
							<?php
							}}
							?>
							</select>
						 </td>
					</tr>
					<tr>
						<td style="padding:5px 10px;">
							<span id="options_right_0" class="ui-state-default ui-corner-all button_s">&gt;</span>
							<br />
							<span id="options_left_0" class="ui-state-default ui-corner-all button_s">&lt;</span>
						</td>
						<td valign="top">
							<div class="label">그룹회원</div>
							<select name="right0[]" id="selected_level_0" multiple="multiple" style="width:180px;" size="8">
							<?php
							$count_0 = count($groupUsrlist_0);
							if($groupUsrlist_0){
							for($i=0; $i<$count_0; $i++ ) { ?>
								<option value="<?=$groupUsrlist_0[$i]['id']?>"><?=$groupUsrlist_0[$i]['name']?>(<?=$sameage[$groupUsrlist_0[$i]['sameage_id']]['title']?>)</option>
							<?php }}?>
							</select>
						 </td>
					</tr>
				</table>
			</li>

		</ul>

	</form>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>
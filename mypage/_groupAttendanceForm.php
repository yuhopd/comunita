<?php
require_once("../_lib/_inner_header.php");
require_once("../_classes/class.GroupManager.php");
require_once("../_classes/class.AttendanceManager.php");

if($_GET['mode'] == 'modify'){
	$attendance = AttendanceManager::getAttendanceSetting($_GET['attendance_id']);
	$group = GroupManager::getGroup($attendance['group_id']);
	$root  = GroupManager::getRootGroup($attendance['group_id']);
	$_GET['group_id'] = $attendance['group_id'];
}else{
	$group = GroupManager::getGroup($_GET['group_id']);
	$root  = GroupManager::getRootGroup($_GET['group_id']);
}
?>

<ul class="groupMenu">
	<li>
		<a href="javascript:$.group.edit(<?=$_GET['group_id']?>, 'O');">그룹설정</a>
	</li>
	<li>
		<a href="javascript:$.group.usrEdit(<?=$_GET['group_id']?>, 'O');">그룹회원관리</a>
	</li>
	<li <?php if($_GET['mode'] == 'modify'){?>  class="active" <?php } ?>>
		<a href="javascript:$.group.attendanceList(<?=$_GET['group_id']?>, 'O');">출석부관리</a>
	</li>
	<li <?php if($_GET['mode'] == 'insert'){?>  class="active" <?php } ?> >
		<a href="javascript:$.group.attendanceInsert(<?=$_GET['group_id']?>, 'O');">출석부추가</a>
	</li>
</ul>

<div class="settings">
	<form id="groupAttendanceForm">
	<?php if($_GET['mode'] == 'modify'){?>
	<input type="hidden" name="mode" value="modifyAttendance" />
	<input type="hidden" id="attendance_id" name="attendance_id" value="<?=$attendance['id']?>" />
	<input type="hidden" id="group_id" name="group_id" value="<?=$group['id']?>" />
    <input type="hidden" name="group_title" value="<?=$group['title']?>" />
    <input type="hidden" name="root_id" value="<?=$root['id']?>" />
    <input type="hidden" name="root_title" value="<?=$root['title']?>" />

	<?php }else{ ?>
	<input type="hidden" name="mode" value="insertAttendance" />
	<input type="hidden" id="group_id" name="group_id" value="<?=$group['id']?>" />
    <input type="hidden" name="group_title" value="<?=$group['title']?>" />
    <input type="hidden" name="root_id" value="<?=$root['id']?>" />
    <input type="hidden" name="root_title" value="<?=$root['rtitle']?>" />
	<?php } ?>

	<ul class="form_fields_container">
		<li>
			<label for="attendance_title">그룹</label>
			<div class="form_input">
				<input type="text" name="groupTitle" value="<?=$group['title']?>" />
			</div>
		</li>
		<li>
			<div>
				<span class="desc" for="attendance_title">출석부 이름:</span>
				<input type="text" id="attendance_title" name="title" value="<?=$attendance['title']?>" class="field text" style="width:250px;"/>
			</div>
		</li>
		<li>
			<div>
				<span class="desc" for="attendance_summary" style="line-height:30px;">설명:</span>
				<textarea id="attendance_summary" name="summary"  class="field textarea"  style="width:250px;height:30px;"><?=$attendance['summary']?></textarea>
			</div>
		</li>
		<li>
			<div>
				<span class="desc" for="is_enable">사용연도:</span>
				<select id="year" name="year" class="field select" style="width:80px;">
					<option value="2017" <?php if($attendance['year'] == "2017"){?> selected <?php } ?> >2017년</option>
					<option value="2016" <?php if($attendance['year'] == "2016"){?> selected <?php } ?> >2016년</option>
					<option value="2015" <?php if($attendance['year'] == "2015"){?> selected <?php } ?> >2015년</option>
					<option value="2014" <?php if($attendance['year'] == "2014"){?> selected <?php } ?> >2014년</option>
					<option value="2013" <?php if($attendance['year'] == "2013"){?> selected <?php } ?> >2013년</option>
					<option value="2012" <?php if($attendance['year'] == "2012"){?> selected <?php } ?> >2012년</option>
					<option value="2011" <?php if($attendance['year'] == "2011"){?> selected <?php } ?> >2011년</option>
				</select>
			</div>
		</li>
		<li>
			<div>
				<span class="desc" for="every_date_type">반복 :</span>
				<select id="every_date_type" name="every_date_type" class="field select" style="width:80px;">
					<option value="week"  <?php if($attendance['every_date_type'] == "week"){?> selected <?php } ?>>매주</option>
					<option value="month" <?php if($attendance['every_date_type'] == "month"){?> selected <?php } ?> disabled="disabled" >매월</option>
					<option value="year"  <?php if($attendance['every_date_type'] == "year"){?> selected <?php } ?> disabled="disabled" >매년</option>
				</select>
				<select id="every_date_week" name="every_date_week" class="field select" style="width:80px;">
					<option value="0" <?php if($attendance['every_date_week'] == "0"){?> selected <?php } ?>>일요일</option>
					<option value="1" <?php if($attendance['every_date_week'] == "1"){?> selected <?php } ?>>월요일</option>
					<option value="2" <?php if($attendance['every_date_week'] == "2"){?> selected <?php } ?>>화요일</option>
					<option value="3" <?php if($attendance['every_date_week'] == "3"){?> selected <?php } ?>>수요일</option>
					<option value="4" <?php if($attendance['every_date_week'] == "4"){?> selected <?php } ?>>목요일</option>
					<option value="5" <?php if($attendance['every_date_week'] == "5"){?> selected <?php } ?>>금요일</option>
					<option value="6" <?php if($attendance['every_date_week'] == "6"){?> selected <?php } ?>>토요일</option>
				</select>

			</div>
		</li>
	</ul>
		<div id="popup_msg" class="popup_msg" style="display:none;"></div>

	</form>
</div>
<?php
require_once("../_lib/_inner_footer.php");
?>

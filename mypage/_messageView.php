<?php
require_once ("../_lib/_inner_header.php");
require_once ("../_classes/class.MessageManager.php");
$view  = MessageManager::getItem($_GET['id']);
$view['content'] = stripslashes($view['content']);
$adddate = date("Y년 n월 j일 h시 i분", $view['adddate']);
?>

	<input type="hidden" id="messageId" name="messageId" value="<?=$_GET['id']?>" />

	<input type="hidden" id="readId" name="readId" value="<?=$_SESSION['sys_id']?>" />
	<input type="hidden" id="sendId" name="sendId" value="<?=$view['send_id']?>" />
	<input type="hidden" id="sendName" name="sendName" value="<?=$view['send_name']?>" />
	<ul class="form">
	    <li>
			<label for="title" class="desc" style="width:50px;">보낸사람</label>
			<div class="send"><a href="javascript:$.common.usr_info(<?=$view['send_id']?>);"><?=$view['send_name']?></a></div>
			<div class="adddate"><?=$adddate?></div>
		</li>
	    <li>
			<div class="contentArea"><?=$view['content']?></div>
		</li>
	</ul>
<?php
require_once ("../_lib/_inner_footer.php");
?>
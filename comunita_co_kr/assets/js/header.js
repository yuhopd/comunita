"use strict";

/* ======= Header animation ======= */   
const header = document.getElementById('header');  

window.onload=function() 
{   
    let scrollTop = window.scrollY;
    headerAnimation(); 

};

window.onresize=function() 
{   
	let scrollTop = window.scrollY;
    headerAnimation(); 

}; 

window.onscroll=function() 
{ 
    let scrollTop = window.scrollY;
    headerAnimation(); 

}; 
    

function headerAnimation () {

    var scrollTop = window.scrollY;
	
	if ( scrollTop > 0 ) {	    
	    header.classList.add('scrolled');    
	    	    
	} else {
	    header.classList.remove('scrolled');
	}

};
    
const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {   
        admin: path.resolve(__dirname + "/js/admin/Main.js")
    },
    
    output: {
        path: __dirname + '/public/admin/assets/js/',
        filename: '[name].bundle.js'
    },

    target : 'node',

    resolve: {
        modules: [
            path.resolve(__dirname)
        ],
        //root: path.resolve(__dirname),
        alias: {
            "jquery" : __dirname + "/js/admin/lib/jquery"
        },
        extensions: ['.js']
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery'",
            "window.$": "jquery"
        })
    ]
};
